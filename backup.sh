#!/bin/sh
DBNAME=peer_assessment
DATE=`date +"%Y%m%d"`
PATHTOSTORE=/root/dev-test-project/storage/backup/sql/${DATE}
#create directory if not exist
mkdir -p $PATHTOSTORE

#sql filename here
SQLFILE=$PATHTOSTORE/$DBNAME-${DATE}.sql

#print fielname
echo $SQLFILE
mysqldump --opt --user=peer --password=peer $DBNAME > $SQLFILE
#mysql -u peer --password=peer $DBNAME < $SQLFILE
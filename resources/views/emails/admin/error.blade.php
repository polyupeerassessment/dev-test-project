
@component('mail::message')
# Dear Developers,
Error occured. Please review following error contents and fix it ASAP!

<hr>
{{$contents}}
<hr>
<br>
**This is an automatically generated email** Please do not reply.

Thanks,<br>
{{ config('app.name') }}
@endcomponent

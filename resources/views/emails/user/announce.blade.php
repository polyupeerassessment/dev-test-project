@component('mail::message')
# Greeting from your teacher

{{$contents}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent


@component('mail::message')
# Greeting from {{ config('app.name') }}

{{$contents}}

<br>
**This is an automatically generated email** Please do not reply.

Thanks,<br>
{{ config('app.name') }}
@endcomponent

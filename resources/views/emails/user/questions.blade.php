
@component('mail::message')
# Greeting from {{ config('app.name') }}

{{$contents}}

Hope you are doing well.

We have attached some questions which are answered by many students on this email.
Please try the questions and prepare for the quiz!

Let's get a good grade in the course!

@foreach($questions as $question)
@component('mail::panel')
**{{$question->topic}}**
<br>
{{$question->contents}}
@component('mail::button', ['url' => route('question.detail.email',['id'=>$question->id,'course_id'=>$question->course_id]), 'color' => 'green'])
    View Detail
@endcomponent
@endcomponent
@endforeach

<br>

**This is an automatically generated email** Please do not reply.

Thanks,<br>
{{ config('app.name') }}
@endcomponent


@component('mail::message')
# Dear Students,
<br>
This is COMP Peer Assessment Team. You account is now ready!
<hr>
{{$contents}}
: <span style="color:red;"> {{$pass}}</span>
<br>
Your Account ID is: {{$email}} <br>
Course Enroll Code is: {{$enroll_code}} <br><br>
**Please change your password ASAP after first time you login **

<hr>
@component('mail::button', ['url'=>config('app.url')])
    Let's Go!
@endcomponent

<br>
**This is an automatically generated email** Please do not reply.

Thanks,<br>
{{ config('app.name') }}
@endcomponent

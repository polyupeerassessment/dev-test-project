
@component('mail::message')
# Greeting from {{ config('app.name') }}

{{$contents}}

<br>

@if(!empty($top_scorers))
## Top Score
<?php $i = 1; ?>
@foreach($top_scorers as $user)
@if($i==1)
![alt text](https://png.icons8.com/color/48/000000/medal.png "1") {{$user->nickname}} : {{$user->Score}}<br>
@elseif($i==2)
![alt text](https://png.icons8.com/color/48/000000/olympic-medal-silver.png "2") {{$user->nickname}} : {{$user->Score}}<br>
@else
![alt text](https://png.icons8.com/color/48/000000/olympic-medal-bronze.png "3") {{$user->nickname}} : {{$user->Score}}<br>
@endif
@php
$i++;
@endphp

@endforeach
@endif

@if(!empty($rating))
## Top Average Rating
<?php $i = 1; ?>
@foreach($rating as $user)
@if($i==1)
![alt text](https://png.icons8.com/color/48/000000/medal.png "1") {{$user->nickname}} : {{$user->rating}}<br>
@elseif($i==2)
![alt text](https://png.icons8.com/color/48/000000/olympic-medal-silver.png "2") {{$user->nickname}} : {{$user->rating}}<br>
@else
![alt text](https://png.icons8.com/color/48/000000/olympic-medal-bronze.png "3") {{$user->nickname}} : {{$user->rating}}<br>
@endif
@php
$i++;
@endphp
@endforeach
@endif


@if(!empty($num_questions))
## Number of Questions They Created
<?php $i = 1; ?>
@foreach($num_questions as $user)
@if($i==1)
![alt text](https://png.icons8.com/color/48/000000/medal.png "1") {{$user->nickname}} : {{$user->num}} <br>
@elseif($i==2)
![alt text](https://png.icons8.com/color/48/000000/olympic-medal-silver.png "2") {{$user->nickname}} : {{$user->num}}<br>
@else
![alt text](https://png.icons8.com/color/48/000000/olympic-medal-bronze.png "3") {{$user->nickname}} : {{$user->num}}<br>

@endif
@php
$i++;
@endphp
@endforeach
@endif


@if(!empty($num_answers))
## Number of Answers They Answered
<?php $i = 1; ?>
@foreach($num_answers as $user)
@if($i==1)
![alt text](https://png.icons8.com/color/48/000000/medal.png "1") {{$user->nickname}} : {{$user->num}}<br>
@elseif($i==2)
![alt text](https://png.icons8.com/color/48/000000/olympic-medal-silver.png "2") {{$user->nickname}} : {{$user->num}}<br>
@else
![alt text](https://png.icons8.com/color/48/000000/olympic-medal-bronze.png "3") {{$user->nickname}} : {{$user->num}}<br>
@endif
@php
$i++;
@endphp
@endforeach
@endif

<br>
**This is an automatically generated email** Please do not reply.

Thanks,<br>
{{ config('app.name') }}
@endcomponent

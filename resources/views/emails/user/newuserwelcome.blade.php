@component('mail::message')
# Welcome  {{$user->name}}

Thanks for sigining up.  **We really appreciate** it.

@component('mail::panel')
  The Email address you signed up with is: {{$user->email}}
@endcomponent

@component('mail::button', ['url'=>config('app.url')])
  Back to page
@endcomponent

Thanks, <br>
{{config('app.name')}}

@endcomponent

@component('mail::message')
# Greeting from PeerU

{{$contents}}

@component('mail::button', ['url'=>config('app.url')])
    Dashboard
@endcomponent

**This is an automatically generated email** Please do not reply.

Thanks,<br>
{{ config('app.name') }}
@endcomponent

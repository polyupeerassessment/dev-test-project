

<nav class="navbar navbar-expand-sm navbar-peer bg-peer">
    <a class="navbar-brand" href="{{config('app.root_dir')}}/">{{config('app.name')}}</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsPeer" aria-controls="navbarsPeer" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsPeer">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ Request::is ('/') ? 'active' : '' }}">
                <a class="nav-link" href="{{config('app.root_dir')}}">Home<span class="sr-only">(current)</span></a>
            </li>

            <li class="nav-item {{ Request::is('about') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('about')}}">About</a>
            </li>
            <li class="nav-item {{ (Request::is('/teacher/dashboard') || Request::is('/teacher')) ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('teacher.dashboard') }}">Dashboard</a>
            </li>

        </ul>

        <ul class="nav navbar-nav navbar-right" style="margin-right:1.5%;">

            <li class="dropdown">
            </li>

            {{--User Information--}}
            <li class="dropdown">

                <a href="#" class="btn dropdown-toggle nav-link" data-toggle="dropdown" role = 'button' aria-expanded="false">
                    <span>User</span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right" role="menu" style="width: 300px;">
                    <div class="container">
                        <div class="">
                            <div class=""><h4>Information</h4></div>

                            <div class="form-group">
                                <p>ID: {{ Auth::guard('teacher')->user()->id }}</p>
                            </div>
                            <div class="form-group">
                                <p>Name: {{ Auth::guard('teacher')->user()->name }}</p>
                            </div>
                            <div class="form-group">
                                <p>Nickname: {{ Auth::guard('teacher')->user()->nickname }}</p>
                            </div>
                            <div class="form-group">
                                <p>Email: {{ Auth::guard('teacher')->user()->email }}</p>
                            </div>
                            <hr />
                            <div class="">
                                {{--<a href="{{ route('student.info') }}" class="btn btn-custom " role="button" >Edit</a>--}}
                                <a href="{{ route('logout') }}" class="btn btn-custom" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;" class="dropdown-item">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </div>
                    </div>
                </ul>

            </li>

        </ul>

    </div>

</nav>

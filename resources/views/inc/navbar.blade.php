

<nav class="navbar navbar-expand-sm navbar-peer bg-peer">
    <a class="navbar-brand" href="{{config('app.root_dir')}}/">{{config('app.name')}}</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsPeer" aria-controls="navbarsPeer" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsPeer">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ Request::is ('/') ? 'active' : '' }}">
                <a class="nav-link" href="{{config('app.root_dir')}}">Home<span class="sr-only">(current)</span></a>
            </li>

            <li class="nav-item {{ Request::is('about') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('about')}}">About</a>
            </li>

            @if (!Auth::guest() && !empty(Auth::user()))
                <li class="nav-item {{ (Request::is('student/home') || Request::is('student')) ? 'active' : '' }}">

                    <a class="nav-link" href="{{ route('student.home') }}">Dashboard</a>
                </li>
            @endif

            @if (Session::get('courseID'))
                <li class="nav-item {{ (Request::is('student/main/*') || Request::is('student')) ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('student.dashboard', ['courseID'=>Session::get('courseID')]) }}">My Course</a>
                </li>
            @endif

        </ul>

        <ul class="nav navbar-nav navbar-right" style="margin-right:1.5%;">

            @if (Auth::guest())
                <li><a href="{{ route('login') }}" class="nav-link">Login</a></li>
                <li><a href="{{ route('register') }}" class="nav-link">Register</a></li>
            @elseif (Auth::user())

            <li class="dropdown">

                <a href="#" class="btn dropdown-toggle nav-link" data-toggle="dropdown" role = 'button' aria-expanded="false">
                    <span class="glyphicon glyphicon-globe">Notification</span>&nbsp;<span class="badge badge-light">{{count(auth()->user()->unreadNotifications) }}</span>
                </a>
                @if (sizeof(auth()->user()->unreadNotifications) > 0)
                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    @foreach(auth()->user()->unreadNotifications as $notification)
                        <a href="#" class="dropdown-item">
                            @include('student.notification.'.snake_case(class_basename($notification->type)))
                        </a>
                    @endforeach
                </ul>
                @endif
            </li>

            {{--User Information--}}
            <li class="dropdown">

                <a href="#" class="btn dropdown-toggle nav-link" data-toggle="dropdown" role = 'button' aria-expanded="false">
                    <span>User</span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right" role="menu" style="width: 300px;">
                    <div class="container">
                        <div class="">
                            <div class=""><h4>Information</h4></div>

                            <div class="form-group">
                                <p>ID: {{ Auth::user()->id }}</p>
                            </div>
                            <div class="form-group">
                                <p>Name: {{ Auth::user()->name }}</p>
                            </div>
                            <div class="form-group">
                                <p>Nickname: {{ Auth::user()->nickname }}</p>
                            </div>
                            <div class="form-group">
                                <p>Email: {{ Auth::user()->email }}</p>
                            </div>
                            <hr />
                            <div class="">
                                <a href="{{ route('student.info') }}" class="btn btn-custom" role="button" >Edit</a>
                                <a href="{{ route('logout') }}" class="btn btn-custom" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;" class="dropdown-item">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </div>
                    </div>
                </ul>

            </li>
            @endif

        </ul>

    </div>

</nav>

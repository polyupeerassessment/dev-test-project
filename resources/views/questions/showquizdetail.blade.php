@extends('layouts.studentdashboard')

@section('css')
	<link rel="stylesheet" href="{{asset('css/dashboard/green.css')}}" /> 
@endsection 

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title" style="padding: 0px;">
            <div class="title_left"></div>
        </div>
        {!! Breadcrumbs::render('question_quiz') !!}
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div id="quiz_content">
                        @if ($quiz_done_flag)
                            <h1 class="text-center">Quiz Result</h1>
                            <h3>Score: {{$score}} / {{$total_score}}</h3>
                        @else
                            <h1 class="text-center">{{$topic}}</h1>
                            <p class="text-left" id="quiz_timer">Timer:</p>
                        @endif 
                            <hr>
                        @if (empty($questions[0]))
                            <p>Question not found</p>
                        @else
                            {!! Form::open(['route' => 'question.success.quiz', 'id'=>'answerFormQuiz']) !!}
                            <?php $question_index = 0; ?>
                            @foreach($questions as $question)
                                @if(empty($question))
                                    @continue
                                @endif
                                <div class="row">
                                    {{--Contents--}}
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <h3 style="word-wrap: break-word;">{{$question->topic}}</h3>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12 question_content" @if($question->mode == 0) style='white-space: pre-line;' @endif>
                                                <p style="max-width:100%;">
                                                    @if($question->mode == 0)
                                                        {{ $question->contents }}
                                                    @else
                                                        <?php
                                                            echo html_entity_decode($question_filter_content[$question->id]);
                                                        ?>
                                                    @endif
                                                </p>
                                            </div>
                                        </div>
                                        <br>
                                        {{--SHOW Answer OR Not--}}
                                        @if ($quiz_done_flag)
                                            {{--List choices--}}
                                            @if(empty($choices[$question->id]))
                                                @continue;
                                            @endif

                                            <?php $i = 1; ?>
                                            <p>
                                                {{--Show Answer made by author--}}
                                                @foreach($choices[$question->id] as $choice)
                                                    {{--Contents HERE! --}}
                                                    @if ($question->type==0)
                                                        {{--MC Contents! --}}
                                                        @if ($choice->answer == 1)
                                                        <div class='row'>
                                                            <div class="col-md-1 col-sm-1 col-xs-1">
                                                                <i class="fa fa-check fa-2x" style="color:#24b999; min-height:35px;"></i>
                                                            </div>
                                                            <div class="col-md-11 col-sm-11 col-xs-11">
                                                                <p style="word-wrap:break-word; min-height:35px; font-size:120%;">{{$choice->choice}}</p>
                                                            </div>
                                                        </div>
                                                        @else
                                                            <div class='row'>
                                                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                                    <i class="fa fa-close fa-2x" style="color:#d43f3a;min-height:35px;"></i>
                                                                </div>
                                                                <div class="col-md-11 col-sm-11 col-xs-11">
                                                                    <p style="word-wrap:break-word; min-height:35px; font-size:120%;">{{$choice->choice}}</p>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @else
                                                        <b>Answers</b>:
                                                        {{--TF Contents! --}}
                                                        @if($choice->answer == 1)
                                                            <?php $ans = "True" ?>
                                                            True
                                                        @else
                                                            <?php $ans = "False" ?>
                                                            False
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </p>
                                            {{--User's Answers Here! --}}
                                            <p>
                                                <b>Your Answers</b>:
                                                @if($question->type == 1) {{--TF--}}
                                                    @if(empty($result_list[$question->id][1][0]))
                                                        False
                                                    @else
                                                        <?php echo $result_list[$question->id][1][0]==1?'True':'False'; ?>
                                                    @endif

                                                @else {{--MC--}}
                                                    @if(empty($result_list[$question->id][1][0]))
                                                        No Answer
                                                    @else
                                                        @if(!is_array($result_list[$question->id][1]))
                                                            @continue;
                                                        @endif

                                                        @foreach($result_list[$question->id][1] as $answer)
                                                            {{htmlentities($answer)}}
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </p>
                                            <p>
                                                @if(!empty($result_list[$question->id][0]) && $result_list[$question->id][0]==1)
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                                            <div class="alert alert-success">Result: Correct</div>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                                            <div class="alert alert-danger">Result: Wrong</div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </p>
                                        @else
                                            {{--User not answered yet! --}}
                                            <div>
                                                    <?php $i = 1; ?>

                                                    {{--List choices--}}
                                                    @if(empty($choices[$question->id]))
                                                        @continue;
                                                    @endif

                                                    @foreach ($choices[$question->id] as $choice)
                                                        @if ($question->type == 0 )
                                                            @if (empty($choice))
                                                                @continue
                                                            @endif
                                                            @if($question_num_answers[$question->id] == 1)
                                                                <div class='row'>
                                                                    <div class="col-md-1 col-sm-1 col-xs-1">
                                                                        <input type="radio" name="{{'choice2answer_'.$question_index.'[]'}}" class="flat" id="{{'choice2answer_'.$question_index.$i}}" value='{{$choice->choice}}'  />
                                                                    </div>
                                                                    <div class="col-md-11 col-sm-11 col-xs-11">
                                                                        <p style="word-wrap:break-word">{{$choice->choice}}</p>
                                                                    </div>
                                                                </div>
                                                            @else
                                                                <div class='row'>
                                                                    <div class="col-md-1 col-sm-1 col-xs-1" >
                                                                        <input type="checkbox" name="{{'choice2answer_'.$question_index.'[]'}}" class="flat" id="{{'choice2answer_'.$question_index.$i}}" value='{{$choice->choice}}'  >
                                                                    </div>
                                                                    <div class="col-md-11 col-sm-11 col-xs-11">
                                                                        <p style="word-wrap:break-word">{{$choice->choice}}</p>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            {{--  <div class="row inputtf">
                                                                <div class="col-md-6 col-sm-12 col-xs-12">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            <input type="checkbox" name="{{'choice2answer_'.$question_index.'[]'}}" class='flat' value='{{$choice->choice}}'/>
                                                                        </div>
                                                                        <div>
                                                                            <p style="word-wrap:break-word">{{$choice->choice}}</p>
                                                                            {{Form::label('choice2answer', $choice->choice, array('class'=>'form-control', 'style'=>'color:#73879C'))}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>  --}}
                                                        @else
                                                            <div class="row inputtf">
                                                                <div class="col-md-6 col-sm-12 col-xs-12">
                                                                    <div class="form-group">
                                                                        <select name="{{'choice2answer_'.$question_index}}" class="form-control input-sm">
                                                                            <option value="0">False</option>
                                                                            <option value="1">True</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                             {{--  {{Form::select('choice2answer_'.$question_index, ['0'=>'False', '1'=>'True'], ['class'=>'form-control input-sm'])}}  --}}
                                                        @endif
                                                    @endforeach
                                                    {{Form::hidden('questionID_'.$question_index, $question->id) }}
                                                    <br>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <hr>
                                <?php $question_index++; ?>
                            @endforeach

                            @if (!$quiz_done_flag)
                                <button type="submit" class="btn btn-success" id='quiz_form_submit_button'>Submit</button>
                            @endif
                            {!! Form::close() !!}
                        @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script>
    $(document).ready(function(){
        var deadline = new Date("{{$deadline->toDateTimeString()}}");
        var form = document.getElementById('answerFormQuiz')

        @if(!$quiz_done_flag)
            var timer_interval = setInterval(function(a){
                var dt = new Date();
                var timeDiff = Math.abs(dt.getTime() - deadline.getTime());
                var diff_sec = Math.ceil(timeDiff / 1000);
                var diff_min = Math.floor(diff_sec % 3600 / 60);
                var diff_hour = Math.floor(diff_sec / 3600);
                diff_sec = Math.floor(diff_sec % 3600 % 60);
                var timer = "<p> Timer: "+diff_hour+" : " + diff_min+" : "+diff_sec+"</p>";
                $('#quiz_timer').html(timer);

                if (diff_hour === 0 && diff_min === 0 && diff_sec === 1) {
                    a.submit();
                    $('#quiz_content').html('<h3>Time Is Up!</h3><p>Processing....</p>');
                    clearInterval(timer_interval);
                    // setTimeout(function () {
                    //     $('#quiz_content').html('<h3>Time Is Up!</h3>');
                    //     clearInterval(timer_interval);
                    // }, 1000);
                }
                // a.getElementById('answerFormQuiz').submit();
            }, 1000, form);

            // document.getElementById('quiz_form_submit_button').onclick = function() {
            //     clearInterval(timer_interval);
            //     form.submit();
            //     $('#quiz_content').html('<p>Processing....</p>');
            // };

        @endif
    });

</script>
<script src="{{ asset('js/dashboard/icheck.min.js') }}"></script>
@endsection
@extends('layouts.studentdashboard') 

@section('css')
	<link rel="stylesheet" href="{{asset('css/dashboard/green.css')}}" /> 
@endsection 

@section('content')

<!-- page content -->
<div class="right_col" role="main">
	{!! Breadcrumbs::render('question_exercise') !!}
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_content">
					@if (empty($questions[0]))
						<hr>
						<p>Question not found</p>
					@else
						<h1 class="text-center">Exercise</h1> <hr>
						{!! Form::open(['route' => 'question.success.exercise', 'id'=>'answerForm']) !!}

						@if ($errors->has('choice2answer'))
							<script type='text/javascript'>
                                $(document).ready(function(e) {
                                    generate_PNotify('You must select at least one answer');
                                });
							</script>
						@endif

					<?php $question_index = 0; ?>
					
						@foreach($questions as $question)
							<div class="row">

							{{--Contents--}}
							@if(session('result_list'))
								<div class="col-md-6 col-sm-6 col-xs-12">
							@else
								<div class="col-md-12 col-sm-12 col-xs-12">
							@endif
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<h3 style="word-wrap: break-word;">{{$question->topic}}</h3>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12 question_content" @if($question->mode == 0) style='white-space: pre-line;' @endif>
										<p style="max-width:100%;">
											@if($question->mode == 0)
												{{ $question->contents }}
											@else
												<?php
													echo html_entity_decode($question_filter_content[$question->id]);
												?>
											@endif
										</p>
									</div>
								</div>
								<br>

								{{--SHOW Answer OR Not--}}
								@if(session('result_list'))
									@if(empty($question_choices[$question->id]))
										@continue
									@endif
									<?php $results = session('result_list'); $i = 1;  $choices = $question_choices[$question->id]; ?>
									<div>
											@if ($question->type==0)
											@foreach($choices as $i=>$choice)
												@if(empty($choice->choice))
													@continue;
												@endif

												@if($choice->answer == 1)
													<div class='row'>
														<div class="col-md-1 col-sm-1 col-xs-1">
															<i class="fa fa-check fa-2x" style="color:#24b999; min-height:35px;"></i>
														</div>
														<div class="col-md-11 col-sm-11 col-xs-11">
															<p style="word-wrap:break-word; min-height:35px; font-size:120%;">{{ $choice->choice }}</p>
														</div>
													</div>
												@else
													<div class='row'>
														<div class="col-md-1 col-sm-1 col-xs-1">
															<i class="fa fa-close fa-2x" style="color:#d43f3a;min-height:35px;"></i>
														</div>
														<div class="col-md-11 col-sm-11 col-xs-11">
															<p style="word-wrap:break-word; min-height:35px; font-size:120%;">{{ $choice->choice }}</p>
														</div>
													</div>
												@endif
											@endforeach

										@elseif ($question->type==1)
											@foreach($choices as $choice)
												<div class="input-group">
													<div>
														@if($choice->answer == 1)
															True
														@else
															False
														@endif
													</div>
												</div>
											@endforeach
										@endif
									</div>
									<p><b>Your Answers</b>:
											@if($question->type==0)
												@if(empty($results[$question_index][1]))
													No Answer
												@else
													@foreach($results[$question_index][1] as $myanswer)
														{{htmlentities($myanswer)}} <br>
													@endforeach
												@endif
											@else
												@if($results[$question_index][1]==0)
													False
												@else
													True
												@endif
											@endif

									</p>
									<p>
										<div class="row">
											@if($results[$question_index][0]==1)
												<div class="alert alert-success">Result: Correct</div>
											@else
												<div class="alert alert-danger">Result: Wrong</div>
											@endif
										</div>
									</p>

								@else

									<p>
									@if(empty($question_choices[$question->id]))
										@continue
									@endif

									{{--List choices--}}
									<?php $choices = $question_choices[$question->id]; $i = 1; ?>

										@foreach ($choices as $choice)
											@if ($question->type == 0 )
												@if (empty($choice))
													@continue
												@endif
												@if($question_num_answers[$question->id] == 1)
													<div class='row'>
														<div class="col-md-1 col-sm-1 col-xs-1">
															<input type="radio" name="{{'choice2answer_'.$question_index.'[]'}}" class="flat" id="{{'choice2answer_'.$question_index.$i}}" value='{{$choice->choice}}'  />
														</div>
														<div class="col-md-11 col-sm-11 col-xs-11">
															<p style="word-wrap:break-word">{{$choice->choice}}</p>
														</div>
													</div>
												@else
													<div class='row'>
														<div class="col-md-1 col-sm-1 col-xs-1" >
															<input type="checkbox" name="{{'choice2answer_'.$question_index.'[]'}}" class="flat" id="{{'choice2answer_'.$question_index.$i}}" value='{{$choice->choice}}'  >
														</div>
														<div class="col-md-11 col-sm-11 col-xs-11">
															<p style="word-wrap:break-word">{{$choice->choice}}</p>
														</div>
													</div>
												@endif
											@else
												<div class="row inputtf">
													<div class="col-md-6 col-sm-12 col-xs-12">
														<div class="form-group">
															<select name="{{'choice2answer_'.$question_index}}" class="form-control input-sm">
																<option value="0">False</option>
																<option value="1">True</option>
															</select>
														</div>
													</div>
												</div>
											@endif
										<?php $i++; ?>
										@endforeach
										{{Form::hidden('questionID_'.$question_index, $question->id) }}

										<br>
									</p>
								@endif
								</div>

							{{--Explanaition & Tips--}}
							@if(session('result_list'))
								@if(!empty($question->explaination))
									<div class="col-md-1 col-sm-1 col-xs-12"><br></div>
									<div class="col-md-5 col-sm-5 col-xs-12">
										<div>
											<div class="title" style="background: #5bc0de; color: #fff; text-align: center; padding:5px;">
												<h2><i class="fa fa-puzzle-piece"></i> Explaination</h2>
											</div>
											<div>
												<div class="pricing_features">
													<ul class="list-unstyled text-left">
														<li>{{ $question->explaination }}</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								@endif
							@endif
							</div>
							<hr>
                            <?php $question_index++; ?>
						@endforeach

						@if(!session('result_list'))
							{{ Form::submit('Answer', array('class'=>'btn btn-success')) }}
						@else
							<a href="{{route('question.exercise')}}" class="btn btn-dark">Retake</a>
						@endif
						{!! Form::close() !!}
					@endif
				</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('javascript')
<script src="{{ asset('js/dashboard/icheck.min.js') }}"></script>
@endsection
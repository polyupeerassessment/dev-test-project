@extends('layouts.studentdashboard') 

@section('content')

<style>
	@import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

	.rating>input {
		display: none;
	}

	.rating>label:before {
		margin: 5px;
		font-size: 1.25em;
		font-family: FontAwesome;
		display: inline-block;
		content: "\f005";
	}

	.rating>.half:before {
		content: "\f089";
		position: absolute;
	}

	.rating>label {
		color: #ddd;
		float: right;
	}
	
	/***** CSS Magic to Highlight Stars on Hover *****/
</style>

<!-- page content -->
<div class="right_col" role="main">
	{!! Breadcrumbs::render('question_show') !!}
	<div class="page-title" style="padding: 0px; margin-top: -4%;">
		{{--<div class="title_right">--}}
			{{--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">--}}
				{{--{!! Form::open(['route' => 'question.searchquestion']) !!}--}}
				{{--<div class="input-group">--}}
					{{--<input id = 'search_keyword_showquestion' type="text" class="form-control" name="search"--}}
						{{--@if(!empty($keyword_searched))--}}
							{{--value="{{$keyword_searched}}">--}}
						{{--@else--}}
							{{--placeholder="Search for...">--}}
						{{--@endif--}}
					{{--<span class="input-group-btn">--}}
						{{--<button class="btn btn-default" type="submit">Go!</button>--}}
					{{--</span>--}}
				{{--</div>--}}
{{--				{!! Form::close() !!}--}}
			{{--</div>--}}
		{{--</div>--}}
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_content">
					@if (empty($questions[0]))
					<hr>
					<p>Question not found</p>
					@else

							<div class="row">
								<div class="list-group">
										<h1 class="text-center">Question</h1>
									<hr />
									<div class="">
										@foreach($questions as $question)
										<!-- blockquote -->
										<blockquote class="blockquote" @if($question->isMadeByTeacher == 1) style="border-color: #990000;"> @else style="border-color: #26B99A;"> @endif
											<div class="form-group">
												<div class="row">
													<div class="col-md-8">
														<h3>
															<a href="{{route('question.detail', ['id' => $question->id])}}" class="search_keyword_target">{{$question->topic}}</a>
														</h3>
													</div>
													<div class="col-md-4">
														<fieldset class="rating pull-right">
															@for($i=5.0;$i>=0.0; $i -= 0.5)
																@if($i==0)
																	@continue
																@endif
																@if(ceil($i) >$i)
																<input class='star_input' type="radio" id="star{{$i}}half" name="rating" value="{{$i}}" />
																<label @if((float)$question->rating >= $i) style="color: #FFD700;" @endif class="half" for="star{{$i}}half"></label>
																@else
																<input class='star_input' type="radio" id="star{{$i}}" name="rating" value="{{$i}}" />
																<label @if((float)$question->rating >= $i) style="color: #FFD700;" @endif class="full rating-start" for="star{{$i}}" ></label>
																@endif
															@endfor
														</fieldset>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<p>			
															<div class="search_keyword_target">
															<?php
//                                                            	$in= preg_replace('#<[^>]+>#', ' ', (string)$question->contents);
                                                            	$in = str_replace( '<', ' <',(string)$question->contents );
																$in = strip_tags((string)$in);
//                                                            	$in= preg_replace('#<[^>]+>#', ' ', (string)$in);
																//$out = strlen(strip_tags($in)) > 30 ? substr(strip_tags($in),0,30)."..." : $in;
																$out = strlen($in) > 30 ? substr($in,0,30)."..." : $in;
																
																echo $out;//$out;
//																echo $question->contents;
															?>
															{{--{{$question->contents}}--}}
															</div>
														</p>
													</div>
												</div>
												<div class="row">
													<div class="col-md-8">
														<h6 class="search_keyword_target">
															Difficulty:
															<?php
																switch($question->difficulty) {
																	case 0:echo "Easy";break;
																	case 1:echo "Normal";break;
																	case 2:echo "Hard<";break;
																	default: echo "Very Hard";break;
																}
															?>
																| {{$question->created_at}} |
																@if(!empty($comments[$question->id])) {{$comments[$question->id]}} @else 0 @endif  Comments | @if(!empty($attempts[$question->id])) {{$attempts[$question->id]}} @else 0 @endif Answers
																| Tags : @if(!empty($keywords[$question->id])) @foreach(explode(',',$keywords[$question->id]) as $keyword)
																<a href="{{route('question.showquestion.get', ['search'=>$keyword])}}" class="badge badge-light">{{$keyword}}</a>

																@endforeach @endif
														</h6>
														<h6 class="search_keyword_target">
															Type:
															<?php
																switch($question->type) {
																	case 0:echo "Multiple Choice";break;
																	case 1:echo "True/False";break;
																	case 2:echo "Short Question";break;
																	default: echo "";break;
																}
															?>
														</h6>
													</div>
													<div class="col-md-4 pull-right">
														<div class="pull-right search_keyword_target">
															<a href="{{route('question.detail', ['id' => $question->id])}}" role="button" class="btn btn-default btn-lg @if($question->author_id == Auth::id()) btn-secondary">View @else btn-success">Answer @endif
															</a>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="text-center">
														@if(!empty($teachersByQuestionId[$question->author_id]))
															<small>created by
																<cite>
																	<a href="#">{{$teachersByQuestionId[$question->author_id]}}</a>
																</cite>
															</small>
														@endif
													</div>
												</div>
											</div>
										</blockquote>
										@if(!$loop->last)
										<hr> @endif
										@endforeach
									</div>
								</div>
							</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('javascript')
	<script>

	</script>
@endsection
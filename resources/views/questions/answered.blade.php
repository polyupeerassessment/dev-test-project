@extends('layouts.studentdashboard')

@section('content')
    <style>
        .table-responsive {
            border:none;
        }
    </style>
    <!-- page content -->
    <div class="right_col" role="main">
        {!! Breadcrumbs::render('question_answered') !!}
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>My Answers</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive">
                        <p class="text-muted font-13 m-b-30">
                            You can download files / copy rows from below buttons for your reference.
                        </p>
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th style="min-width:100px;">Topic</th>
                                <th>Difficulty</th>
                                <th>Date</th>
                                <th style="min-width:75px;">Result</th>
                                <th>Num of Attempts</th>
                                <th>Maximum Attempts</th>
                                <th>Your Rating</th>
                                <th>Avg. Rating</th>
                                <th>Open</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($questions as $question)
                                <tr>
                                    <td>{{$question->id}}</td>
                                    <td>{{$question->topic}}</td>
                                    <td>
                                        @if($question->difficulty == 0)
                                            Easy
                                        @elseif($question->difficulty == 1)
                                            Normal
                                        @else
                                            Hard
                                        @endif
                                    </td>
                                    <td>
                                        <?php
                                        $date = date_create($question->created_at);
                                        echo date_format($date,"Y/m/d H:i");
                                        ?>
                                    </td>
                                    <td>
                                        @if($question->result==1)
                                            <a class=" btn-primary btn-xs"><i class="fa fa-check" aria-hidden="true"></i>&nbspCorrect</a>
                                        @else
                                            <a class=" btn-danger btn-xs"><i class="fa fa-times" aria-hidden="true"></i>&nbspWrong</a>
                                        @endif

                                    </td>
                                    <td>{{$question->num_attempts}}</td>
                                    <td>{{$question->max_attempts}}</td>
                                    <td>
                                        @if(empty($question->rating))
                                            -
                                        @else
                                            {{$question->rating}}
                                        @endif
                                    </td>
                                    <td>
                                        @if(empty($question->avg_rating))
                                            -
                                        @else
                                            {{$question->avg_rating}}
                                        @endif
                                    </td>

                                    <td><a href="{{route('question.detail', ['id' => $question->id])}}" class="btn btn-xs btn-success">Open</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
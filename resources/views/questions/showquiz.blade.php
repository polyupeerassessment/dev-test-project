@extends('layouts.studentdashboard')
<?php use Carbon\Carbon;?>
@section('content')
    <!-- page content -->
    <div class="right_col" role="main">

        <div class="page-title" style="padding: 0px;">
            <div class="title_left">
            </div>
        </div>
        {!! Breadcrumbs::render('question_quiz') !!}
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <h1 class="text-center">Quiz</h1>
                        <hr>
                        <div>
                            @if($is_test_now_flag==false)
                                Quiz is not available at this time
                            @else
                                <h4>Please read following attention before you start taking quiz</h4>
                                <ol>
                                    <li>Please make sure that you are ready to take this quiz.</li>
                                    <li>Once you clicked bellow's button, you may not retake this quiz</li>
                                    <li>Quiz's clock will run and it will not stop even you close or refresh page</li>
                                    <li>Please contact your teacher if you encounter a system error while taking the quiz</li>
                                    {{--<li>Please be notised that the quiz </li>--}}
                                    <li><b>Please use Chrome / Firefox for this quiz </b> </li>
                                    {{--<li>The deadline for this quiz is <b>{{$deadline}}</b></li>--}}
                                </ol>
                                <br>

                                <p>If you are ready, please click bellow's start button to start your quiz</p>
                                <p>Good Luck!</p>

                                <hr>

                                <div class="text-center">
                                    <a href="{{route('question.quizdetail')}}" class="btn btn-primary">Start</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(!empty($pre_quiz))
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <h1 class="text-center">Quiz Took</h1>

                        @foreach($pre_quiz as $q)
                            <hr>
                            <div>
                                <p class="pull-right">{{ (new Carbon($q->created_at))->diffForHumans() }}</p>
                                <a href="{{route('question.quiz.my',['id'=>$q->id])}}"><h2>{{$q->topic}}</h2></a>

                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif

    </div>
@endsection
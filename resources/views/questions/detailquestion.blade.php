@extends('layouts.studentdashboard') 

@section('css')
	<link rel="stylesheet" href="{{asset('css/dashboard/green.css')}}" /> 
@endsection 

@section('content')

<style>
	@import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);
	.message {
		display:block;
		/*width:150px;*/
		max-width:75%;
		word-wrap:break-word;
	}
	.aligned-row {
		display: flex;
		flex-flow: row wrap;
		/*padding:0px;*/
		/*margin:0px;*/
	}

	.rating {
		padding: 1%;
	}

	.rating {
		border: none;
		float: left;
	}

	.rating>input {
		display: none;
	}

	.rating>label:before {
		margin: 5px;
		font-size: 1.25em;
		font-family: FontAwesome;
		display: inline-block;
		content: "\f005";
	}

	.rating>.half:before {
		content: "\f089";
		position: absolute;
	}

	.rating>label {
		color: #ddd;
		float: right;
	}

	#custom-rating-click {
		color: #FFD700;
	}
	/***** CSS Magic to Highlight Stars on Hover *****/

	.rating>input:checked~label,
	/* show gold star when clicked */

	.rating:not(:checked)>label:hover,
	/* hover current star */

	.rating:not(:checked)>label:hover~label {
		color: #FFD700;
	}
	/* hover previous stars in list */

	.rating>input:checked+label:hover,
	/* hover current star when changing rating */

	.rating>input:checked~label:hover,
	.rating>label:hover~input:checked~label,
	/* lighten current selection */

	.rating>input:checked~label:hover~label {
		color: #FFED85;
	}

	.rating-modal {
		text-align: center;
		padding: 0!important;
	}

	.rating-modal:before {
		content: '';
		display: inline-block;
		height: 100%;
		vertical-align: middle;
		margin-right: -4px;
	}

	.rating-modal-dialog {
		display: inline-block;
		text-align: left;
		vertical-align: middle;
	}

	div {
		font-size:101%;
	}
	h2 {
		font-size:21px;
	}

</style>

<!-- Small modal -->
@if($show_rating_popup_box && $showAnswer)
<div class="modal fade bs-example-modal-sm rating-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md rating-modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel2">Rate For Your Peers!</h4>
			</div>
			<div class="modal-body">
				<p>Please rate this question. Your rating may help the author's improvement.</p>
				<fieldset class="rating">
					@for($i=5.0;$i>=0.0; $i -= 0.5)
						@if($i==0)
							@continue
						@endif
						@if(ceil($i) >$i)
							<input class='star_input_modal' type="radio" id="star{{$i}}half" name="rating" value="{{$i}}" style=" font-size: 25px;"/>
							<label @if((float)$rating>= $i) id="custom-rating-click" @endif class="half" for="star{{$i}}half" style="font-size: 25px;"></label>
						@else
							<input class='star_input_modal' type="radio" id="star{{$i}}" name="rating" value="{{$i}}" />
							<label @if((float)$rating>= $i) id="custom-rating-click" @endif class = "full rating-start" for="star{{$i}}" style="font-size: 25px;"></label>
						@endif
					@endfor
				</fieldset>
			</div>
			<br><br><br>
			<div style="padding: 15px;">
				<button type="button" class="btn btn-default" data-dismiss="modal">Ok Got It!</button>
			</div>
		</div>
	</div>
</div>
<!-- /modals -->
@endif

<!-- page content -->
<div class="right_col" role="main">
	{!! Breadcrumbs::render('question_detail', $question) !!}
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title" >
					<h2 style="overflow-wrap: break-word;">{{$question->topic}}</h2>
                    <div class="checkmark-circle">
                        <div class="background"></div>
                        <div class="checkmark draw"></div>
                    </div>
					<div class="nav navbar-right">
						@if($question->isMadeByTeacher == 1 || $question->author_id != Auth::id())
							@if(!is_null($pre_question_id))
                                <a href= " {{route('question.detail', ['id' => $pre_question_id])}} " class="btn btn-dark"><i class="fa fa-arrow-left"></i> Previous Question</a>
							@endif
                            @if(!is_null($next_question_id))
                                <a href= "{{route('question.detail', ['id' => $next_question_id])}} " class="btn btn-dark">Next Question <i class="fa fa-arrow-right"></i></a>
                            @endif
						@endif
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					{{--Question contents--}}
					<div class='row'>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<div class="row">
									<div class="col-md-12 col-xs-12 question_content" @if($question->mode == 0) style='white-space: pre-line;' @endif>
										<p style="max-width:100%;" id="question_content_detail">
											@if($question->mode == 0)
												{{$question->contents }}
											@else
												<?php echo html_entity_decode($contents); ?>
											@endif
										</p>
									</div>
								</div>
							</div>
						</div>

					</div><br>
					
					<div class="row">
						@if(($toShowChatFlag==true && $showAnswer==false && !empty(strip_tags($question->tips)) != 0) || ($question->isMadeByTeacher == 0 && $question->author_id == Auth::id() &&!empty(strip_tags($question->tips))  ))
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="list-group">
								<ul class="list-group">
									<li class="list-group-item">
										<div class="form-group">
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<h3><i class="fa fa-lightbulb-o"></i> Tips</h3>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<p style="overflow-wrap: break-word;">
													<strong >
														{{ $question->tips }}
													</strong>
													</p>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
						@endif
						@if($showAnswer==true && !empty(strip_tags($question->explaination)))
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="list-group">
								<ul class="list-group">
									<li class="list-group-item">
										<div class="form-group">
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<h3><i class="fa fa-puzzle-piece"></i> Explanation</h3>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-12">
                                                    <p style ="overflow-wrap: break-word;">
													<strong>
                                                        {{ $question->explaination }}
													</strong>
                                                    </p>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
	{{--Answer--}}
	<div class="row aligned-row ">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			<div class="x_content">

                <div class="col-xs-12 col-lg-7" >
					<div class="x_title">
						<h2>Answer</h2>
						<div class="clearfix"></div>
					</div>
					<div class="">
						{{--Answer Choice--}}
						<div class='row'>
							<div class="col-md-12 col-sm-12 col-xs-12">
								@if($showAnswer)
								{{--Show Answer made by author--}}
									<!-- MC -->
									@if ($question->type==0)
										@foreach($choices as $i=>$choice)
											@if(empty($choice->choice))
												@continue;
											@endif
											<?php
											$prefix_character = '';
											if ($i == 0) {
												$prefix_character = 'A: ';
											} elseif ($i == 1) {
												$prefix_character = 'B: ';
											} elseif ($i == 2) {
												$prefix_character = 'C: ';
											} elseif ($i == 3) {
												$prefix_character = 'D: ';
											} elseif ($i == 4) {
												$prefix_character = 'E: ';
											} elseif ($i == 5) {
												$prefix_character = 'F: ';
											}

											?>

											@if($choice->answer == 1)
												<div class='row'>
													<div class="col-md-1 col-sm-1 col-xs-1">
														<i class="fa fa-check fa-2x" style="color:#24b999; min-height:35px;"></i>
													</div>
													<div class="col-md-11 col-sm-11 col-xs-11">
														<p style="word-wrap:break-word; min-height:35px; font-size:120%;">{{$prefix_character.' '.$choice->choice}}</p>
													</div>
												</div>
											@else
												<div class='row'>
													<div class="col-md-1 col-sm-1 col-xs-1">
														<i class="fa fa-close fa-2x" style="color:#d43f3a;min-height:35px;"></i>
													</div>
													<div class="col-md-11 col-sm-11 col-xs-11">
														<p style="word-wrap:break-word; min-height:35px; font-size:120%;">{{$prefix_character.$choice->choice}}</p>
													</div>
												</div>
											@endif
										@endforeach

									@elseif ($question->type==1)
										@foreach($choices as $choice)
											<div class="input-group">
												<div>
													@if($choice->answer == 1)
														True
													@else
														False
													@endif
												</div>
											</div>
										@endforeach
									@endif
								{{--Answer Question Here--}}
								@else
									<p>
										@if ($errors->has('choice2answer'))
										<script type='text/javascript'>
											$(document).ready(function(e) {
												generate_PNotify('You must select at least one answer');
											});
										</script>
										@endif
										{!! Form::open(['route' => 'question.success', 'id'=>'answerForm', 'class'=>'form-horizontal' ]) !!}
											<?php $i = 1; $answerCnt = 0;?>
											{{--List choices--}}

											@foreach ($choices as $choice)
												@if (empty($choice->choice))
													@continue
												@endif
												@if ($choice->answer == 1 )
													<?php $answerCnt++ ?>
												@endif
											@endforeach

											@foreach ($choices as $i=>$choice)
												@if ($question->type == 0 )
                                                    @if (empty($choice->choice))
                                                        @continue
                                                    @endif

													@if($answerCnt == 1)
														<div class='row'>
															<div class="col-md-1 col-sm-1 col-xs-1">
																<input type="radio" name="choice2answer[]" class="flat" id="{{'choice2answer'.$i}}" value='{{$choice->choice}}'  />
															</div>
															<div class="col-md-11 col-sm-11 col-xs-11">
																<label style="word-wrap:break-word" for="{{'choice2answer'.$i}}">{{$choice->choice}}</label>
															</div>
														</div>
														<br>				
													@else
														<div class='row'>
															<div class="col-md-1 col-sm-1 col-xs-1" >
																<input type="checkbox" name="choice2answer[]" class="flat" id="{{'choice2answer'.$i}}" value='{{$choice->choice}}'  >						
															</div>
															<div class="col-md-11 col-sm-11 col-xs-11">
																<label for="{{'choice2answer'.$i}}" style="word-wrap:break-word">{{$choice->choice}}</label>
															</div>
														</div>
														<br>
													@endif
												@else
													<div class="row inputtf">
														<div class="form-group">
															<select name="choice2answer" class="form-control">
																<option value="0">False</option>
																<option value="1">True</option>
															</select>
														</div>
													</div>
												@endif
												<?php $i++;?>
											@endforeach
											<br>
											{{Form::hidden('questionID', $question->id, ['id'=>'questionID']) }}
											{{Form::submit('Answer', array('class'=>'btn btn-success pull-right'))}}
										{!! Form::close() !!}
									</p>
								@endif
							</div>
						</div>
						<br>
						<br>
						<br>
				</div>
			</div>


			@if(($question->author_id != Auth::id() || $question->isMadeByTeacher == 1  ) && $showAnswer == true && $show_rating_popup_box==false )
			<div class="col-lg-1"><br><br></div>
			<div class="col-xs-12 col-lg-4">
                <div class="x_title">
                    <h2>Rating</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="" style="margin-left: 10px;">
                    
                    <div class="row">
						<p id="pleaseRatetxt">Please Rate This Question</p>
                        <fieldset class="rating">
                            @for($i=5.0;$i>=0.0; $i -= 0.5)
                                @if($i==0)
                                    @continue
                                @endif
                                @if(ceil($i) >$i)
                                <input class='star_input' type="radio" id="star{{$i}}half" name="rating" value="{{$i}}" style=" font-size: 30px;"/>
                                <label @if((float)$rating>= $i) id="custom-rating-click" @endif class="half" for="star{{$i}}half" style="font-size: 30px;"></label>
                                @else
                                <input class='star_input' type="radio" id="star{{$i}}" name="rating" value="{{$i}}" />
                                <label @if((float)$rating>= $i) id="custom-rating-click" @endif class = "full rating-start" for="star{{$i}}" style="font-size: 30px;"></label>
                                @endif
                            @endfor
						</fieldset>
						<p id ='thankurating' style="float: left; padding-top:11px;padding-left:5px;"></p>
                    </div>
                </div>
			</div>

			@endif

            @if($question->isMadeByTeacher == 0 && $question->author_id==Auth::id())
            	<div class="col-xs-12 col-lg-4 col-lg-offset-1">
                    <div class="x_title">
                        <h2>Statistics<small>Choices</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="" >
                        @if($total_num==0)
                            No Answer Yet
                        @else
                            <div id="graph_donut" style="width:100%; height:300px;"></div>
                        @endif
                    </div>
				</div>
            @endif
	</div>
</div>
{{--CHAT--}}
@if($showAnswer==true || ($question->isMadeByTeacher == 0 && $question->author_id==Auth::id()) )
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Chat</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div id ='messages_ajax' class='col-xs-12 col-md-12'>
                        <?php foreach($comments as $post) {
                            $date = date_create($post->created_at);
                            $date = date_format($date,"Y/m/d H:i");?>

							{{--Peers' Comments--}}
                            @if(!empty($post->comment) && $post->author_id != Auth::id())
                                <div class="row">
                                    <h4 class="heading">{{$post->nickname}} : <small>{{$date}}</small></h4>
                                </div>
                                <div class="row">
                                    <blockquote class="message" style="border-color:#3597db;">
                                        {{htmlentities($post->comment)}}
                                    </blockquote>
                                </div>
                            @endif

							{{--Your Comment--}}
                            @if(!empty($post->comment) && $post->author_id == Auth::id())
                                <div class="row">
                                    <h4 class="heading pull-right">You : <small>{{$date}}</small></h4>
                                </div>
                                <div class="row ">
                                    <blockquote class="message blockquote-reverse pull-right" style="border-color:#28b999;">
                                        <a href="{{route('question.removemycomment', ['id'=>$post->id])}}" class="removemycomment" data-filter="{{$post->id}}"><i style='color:palevioletred; font-size:90%;' class="fa fa-close fa-1x"></i></a>
                                        {{htmlentities($post->comment)}}
                                    </blockquote>
                                </div>
                            @endif
                        <?php } ?>
                    </div>
                    <div class='col-md-12 col-xs-12'>
                        <div style="margin-top:3%;">
                            {!! Form::open(['id'=>'messageForm']) !!}
                                <h4>Message</h4>
                                {{Form::textarea('message', null, array('class' => 'form-control', 'rows'=>'5','id'=>'detail-message','required'))}}
                                {{Form::hidden('questionID', $question->id ,array('id'=>'questionID'))}}
                                @if ($errors->has('message'))
                                <script type='text/javascript'>
                                    $(document).ready(function(e) {
                                        generate_PNotify('Please write message before send');
                                    });
                                </script>
                                @endif
                                <br>
                                {{Form::submit('Send', array('class'=>'btn btn-success pull-right'))}}
                            {!! Form::close() !!}
                            <br>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
    </div>
</div>
</div>
@endif
</div>

</div></div>

@endsection

@section('javascript')
<script src="{{ asset('js/dashboard/icheck.min.js') }}"></script>
<script>
$('.rating-modal').modal('show');
$(function(){

    function htmlDecode(input){
        var e = document.createElement('div');
        e.innerHTML = input;
        // handle case of empty input
        return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
    }

    if ($('#graph_donut').length ){
        choicedata = [];

        @if($total_num!=0)

			@foreach($choices as $i=>$choice)
				<?php
					$prefix_character = '';
					if ($i == 0) {
						$prefix_character = 'A';
					} elseif ($i == 1) {
						$prefix_character = 'B';
					} elseif ($i == 2) {
						$prefix_character = 'C';
					} elseif ($i == 3) {
						$prefix_character = 'D';
					} elseif ($i == 4) {
						$prefix_character = 'E';
					} elseif ($i == 5) {
						$prefix_character = 'F';
					}
				?>

				@if ($question->type == 0 )
					choicedata.push({label:htmlDecode("{{($prefix_character)}}"), value: "{{round(($choice->num_attempts / $total_num) * 100, 2)}}" });
				@else
					@if($choice->answer == 1)
						choicedata.push({label:"False", value: "{{ round((($total_num - $choice->num_attempts) / $total_num) * 100 , 2)}}" });
						choicedata.push({label:"True", value: "{{round(($choice->num_attempts / $total_num)*100, 2)}}" });
					@else
						choicedata.push({label:"True", value: "{{ round((($total_num - $choice->num_attempts) / $total_num) * 100 ,2)}}" });
						choicedata.push({label:"False", value: "{{round(($choice->num_attempts / $total_num)*100, 2)}}" });
					@endif
				@endif
			@endforeach
        @endif
        Morris.Donut({
            element: 'graph_donut',
            data: choicedata,
            colors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB', '#FF1493', '#DAA520'],
            formatter: function (y) {
                return y + "%";
            },
            resize: true
        });



    }
});
</script>
@endsection
@extends('layouts.studentdashboard') 

@section('content')

	<style>
		.table-responsive {
			border:none;
		}
	</style>
<!-- page content -->
<div class="right_col" role="main">
	{!! Breadcrumbs::render('question_my') !!}
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>My Questions</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content table-responsive borderless">
					<p class="text-muted font-13 m-b-30">
						You can download files / copy rows from below buttons for your reference.
					</p>
					<table id="datatable-buttons" class="table table-striped table-bordered table-custom-by-masa">
						<thead>
						<tr>
							<th>ID</th>
							<th>Date</th>
							<th>Topic</th>
							<th>Difficulty</th>
							<th>Type</th>
							<th>Content Mode</th>
							<th>Average Rating</th>
							<th>Average Score</th>
							<th>No. of Corrects</th>
							<th>No. of Answers</th>
							<th>No. of Views</th>
							<th>Action</th>
						</tr>
						</thead>

						<tbody>
							@foreach($questions as $question)
								<tr @if(session('justcreated_id'))
										@if($question['id'] == session('justcreated_id'))
											style="color:blue;"
										@endif
										 @endif
								>
									<td>{{$question['id']}}</td>
									<td>{{$question['created_at']}}</td>
									<td>{{$question['topic']}}</td>
									<td>
										@if($question['difficulty'] == 0)
											Easy
										@elseif($question['difficulty'] == 1)
											Normal
										@else
											Hard
										@endif
									</td>
									<td>
                                        @if($question['type'] == 0)
                                            MC
                                        @else
                                            T/F
                                        @endif
									</td>
                                    <td>
                                        @if($question['mode'] == 0)
                                            Normal
                                        @else
                                            HTML
                                        @endif
                                    </td>
									<td>{{$question['rating']}}</td>
									<td>{{number_format($question['avg_score'],1)}}</td>
									<td>{{$question['num_corrects']}}</td>
									<td>{{$question['num_answers']}}</td>
									<td>{{$question['num_views']}}</td>
									<td>
										<a href="{{route('question.detail', ['id' => $question['id']])}}" class="btn btn-xs btn-info">View</a>
										<a href="{{route('question.edit', ['id' => $question['id']])}}" class="btn btn-xs btn-warning">Edit</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('javascript')
<script>
    // $('.table-custom-by-masa').dataTable({
    //     order: [[3, 'asc']]
    // });
</script>
@endsection('javascript')

@extends('layouts.studentdashboard')

@section('css')
    <style>
        .vote_modal_box {
            margin-top:15%;
        }
    </style>
@endsection
@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title" style="padding: 0px;">
            <div class="title_left"></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <h1>Voting Questions</h1><br><br>

                    <div class="x_content">

                        @foreach($questions as $question)
                            @if(empty($question))
                                @continue
                            @endif
                            <div class="row">
                                <hr><div class="clearfix"></div>
                                <div class="col-md-9 col-xs-9">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <h3 style="word-wrap: break-word;">{{$question->topic}}</h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 question_content" @if($question->mode == 0) style='white-space: pre-line;' @endif>
                                            <p style="max-width:100%;">
                                                @if($question->mode == 0)
                                                    {{ $question->contents }}
                                                @else
                                                    <?php
                                                    echo html_entity_decode($question_filter_content[$question->id]);
                                                    ?>
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-3" >
                                    {{--<a href="{{route('question.detail', ['id'=>$question->id])}}" class="btn btn-md btn-danger">Vote</a>--}}
                                    @if ($errors->has('vote'))
                                        <script type='text/javascript'>
                                            $(document).ready(function(e) {
                                                generate_PNotify('Failed to vote!');
                                            });
                                        </script>
                                    @endif

                                    {!! Form::open(['route' => 'question.vote', 'class'=>'form-horizontal ajax-button' ]) !!}
                                        {{Form::hidden('vote', 1)}}
                                        {{Form::hidden('question_id', $question->id)}}

                                        <a class="btn btn-danger btn-md" data-toggle="modal" data-target=".vote_q_{{$question->id}}"> Vote </a>

                                        <div class="modal fade bs-example-modal-sm vote_q_{{$question->id}} vote_modal_box" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                                        </button>
                                                        <h4 class="modal-title" id="myModalLabel2">Really?</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure to vote this question?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        {{Form::submit('Vote', array('class'=>'btn btn-danger submit'))}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="{{route('question.detail', ['id'=>$question->id])}}" class="btn btn-md btn-primary">View</a>
                                    {!! Form::close() !!}

                                </div>
                            </div><div class="clearfix"></div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

@section('javascript')
@endsection
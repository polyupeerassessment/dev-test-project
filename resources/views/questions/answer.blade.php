@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Thank you for your answer</h2>
        <br>
        Result: <strong>
        @if($result == 1)
            All correct
        @else
            You are wrong
        @endif
        </strong>
        {{--<br><br>--}}

        {{--<div class="panel panel-default">--}}
            {{--<div class="panel-heading">Answers: </div>--}}
            {{--<div class="panel-body">--}}


                {{--@if ($type == 0)--}}
                    {{--@foreach($answers as $answer)--}}
                        {{--{{$choices[(int)($answer)-1]}}<br>--}}
                    {{--@endforeach--}}
                {{--@elseif ($type == 1)--}}
                    {{--@foreach($answers as $answer)--}}
                        {{--{{$choices[(int)($answer)]}}<br>--}}
                    {{--@endforeach--}}

                {{--@endif--}}

            {{--</div>--}}

        {{--</div>--}}

        <a class="nav-link" href="{{ route('question.showquestion') }}">Back To Question List</a>
        <a class="nav-link" href="{{ route('student.dashboard') }}">Back To Dashboard</a>

    </div>
@endsection

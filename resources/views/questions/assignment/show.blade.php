@extends('layouts.studentdashboard')

<?php use Carbon\Carbon;?>

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title" style="padding: 0px;">
            <div class="title_left"></div>
        </div>
        {!! Breadcrumbs::render('assignment') !!}
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <h1 class="text-center">Assignment</h1>
                        <hr>
                        <br>
                        @if (count($activeAssignments) > 0)
                            @foreach($activeAssignments as $assignment)
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        {{--  <p class="pull-right"> Score: {{ $assignment->score }} / {{ $assignment->score }}</p>   --}}
                                        @if ($activeAssignmentsStatus[$assignment->id] == "Finish")
                                            <h2 class="pull-right"><span class="label label-primary"> {{ $activeAssignmentsStatus[$assignment->id] }}</span></h2>
                                        @else
                                            <p class="pull-right"><a class="btn btn-success" href="{{route('question.assignment.detail',['id'=>$assignment->id])}}"> {{ $activeAssignmentsStatus[$assignment->id] }}</a></p>
                                        @endif

                                        <div><h3>{{ $assignment->topic }}</h3><p>Score: {{$assignmentsTook_scores[$assignment->id]}} / {{ $assignment->score }}</p></div>
                                        <hr>   
                                    </div>
                                </div>    
                            @endforeach
                        @else
                            <div style="text-align: center;">
                                Assignment is not available at this time
                            </div>
                        @endif
                        <br>
                    </div>
                </div>
            </div>
        </div>
        
        @if (!empty($assignnmentsTook) && count($assignnmentsTook) > 0)
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <h2 class="x_content">
                            <h1 class="text-center">Assignment Took</h1>
                            <hr>
                            @foreach($assignnmentsTook as $assignment)
                                <div>
                                    <h2>
                                        <div class="pull-right"> Score: {{ $assignmentsTook_scores[$assignment->id] }} / {{ $assignment->score }}</div>
                                        <span class="label label-success" style="padding-bottom:5px;padding-top:5px;margin-right:5px;">Finish</span> {{ $assignment->topic }}
                                    </h2>
                                </div>
                                <hr>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
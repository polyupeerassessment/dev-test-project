@extends('layouts.studentdashboard')

@section('css')
<link rel="stylesheet" href="{{asset('css/dashboard/green.css')}}" /> 
@endsection 

@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="page-title" style="padding: 0px;">
        <div class="title_left"></div>
    </div>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    {!! Form::open(['route' => 'question.assignment.success', 'id'=>'answerFormAssignment']) !!}
                    <div class="row">
                        {{--Contents--}}
                        {{Form::hidden('questionID', $question->id) }}
                        {{Form::hidden('assignmentID', $assignmentID) }}
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <h6 style="word-wrap: break-word;">Target Score: {{$currentResult}}</h6>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <h6 style="word-wrap: break-word; text-align: right;" >Number of Attempts: {{$totalCount}} / {{$maxAttempts}}</h6>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <h3 style="word-wrap: break-word;">{{$question->topic}}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 question_content" @if($question->mode == 0) style='white-space: pre-line;' @endif>
                                    <p style="max-width:100%;">
                                        @if($question->mode == 0)
                                            {{ $question->contents }}
                                        @else
                                            <?php echo html_entity_decode($question_filter_content[$question->id]); ?>
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <hr>
                            <div class="row">           
                                <div class="col-md-12 col-sm-12 col-xs-12">Your Answer:</div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <p style="max-width:100%;">               
                                        @foreach ($question_choice[$question->id] as $choice)
                                        @if ($question->type == 0 )
                                            @if (empty($choice))
                                                @continue
                                            @endif
                                            @if($question_num_answers[$question->id] == 1)
                                                <div class='row'>
                                                    <div class="col-md-1 col-sm-1 col-xs-1">
                                                        <input type="radio" class="flat" name="choice2answer[]" value='{{$choice->choice}}' />
                                                    </div>
                                                    <div class="col-md-11 col-sm-11 col-xs-11">
                                                        <p style="word-wrap:break-word">{{$choice->choice}}</p>
                                                    </div>
                                                </div>
                                            @else
                                                <div class='row'>
                                                    <div class="col-md-1 col-sm-1 col-xs-1" >
                                                        <input type="checkbox" class="flat" name="choice2answer[]" value='{{$choice->choice}}' />
                                                    </div>
                                                    <div class="col-md-11 col-sm-11 col-xs-11">
                                                        <p style="word-wrap:break-word">{{$choice->choice}}</p>
                                                    </div>
                                                </div>
                                            @endif
                                        @else
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <select name="choice2answer[]" class="form-control input-sm">
                                                            <option value="0">False</option>
                                                            <option value="1">True</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            @break
                                        @endif
                                        @endforeach
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div style="text-align: center;">
                        {{ Form::submit('Next', array('class'=>'btn btn-success')) }}
                    </div>
					{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection

@section('javascript')
<script src="{{ asset('js/dashboard/icheck.min.js') }}"></script>
@endsection
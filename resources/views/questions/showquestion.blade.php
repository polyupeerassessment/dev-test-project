@extends('layouts.studentdashboard')

@section('content')

<style>
	@import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

	.rating>input {
		display: none;
	}

	.rating>label:before {
		margin: 5px;
		font-size: 1.25em;
		font-family: FontAwesome;
		display: inline-block;
		content: "\f005";
	}

	.rating>.half:before {
		content: "\f089";
		position: absolute;
	}

    .create_question_button{
        border-color:forestgreen;
    }

    /*.create_question_button:hover {*/
        /*background-color:seagreen;*/
        /*border-color:forestgreen;*/
        /*color:white;*/
    /*}*/

	.rating>label {
		color: #ddd;
		float: right;
	}
	
	/***** CSS Magic to Highlight Stars on Hover *****/
</style>

<!-- page content -->
<div class="right_col" role="main" style="min-height:auto;">

	{!! Breadcrumbs::render('question_show') !!}

	<div class="page-title" style="padding: 0px;">
		<div class="title_left">
			<h3></h3>
		</div>
		<div class="title_right" >
			<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
				{!! Form::open(['route' => 'question.searchquestion']) !!}
				<div class="input-group">
					<input id = 'search_keyword_showquestion' type="text" class="form-control" name="search"
						@if(!empty($keyword_searched))
							value="{{$keyword_searched}}">
						@else
							placeholder="Search for...">
						@endif
					<span class="input-group-btn">
						<button class="btn btn-default" type="submit">Go!</button>
					</span>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-13 col-sm-13 col-xs-13" style="min-height:auto;">
			<div class="x_panel">
				<div class="x_content">
					@if (empty($questions[0]))
						@if(!empty($keyword_searched))
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="pull-right">
									<a href="{{route('question.clear')}}" class="btn btn-round btn-dark">Clear Search</a>
								</div>
							</div>
						</div>
						@endif
					<hr>
					<p>Question not found</p>
					@else
						<!-- check has teacher or student question -->
						<?php $hasTQ = "False"; $hasSQ = "False"; ?>
						@foreach($questions as $question)
							@if($hasTQ == "True" && $hasSQ == "True" )
								@break;
							@endif
							@if($question->isMadeByTeacher == 1)
								<?php $hasTQ = "True" ?>
							@elseif($question->isMadeByTeacher == 0)
								<?php $hasSQ = "True" ?>
							@endif
						@endforeach
						<!-- End check -->

						@if($hasTQ == "True")
							<div class="row">
							@if($hasSQ == "True")
								<!-- Has Student Question -->
								<div class="text-center">
									<h1>Recommendation</h1>
								</div>
							@else
								<!-- No Student Question-->
								<h1 class="text-center">Question</h1>
							@endif
							<br>
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<button type="button" class="btn btn-default" data-toggle="collapse" data-target="#filter_buttons"><i class="fa fa-filter"></i> Filter </button>
                                    @if($order_popularity_mode == 1)
                                        <a href="{{route('question.showquestion.popularity',[$order_popularity_mode])}}" class="btn btn-dark"><i class="fa fa-trophy"></i> Popularity&nbsp;&nbsp; <i class="fa fa-caret-down"></i></a>
                                    @elseif($order_popularity_mode == 2)
                                        <a href="{{route('question.showquestion.popularity',[$order_popularity_mode])}}" class="btn btn-dark"><i class="fa fa-trophy"></i> Popularity&nbsp; <i class="fa fa-caret-up"></i></a>
                                    @else
                                        <a href="{{route('question.showquestion.popularity',[$order_popularity_mode])}}" class="btn btn-default"><i class="fa fa-trophy"></i> Popularity&nbsp;<i class="fa fa-sort"></i></a>
                                    @endif

									@if($order_date_mode == 1)
										<a href="{{route('question.showquestion.date',[$order_date_mode])}}" class="btn btn-info"><i class="fa fa-calendar"></i> Date&nbsp;&nbsp; <i class="fa fa-caret-down"></i></a>
									@elseif($order_date_mode == 2)
										<a href="{{route('question.showquestion.date',[$order_date_mode])}}" class="btn btn-info"><i class="fa fa-calendar"></i> Date&nbsp;&nbsp; <i class="fa fa-caret-up"></i></a>
									@else
										<a href="{{route('question.showquestion.date',[$order_date_mode])}}" class="btn btn-default"><i class="fa fa-calendar"></i> Date&nbsp;&nbsp; <i class="fa fa-sort"></i></a>
									@endif

									@if($order_rating_mode == 1)
										<a href="{{route('question.showquestion.rating',[$order_rating_mode])}}" class="btn btn-warning"><i class="fa fa-star"></i> Rating&nbsp;&nbsp; <i class="fa fa-caret-down"></i></a>
									@elseif($order_rating_mode == 2)
										<a href="{{route('question.showquestion.rating',[$order_rating_mode])}}" class="btn btn-warning"><i class="fa fa-star"></i> Rating&nbsp;&nbsp; <i class="fa fa-caret-up"></i></a>
									@else
										<a href="{{route('question.showquestion.rating',[$order_rating_mode])}}" class="btn btn-default"><i class="fa fa-star"></i> Rating&nbsp;&nbsp; <i class="fa fa-sort"></i></a>
									@endif

									<div class="pull-right">
										<a href="{{route('question.create')}}" class="btn btn-round btn-success">Create a question</a>
									</div>
									<div class="pull-right">
										<a href="{{route('question.clear')}}" class="btn btn-round btn-dark">Clear Search</a>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div id="filter_buttons" class="collapse col-md-12 col-sm-12 col-xs-12">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<table>
												<tr>
													<td class="col-md-1 col-xs-1 col-sm-1">Difficulty:</td>
													<td class="col-md-11 col-xs-11 col-sm-11">
														<div class="btn-group">
															<button title="All Difficulty Question" class="btn btn-success filter_link_difficulty d-inline-block bg-info" data-filter="all" type="button">All</button>
															<button title="Difficulty: Easy" class="btn btn-default filter_link_difficulty d-inline-block bg-info" data-filter="easy" type="button">Easy</button>
															<button title="Difficulty: Normal" class="btn btn-default filter_link_difficulty d-inline-block bg-info" data-filter="normal" type="button">Normal</button>
															<button title="Difficulty: Hard" class="btn btn-default filter_link_difficulty d-inline-block bg-info" data-filter="hard" type="button">Hard</button>
														</div>
													</td>
												</tr>
												<tr>
													<td><br></td>
													<td></td>
												</tr>
												<tr>
													<td class="col-md-1 col-xs-1 col-sm-1">Type:</td>
													<td class="col-md-11 col-xs-11 col-sm-11">
														<div class="btn-group">
															<button title="All Type Question" class="btn btn-success filter_link d-inline-block bg-success" data-filter="all" type="button">All</button>
															<button title="Multiple Choice Question" class="btn btn-default filter_link d-inline-block bg-success" data-filter="mc" type="button">MC</button>
															<button title="True/False Question" class="btn btn-default filter_link d-inline-block bg-success" data-filter="tf" type="button">TF</button>
														</div>
													</td>
												</tr>
												<tr>
													<td><br></td>
													<td></td>
												</tr>
												<tr>
													<td class="col-md-1 col-xs-1 col-sm-1">Category:</td>
													<td class="col-md-11 col-xs-11 col-sm-11">
														<select id="keyword_selector" class="form-control" onchange="window.location.href=this.value;">
															<option selected disabled>Select a category</option>
															<?php $keyword_already_displayed = []; ?>
																@foreach($keywords as $keyword)
																	<?php $keyword_arr = explode(',',$keyword);  ?>
																	@foreach($keyword_arr as $k)

																		@if(in_array(strtolower($k), $keyword_already_displayed))
																			@continue
																		@else
																			<?php $keyword_already_displayed[] = strtolower($k); ?>
																				@if(empty($k) || is_null($k))
																					@continue;
																				@endif
																			<option value="{{route('question.showquestion.get', ['search'=>$k])}}">{{$k}}</option>
																		@endif
																	@endforeach
																@endforeach
														</select>
													</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
							<hr>
							<ul class="list-group">
								<div class="">
									@foreach($questions as $question)
										@if($question->isMadeByTeacher == 0 )
											@continue;
										@endif
										<div class="@if($question->type == 0) MC_block
											@else TF_block
											@endif
											@if($question->difficulty == 0) easy_block
											@elseif($question->difficulty == 1) normal_block @else hard_block
											@endif">
											<!-- blockquote -->
											<blockquote class="blockquote" style="border-color: #990000;">
												<div class="form-group">
													<div class="row">
														<div class="col-md-8 col-xs-12 col-sm-12">
															<h3>
																<a href="{{route('question.detail', ['id' => $question->id])}}" style="word-wrap: break-word;">{{$question->topic}} </a>
															</h3>
														</div>
														<div class="col-md-4">
															<fieldset class="rating pull-right">
																@for($i=5.0;$i>=0.0; $i -= 0.5)
																	@if($i==0)
																		@continue
																	@endif
																	@if(ceil($i) >$i)
																	<input class='' type="radio" id="star{{$i}}half" name="rating" value="{{$i}}" />
																	<label @if((float)$question->rating >= $i) style="color: #FFD700;" @endif class="half" for="star{{$i}}half"></label>
																	@else
																	<input class='' type="radio" id="star{{$i}}" name="rating" value="{{$i}}" />
																	<label @if((float)$question->rating >= $i) style="color: #FFD700;" @endif class="full rating-start" for="star{{$i}}" ></label>
																	@endif
																@endfor
															</fieldset>
														</div>
													</div>
													<div class="row">
														<div class="col-md-8">
															<h6>
																Difficulty:
																<?php
																	switch($question->difficulty) {
																		case 0:echo "Easy";break;
																		case 1:echo "Normal";break;
																		case 2:echo "Hard";break;
																		default: echo "Very Hard";break;
																	}
																?>

																	| {{$question->created_at}} |
																	@if(!empty($comments[$question->id])) {{$comments[$question->id]}} @else 0 @endif  Comments | @if(!empty($attempts[$question->id])) {{$attempts[$question->id]}} @else 0 @endif Answers
																	| Tags : @if(!empty($keywords[$question->id])) @foreach(explode(',',$keywords[$question->id]) as $keyword)
																	<a href="{{route('question.showquestion.get', ['search'=>$keyword])}}" class="badge badge-light">{{$keyword}}</a>
																	@endforeach @endif
															</h6>
															<h6>
																Type:
																<?php
																	switch($question->type) {
																		case 0:echo "Multiple Choice";break;
																		case 1:echo "True/False";break;
																		case 2:echo "Short Question";break;
																		default: echo "";break;
																	}
																?>
															</h6>
														</div>
														<div class="col-md-4 pull-right">
															<div class="pull-right">
																<a href="{{route('question.detail', ['id' => $question->id])}}" role="button" class="btn btn-default btn-lg btn-success">Answer</a>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="text-center">
															@if(!empty($teachersByQuestionId[$question->author_id]))
															<small>created by
																<cite>
																	<a href="#">{{$teachersByQuestionId[$question->author_id]}}</a>
																</cite>
															</small>
															@endif
														</div>
													</div>
												</div>
											</blockquote>
											<hr>
										</div> 
										@endforeach
									</div>
								</ul>
							</div>
						@endif
						
						@if($hasSQ == "True")
							<div class="row">				
								<div class="list-group">
									@if($hasTQ == "True")
										<!-- Has Teacher Question -->
										<h1 class="text-center">Others</h1>
									@else
										<!-- No Teacher Question-->
										<h1 class="text-center">Question</h1>
									@endif
									
									@if($hasTQ != "True")
										<br>
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<button type="button" class="btn btn-default" data-toggle="collapse" data-target="#filter_buttons"><i class="fa fa-filter"></i> Filter </button>

                                                @if($order_popularity_mode == 1)
                                                    <a href="{{route('question.showquestion.popularity',[$order_popularity_mode])}}" class="btn btn-primary"><i class="fa fa-trophy"></i> Popularity&nbsp; <i class="fa fa-caret-down"></i></a>
                                                @elseif($order_popularity_mode == 2)
                                                    <a href="{{route('question.showquestion.popularity',[$order_popularity_mode])}}" class="btn btn-primary"><i class="fa fa-trophy"></i> Popularity&nbsp;&nbsp; <i class="fa fa-caret-up"></i></a>
                                                @else
                                                    <a href="{{route('question.showquestion.popularity',[$order_popularity_mode])}}" class="btn btn-default"><i class="fa fa-trophy"></i> Popularity&nbsp; <i class="fa fa-sort"></i></a>
                                                @endif

												@if($order_date_mode == 1)
													<a href="{{route('question.showquestion.date',[$order_date_mode])}}" class="btn btn-info"><i class="fa fa-calendar"></i> Date&nbsp;&nbsp; <i class="fa fa-caret-down"></i></a>
												@elseif($order_date_mode == 2)
													<a href="{{route('question.showquestion.date',[$order_date_mode])}}" class="btn btn-info"><i class="fa fa-calendar"></i> Date&nbsp;&nbsp; <i class="fa fa-caret-up"></i></a>
												@else
													<a href="{{route('question.showquestion.date',[$order_date_mode])}}" class="btn btn-default"><i class="fa fa-calendar"></i> Date&nbsp;&nbsp; <i class="fa fa-sort"></i></a>
												@endif

												@if($order_rating_mode == 1)
													<a href="{{route('question.showquestion.rating',[$order_rating_mode])}}" class="btn btn-warning"><i class="fa fa-star"></i> Rating&nbsp;&nbsp; <i class="fa fa-caret-down"></i></a>
												@elseif($order_rating_mode == 2)
													<a href="{{route('question.showquestion.rating',[$order_rating_mode])}}" class="btn btn-warning"><i class="fa fa-star"></i> Rating&nbsp;&nbsp; <i class="fa fa-caret-up"></i></a>
												@else
													<a href="{{route('question.showquestion.rating',[$order_rating_mode])}}" class="btn btn-default"><i class="fa fa-star"></i> Rating&nbsp;&nbsp; <i class="fa fa-sort"></i></a>
												@endif

												<div class="pull-right">
													<a href="{{route('question.create')}}" class="btn btn-round btn-success">Create a question</a>
												</div>
												<div class="pull-right">
													<a href="{{route('question.clear')}}" class="btn btn-round btn-dark">Clear Search</a>
												</div>
											</div>
										</div>
										<br>
										<div class="row">
											<div id="filter_buttons" class="collapse col-md-12 col-sm-12 col-xs-12">
												<div class="row">
													<div class="col-md-12 col-sm-12 col-xs-12">
														<table>
															<tr>
																<td class="col-md-1 col-xs-1 col-sm-1">Difficulty:</td>
																<td class="col-md-11 col-xs-11 col-sm-11">
																	<div class="btn-group">
																		<button title="All Difficulty Question" class="btn btn-success filter_link_difficulty d-inline-block bg-info" data-filter="all" type="button">All</button>
																		<button title="Difficulty: Easy" class="btn btn-default filter_link_difficulty d-inline-block bg-info" data-filter="easy" type="button">Easy</button>
																		<button title="Difficulty: Normal" class="btn btn-default filter_link_difficulty d-inline-block bg-info" data-filter="normal" type="button">Normal</button>
																		<button title="Difficulty: Hard" class="btn btn-default filter_link_difficulty d-inline-block bg-info" data-filter="hard" type="button">Hard</button>
																	</div>
																</td>
															</tr>
															<tr>
																<td><br></td>
																<td></td>
															</tr>
															<tr>
																<td class="col-md-1 col-xs-1 col-sm-1">Type:</td>
																<td class="col-md-11 col-xs-11 col-sm-11">
																	<div class="btn-group">
																		<button title="All Type Question" class="btn btn-success filter_link d-inline-block bg-success" data-filter="all" type="button">All</button>
																		<button title="Multiple Choice Question" class="btn btn-default filter_link d-inline-block bg-success" data-filter="mc" type="button">MC</button>
																		<button title="True/False Question" class="btn btn-default filter_link d-inline-block bg-success" data-filter="tf" type="button">TF</button>
																	</div>
																</td>
															</tr>
															<tr>
																<td><br></td>
																<td></td>
															</tr> 
															<tr>
																<td class="col-md-1 col-xs-1 col-sm-1">Category:</td>
																<td class="col-md-11 col-xs-11 col-sm-11">
																	<select id="keyword_selector" class="form-control" onchange="window.location.href=this.value;">
																		<option selected disabled>Select a category</option>
                                                                        <?php $keyword_already_displayed = []; ?>
																		@foreach($keywords as $keyword)
                                                                            <?php $keyword_arr = explode(',',$keyword);  ?>
																			@foreach($keyword_arr as $k)

																				@if(in_array(strtolower($k), $keyword_already_displayed))
																					@continue
																				@else
                                                                                    <?php $keyword_already_displayed[] = strtolower($k); ?>
																					@if(empty($k) || is_null($k))
																						@continue;
																					@endif
																					<option value="{{route('question.showquestion.get', ['search'=>$k])}}">{{$k}}</option>
																				@endif
																			@endforeach
																		@endforeach
																	</select>
																</td>
															</tr>
														</table>
													</div>
												</div>
											</div>

										</div>

									@endif
									<hr>
									<div class="">
										@foreach($questions as $question)
											@if($question->isMadeByTeacher == 1)
												@continue;
											@endif

											@if($question->author_id == Auth::id())
												@continue
											@endif

										<!-- blockquote -->
										<div class="@if($question->type == 0) MC_block
												@else TF_block
												@endif
												@if($question->difficulty == 0) easy_block
												@elseif($question->difficulty == 1) normal_block @else hard_block
												@endif ">
											<blockquote class="blockquote" style="border-color: #26B99A;">
												<div class="form-group">
													<div class="row">
														<div class="col-md-8 col-xs-12 col-sm-12">
															<h3>
																<a href="{{route('question.detail', ['id' => $question->id])}}" class="search_keyword_target" style="word-wrap: break-word;">{{$question->topic}}</a>
															</h3>
														</div>
														<div class="col-md-4">
															<fieldset class="rating pull-right">
																@for($i=5.0;$i>=0.0; $i -= 0.5)
																	@if($i==0)
																		@continue
																	@endif
																	@if(ceil($i) >$i)
																	<input class='' type="radio" id="star{{$i}}half" name="rating" value="{{$i}}" />
																	<label @if((float)$question->rating >= $i) style="color: #FFD700;" @endif class="half" for="star{{$i}}half"></label>
																	@else
																	<input class='' type="radio" id="star{{$i}}" name="rating" value="{{$i}}" />
																	<label @if((float)$question->rating >= $i) style="color: #FFD700;" @endif class="full rating-start" for="star{{$i}}" ></label>
																	@endif
																@endfor
															</fieldset>
														</div>
													</div>
													<div class="row">
														<div class="col-md-8">
															<h6 class="search_keyword_target">
																Difficulty:
																<?php
																	switch($question->difficulty) {
																		case 0:echo "Easy";break;
																		case 1:echo "Normal";break;
																		case 2:echo "Hard";break;
																		default: echo "Very Hard";break;
																	}
																?>
																	| {{$question->created_at}} |
																	@if(!empty($comments[$question->id])) {{$comments[$question->id]}} @else 0 @endif  Comments | @if(!empty($attempts[$question->id])) {{$attempts[$question->id]}} @else 0 @endif Answers
																	| Tags : @if(!empty($keywords[$question->id])) @foreach(explode(',',$keywords[$question->id]) as $keyword)
																	<a href="{{route('question.showquestion.get', ['search'=>$keyword])}}" class="badge badge-light">{{$keyword}}</a>

																	@endforeach @endif
															</h6>
															<h6 class="search_keyword_target">
																Type:
																<?php
																	switch($question->type) {
																		case 0:echo "Multiple Choice";break;
																		case 1:echo "True/False";break;
																		case 2:echo "Short Question";break;
																		default: echo "";break;
																	}
																?>
															</h6>
														</div>
														<div class="col-md-4 pull-right">
															<div class="pull-right search_keyword_target">
																<a href="{{route('question.detail', ['id' => $question->id])}}" role="button" class="btn btn-default btn-lg @if($question->author_id == Auth::id()) btn-secondary">View @else btn-success">Answer @endif
																</a>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
													</div>
												</div>
												@if(!$loop->last)
													
												@endif
											</blockquote>
											<hr> 
										</div>
										@endforeach
									</div>
								</div>
							</div>
						@endif
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>

@endsection

@section('javascript')
	<script>
        // cache collection of elements so only one dom search needed
        var $mcElements = $('.MC_block');
        var $tfElements = $('.TF_block');
        var $easyElements = $('.easy_block');
        var $normalElements = $('.normal_block');
        var $hardElements = $('.hard_block');
		var classname = '';
		
		function setContentHeightShowQuestion() {
			// reset height
			$RIGHT_COL.css('min-height', $(window).height());
		
			var bodyHeight = $BODY.outerHeight(),
				footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
				leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
				contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;
		
			// normalize content
			contentHeight -= $NAV_MENU.height() + footerHeight;
		
			$RIGHT_COL.css('min-height', contentHeight);
		};

		function sessionstorageTypeHandle() {
            if (sessionStorage.getItem("type") !== null) {
                switch(sessionStorage.getItem("type")){
                    case 'all':
                        classname += '';
                        break;
                    case 'mc':
                        classname += '.MC_block';
                        break;
                    default:
                        classname += '.TF_block';
                        break;
                }
            }
		}
		function sessionstorageDifficultyHandle() {
            if (sessionStorage.getItem("difficulty") !== null) {
                switch(sessionStorage.getItem("difficulty")){
					case 'all':
                        classname += '';
					    break;
                    case 'easy':
                        classname += '.easy_block';
                        break;
                    case 'normal':
                        classname += '.normal_block';
                        break;
                    default:
                        classname += '.hard_block';
                        break;
                }
            }
		}

		function allhide() {
		    classname = '';
            $easyElements.hide();
            $normalElements.hide();
            $hardElements.hide();
            $mcElements.hide();
            $tfElements.hide();
		}

		function checkifsession() {

            if (sessionStorage.getItem("difficulty") !== null)
			{
			    return true;
			}

            if (sessionStorage.getItem("type") !== null)
            {
                return true;
            }
		}

        $( document ).ready(function() {

			sessionStorage.clear();

			// After selection category set back selected 
			if ($("#search_keyword_showquestion").val() != "")
			{
				$("#keyword_selector option").filter(function() {
					return $(this).text() == $("#search_keyword_showquestion").val(); 
				}).attr('selected', true);
			}

            // cache collection of elements so only one dom search needed
            var $mcElements = $('.MC_block');
            var $tfElements = $('.TF_block');
            var $easyElements = $('.easy_block');
            var $normalElements = $('.normal_block');
			var $hardElements = $('.hard_block');

            if (checkifsession()) {
                allhide();
                sessionstorageTypeHandle();
                sessionstorageDifficultyHandle();

                if(sessionStorage.getItem("difficulty") === 'all' && sessionStorage.getItem("type") === 'all') {
					classname = '.MC_block,.TF_block,.easy_block,.normal_block,.hard_block';		
				}

				$(classname).show();
			}

            $('.filter_link').click(function(e){
                e.preventDefault();

                $('.filter_link').removeClass('btn-success').addClass('btn-default ');
                $(this).addClass('btn-success').removeClass('btn-default ');

                // get the category from the attribute
                var filterVal = $(this).data('filter');
                sessionStorage.setItem('type',filterVal );

                allhide();
                sessionstorageTypeHandle();
                sessionstorageDifficultyHandle();

                if(sessionStorage.getItem("difficulty") === 'all' && sessionStorage.getItem("type") === 'all') {
                    classname = '.MC_block,.TF_block,.easy_block,.normal_block,.hard_block';
				}

				if(sessionStorage.getItem("difficulty") === 'all' && sessionStorage.getItem("type") === null) {
                    classname = '.MC_block,.TF_block,.easy_block,.normal_block,.hard_block';
				}

                if(sessionStorage.getItem("difficulty") === null && sessionStorage.getItem("type") === 'all') {
                    classname = '.MC_block,.TF_block,.easy_block,.normal_block,.hard_block';
                }
				
				setContentHeightShowQuestion();

                $(classname).show();
            });

            $('.filter_link_difficulty').click(function(e){
                e.preventDefault();

                $('.filter_link_difficulty').removeClass('btn-success').addClass('btn-default ');
                $(this).addClass('btn-success').removeClass('btn-default ');

                // get the category from the attribute
                var filterVal = $(this).data('filter');
                sessionStorage.setItem('difficulty',filterVal );

                allhide();
                sessionstorageTypeHandle();
                sessionstorageDifficultyHandle();

                if(sessionStorage.getItem("difficulty") === 'all' && sessionStorage.getItem("type") === 'all') {
					classname = '.MC_block,.TF_block,.easy_block,.normal_block,.hard_block';
				}

                if(sessionStorage.getItem("difficulty") === 'all' && sessionStorage.getItem("type") === null) {
                    classname = '.MC_block,.TF_block,.easy_block,.normal_block,.hard_block';
                }

                if(sessionStorage.getItem("difficulty") === null && sessionStorage.getItem("type") === 'all') {
                    classname = '.MC_block,.TF_block,.easy_block,.normal_block,.hard_block';
				}
				
				setContentHeightShowQuestion();

                $(classname).show();
            });
        });

	</script>
@endsection
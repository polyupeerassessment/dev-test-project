@extends('layouts.studentdashboard')
<?php use Illuminate\Support\Facades\Input; use Illuminate\Support\Facades\Log;?>

@section('css')
	<link rel="stylesheet" href="{{asset('css/dashboard/green.css')}}" />
	<style>
		strong {
			color:red;
		}
		.fa-close {
			color:orangered;
		}
	</style>
@endsection

@section('content')
	<style>
		#showadvanceoption:hover {
			background-color: lightgrey;
		}
		.form-createquestion-label {
			font-size:120%;
		}
		.choice_createquestion_form {
			height: 70px;
		}
		.white_space_form {
			white-space: pre-line
		}
	</style>

	@if ($errors->any())
		<script type='text/javascript'>
            $(document).ready(function(e) {
                generate_PNotify('Please complete all.');
            });
		</script>
	@endif

	<!-- page content -->
	<div class="right_col" role="main">
		{!! Breadcrumbs::render('question_edit',$question) !!}
		<h3>Edit My Question</h3>
		<div class="clearfix"></div>
		<div class="row">
			{{--My Courses--}}
			<div class="col-md-13 col-sm-13 col-xs-13">
				<div class="x_panel">
					<br />
					<div class="row">
						<div class="col-md-10 col-xs-12 col-md-offset-1">

							{!! Form::open(['route' => 'question.update']) !!}
							<div class="form-group">
								{{Form::label('topic', 'Topic ', array('class'=>'form-createquestion-label'))}}<strong style="font-size: 120%;">*</strong>
								<i class="fa fa fa-question-circle-o" data-toggle="tooltip" data-placement="right" title="Please write the topic or title of your question."></i>
								{{Form::text('topic', $question->topic, array('class' => 'form-control', 'required'))}}
								@if ($errors->has('topic'))
									<span class="help-block">
										<strong>{{ $errors->first('topic') }}</strong>
									</span>
								@endif
							</div>

							<div class="form-group">
								<label class = 'form-createquestion-label'>Keywords</label><strong style="font-size: 120%;">*</strong>
								<i class="fa fa fa-question-circle-o" data-toggle="tooltip" data-placement="right" title="You can add keyword related to the question, for example, the topic covered in this question."></i>
								{{Form::text('tags', $keywords, array('class'=>'tags', 'id'=>'tags_1'))}}
								@if ($errors->has('tags'))
									<span class="help-block">
									<strong>The keyword should fill</strong>
								</span>
								@endif
							</div>

							<div class="form-group">
								{{Form::label('contents', "Contents:" , array('class'=>'form-createquestion-label'))}}<strong style="font-size: 120%;">*</strong>
								<i class="fa fa fa-question-circle-o" data-toggle="tooltip" data-placement="right" title="Please write the explaination or the question that you want people to answer.You can type HTML programming here."></i>
								{{ Form::textarea('contents',$question->contents,array('rows'=>'20','id' => 'content_editor', 'class'=>'form-control', 'placeholder'=>'<h3>Question Here</h3><br><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1200px-PHP-logo.svg.png" /><br><p>What is an ouput of following code?</p><code> echo "Hello World"; </code><br><br><br><p>Please choose an answer!</p>')) }}
								@if ($errors->has('contents'))
									<span class="help-block">
										<strong>{{ $errors->first('contents') }}</strong>
									</span>
								@endif
							</div>

							<input type="button" class="btn btn-default" @if($question->mode == 1) value="HTML" @else value = "Normal" @endif  name="content_type" id="content_type" />
							<button type="button" class="btn btn-dark content_type_1" data-toggle="collapse" data-target="#html_output_div">Content Output</button>

							<div id="html_output_div" class="collapse" style="max-width: 100%;">
								<br>
								<div class="text-center"><i class="fa fa-arrow-down fa-3x" aria-hidden="true"></i></div>
								<br>
								<div class='question_content' id="go" style="min-height:250px; border:1px #ccc solid; padding: 6px 12px 6px 12px;">
								</div>
							</div>

							<div class='content_type_1'>[<a href="https://jsfiddle.net/" style="text-decoration: underline;color:cornflowerblue;" target="_blank" >You Can Also Test Your Code Here</a>]</div><br><br>

							<br />
							<br />

							{{--ALTERNATIVES--}}
							<div class="x_title">
								<h2>
									@if($question->type == 0) 
										MC 
									@else 
										True/False 
									@endif
									<strong style="font-size: 120%;">*</strong>
								</h2>
								<div class="clearfix"></div>
							</div>
							<div class="">
								<br>
								@if($question->type==0)
									@if ($errors->has('answer'))
										<span class="help-block">
											<strong>Answer fields are required</strong>
										</span>
									@endif
									<div id="mc_content">
										<?php $i=1; ?>
										@foreach($choices as $choice)
											<div class="input-group inputmc">
												<div class="input-group-addon">
													<input type="checkbox" name="answer[]" class="flat" value='{{$i++}}' @if($choice->answer==1) checked @endif>
												</div>
												<div class="">
													{{Form::text('choice[]', $choice->choice, array('class' => 'form-control choice_createquestion_form', 'id'=>'mc_question'.$i,'placeholder'=>'Choice', 'required'=>'required'))}}
												</div>
											</div>
										@endforeach
									</div>
									<div class="form-group">
										<button type="button" class="btn btn-link" id="addAnswerMCBtn" onclick="addAnswerMC()" @if(sizeof($choices) == 6) style="display:none;" @endif><i class="fa fa-plus"></i> Add More Answer</button>
										<button type="button" class="btn btn-link" id="removeAnswerMCBtn" onclick="removeAnswerMC()" @if(sizeof($choices) <= 3) style="display:none;" @endif><i class="fa fa-close"></i> Remove More Answer</button>
									</div>
								@else
									@if(!empty($choices[0]))
									<div id="tf_content">
										<div class="row inputtf" style="margin-bottom:5px;">
											<div class="col-md-7">
												<select name="answer" class="form-control">
													@if($choices[0]->answer == 1)
														<option value="1">True</option>
														<option value="0">False</option>
													@else
														<option value="0">False</option>
														<option value="1">True</option>
													@endif
												</select>
											</div>
										</div>
									</div>
									@else
										<p>Sorry We cannot find your choice</p>
									@endif
								@endif
							</div>
							<br />
							<hr>
							<br><br />
							<div class="accordion" id="accordionAdvancedOption" role="tablist" aria-multiselectable="true">
								<div class="panel">
									<a class="panel-heading collapsed" role="tab" data-toggle="collapse" data-parent="#accordionAdvancedOption" href="#collapseAdvancedOption"
									   aria-expanded="true" aria-controls="collapseAdvancedOption">
										<h3 class="panel-title">Advanced Option</h3>
									</a>
									<div id="collapseAdvancedOption" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="AdvancedOption">
										<div class="panel-body">
											<div id="advancedoption_contents">
												<div class="form-group">
													<div class="row">
														<div class="col-md-12 col-sm-12 col-xs-12">
															{{Form::label('number_attempts', 'Number Of Attempts: ')}}
															<i class="fa fa fa-question-circle-o" data-toggle="tooltip" data-placement="right" title="The number of attempt that a student can try to answer the question."></i>
															@if($question->type == 0)
																{{Form::number('number_attempts', $question->num_attempts, array('class' => 'form-control', 'min'=>2, 'max'=>10))}}
															@else
																{{Form::number('number_attempts', 1, array('class' => 'form-control', 'min'=>1, 'max'=>1))}}
															@endif
														</div>
													</div>
												</div>
												<br>
												<div class="form-group">
													<div class="row">
														<div class="col-md-12 col-sm-12 col-xs-12">
															{{Form::label('tips', 'Tips ')}}
															<i class="fa fa fa-question-circle-o" data-toggle="tooltip" data-placement="right" title="When the student wrongly answer the question, tips will be displayed as hints for them to attempt the question again. Example of hints: The related topics covered in the question."></i>
															{{Form::textarea('tips', $question->tips, array('class' => 'form-control','rows'=>4))}}
														</div>
													</div>
												</div>
												<br>
												<div class="form-group">
													<div class="row">
														<div class="col-md-12 col-sm-12 col-xs-12">
															{{Form::label('explaination', 'Explanation ')}}
															<i class="fa fa fa-question-circle-o" data-toggle="tooltip" data-placement="right" title="Explain about the correct answer(s) once user answered this question"></i>
															{{Form::textarea('explaination', $question->explaination, array('class' => 'form-control','rows'=>4))}}
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<br />
							<br />
							<br />
							<div class="form-group">
								<div id="choice_type">{{Form::hidden('choice_type', $question->type) }}</div>
								@if($question->mode == 1)
									<div id="content_type_hidden_value">{{Form::hidden('content_type', 1) }}</div>
								@else
									<div id="content_type_hidden_value">{{Form::hidden('content_type', 0) }}</div>
								@endif

								{{Form::hidden('question_id', $question->id) }}
								{{ Form::submit('Update', array('class'=>'btn btn-success btn-md btn-black'))}}
								{{ Form::reset('Reset',array('class'=>'btn btn-md btn-default'))}}
							</div>
							<br>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('javascript')
	<script>
        function validateContents(contents) {
            contents = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('validateQuestionContents')}}",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                data: {'contents':contents },
                success:function(response){
                    $('#go').html(response);
                }
            });
        }

        $(document).ready(function() {
			
            @if($question->mode == 0)
            	$('#go').addClass('white_space_form');
			@endif
		  
			$("#content_type").click(function(){

				if ($(this).val() == 'Normal') {
					$(this).val('HTML');
					if ($('#content_editor').val() == '') {
						$('#go').html($('#content_editor').attr('placeholder'));
					} else {
						$('#go').html(validateContents($('#content_editor').val()));
					}

                    $('#go').removeClass('white_space_form');
                    document.getElementById("content_type_hidden_value").innerHTML = '{{Form::hidden('content_type', 1) }}';
					
				} else {
					$(this).val('Normal');
					if ($('#content_editor').val() == '') {
						$('#go').text($('#content_editor').attr('placeholder'));
					} else {
						$('#go').text($('#content_editor').val());
					}
                    $('#go').addClass('white_space_form');
                    document.getElementById("content_type_hidden_value").innerHTML = '{{Form::hidden('content_type', 0) }}';
				}
			});

            if($('#content_editor').val()) {
				@if($question->mode==1)
                	$('#go').html(validateContents($('#content_editor').val()));
				@else
                	$('#go').text($('#content_editor').val());
				@endif

            } else {
                $('#go').html($('#content_editor').attr('placeholder'));
			}
			
			$('#myNicEditor_formgroup').hide();
			
            $('#content_editor').keyup(function() {
				if($('#content_type').val() =='Normal') {
					$('#go').text($(this).val());
				} else {
                    validateContents($('#content_editor').val());
				}
            });

            $("select#content_type").change(function(){
                if ($(this).val() === 'normal') {
                    $('#myNicEditor_formgroup').show();
                    $('#content_editor').hide();
                    $('#go').hide();
                } else {
                    $('#myNicEditor_formgroup').hide();
                    $('#content_editor').show();
                    $('#go').show();
                }
            });

            $('#mc_tab').click(function(){
                document.getElementById("choice_type").innerHTML = '{{Form::hidden('choice_type', 0) }}';
            });

            $('#tf_tab').click(function(){
                document.getElementById("choice_type").innerHTML = '{{Form::hidden('choice_type', 1) }}';
            });

        });
	</script>
	<script src="{{ asset('js/dashboard/icheck.min.js') }}"></script>
	<script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
	<script type="text/javascript">

        function addAnswerMC() {
            var numItems = $('.inputmc').length;
            if(numItems <6)
            {
                var div = document.createElement('div');
                div.className = '';
                div.innerHTML =
                    '<div class="input-group inputmc">\
                        <div class="input-group-addon">\
                            <input type="checkbox" name="answer[]" class="flat" value="'+(numItems+1)+'">\
                        </div>\
                        <div class="">\
                            <input name="choice[]" class="form-control choice_createquestion_form" id="mc_question' + (numItems + 1 ) + '"placeholder="Choice" type="text" required>\
						</div>\
					</div>';

                document.getElementById('mc_content').appendChild(div);
                $('input.flat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });
            }

            if(numItems + 1 > 3){
                $("#removeAnswerMCBtn").show();
            }

            if(numItems + 1 == 6){
                $("#addAnswerMCBtn").hide();
            }
        }

        function removeAnswerMC() {
            var numItems = $('.inputmc').length;
            if(numItems > 3) {
                var i = 1;
                $( ".inputmc" ).last().remove();
                $('.inputmc').each(function(){
                    $(this).find('input:checkbox').val(i);
                    i++;
                });
                $("#addAnswerMCBtn").show();
            }

            if(numItems - 1 <= 3) {
                $("#removeAnswerMCBtn").hide();
            }
        }

        function addAnswerTF(){
            var numItems = $('.inputtf').length;
            if(numItems < 3)
            {
                var div = document.createElement('div');
                div.className = '';
                div.innerHTML =
                    '<div class="row inputtf" style="margin-bottom:5px;">\
                        <div class="col-md-6">\
                            <input name="choice[]" class="form-control" id="tf_question' + (numItems + 1 ) + '" placeholder="Question" type="text" required>\
						</div>\
						<div class="col-md-6">\
							<select name="answer[]" class="form-control">\
								<option value="0">False</option>\
								<option value="1">True</option>\
							</select>\
						</div>\
					</div>';

                document.getElementById('tf_content').appendChild(div);
            }

            if(numItems + 1 > 1){
                $("#removeAnswerTFBtn").show();
            }

            if(numItems + 1 == 3){
                $("#addAnswerTFBtn").hide();
            }
        }

        function removeAnswerTF(){
            var numItems = $('.inputtf').length;
            if(numItems > 1)
            {
                $( ".inputtf" ).last().remove();
                $("#addAnswerTFBtn").show();
            }

            if(numItems - 1 <= 1) {
                $("#removeAnswerTFBtn").hide();
            }
        }
	</script>


@endsection
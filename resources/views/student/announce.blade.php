@extends('layouts.studentdashboard') 

@section('content')

<!-- page content -->
<div class="right_col" role="main">

	{{--Notification contents--}}
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="page-title">
				<div class="title_left">
					<h3>Notification</h3>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<ul class="list-group">		
				@forelse($announcements as $announcement)
					@if($announcement->data['type'] == 1)
						{{--THIS IS ANNOUNCEMENT--}}
						@continue;
					@endif
					
					@if($announcement->unread())
					<?php $announcement->markAsRead(); ?>
					<li class="list-group-item" style="background-color:#fbffda">
					@else
					<li class="list-group-item">
					@endif
						<h4>{{$announcement->data['content']['topic']}}</h4>
						<hr>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-md-9 col-sm-9 col-xs-12">
										<p>{{$announcement->data['content']['content']}}</p>
										@if(!empty($announcement->data['content']['comment']))
											<p>{{$announcement->data['content']['comment']}}</p> <br>
										@endif
									</div>
									<div class="col-md-3 col-sm-3 col-xs-12">
										<p>Course: {{$announcement->data['content']['course_id']}}</p>
										<p>Date:  {{$announcement->data['repliedTime']['date']}}</p>	
									</div>
								</div>
								@if(!empty($announcement->data['content']['question_id']))
									<a class="btn btn-default" href="{{route('question.detail',['id'=>$announcement->data['content']['question_id']])}}"> View My Question </a>
								@else
									<a class="btn btn-default" href="{{ route('question.my_withSession',['sessionID'=>'COMP1111']) }}"> Detail</a>
								@endif

							</div>
						</div>
					</li>
				@empty
					<li class="list-group-item">
						<p>No Notification Found</p>
					</li>
				@endforelse					
			</ul>
		</div>
	</div>

	{{--Announcement contents--}}
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="page-title">
				<div class="title_left">
					<h3>Announcement</h3>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<ul class="list-group">
				@forelse($announcements as $announcement)
					@if($announcement->data['type'] == 0)
						{{--THIS IS NOTIFICATION--}}
						@continue;
					@endif

					@if($announcement->unread())
					<?php $announcement->markAsRead(); ?>
					<li class="list-group-item" style="background-color:#fbffda">
					@else
					<li class="list-group-item">
					@endif
						<h4>{{$announcement->data['content']['topic']}}</h4>
						<hr>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-md-9 col-sm-9 col-xs-12">
										<p>{{$announcement->data['content']['content']}}</p>
									</div>
									<div class="col-md-3 col-sm-3 col-xs-12">
										<p>Course: {{$announcement->data['content']['course_id']}}</p>
										<p>Date:  {{$announcement->data['repliedTime']['date']}}</p>	
									</div>
								</div>
							</div>
						</div>
					</li>
				@empty
					<li class="list-group-item">
						<p>No Announcement Found</p>
					</li>					
				@endforelse	
			</ul>
		</div>
	</div>

</div>

@endsection
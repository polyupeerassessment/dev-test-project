@extends('layouts.studentdashboard') 

@section('content')

<style>
	.clickable-row {
		cursor: pointer;
	}

	.aligned-row {
		display: flex;
		/*flex-flow: row wrap;*/
		padding:0px;
		margin:0px;
	}

	.white_space_form {
		white-space: pre-line
	}

	table {border-collapse:collapse; table-layout:fixed;}
	table td { word-wrap:break-word;}
</style>

<!-- page content -->
<div class="right_col" role="main">
	<div class="page-title">
		<div class="title_left">
			{{--<h3>Welcome!</h3>--}}
			{{--<p>{!! Breadcrumbs::render('student_home') !!}</p>--}}
		</div>
	</div>
    {{--  <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif @if(session('fail'))
            <div class="alert alert-danger">
                {{ session('fail') }}
            </div>
            @elseif ($enrolled == false)
            <div class="alert alert-warning">
                Please enroll course or ask your teacher to add your ID into the system
            </div>
            @endif
        </div>
    </div>
	<div class="clearfix"></div>  --}}

	<div class="row">
		<div class="col-xs-12 col-md-7">
			<div class="x_panel" >
				<div class="x_title">
					<h2>Course Information</h2>
					<div class="nav navbar-right panel_toolbox">
						<a href="{{ route('student.enroll') }}" class="btn btn-success" role="button">Enroll New Course</a>
					</div>
					<div class="clearfix"></div>
				</div>
				<br>
				<div class="x_content">
					<div class="row">

						@if($enrolled == true)
							<table  id="" class="table table-hover dt-responsive wrap" >
								<thead>
									<tr>
										<th>Course Code</th>
										<th>Course Name</th>
										<th>Instructor</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php foreach($courses as $course) { ?>
									<tr class='clickable-row' data-href="{{route('student.dashboard', ['courseID' => $course->course_code])}}">
									<?php
                                        foreach (array_keys((array)$courses[0]) as $key) {
                    		                echo "<td>".$course->$key."</td>";
										}
										echo "<td>"."<button type='button' style='width:100%' class='btn btn-dark btn-md pull-right'>Entry</button>"."</td>";
                                    ?>
									</tr>
								<?php } ?>
								</tbody>
							</table>
						@endif

					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-5">
			<div class="">
				@if ($enrolled == true)
					<div class="">
						<div class="x_panel">
							<h2>Announcement</h2>
							<div class="x_title">
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<ul class="to_do">
									<?php $is_announce_empty = true; $is_notify_empty = true; $count_announce = 0; $max_announce = 5;?>
									@foreach(auth()->user()->unreadNotifications as $notification)
										@if($notification->data['type'] == 1)
											<?php $count_announce++; $is_announce_empty=false ?>
											@if($count_announce>=$max_announce)
												<li>
													<div class="text-center">
														<a href="{{ route('student.announcement') }}">
															<strong>See More</strong>
															<i class="fa fa-angle-right"></i>
														</a>
													</div>
												</li>
												@break
											@endif
												<li>
													<a href="{{route('student.announcement')}}"><strong>{{$notification->data['content']['topic']}}</strong> :
														<small>{{date('Y/m/d H:m',strtotime($notification->data['repliedTime']['date']))}}</small>
													</a>
												</li>
										@endif
									@endforeach
									@if($is_announce_empty)
										<p>There is no new announcement</p>
									@endif
								</ul>
							</div>
						</div>
					</div>
				@endif
			</div>
			<div class="">
				<div class="x_panel">
					<h2>Notification</h2>
					<div class="x_title">
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="">
							<ul class="to_do">
								<?php $is_notify_empty = true; $count_notification = 0; $max_notification = 5; ?>
								@foreach(auth()->user()->unreadNotifications as $notification)
									@if($notification->data['type'] == 0)
										<?php $is_notify_empty = false; $count_notification+=1; ?>
										@if($count_notification>=$max_notification)
											<li>
												<div class="text-center">
													<a href="{{ route('student.announcement') }}">
														<strong>See More</strong>
														<i class="fa fa-angle-right"></i>
													</a>
												</div>
											</li>
											@break
										@endif
										<li>
										<a href="{{route('question.my_withSession',['sessionID'=>$notification->data['content']['course_id']])}}">{{$notification->data['content']['topic']}}:
											<small>{{date('Y/m/d H:m',strtotime($notification->data['repliedTime']['date']))}}</small>
										</a>
										</li>
									@endif
								@endforeach

								@if($is_notify_empty)
									<p>There is no new notification</p>
								@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    @if(!empty($sysannounce))
        @foreach($sysannounce as $announce)
            <div class="row "  style="margin-top: 4%; ">
                <div class="col-xs-12 col-md-12" >
                    <div class="x_panel " style="border:dashed 1px  mediumvioletred">
                        <div class="x_content">
                            <div class="text-center white_space_form">
                                <i class="fa fa-exclamation-triangle fa-4x" aria-hidden="true" style="color:orangered"></i>
                                <h3>{{$announce->topic}}</h3>
                                <p>{{$announce->contents}}</p>
                                <p style="font-size: 85%;">This announcement is displayed during <br>{{date('Y:m:d', strtotime($announce->start_at))}} ~ {{date('Y:m:d', strtotime($announce->end_at))}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif

	@if(config('app.env') != 'production')
	<div class="row" id="cute_animation">
		<div class="container">
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
		</div>
	</div>
	@endif
</div>
<style>

	#cute_animation .container {
		width: 200px;
		height: 100px;
		padding-top: 100px;
		margin: 0 auto;
	}

	#cute_animation .ball {
		width: 10px;
		height: 10px;
		margin: 10px auto;
		border-radius: 50px;
	}

	#cute_animation .ball:nth-child(1) {
		background: navy;
		-webkit-animation: right 1s infinite ease-in-out;
		-moz-animation: right 1s infinite ease-in-out;
		animation: right 1s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(2) {
		background: navy;
		-webkit-animation: left 1.1s infinite ease-in-out;
		-moz-animation: left 1.1s infinite ease-in-out;
		animation: left 1.1s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(3) {
		background: navy;
		-webkit-animation: right 1.05s infinite ease-in-out;
		-moz-animation: right 1.05s infinite ease-in-out;
		animation: right 1.05s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(4) {
		background: navy;
		-webkit-animation: left 1.15s infinite ease-in-out;
		-moz-animation: left 1.15s infinite ease-in-out;
		animation: left 1.15s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(5) {
		background: navy;
		-webkit-animation: right 1.1s infinite ease-in-out;
		-moz-animation: right 1.1s infinite ease-in-out;
		animation: right 1.1s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(6) {
		background: navy;
		-webkit-animation: left 1.05s infinite ease-in-out;
		-moz-animation: left 1.05s infinite ease-in-out;
		animation: left 1.05s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(7) {
		background: navy;
		-webkit-animation: right 1s infinite ease-in-out;
		-moz-animation: right 1s infinite ease-in-out;
		animation: right 1s infinite ease-in-out;
	}

	@-webkit-keyframes right {
		0% {
			-webkit-transform: translate(-15px);
		}
		50% {
			-webkit-transform: translate(15px);
		}
		100% {
			-webkit-transform: translate(-15px);
		}
	}

	@-webkit-keyframes left {
		0% {
			-webkit-transform: translate(15px);
		}
		50% {
			-webkit-transform: translate(-15px);
		}
		100% {
			-webkit-transform: translate(15px);
		}
	}

	@-moz-keyframes right {
		0% {
			-moz-transform: translate(-15px);
		}
		50% {
			-moz-transform: translate(15px);
		}
		100% {
			-moz-transform: translate(-15px);
		}
	}

	@-moz-keyframes left {
		0% {
			-moz-transform: translate(15px);
		}
		50% {
			-moz-transform: translate(-15px);
		}
		100% {
			-moz-transform: translate(15px);
		}
	}

	@keyframes right {
		0% {
			transform: translate(-15px);
		}
		50% {
			transform: translate(15px);
		}
		100% {
			transform: translate(-15px);
		}
	}

	@keyframes left {
		0% {
			transform: translate(15px);
		}
		50% {
			transform: translate(-15px);
		}
		100% {
			transform: translate(15px);
		}
	}
</style>

<script>
	$(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });

</script>

@endsection
@extends('layouts.studentdashboard') 

@section('css')
<link rel="stylesheet" href="{{asset('css/dashboard/green.css')}}" />
@endsection

@section('content')
<style>
    #answer_box_button:hover {
        border:solid 1px lightseagreen;
    }
    #create_box_button:hover {
        border:solid 1px orangered;
    }
    #my_box_button:hover {
        border:solid 1px yellowgreen;
    }
    #my_answer_box_button:hover {
        border:solid 1px navy;
    }
    #bubbles {
        /*position: relative;*/
        width: 100%;
        height: 500px;
        /*top: 0;*/
        /*left: 0;*/
    }
    .modal {
        text-align:center;
        width:100%;
        height:100%;
        margin:0 auto;
    }
    .cover-container {
        height: 190px;
        width: 100%;
        white-space: nowrap;
        overflow-x: auto;
        overflow-y: hidden;

    }
    .cover-item {
        position: relative;
        display: inline-block;
        margin: 8px 8px;
        box-shadow: 2px 2px 4px #bbb;
        border-top-right-radius: 4px;
        width: 340px;
        height: 158px;
        vertical-align: bottom;
        background-position: top left;
        background-repeat: no-repeat;
        background-size: cover;
    }

    .rating>input {
		display: none;
	}

	.rating>label:before {
		margin: 5px;
		font-size: 1.25em;
		font-family: FontAwesome;
		display: inline-block;
		content: "\f005";
	}

	.rating>.half:before {
		content: "\f089";
		position: absolute;
    }

	.rating>label {
		color: #ddd;
		float: right;
	}

    /* width */
    ::-webkit-scrollbar {
        width: 3px;
        height:1px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        box-shadow: inset 0 0 5px grey;
        border-radius: 20px;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        /*background: black;*/
        border-radius: 20px;
        border:1px grey solid;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
        background: darkgrey;
        width: 3px;
    }
    
</style>
<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="dashboard_graph">
				<div class="row x_title">
					<div class="col-md-6">
						<h3>{{$courseInformation->course_name}}</h3>
                        <p>{!! Breadcrumbs::render('student_dashboard') !!}</p>
					</div>
                    <ul class="nav navbar-right panel_toolbox">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" style="right:0; left:auto;" role="menu">
                                <li>&nbsp<input id="check-answer-question" type="checkbox" onclick="checkedBox('answer-question')"> Answer Questions</li>
                                <li>&nbsp<input id="check-create-question" type="checkbox" onclick="checkedBox('create-question')"> Create Questions</li>
                                <li>&nbsp<input id="check-show-created" type="checkbox" onclick="checkedBox('show-created')"> My Questions</li>
                                <li>&nbsp<input id="check-show-answered" type="checkbox" onclick="checkedBox('show-answered')"> My Answers</li>
                                <li>&nbsp<input id="check-forum" type="checkbox" onclick="checkedBox('forum')"> Forum</li>
                                <li>&nbsp<input id="check-exercise" type="checkbox" onclick="checkedBox('exercise')"> Exercise</li>
                                <li>&nbsp<input id="check-quiz" type="checkbox" onclick="checkedBox('quiz')"> Quiz</li>
                            </ul>
                        </li>
                    </ul>
				</div>
				<div class="row top_tiles">
                    <div id="box-start"></div>
                    <div id="box-answer-question">
                        <a class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12" href="{{route('question.showquestion')}}">
                            <div class="tile-stats" id= 'answer_box_button'>
                                <div class="icon"></div>
                                <div class="count">Answer<i class="fa fa-paper-plane pull-right" style="color:lightseagreen;font-size:60px; margin:10px;"></i></div>
                                <p style="margin-left:2%;">Answer your questions here!</p>
                            </div>
                         </a>
                    </div>               
                    <div id="box-create-question">
                        <a class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12" href="{{route('question.create')}}">
                            <div class="tile-stats" id="create_box_button">
                                <div class="icon"></div>
                                <div class="count">Create<i class="fa fa-plus pull-right" style="color:palevioletred;font-size:60px; margin:10px;"></i></div>
                                <p style="margin-left:2%;">Create your own questions!</p>
                            </div>
                        </a>
                    </div>   
                    <div id="box-show-created">
                        <a class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12" href="{{ route('question.my') }}">
                            <div class="tile-stats" id="my_box_button">
                                <div class="icon"></div>
                                <div class="count">My Questions<i class="fa fa-question-circle-o pull-right" style="color:yellowgreen; font-size:60px; margin:10px;"></i></div>
                                <p style="margin-left:2%;">Show all the questions that you created here!</p>
                            </div>
                        </a>
                    </div>      
                    <div id="box-show-answered">
                        <a class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12" href="{{route('question.answered')}}">
                            <div class="tile-stats" id="my_answer_box_button">
                                <div class="icon"></div>
                                <div class="count">My Answers<i class="fa fa-commenting-o pull-right" style="color:darkslategrey; font-size:60px;margin:10px;"></i></div>
                                <p style="margin-left:2%;">Show all the questions that you answered here!</p>
                            </div>
                        </a>
                    </div>
                    <div id="box-forum" style="display:none">
                        <a class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12" href="{{route('discussion.index')}}">
                            <div class="tile-stats" id="forum_box_button">
                                <div class="icon"></div>
                                <div class="count">Forum<i class="fa fa-quote-right pull-right" style="color:#DCDCDC; font-size:60px;margin:10px;"></i></div>
                                <p style="margin-left:2%;">Discuss questions with your peers here!</p>
                            </div>
                        </a>
                    </div>
                    <div id="box-exercise" style="display:none">
                        <a class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12" href="{{route('question.exercise')}}">
                            <div class="tile-stats" id="exercise_box_button">
                                <div class="icon"></div>
                                <div class="count">Exercise<i class="fa fa-pencil pull-right" style="color:#DC143C; font-size:60px;margin:10px;"></i></div>
                                <p style="margin-left:2%;">Complete an exercise with customized questions for you!</p>
                            </div>
                        </a>
                    </div>                     
                    <div id="box-quiz" style="display:none">
                         <a class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12" href="{{route('question.quiz')}}">
                            <div class="tile-stats" id="quiz_box_button">
                                <div class="icon"></div>
                                <div class="count">Quiz<i class="fa fa-graduation-cap pull-right" style="color:#483D8B; font-size:60px;margin:10px;"></i></div>
                                <p style="margin-left:2%;">Complete the assigned quiz assignment here!</p>
                            </div>
                        </a>
                    </div>
                    <div id="box-last"></div>
                </div>
			</div>
		</div>
	</div>
	<br />
	<div class="row">
		<!-- line graph -->
        <div class="col-md-8 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2 id="graph_title">Timeline</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a id = 'previous_week' href="#">Previous Week</a></li>
						<li><a id = 'next_week' href="#">Next Week</a></li>
						<li><a id = 'reset_week' href="#">Current</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							<ul class="dropdown-menu" style="right:0; left:auto;" role="menu">
								<li><a id = 'overall_statistic' href="#">Overall</a></li>
								<li><a id = 'answerQ_statistic' href="#">Answer Questions</a></li>
								<li><a id = 'createQ_statistic' href="#">Create Questions</a></li>
								<li><a id = 'peersAnswered_statistic' href="#">Peers Answered Your Questions</a></li>
							</ul>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content2" style="width:100%;min-height:330px;">
					<div id="graph_line" style="width:100%; height:300px;"></div>
				</div>
			</div>
		</div>
		<!-- /line graph -->
        <div class="col-md-4 col-xs-12" >
            <div class="x_panel" style="width:100%;min-height:400px;">
                <div class="x_title">
                    <h2 id="graph_title">Hot Topic <small><i class="fa fa-fire fa-2x" style="color:orangered; "></i></small></h2>

                    <button type="button" class="pull-right btn btn-primary btn-md" id="see_more_hot_topic_btn" data-toggle="modal" data-target="#see_more_hot_topic">See More</button>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                    <ul class="list-unstyled scroll-view" id="hot_topic_list">
                        <?php $count_hot_topc = 0; ?>
                        @foreach($hot_topics as $topic=>$arr)
                            @if ($count_hot_topc++ >= 4)
                                @break
                            @endif

                            <li class="media event">
                                <a class="pull-left border-red profile_thumb">
                                    @if($arr[1] ==0)
                                        &nbsp;<i class="fa fa-angle-down" style="color:blue; "></i>
                                    @elseif( $arr[1] == 1)
                                        &nbsp;&nbsp;<i class="fa fa-angle-right" style="color:green;"></i>
                                    @else
                                        &nbsp;<i class="fa fa-angle-double-up" style="color:orangered; "></i>
                                    @endif
                                </a>
                                <div class="media-body">
                                    <a class="title" href="{{route('question.showquestion.get', ['search'=>$topic])}}"><p style="margin-top: 1%;">{{$topic}}</p></a>
                                    <p><strong>{{round($arr[0] / $total_num, 2) * 100}}%</strong></p>
                                    <p>
                                        <small>
                                            @if($arr[1] ==0)
                                                Decreasing
                                            @elseif( $arr[1] == 1)
                                                Constant
                                            @else
                                                Increasing
                                            @endif
                                        </small>
                                    </p>
                                </div>
                            </li>

                        @endforeach
                    </ul>

                    <div class="modal fade" id="see_more_hot_topic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <p class="modal-title" id="exampleModalLabel">Click to jump to question list</p>
                                    <small style="font-size:70%;">We have removed long keywords for better performance. Thank you for your understanding.</small>
                                </div>
                                <div class="modal-body">
                                    <div id="bubbles"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
	</div>

    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <small style="margin-left:1%;">Recommendation</small>
            <div class="cover-container">
                @foreach ($recommend_questions as $recommend_question)
                <div class="cover-item">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <a href="{{route('question.detail', ['id' => $recommend_question->id])}}" style='text-overflow:ellipsis;display: block;white-space: nowrap;overflow: hidden;'>{{ $recommend_question->topic }} </a>
                        </div>                    
                        <div class="panel-body" style="height:119px;">
                            <a href="{{route('question.detail', ['id' => $recommend_question->id])}}" style='text-overflow:ellipsis;display: block;white-space: nowrap;overflow: hidden;' >
                                @if ($recommend_question->mode == "0")
                                    {{ $recommend_question->contents }}
                                @else
                                    HTML content
                                @endif
                            </a>
                            <br>
                            <div>
                                <fieldset class="rating pull-right">
                                    @for($i=5.0;$i>=0.0; $i -= 0.5)
                                        @if($i==0)
                                            @continue
                                        @endif
                                        @if(ceil($i) >$i)
                                            <input type="radio" id="star{{$i}}half" name="rating" value="{{$i}}" />
                                            <label @if((float)$recommend_question->rating >= $i) style="color: #FFD700;" @endif class="half" for="star{{$i}}half"></label>
                                        @else
                                            <input type="radio" id="star{{$i}}" name="rating" value="{{$i}}" />
                                            <label @if((float)$recommend_question->rating >= $i) style="color: #FFD700;" @endif class="full rating-start" for="star{{$i}}" ></label>
                                        @endif
                                    @endfor
                                </fieldset>
                            </div>
                            <p>Type: @if($recommend_question->type == '0')MC @else True/False @endif</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel" style="width:100%">
                <div class="x_title">
                    <h2>Your Performance</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row" style=" padding-bottom: 5px; margin-bottom: 5px;">
                        <div class="" style="text-align: center;">
                                <div class="col-md-3 col-xs-6">
                                    <div style="text-align: center; margin-bottom: 17px">
                                        {{--<span class="chart" data-percent="{{$user_performance['rank_scores'][0]}}">--}}
                                            {{--<span class="percent"></span>--}}
                                        {{--</span>--}}
                                        @if( $user_performance['rank_scores'][0] ==1 )
                                            <img src="https://png.icons8.com/color/96/000000/medal.png">
                                        @elseif ($user_performance['rank_scores'][0] ==2)
                                            <img src="https://png.icons8.com/office/80/000000/olympic-medal-silver.png">
                                        @elseif ($user_performance['rank_scores'][0] ==3)
                                            <img src="https://png.icons8.com/office/80/000000/olympic-medal-bronze.png">
                                        @else
                                            <img src="https://png.icons8.com/dotty/80/000000/laurel-wreath.png">
                                        @endif
                                        <h4>Total Scores</h4>
                                        <p>Yours: {{$user_performance['rank_scores'][1]}}</p>
                                        <p>
                                            Rank: {{$user_performance['rank_scores'][0]}}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <div style="text-align: center; margin-bottom: 17px">
                                        {{--<span class="chart" data-percent="{{$user_performance['rank_num_questions'][0]}}">--}}
                                            {{--<span class="percent"></span>--}}
                                        {{--</span>--}}
                                        @if( $user_performance['rank_num_questions'][0] ==1 )
                                            <img src="https://png.icons8.com/color/96/000000/medal.png">
                                        @elseif ($user_performance['rank_num_questions'][0] ==2)
                                            <img src="https://png.icons8.com/office/80/000000/olympic-medal-silver.png">
                                        @elseif ($user_performance['rank_num_questions'][0] ==3)
                                            <img src="https://png.icons8.com/office/80/000000/olympic-medal-bronze.png">
                                        @else
                                            <img src="https://png.icons8.com/dotty/80/000000/laurel-wreath.png">
                                        @endif
                                        <h4>No. Questions</h4>
                                        <p>Yours: {{$user_performance['rank_num_questions'][1]}}</p>
                                        <p>
                                            Rank: {{$user_performance['rank_num_questions'][0]}}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <div style="text-align: center; margin-bottom: 17px">
                                        {{--<span class="chart" data-percent="{{$user_performance['rank_answers'][0]}}">--}}
                                            {{--<span class="percent"></span>--}}
                                        {{--</span>--}}
                                        @if( $user_performance['rank_answers'][0] ==1 )
                                            <img src="https://png.icons8.com/color/96/000000/medal.png">
                                        @elseif ($user_performance['rank_answers'][0] ==2)
                                            <img src="https://png.icons8.com/office/80/000000/olympic-medal-silver.png">
                                        @elseif ($user_performance['rank_answers'][0] ==3)
                                            <img src="https://png.icons8.com/office/80/000000/olympic-medal-bronze.png">
                                        @else
                                            <img src="https://png.icons8.com/dotty/80/000000/laurel-wreath.png">
                                        @endif
                                        <h4>No. Answers</h4>
                                        <p>Yours: {{$user_performance['rank_answers'][1]}}</p>
                                        <p>
                                            Rank: {{$user_performance['rank_answers'][0]}}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <div style="text-align: center; margin-bottom: 17px">
                                        {{--<span class="chart" data-percent="{{$user_performance['rank_rating'][0]}}">--}}
                                            {{--<span class="percent"></span>--}}
                                        {{--</span>--}}
                                        @if( $user_performance['rank_rating'][0] ==1 )
                                            <img src="https://png.icons8.com/color/96/000000/medal.png">
                                        @elseif ($user_performance['rank_rating'][0] ==2)
                                            <img src="https://png.icons8.com/office/80/000000/olympic-medal-silver.png">
                                        @elseif ($user_performance['rank_rating'][0] ==3)
                                            <img src="https://png.icons8.com/office/80/000000/olympic-medal-bronze.png">
                                        @else
                                            <img src="https://png.icons8.com/dotty/80/000000/laurel-wreath.png">
                                        @endif
                                        <h4>Average Rating</h4>
                                        <p>
                                            @if($user_performance['rank_rating'][1] == -1)
                                                No Questions Yet
                                            @else
                                                Yours: <?php echo round(floatval($user_performance['rank_rating'][1]), 1, PHP_ROUND_HALF_UP) ?>/5
                                            @endif
                                        </p>
                                        <p>
                                            Rank: {{$user_performance['rank_rating'][0]}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Top 5 Highest Scores
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(!empty($top5_ranking))
                        <?php $previous_num = -1;  $rank_scores = 0; $sp = 'st'; ?>
                        @for ($i = 0; $i < count($top5_ranking); $i++)
                            <?php
                                if ($top5_ranking[$i]->Score != $previous_num) {
                                    $previous_num = $top5_ranking[$i]->Score;
                                    $rank_scores++;
                                }

                                if ($rank_scores == 1) {
                                    $color = 'background-color:#f6df78;';
                                    $sp = 'st';
                                } elseif ($rank_scores == 2) {
                                    $color = 'background-color:#d8d7de;';
                                    $sp = 'nd';
                                } elseif ($rank_scores == 3) {
                                    $color = 'background-color:#c2937f;';
                                    $sp = 'rd';
                                } else {
                                    $color = 'background-color:#eae7e3;';
                                    $sp = 'th';
                                }

                            ?>
                            <article class="media event">
                                <a class="pull-left date" style="{{$color}}">
                                    <p class="day">{{$rank_scores}}<sup>{{$sp}}</sup></p>
                                </a>
                                <div class="media-body">
                                    <a ><p style="margin-top: 5%;">{{$top5_ranking[$i]->Score}}</p></a>
                                </div>
                            </article>

                        @endfor
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-3 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Top 5 Question Created</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(!empty($top_created_questions))
                        <?php $previous_num = -1;  $rank_scores = 0; $sp = 'st'; ?>
                        @for ($i = 0; $i < count($top_created_questions); $i++)
                            <?php

                                if ($top_created_questions[$i]->num != $previous_num) {
                                    $previous_num = $top_created_questions[$i]->num;
                                    $rank_scores++;
                                }

                                if ($rank_scores == 1) {
                                    $color = 'background-color:#f6df78;';
                                    $sp = 'st';
                                } elseif ($rank_scores == 2) {
                                    $color = 'background-color:#d8d7de;';
                                    $sp = 'nd';
                                } elseif ($rank_scores == 3) {
                                    $color = 'background-color:#c2937f;';
                                    $sp = 'rd';
                                } else {
                                    $color = 'background-color:#eae7e3;';
                                    $sp = 'th';
                                }
                            ?>

                            <article class="media event">
                                <a class="pull-left date" style="{{$color}}">
                                    <p class="day">{{$rank_scores}}<sup>{{$sp}}</sup></p>
                                </a>
                                <div class="media-body">
                                    <a ><p style="margin-top: 5%;">{{$top_created_questions[$i]->num}}</p></a>
                                </div>
                            </article>
                        @endfor
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-3 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Top 5 Keywords</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(!empty($top_keywords))
                        @for ($i = 0; $i < count($top_keywords); $i++)
                            @if ($i == 0)
                                <article class="media event"> 
                                    <a class="pull-left date" style="background-color:#f6df78;">
                                        <p class="day">1<sup>st</sup></p>
                                    </a>
                                    <div class="media-body">
                                        <a href="{{route('question.showquestion.get', ['search'=>$top_keywords[$i]->keyword])}}"><p style="margin-top: 5%;">{{$top_keywords[$i]->keyword}}</p></a>
                                    </div>
                                </article>
                            @elseif ($i == 1)
                                <article class="media event"> 
                                    <a class="pull-left date" style="background-color:#d8d7de;">
                                        <p class="day">2<sup>nd</sup></p>
                                    </a>
                                    <div class="media-body">
                                        <a href="{{route('question.showquestion.get', ['search'=>$top_keywords[$i]->keyword])}}"><p style="margin-top: 5%;">{{$top_keywords[$i]->keyword}}</p></a>
                                    </div>
                                </article>
                            @elseif ($i == 2)
                                <article class="media event"> 
                                    <a class="pull-left date" style="background-color:#c2937f;">
                                        <p class="day">3<sup>rd</sup></p>
                                    </a>
                                    <div class="media-body">
                                        <a href="{{route('question.showquestion.get', ['search'=>$top_keywords[$i]->keyword])}}"><p style="margin-top: 5%;">{{$top_keywords[$i]->keyword}}</p></a>
                                    </div>
                                </article>
                            @elseif ($i == 3)
                                <article class="media event"> 
                                    <a class="pull-left date" style="background-color:#eae7e3;">
                                        <p class="day">4<sup>th</sup></p>
                                    </a>
                                    <div class="media-body">
                                        <a href="{{route('question.showquestion.get', ['search'=>$top_keywords[$i]->keyword])}}"><p style="margin-top: 5%;">{{$top_keywords[$i]->keyword}}</p></a>
                                    </div>
                                </article>
                            @else
                                <article class="media event"> 
                                    <a class="pull-left date" style="background-color:#eae7e3;">
                                        <p class="day">5<sup>th</sup></p>
                                    </a>
                                    <div class="media-body">
                                        <a href="{{route('question.showquestion.get', ['search'=>$top_keywords[$i]->keyword])}}"><p style="margin-top: 5%;">{{$top_keywords[$i]->keyword}}</p></a>
                                    </div>
                                </article>
                            @endif
                        @endfor
                    @endif
                </div>
            </div>
        </div>

		<div class="col-md-3 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Top 5 Popular Questions</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
                    @if(!empty($top_questions))
                        @for ($i = 0; $i < count($top_questions); $i++)
                            @if ($i == 0)
                                <article class="media event"> 
                                    <a class="pull-left date" style="background-color:#f6df78;">
                                        <p class="day">1<sup>st</sup></p>
                                    </a>
                                    <div class="media-body">
                                        <a href="{{route('question.detail',['id'=>$top_questions[$i]->id])}}"><p style="margin-top: 5%;">{{$top_questions[$i]->topic}}</p></a>
                                    </div>
                                </article>
                            @elseif ($i == 1)
                                <article class="media event"> 
                                    <a class="pull-left date" style="background-color:#d8d7de;">
                                        <p class="day">2<sup>nd</sup></p>
                                    </a>
                                    <div class="media-body">
                                        <a href="{{route('question.detail',['id'=>$top_questions[$i]->id])}}"><p style="margin-top: 5%;">{{$top_questions[$i]->topic}}</p></a>
                                    </div>
                                </article>
                            @elseif ($i == 2)
                                <article class="media event"> 
                                    <a class="pull-left date" style="background-color:#c2937f;">
                                        <p class="day">3<sup>rd</sup></p>
                                    </a>
                                    <div class="media-body">
                                        <a href="{{route('question.detail',['id'=>$top_questions[$i]->id])}}"><p style="margin-top: 5%;">{{$top_questions[$i]->topic}}</p></a>
                                    </div>
                                </article>
                            @elseif ($i == 3)
                                <article class="media event"> 
                                    <a class="pull-left date" style="background-color:#eae7e3;">
                                        <p class="day">4<sup>th</sup></p>
                                    </a>
                                    <div class="media-body">
                                        <a href="{{route('question.detail',['id'=>$top_questions[$i]->id])}}"><p style="margin-top: 5%;">{{$top_questions[$i]->topic}}</p></a>
                                    </div>
                                </article>
                            @else
                                <article class="media event"> 
                                    <a class="pull-left date" style="background-color:#eae7e3;">
                                        <p class="day">5<sup>th</sup></p>
                                    </a>
                                    <div class="media-body">
                                        <a href="{{route('question.detail',['id'=>$top_questions[$i]->id])}}"><p style="margin-top: 5%;">{{$top_questions[$i]->topic}}</p></a>
                                    </div>
                                </article>
                            @endif
                        @endfor
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('javascript')
    <script src="{{ asset('js/dashboard/icheck.min.js') }}"></script>
    <script src="{{asset('js/dashboard/jquery.easypiechart.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vis/4.16.1/vis.min.js"></script>
	<script>
        var timeline_type = 0;
        var order_counter = 1;

        $('#see_more_hot_topic_btn').on('click', function() {
            get_frequent_keywords();
        });

        function get_frequent_keywords()
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('keywords_frequency')}}",

                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },

                data: {'courseID':'{{$courseInformation->course_id}}' , 'studentID':'{{Auth::id()}}'},
                success:function(response){
                    data = [];
                    //console.log(response);
                    total_num = response['total_num'];
                    keywords = response['contents'];
                    tmp_data = [];
                    for (var index in keywords) {
                        tmp_data[index] = +(keywords[index] / total_num).toFixed(2);
                    }
                    count = 0;
                    for (var index in tmp_data) {
                        if (count++ <=4) {
                            data.push({label:index, font:{size: (14*(2+tmp_data[index]))}, color:{background:'#228b22', highlight:{background:'#267347', border: '#267347'}}});
                        } else if (count > 4 && count <=10 ){
                            data.push({label:index, font:{size: (12*(1.2+tmp_data[index]))}, color:{background:'#2db92d', highlight:{background:'#267347', border: '#267347'}}});
                        } else {
                            data.push({label:index, font:{size: (10*(1+tmp_data[index]))}, color:{background:'#2db92d', highlight:{background:'#267347', border: '#267347'}}});
                        }
                    }

                    var nodes = new vis.DataSet(data);
                    var edges = new vis.DataSet();

                    var container = document.getElementById('bubbles');
                    var data = {
                        nodes: nodes,
                        edges: edges
                    };

                    var options = {
                        nodes: {borderWidth:0,shape:"circle",color:{background:'#2db92d', highlight:{background:'#267347', border: '#267347'}},font:{color:'#fff'}},
                        physics: {
                            stabilization: false,
                            minVelocity:  0.01,
                            solver: "repulsion",
                            repulsion: {
                                nodeDistance: 100
                            }
                        },
                        interaction:{
                            dragNodes:true,
                            dragView: true,
                            hideEdgesOnDrag: false,
                            hideNodesOnDrag: false,
                            hover: false,
                            hoverConnectedEdges: true,
                            keyboard: {
                                enabled: false,
                                speed: {x: 10, y: 10, zoom: 0.02},
                                bindToWindow: true
                            },
                            multiselect: false,
                            navigationButtons: false,
                            selectable: true,
                            selectConnectedEdges: true,
                            tooltipDelay: 300,
                            zoomView: true
                        }
                    };
                    var network = new vis.Network(container, data, options);

                    // Events
                    network.on("click", function(e) {
                        if (e.nodes.length) {
                            var node = nodes.get(e.nodes[0]);
                            nodes.update(node);
                            $(location).attr('href', "{{(route('question.showquestion.get.without_search'))}}/"+node.label);
                        }
                    });

                }, error:function(e){
                    $('#display_comment_on_dashboard').html(e);
                }
            });
        }

        function showDefault(){
            $('#box-answer-question').hide();
            $('#box-create-question').hide();
            $('#box-show-answered').hide();
            $('#box-show-created').hide();
            $('#box-forum').hide();
            $('#box-exercise').hide();
            $('#box-quiz').hide();
            if (!localStorage['menuitem1'] && !localStorage['menuitem2'] && !localStorage['menuitem3'] && !localStorage['menuitem4']){
                $('#check-answer-question').attr('checked', true);
                $('#check-create-question').attr('checked', true);
                $('#check-show-answered').attr('checked', true);
                $('#check-show-created').attr('checked', true); 
                $('#box-answer-question').show();
                $('#box-create-question').show();
                $('#box-show-answered').show();
                $('#box-show-created').show();
                localStorage.setItem('menuitem1','answer-question');
                localStorage.setItem('menuitem2','create-question');
                localStorage.setItem('menuitem3','show-created');
                localStorage.setItem('menuitem4','show-answered');
            } else {
                for (i=1;i<=4;i++) {
                    if(localStorage.getItem('menuitem'+i)!="NA"){
                        $("#box-"+localStorage.getItem('menuitem'+i)).show();
                        $("#box-"+localStorage.getItem('menuitem'+i)).insertBefore($("#box-last"));
                        $('#check-'+localStorage.getItem('menuitem'+i)).attr('checked', true);
                    }
                }
            }
        
        }
        window.onload = showDefault();

        function checkedBox(clicked_box){
            if($("#check-"+clicked_box).is(":checked")){
                $("#box-"+clicked_box).show();
                $("#box-"+clicked_box).insertBefore($("#box-last"));
                var flag = true;
                var i = 0;
                while(flag){
                    if(localStorage.getItem('menuitem'+i)=="NA"){
                        localStorage.setItem('menuitem'+i,clicked_box);
                        flag = false;
                    }else{
                        i++;
                    }
                }
            } else {
                $("#box-"+clicked_box).hide();
                var i = 1;
                while(i != 4){
                    if(clicked_box==localStorage.getItem('menuitem'+i) && i != 4){
                        for(j=i;j<4;j++){
                            localStorage.setItem('menuitem'+j, localStorage.getItem('menuitem'+(j+1)))
                        }
                    }else{
                        i++;
                    }
                }
                localStorage.setItem('menuitem4','NA');
            }
            checkCount();
        }

        function checkCount(){
            var checked_counter = 0;
            if($('#check-answer-question').is(":checked")){
                checked_counter += 1;
            }
            if($('#check-create-question').is(":checked")){
                checked_counter += 1;
            }
            if($('#check-show-answered').is(":checked")){
                checked_counter += 1;
            }
            if($('#check-show-created').is(":checked")){
                checked_counter += 1;
            }
            if($('#check-forum').is(":checked")){
                checked_counter += 1;
            }
            if($('#check-exercise').is(":checked")){
                checked_counter += 1;
            }
            if($('#check-quiz').is(":checked")){
                checked_counter += 1;
            }
            if (checked_counter == 4){
                if(!$('#check-answer-question').is(":checked")){
                    $('#check-answer-question').attr('disabled', true);
                }
                if(!$('#check-create-question').is(":checked")){
                    $('#check-create-question').attr('disabled', true);
                }
                if(!$('#check-show-answered').is(":checked")){
                    $('#check-show-answered').attr('disabled', true);
                }
                if(!$('#check-show-created').is(":checked")){
                    $('#check-show-created').attr('disabled', true);
                }
                if(!$('#check-forum').is(":checked")){
                    $('#check-forum').attr('disabled', true);
                }
                if(!$('#check-exercise').is(":checked")){
                    $('#check-exercise').attr('disabled', true);
                }
                if(!$('#check-quiz').is(":checked")){
                    $('#check-quiz').attr('disabled', true);
                }
            }else{
                $('#check-answer-question').attr('disabled', false);
                $('#check-create-question').attr('disabled', false);
                $('#check-show-answered').attr('disabled', false);
                $('#check-show-created').attr('disabled', false);
                $('#check-forum').attr('disabled', false);
                $('#check-exercise').attr('disabled', false);
                $('#check-quiz').attr('disabled', false);
            }
        }
        window.onload = checkCount();

        function display_comments()
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('getCommentsReceived')}}",

                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },

                data: {'courseID':'{{$courseInformation->course_id}}' , 'studentID':'{{Auth::id()}}'},
                success:function(response){
                    if (response['comments'] === '') {
                        $('#display_comment_on_dashboard').html('<p>No Comments Yet</p>');
                    } else {
                        $('#display_comment_on_dashboard').html(response['comments']);
                    }

                }, error:function(e){
                    $('#display_comment_on_dashboard').html(e);
                }
            });
        }

		function drawTimeline(offset)
		{
            timeline_type = 0;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('getTimelineOverall')}}",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                data: {'offset': offset, 'courseID':'{{$courseInformation->course_id}}' , 'studentID':'{{Auth::id()}}'},
                success:function(response){
                    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                    $('#graph_line').html('');
                    $('#graph_title').html('Timeline: Overall');
                    //console.log(response['error']);

                    tmpdata_answer = response['data_answer'];
                    tmpdata_answered = response['data_answered'];
                    tmpdata_create = response['data_create'];
                    tmpweek = response['week'];

                    len = tmpdata_answer.length;
                    data=[];

                    for(i=0;i<7;i++) {
                        data[i] = {period:tmpweek[i], answered:tmpdata_answer[tmpweek[i]], created:tmpdata_create[tmpweek[i]], answered_by_peers: tmpdata_answered[tmpweek[i]]};
                    }

                    Morris.Line({
                        element: 'graph_line',
                        xkey: 'period',
                        ykeys: ['answered', 'created','answered_by_peers'],
                        labels: ['Answered', "Created", 'Peers Answered Your Questions'],
                        hideHover: 'auto',
                        lineColors: ['#26B99A', '#34495E', '#F08080', '#3498DB'],
                        data: data,
                        xLabelFormat: function (x) {
                            // return (d.getMonth()+1).toString()+'/'+(d.getDate()).toString();
                            // console.log(d.getDate());
                            var d = new Date(x.label);
                            // console.log(x.label);
                            return d.getDate() + ' ' + months[d.getMonth()];
                        },
                        parseTime: false,
                        resize: true
                    });

                    $MENU_TOGGLE.on('click', function() {
                        $(window).resize();
                    });
                }
            });
		}

		function drawTimeLineCreate(offset)
		{
            timeline_type = 2;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('getTimelineQuestions')}}",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                data: {'offset': offset, 'courseID':'{{$courseInformation->course_id}}' , 'studentID':'{{Auth::id()}}'},
                success:function(response){
                    $('#graph_line').html('');
                    $('#graph_title').html('Timeline: Create Questions');

                    tmpdata = response['data'];
                    tmpweek = response['week'];

                    //console.log(tmpdata);

                    len = tmpdata.length;
                    data=[];

                    for(i=0;i<7;i++) {
                        data[i] = {period:tmpweek[i], value:tmpdata[tmpweek[i]]};
                    }

                    Morris.Line({
                        element: 'graph_line',
                        xkey: 'period',
                        ykeys: ['value'],
                        labels: ['Value'],
                        hideHover: 'auto',
                        lineColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                        data: data,
                        xLabelFormat: function (d) {
                            return (d.getMonth()+1).toString()+'/'+d.getDate();
                        },
                        resize: true
                    });

                    $MENU_TOGGLE.on('click', function() {
                        $(window).resize();
                    });
                }
            });
		}

        function drawTimeLineAnswer(offset)
        {
            timeline_type = 1;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('getTimelineAnswers')}}",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                data: {'offset': offset, 'courseID':'{{$courseInformation->course_id}}' , 'studentID':'{{Auth::id()}}'},
                success:function(response){
                    $('#graph_line').html('');
                    $('#graph_title').html('Timeline: Answer Questions');

                    tmpdata = response['data'];
                    tmpweek = response['week'];

                    //console.log(tmpdata);

                    len = tmpdata.length;
                    data=[];

                    for(i=0;i<7;i++) {
                        data[i] = {period:tmpweek[i], value:tmpdata[tmpweek[i]]};
                    }

                    Morris.Line({
                        element: 'graph_line',
                        xkey: 'period',
                        ykeys: ['value'],
                        labels: ['Value'],
                        hideHover: 'auto',
                        lineColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                        data: data,
                        xLabelFormat: function (d) {
                            return (d.getMonth()+1).toString()+'/'+d.getDate();
                        },
                        resize: true
                    });

                    $MENU_TOGGLE.on('click', function() {
                        $(window).resize();
                    });
                }
            });
        }

        function drawTimeLinePeersAnswered(offset)
        {
            timeline_type = 3;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('getTimelinePeersAnswered')}}",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                data: {'offset': offset, 'courseID':'{{$courseInformation->course_id}}' , 'studentID':'{{Auth::id()}}'},
                success:function(response){
                    $('#graph_line').html('');
                    $('#graph_title').html('Timeline: Peers Answered Your Questions');

                    tmpdata = response['data'];
                    tmpweek = response['week'];

                    //console.log(tmpdata);

                    len = tmpdata.length;
                    data=[];

                    for(i=0;i<7;i++) {
                        data[i] = {period:tmpweek[i], value:tmpdata[tmpweek[i]]};
                    }

                    Morris.Line({
                        element: 'graph_line',
                        xkey: 'period',
                        ykeys: ['value'],
                        labels: ['Value'],
                        hideHover: 'auto',
                        lineColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                        data: data,
                        xLabelFormat: function (d) {
                            return (d.getMonth()+1).toString()+'/'+d.getDate();
                        },
                        resize: true
                    });

                    $MENU_TOGGLE.on('click', function() {
                        $(window).resize();
                    });
                }
            });
        }

        $(function() {
            display_comments();
            // get_frequent_keywords();
            var t=setInterval(display_comments,2000);

			@if(Session::has('timeline_type'))
            	timeline_type = parseInt('{{Session::get('timeline_type')}}');
            	// console.log('time'+timeline_type);
			@endif

            if ($('#graph_line').length ){
				@if(Session::has('timeline_offset'))
					if(timeline_type == 0) {
						drawTimeline(1000);
					}
					else if(timeline_type == 1) {
                    	drawTimeLineAnswer(1000);
                	} else if (timeline_type == 2) {
                    	drawTimeLineCreate(1000);
					} else {
                    	drawTimeLinePeersAnswered(-1);
                	}
				@else
					drawTimeline(0);
				@endif
            }
        });

        $("#answerQ_statistic").click(function(e) {
            if ($('#graph_line').length ){
				@if(Session::has('timeline_offset'))
                	drawTimeLineAnswer(1000);
				@else
                	drawTimeLineAnswer(0);
				@endif
            }

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $("#createQ_statistic").click(function(e) {
            if ($('#graph_line').length ){
				@if(Session::has('timeline_offset'))
                    drawTimeLineCreate(1000);
				@else
                    drawTimeLineCreate(0);
				@endif
            }

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $("#peersAnswered_statistic").click(function(e) {
            if ($('#graph_line').length ){
				@if(Session::has('timeline_offset'))
                    drawTimeLinePeersAnswered(1000);
				@else
                    drawTimeLinePeersAnswered(0);
				@endif
            }

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $("#overall_statistic").click(function(e) {
            drawTimeline(0);

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $("#previous_week").click(function(e) {
			if(timeline_type == 0 ) {
                drawTimeline(-1);
                // console.log('ok');
			} else if(timeline_type == 1) {
                drawTimeLineAnswer(-1);
                // console.log('ok_1');
			} else if(timeline_type == 2) {
                drawTimeLineCreate(-1);
                // console.log('ok_2');
			} else {
                drawTimeLinePeersAnswered(-1);
			}

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $("#next_week").click(function(e) {
            if(timeline_type == 0 ) {
                drawTimeline(1);
                //console.log('ok_0');
            } else if(timeline_type == 1) {
                drawTimeLineAnswer(1);
                //console.log('ok_1');
            } else if (timeline_type == 2) {
                drawTimeLineCreate(1);
                //console.log('ok_2');
            } else {
                drawTimeLinePeersAnswered(1);
            }

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $("#reset_week").click(function(e) {
            if(timeline_type == 0 ) {
                drawTimeline(0);
                // console.log('ok_0');
            } else if(timeline_type == 1) {
                drawTimeLineAnswer(0);
                // console.log('ok_1');
            } else if (timeline_type == 2) {
                drawTimeLineCreate(0);
                // console.log('ok_2');
            } else {
                drawTimeLinePeersAnswered(0);
            }

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
    </script>
@endsection
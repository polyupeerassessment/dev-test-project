
@extends('layouts.studentdashboard')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title">
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <script>(function(t,e,s,n){var o,a,c;t.SMCX=t.SMCX||[],e.getElementById(n)||(o=e.getElementsByTagName(s),a=o[o.length-1],c=e.createElement(s),c.type="text/javascript",c.async=!0,c.id=n,c.src=["https:"===location.protocol?"https://":"http://","widget.surveymonkey.com/collect/website/js/tRaiETqnLgj758hTBazgd9e452CQqj7nUSzQx95VKnWfjEdTXb5TYZprXVInf9Jh.js"].join(""),a.parentNode.insertBefore(c,a))})(window,document,"script","smcx-sdk");</script><a style="font: 12px Helvetica, sans-serif; color: #999; text-decoration: none;" href=https://www.surveymonkey.com> Create your own user feedback survey </a>
        </div>
    </div>

@endsection


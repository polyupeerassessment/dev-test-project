@extends('layouts.studentdashboard') 

@section('content')

<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Student Ranking</h2>
					<ul class="nav navbar-right panel_toolbox">
						<li>
							<a class="collapse-link">
								<i class="fa fa-chevron-up"></i>
							</a>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								<i class="fa fa-wrench"></i>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li>
									<a href="#">Settings 1</a>
								</li>
							</ul>
						</li>
						<li>
							<a class="close-link">
								<i class="fa fa-close"></i>
							</a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							@if (empty($students))
							<hr>
							<p>Students Not Registered or Answered Yet</p>
							@else
							<table class="table table-hover table-bordered">
								<thead>
									<tr>
										<?php
										foreach (array_keys((array)$students[0]) as $student_type) {
											echo "<th>".$student_type."</th>";
										}
										?>
									</tr>
								</thead>
								<tbody>
									<?php foreach($students as $student) { ?> 
									{{--<tr class='clickable-row' data-href="{{config('app.root_dir')}}/about">--}}
										<tr class=''>
											<?php
											foreach (array_keys((array)$students[0]) as $student_key) {
												echo "<td>".$student->$student_key."</td>";
											}
											?>
										</tr>
									<?php } ?>
								</tbody>
							</table>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
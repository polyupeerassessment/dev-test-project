@extends('layouts.studentdashboard') 

@section('content')

<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Personal Information</h2>

					<div class="clearfix"></div>
				</div>
				<div class="x_content">
                    <br />
                    {!! Form::open(['route' => 'student.update', 'class' => 'form-horizontal form-label-left']) !!}
					<div class="form-group">
						{{ Form::label('name', 'Name: ', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12' )) }}
						<div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="input-group">
							    <div class="input-group-addon">
								    <span class="fa fa-user"></span>
							    </div>
                                {{ Form::text('name', $user->name, array('class' => 'form-control col-md-7 col-xs-12')) }}
						    </div>
                        </div>
					</div>
					<div class="form-group">
						{{ Form::label('nickname', 'Nickname: ', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
						<div class="ccol-md-6 col-sm-6 col-xs-12">
							<div class="input-group">
								<div class="input-group-addon">
									<span class="fa fa-user"></span>
								</div>
								{{ Form::text('nickname', $user->nickname, array('class' => 'form-control')) }}
                                @if ($errors->has('nickname'))
								<span class="help-block">
									<strong>{{ $errors->first('nickname') }}</strong>
								</span>
								<br>
                                @endif
							</div>
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('email', 'Email: ', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="input-group">
								<div class="input-group-addon">
									<span class="fa fa-envelope "></span>
								</div>
								{{ Form::text('email', $user->email, array('class' => 'form-control','disabled')) }}
                                @if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
								<br>
                                @endif
							</div>
						</div>
					</div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          {{ Form::reset('Reset',array('class'=>'btn btn-md btn-primary')) }}
                          {{ Form::submit('Update', array('class'=>'btn btn-success btn-md btn-black')) }}
							<a href="{{route('student.newpassword')}}" class="btn btn-md btn-warning">Reset Password</a>
                        </div>

                    </div>
					{!! Form::close() !!}
					<br>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
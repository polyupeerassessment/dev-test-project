@extends('layouts.studentdashboard') 

@section('content')

<!-- page content -->
<div class="right_col" role="main">
	<div class="page-title">
	</div>
	<div class="clearfix"></div>
	<div class="row">
		{{--My Courses--}}
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Enroll Course</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					@if(empty($courses[0]))
					    <h4>No Available Course Now</h4>
					@else
					<table id="enrollTable" class="table table-hover">
						<thead>
							<tr>
								<th>Course Code</th>
								<th>Course Name</th>
								<th>Department</th>
								<th>Description</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php $i = 0; ?>
						@foreach($courses as $course)
                            <?php $i++; ?>
							<tr>

							<td>{{$course->course_code}}</td>
							<td>{{$course->course_name}}</td>
							<td>{{$course->department}}</td>
							<td>{{$course->description}}</td>
							<td>
								<!-- Button trigger modal -->
								@if( in_array($course->course_code, $enrolled))
									<button type="button" class="btn btn-info disabled">
										Enrolled
									</button>
								@else
									<button type="button" class="btn btn-success" data-toggle="modal" data-target="{{'#enrollModal'.$i}}">
										Enroll
									</button>

							@endif

							<!-- Modal -->
								<div class="modal fade" id="{{'enrollModal'.$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
												<h4 class="modal-title" id="exampleModalLabel">Enroll Code</h4>
											</div>
											<div class="modal-body">
												{!! Form::open(['route' => 'student.successenroll']) !!}
												{{Form::label('enrollcode', "Please type course code that your instructor provided:")}}
												{{Form::text('enrollcode', '', array('class'=>'form-control'))}}
												{{ Form::hidden('course_id', $course->course_code) }}
												<br>
												<br>
												<br>
												<div class="pull-right">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													{{Form::submit('Submit', array('class'=>'btn btn-primary'))}}
													{!! Form::close() !!}
												</div>
											</div>
										</div>
									</div>
								</div>
							</td>

							</tr>
						@endforeach

						</tbody>
					</table>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
        $('#enrollTable').DataTable();
    });

</script>

<style>
	.scroll {
			width: 100%;
			height: 100px;
			margin: 0;
			padding: 0;
			overflow-y: scroll;
		}
</style>
@endsection
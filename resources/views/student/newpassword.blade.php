@extends('layouts.studentdashboard')

@section('content')

<!-- page content -->
<div class="right_col" role="main">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>New Password</h2>

					<div class="clearfix"></div>
				</div>
				<div class="x_content">
                    <br />
                    {!! Form::open(['route' => 'student.newpass', 'class' => 'form-horizontal form-label-left']) !!}

					<div class="form-group">


						{{ Form::label('oldpass', 'Old Password: ', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12' )) }}
						<div class="col-md-6 col-sm-6 col-xs-12">
							@if ($errors->has('oldpass'))
								<span class="help-block">
									<strong style="color:red;">{{ $errors->first('oldpass') }}</strong>
								</span>
							@endif

							<div class="input-group">
								<div class="input-group-addon">
									<span class="fa fa-key"></span>
								</div>
								{{ Form::password('oldpass', array('class' => 'form-control col-md-7 col-xs-12', 'required')) }}
							</div>
						</div>
					</div>


					<div class="form-group">
						{{ Form::label('pass', 'New Password: ', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12' )) }}

						<div class="col-md-6 col-sm-6 col-xs-12">
							@if ($errors->has('pass'))
								<span class="help-block">
								<strong style="color:red;">{{ $errors->first('pass') }}</strong>
							</span>
							@endif
                            <div class="input-group">
							    <div class="input-group-addon">
								    <span class="fa fa-key" style="color:red"></span>
							    </div>
                                {{ Form::password('pass',  array('class' => 'form-control col-md-7 col-xs-12' ,'required')) }}
						    </div>
                        </div>
					</div>

					<div class="form-group">
						{{ Form::label('confirm', 'Confirm Password: ', array('class' => 'control-label col-md-3 col-sm-3 col-xs-12')) }}


						<div class="ccol-md-6 col-sm-6 col-xs-12">
							@if ($errors->has('confirm'))
								<span class="help-block">
								<strong style="color:red;">{{ $errors->first('confirm') }}</strong>
							</span>
							@endif
							<div class="input-group">
								<div class="input-group-addon">
									<span class="fa fa-key" style="color:red"></span>
								</div>
								{{ Form::password('confirm', array('class' => 'form-control', 'required')) }}
								<br>
							</div>
						</div>
					</div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          {{ Form::reset('Reset',array('class'=>'btn btn-md btn-primary')) }}
                          {{ Form::submit('Update', array('class'=>'btn btn-success btn-md btn-black')) }} 
                        </div>
                    </div>
					{!! Form::close() !!}
					<br>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
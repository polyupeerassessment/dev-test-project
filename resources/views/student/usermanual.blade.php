@extends('layouts.studentdashboard') 

@section('content')

<style>
	.clickable-row {
		cursor: pointer;
	}

	.aligned-row {
		display: flex;
		/*flex-flow: row wrap;*/
		padding:0px;
		margin:0px;
	}

    table,td,th{ border:1px solid grey; padding:10px;}
    .table-div{ padding-left: 20px;}
</style>

<!-- page content -->
<div class="right_col" role="main">
	<div class="page-title">
		<div class="title_left">
			{{--<h3>Welcome!</h3>--}}
			{{--<p>{!! Breadcrumbs::render('student_home') !!}</p>--}}
		</div>
	</div>
    {{--  <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif @if(session('fail'))
            <div class="alert alert-danger">
                {{ session('fail') }}
            </div>
            @elseif ($enrolled == false)
            <div class="alert alert-warning">
                Please enroll course or ask your teacher to add your ID into the system
            </div>
            @endif
        </div>
    </div>
	<div class="clearfix"></div>  --}}

    <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="x_title">
                <h2><i class="fa fa-align-left"></i> Home Page <small>Overview</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">

                <!-- start accordion -->
                <div class="accordion col-md-12 col-sm-12 col-xs-12" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel">
                    <a class="panel-heading" role="tab" id="heading1-1" data-toggle="collapse" data-parent="#accordion" href="#collapse1-1" aria-expanded="false" aria-controls="collapse1-1">
                      <h4 class="panel-title">1. Enter the course dashboard</h4>
                    </a>
                    <div id="collapse1-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1-1">
                      <div class="panel-body">
                        Click the row from the table for the course dashboard you want to enter, or click the Entry button.<br><br>
                        <img src="{{asset('images/manual1.png ')}}" class="img-responsive"/><br>
                      </div>
                    </div>
                  </div>
                  <div class="panel">
                    <a class="panel-heading collapsed" role="tab" id="heading1-2" data-toggle="collapse" data-parent="#accordion" href="#collapse1-2" aria-expanded="false" aria-controls="collapse1-2">
                      <h4 class="panel-title">2. Student Information</h4>
                    </a>
                    <div id="collapse1-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1-2">
                      <div class="panel-body">
                            To change the student information, follow the below instructions.<br><br>
                            <ol>
                                <li>Click the System button on the side bar to extend the menu.</li>
                                <li>Click Student Info button and go to the information page.</li>
                            </ol>
                            <br>
                            <img src="{{asset('images/manual2.png ')}}" class="img-responsive"/><br><br>
                            Enter the information you would like to change and click Update button.<br><br>
                            <img src="{{asset('images/manual3.png ')}}" class="img-responsive"/><br>
                      </div>
                    </div>
                  </div>
                  <div class="panel">
                    <a class="panel-heading collapsed" role="tab" id="heading1-3" data-toggle="collapse" data-parent="#accordion" href="#collapse1-3" aria-expanded="false" aria-controls="collapse1-3">
                      <h4 class="panel-title">3. Announcement and Notification</h4>
                    </a>
                    <div id="collapse1-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1-3">
                      <div class="panel-body">
                            Announcement and Notification board are shown in the home page.<br><br>
                            <ul>
                                <li>Announcement: Announcements released from teachers or administrators for updates or system changes.</li>
                                <li>Notification: Messages that notify the activities related to the user, which include: Someone has answered the user's question, someone has rated the user's question, daily progress report.</li>
                            </ul>
                            <br>
                            <img src="{{asset('images/manual4.png ')}}" class="img-responsive"/><br>
                      </div>
                    </div>
                  </div>
                  <div class="panel">
                    <a class="panel-heading collapsed" role="tab" id="heading1-4" data-toggle="collapse" data-parent="#accordion" href="#collapse1-4" aria-expanded="false" aria-controls="collapse1-4">
                      <h4 class="panel-title">4. Mail Box</h4>
                    </a>
                    <div id="collapse1-4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1-4">
                      <div class="panel-body">
                            Click the System button on the side bar to extend the menu.<br><br>
                            Click Mail box button and go to the user mail box.<br><br>
                            <img src="{{asset('images/manual5.png ')}}" class="img-responsive"/><br>   
                            Mail box displays the detailed content of Announcement and Notification:<br><br>
                                <ul>
                                    <li>Announcement: Announcements released from teachers or administrators for updates or system changes</li>
                                    <li>Notification: Messages that notify the activities related to the user, which include: Someone has answered the user's question, someone has rated the user's question, daily progress report. </li>
                                </ul>
                                <br><br> 
                                <img src="{{asset('images/manual6.png ')}}" class="img-responsive"/><br>  
                      </div>
                    </div>
                  </div>
                  <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="heading1-5" data-toggle="collapse" data-parent="#accordion" href="#collapse1-5" aria-expanded="false" aria-controls="collapse1-5">
                          <h4 class="panel-title">5. Setting</h4>
                        </a>
                        <div id="collapse1-5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1-5">
                          <div class="panel-body">
                                To change the system setting, please follow the instructions below.<br><br>
                                <ol>
                                    <li>1.	Click the user name on the top right corner.</li>
                                    <li>2.	Click Setting button to open the setting menu</li>
                                </ol>
                                <img src="{{asset('images/manual7.png ')}}" class="img-responsive"/><br><br>
                                
                                You can change your preference in the setting menu<br><br>
                                <img src="{{asset('images/manual8.png ')}}" class="img-responsive"/><br><br>
                                <ul>
                                    <li>Notification: Select the activity notify message you want to receive</li>
                                    <li>Announcement: Select whether the system will send announcement email automatically</li>
                                    <li>Report: Select whether the system will send weekly report automatically</li>
                                </ul>
                                <br>
                          </div>
                        </div>
                      </div>
                      <div class="panel">
                            <a class="panel-heading collapsed" role="tab" id="heading1-6" data-toggle="collapse" data-parent="#accordion" href="#collapse1-6" aria-expanded="false" aria-controls="collapse1-6">
                              <h4 class="panel-title">6. Logout</h4>
                            </a>
                            <div id="collapse1-6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1-6">
                              <div class="panel-body">
                                    <ol>
                                        <li>Click the user name on the top right corner</li>
                                        <li>Click Log Out to leave the system</li>
                                    </ol>
                                    <br>
                                    <img src="{{asset('images/manual9.png ')}}" class="img-responsive"/><br>   
                              </div>
                            </div>
                          </div>
                </div>
                <!-- end of accordion -->


              </div>
            </div>
          </div>


          <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-align-left"></i> Dashboard Page <small>Overview</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- start accordion -->
                    <div class="accordion col-md-12 col-sm-12 col-xs-12" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel">
                                    <a class="panel-heading collapsed" role="tab" id="heading2-1" data-toggle="collapse" data-parent="#accordion" href="#collapse2-1" aria-expanded="false" aria-controls="collapse2-1">
                                      <h4 class="panel-title">1. Dashboard Page</h4>
                                    </a>
                                    <div id="collapse2-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2-1">
                                      <div class="panel-body">
                                            To navigate to the Dashboard page, follow the below instructions.<br><br>
                                            Click on the course that you are enrolled in as shown in the below figure<br><br>
                                            <img src="{{asset('images/manual10.png ')}}" class="img-responsive" class="img-responsive"/><br><br>
                                            The Dashboard page is as shown in the below figures <br><br>
                                            <img src="{{asset('images/manual11.png ')}}" class="img-responsive"/><br><br>
                                            <img src="{{asset('images/manual12.png ')}}" class="img-responsive"/><br>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel">
                                        <a class="panel-heading collapsed" role="tab" id="heading2-2" data-toggle="collapse" data-parent="#accordion" href="#collapse2-2" aria-expanded="false" aria-controls="collapse2-2">
                                          <h4 class="panel-title">2. How to create a question?</h4>
                                        </a>
                                        <div id="collapse2-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2-2">
                                          <div class="panel-body">
                                                Click on Create as shown in the below figure<br><br>
                                                <img src="{{asset('images/manual13.png ')}}" class="img-responsive"/><br><br>
                                                This will take you to the create question page as shown below.<br>
                                                <ul>
                                                    <li>Topic(Mandatory): Enter a topic name</li>
                                                    <li>Keywords(Mandatory): Enter keywords relevant to the question</li>
                                                    <li>Contents(Mandatory): Enter the question here</li>
                                                </ul>
                                                <br><br>
                                                <img src="{{asset('images/manual14.png ')}}" class="img-responsive"/><br><br>
                                                If you have written a code snippet in the question and would want to test it, then click on content output<br><br>
                                                <img src="{{asset('images/manual15.png ')}}" class="img-responsive"/><br><br>
                                                Once you have entered the question, you can now enter the answer options as shown in the below figure.<br><br>
                                                The 2 types of questions that you can create are:<br><br>
                                                Multiple Choice: It can have a maximum of 6 answer options. It can have 1 or more than 1 right answer. Initially, only 3 answer options are visible on the screen. To add more answer options, click on '+ Add More Answer' button<br><br>
                                                <img src="{{asset('images/manual16.png ')}}" class="img-responsive"/><br><br>
                                                True/False: It has only 2 answer options. The answer could be either True or False.<br><br>
                                                <img src="{{asset('images/manual17.png ')}}" class="img-responsive"/><br><br>
                                                You can specify the Number of attempts that a student can make to answer the question using the 'Number of Attempts' field in the Advanced Option section of the page.<br><br>
                                                <ul>
                                                    <li>Maximum number of attempts for Multiple Choice question is 10</li>
                                                    <li>Maximum number of attempts for True/false question is 1.</li>
                                                </ul><br>
                                                <img src="{{asset('images/manual18.png ')}}" class="img-responsive"/><br><br>
                                                Tips to answer the question can also be added as shown in the below figure. This is an optional field.<br><br>
                                                <img src="{{asset('images/manual19.png ')}}" class="img-responsive"/><br><br>
                                                Explanation:<br><br>
                                                <img src="{{asset('images/manual20.png ')}}" class="img-responsive"/><br><br>
                                                Once you have entered all the details, you can click on the create button as shown below.<br><br>
                                                <img src="{{asset('images/manual21.png ')}}" class="img-responsive"/><br><br>
                                                Reset button clears all the content that has been entered.<br>
                                            
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel">
                                            <a class="panel-heading collapsed" role="tab" id="heading2-3" data-toggle="collapse" data-parent="#accordion" href="#collapse2-3" aria-expanded="false" aria-controls="collapse2-3">
                                              <h4 class="panel-title">3. How to answer a question?</h4>
                                            </a>
                                            <div id="collapse2-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2-3">
                                              <div class="panel-body">
                                                    To navigate to the Show Questions page<br><br>
                                                    Click on Show Question in the navigation bar and the following page will appear<br><br>
                                                    <img src="{{asset('images/manual22.png ')}}" class="img-responsive"/><br><br>
                                                    To answer a question - Click on Answer button<br><br>
                                                    <img src="{{asset('images/manual23.png ')}}" class="img-responsive"/><br>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="panel">
                                                <a class="panel-heading collapsed" role="tab" id="heading2-4" data-toggle="collapse" data-parent="#accordion" href="#collapse2-4" aria-expanded="false" aria-controls="collapse2-4">
                                                  <h4 class="panel-title">4. How to manage my question?</h4>
                                                </a>
                                                <div id="collapse2-4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2-4">
                                                  <div class="panel-body">
                                                        Click on My Question in the Navigation bar to see all the questions created.<br><br>
                                                        <ul><li>To edit the question, click on edit button</li>
                                                        <li>To view the question, click View button</li></ul><br><br>
                                                        This page also shows different statistics of your question<br><br>
                                                        <img src="{{asset('images/manual24.png ')}}" class="img-responsive"/><br>   
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="panel">
                                                    <a class="panel-heading collapsed" role="tab" id="heading2-5" data-toggle="collapse" data-parent="#accordion" href="#collapse2-5" aria-expanded="false" aria-controls="collapse2-5">
                                                      <h4 class="panel-title">5. How to view my answer history?</h4>
                                                    </a>
                                                    <div id="collapse2-5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2-5">
                                                      <div class="panel-body">
                                                            Click on Answered Question to see the questions answered by you<br><br>
                                                            <img src="{{asset('images/manual25.png ')}}" class="img-responsive"/><br> 
                                                      </div>
                                                    </div>
                                                </div>
                                                    <div class="panel">
                                                            <a class="panel-heading collapsed" role="tab" id="heading2-6" data-toggle="collapse" data-parent="#accordion" href="#collapse2-6" aria-expanded="false" aria-controls="collapse2-6">
                                                              <h4 class="panel-title">6. Overview for Discussion Forum</h4>
                                                            </a>
                                                            <div id="collapse2-6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2-6">
                                                              <div class="panel-body">
                                                                    Click on the Forum to start a discussion thread<br><br>
                                                                    <img src="{{asset('images/manual26.png ')}}" class="img-responsive"/><br> 
                                                              </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel">
                                                                <a class="panel-heading collapsed" role="tab" id="heading2-7" data-toggle="collapse" data-parent="#accordion" href="#collapse2-7" aria-expanded="false" aria-controls="collapse2-7">
                                                                  <h4 class="panel-title">7. Learning progress and Leader board</h4>
                                                                </a>
                                                                <div id="collapse2-7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2-7">
                                                                  <div class="panel-body">
                                                                        Click on the Home button under the course <br><br>
                                                                        Scroll down to see the Learning Progress to see the graph<br><br>
                                                                        Small button on the right-hand corner for settings<br><br> 
                                                                        Scroll down to find the leader board<br><br>
                                                                        <img src="{{asset('images/manual27.png ')}}" class="img-responsive"/><br><br>
                                                                        <img src="{{asset('images/manual28.png ')}}" class="img-responsive"/><br>
                                                                  </div>
                                                                </div>
                                                            </div>
                                                  </div>
                    </div>
                    <!-- end of accordion -->


                  </div>
                </div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2><i class="fa fa-align-left"></i> About COMP3421 and System</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
    
                        <!-- start accordion -->
                        <div class="accordion col-md-12 col-sm-12 col-xs-12" id="accordion" role="tablist" aria-multiselectable="true">
                          <div class="panel">
                            <a class="panel-heading" role="tab" id="heading3-1" data-toggle="collapse" data-parent="#accordion" href="#collapse3-1" aria-expanded="false" aria-controls="collapse3-1">
                              <h4 class="panel-title">1. Weekly Task</h4>
                            </a>
                            <div id="collapse3-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3-1">
                              <div class="panel-body">
                                  <ul>
                                      <li>You are required to complete 5 pairs of Pre-exercise and Post-exercise in any 5 weeks throughout the semester.</li>
                                      <li>Some of the <b>Post-exercise</b> require you to create and answer questions by <b>using this system</b>. </li>
                                      <li>For the weekly exercise, you can create questions related to the web programming topic that you have learned in that week. </li>
                                      <li>After you have completed the exercise, please fill in the report and submit it during the lecture</li>
                                      <li>Please check the weekly task schedule below.</li>
                                  </ul>
                                  <br>
                                <div class="table-div table-responsive">
                                  <table>
                                      <tr>
                                          <th>Date</th>
                                          <th>Week</th>
                                          <th>Post-Task</th>
                                          <th>Topic</th>
                                      </tr>
                                      <tr>
                                          <td>7 Feb</td>
                                          <td>4</td>
                                          <td>Create 2 questions + Answer 5 questions</td>
                                          <td>Javascript - 1</td>
                                      </tr>
                                      <tr>
                                            <td>14 Feb</td>
                                            <td>5</td>
                                            <td>Create 2 questions + Answer 5 questions</td>
                                            <td>Javascript - 2</td>
                                        </tr>
                                        <tr>
                                            <td>21 Feb</td>
                                            <td>Lunar New Year Break</td>
                                            <td>N/A</td>
                                            <td>N/A</td>
                                        </tr>
                                        <tr>
                                            <td>28 Feb</td>
                                            <td>6 (Quiz)</td>
                                            <td>N/A</td>
                                            <td>N/A</td>
                                        </tr>
                                        <tr>
                                                <td>7 Mar</td>
                                                <td>7</td>
                                                <td>Create 3 questions + Answer 5 questions *</td>
                                                <td>JQuery and Node.js</td>
                                            </tr>
                                        
                                        <tr>
                                            <td>14 Mar</td>
                                            <td>8</td>
                                            <td>Create 3 questions + Answer 5 questions *</td>
                                            <td>Ajax and Google API</td>
                                        </tr>
                                        <tr>
                                            <td>21 Mar</td>
                                            <td>9</td>
                                            <td>Create 3 questions + Answer 5 questions *</td>
                                            <td>PHP - 1</td>
                                        </tr>
                                        <tr>
                                            <td>28 Mar</td>
                                            <td>10</td>
                                            <td>Create 3 questions + Answer 5 questions *</td>
                                            <td>PHP - 2</td>
                                        </tr>
                                    </table>   
                                    <br>* The task will be adjusted after lunar new year break <br><br>
                                    <h3>You can download the report template here: <a href='{{asset('file/ClassEx4-post_PeerU.doc')}}' >Download</a></h3>
                                </div>         
                              </div>
                            </div>
                          </div>
                          <div class="panel">
                            <a class="panel-heading collapsed" role="tab" id="heading3-2" data-toggle="collapse" data-parent="#accordion" href="#collapse3-2" aria-expanded="false" aria-controls="collapse3-2">
                              <h4 class="panel-title">2. Feedback and Bug Report</h4>
                            </a>
                            <div id="collapse3-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3-2">
                              <div class="panel-body">
                                If you want to provide feedback or report error of our system, please contact the administrators by email.<br><br>
                                <a href="mailto:comp.peerassessment@gmail.com">comp.peerassessment@gmail.com</a>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- end of accordion -->
    
    
                      </div>
                    </div>
                  </div>

	@if(config('app.env') != 'production')
	<div class="row" id="cute_animation">
		<div class="container">
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
		</div>
	</div>
	@endif
</div>
<style>

	#cute_animation .container {
		width: 200px;
		height: 100px;
		padding-top: 100px;
		margin: 0 auto;
	}

	#cute_animation .ball {
		width: 10px;
		height: 10px;
		margin: 10px auto;
		border-radius: 50px;
	}

	#cute_animation .ball:nth-child(1) {
		background: navy;
		-webkit-animation: right 1s infinite ease-in-out;
		-moz-animation: right 1s infinite ease-in-out;
		animation: right 1s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(2) {
		background: navy;
		-webkit-animation: left 1.1s infinite ease-in-out;
		-moz-animation: left 1.1s infinite ease-in-out;
		animation: left 1.1s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(3) {
		background: navy;
		-webkit-animation: right 1.05s infinite ease-in-out;
		-moz-animation: right 1.05s infinite ease-in-out;
		animation: right 1.05s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(4) {
		background: navy;
		-webkit-animation: left 1.15s infinite ease-in-out;
		-moz-animation: left 1.15s infinite ease-in-out;
		animation: left 1.15s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(5) {
		background: navy;
		-webkit-animation: right 1.1s infinite ease-in-out;
		-moz-animation: right 1.1s infinite ease-in-out;
		animation: right 1.1s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(6) {
		background: navy;
		-webkit-animation: left 1.05s infinite ease-in-out;
		-moz-animation: left 1.05s infinite ease-in-out;
		animation: left 1.05s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(7) {
		background: navy;
		-webkit-animation: right 1s infinite ease-in-out;
		-moz-animation: right 1s infinite ease-in-out;
		animation: right 1s infinite ease-in-out;
	}

	@-webkit-keyframes right {
		0% {
			-webkit-transform: translate(-15px);
		}
		50% {
			-webkit-transform: translate(15px);
		}
		100% {
			-webkit-transform: translate(-15px);
		}
	}

	@-webkit-keyframes left {
		0% {
			-webkit-transform: translate(15px);
		}
		50% {
			-webkit-transform: translate(-15px);
		}
		100% {
			-webkit-transform: translate(15px);
		}
	}

	@-moz-keyframes right {
		0% {
			-moz-transform: translate(-15px);
		}
		50% {
			-moz-transform: translate(15px);
		}
		100% {
			-moz-transform: translate(-15px);
		}
	}

	@-moz-keyframes left {
		0% {
			-moz-transform: translate(15px);
		}
		50% {
			-moz-transform: translate(-15px);
		}
		100% {
			-moz-transform: translate(15px);
		}
	}

	@keyframes right {
		0% {
			transform: translate(-15px);
		}
		50% {
			transform: translate(15px);
		}
		100% {
			transform: translate(-15px);
		}
	}

	@keyframes left {
		0% {
			transform: translate(15px);
		}
		50% {
			transform: translate(-15px);
		}
		100% {
			transform: translate(15px);
		}
	}
</style>

@endsection
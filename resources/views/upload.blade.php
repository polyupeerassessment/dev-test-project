@extends('layouts.app')

@section('content')
    <br>
    @if (session('success') ==1 )
        <div class="alert alert-success">
            Enrolled Successfully
        </div>
    @elseif (session('success') == -1)
        <div class="alert alert-danger">
            Failed to Enroll
        </div>
    @endif

    <h2>Enroll Students</h2>
    <form action="{{route('upload')}}" method="post" enctype="multipart/form-data">

        {{csrf_field()}}
        <div class="form-group">
            <label for="enroll_course_id">Course Code</label>
            <input type="text" name="enroll_course_id" class="form-control">
        </div>

        <div class="form-group">
            <label for="upload_file">Upload</label>
            <input type="file" name="upload_file" class="form-control">
        </div>

        <input class="btn btn-success" type="submit" value="Upload Image" name="submit">
    </form>


@endsection
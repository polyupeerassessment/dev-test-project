<?php

use Illuminate\Support\Facades\Auth;

function get_guard() {
//    if (!Auth::check()) {
//        return false;
//    }



    $tmp_teacher = Auth::guard('teacher')->user();

    if (is_null($tmp_teacher) || empty($tmp_teacher)) {
        return false;
    }

    if(Auth::guard('teacher')->check())
    {return "teacher";}
    elseif(Auth::check())
    {return "user";}
    elseif(Auth::guard('admin')->check())
    {return "admin";}

    return false;


}
?>

<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="stylesheet" href="{{asset('css/app.css')}}" />
    <link rel="stylesheet" href="{{asset('css/jquery.tagsinput.css')}}" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
    <link rel="stylesheet" href="{{asset('css/peer-assessment.css')}}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    
    <link rel="stylesheet" href="{{asset('css/siderbar.css')}}" />
    <link rel="stylesheet" href="{{asset('css/home/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/home/stylish-portfolio.min.css')}}" />

    {{--<script src="{{ asset('js/app.js') }}"></script>--}}
    {{--<link rel="stylesheet" href="{{asset('css/bootstrap-tagsinput.css')}}" />--}}

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    
    <link rel="stylesheet" href="{{asset('css/dashboard/custom.min.css')}}" />
    <title>{{config('app.name')}}</title>

    <style>
        ul{
            list-style-type: none;
        }
        #scrollarea-invalid {
            overflow-y: scroll;
            height: 500px;
        }
        #scrollarea-content{
            min-height:110%;
            /*overflow: scroll;*/
        }
    </style>
</head>

<body>
    @if (!Auth::guest())
        @if (get_guard()=="teacher")
            @include('inc.teacher_navbar')
        @else
            @include('inc.navbar')
        @endif
    @endif

    <div class="wrapper">
        <!-- Sidebar Holder -->
        @if (!Auth::guest())
        <nav id="sidebar" class="active">
            <ul class="list-unstyled components">
                <li class="active">
                    <a href="{{ config('app.root_dir') }}">
                        <i class="fa fa-home"></i>
                        Home
                    </a>
                </li>
                <li>
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">
                        <i class="fa fa-tachometer"></i>
                        Dashboard
                    </a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li><a href="#">Page 1</a></li>
                        <li><a href="#">Page 2</a></li>
                        <li><a href="#">Page 3</a></li>
                    </ul>
                    @if (Session::get('courseID'))
                    <a href="{{ route('student.dashboard', ['courseID'=>Session::get('courseID')]) }}">
                        <i class="fa fa-book"></i>
                        Course
                    </a>
                    @endif
                </li>
            </ul>
        </nav>
        @endif
            
        <main role="main" style="flex-grow:1;">
            @yield('content')
        </main>

    </div>

    <!-- Scripts -->
    {{--<script src="{{ asset('js/bootstrap-tagsinput.js') }}"></script>--}}
    <script src="{{ asset('js/nicEdit.js') }}"></script>
    <script src="{{ asset('js/jquery.tagsinput.js') }}"></script>
    {{--<script src="{{ asset('js/app.js') }}"></script>--}}

    <script type="text/javascript">

        showAdvanceOption = false;
        bkLib.onDomLoaded(function() {
            new nicEditor().panelInstance('area1');
        });

        $( "#choice_type" ).change(function()
        {
            select = $(this).val();
            switch(parseInt(select)){
                case 0:
                    $('#alternative_board_mc').show();
                    $('#alternative_board_tf').hide();break;
                case 1:
                    $('#alternative_board_mc').hide();
                    $('#alternative_board_tf').show();break;

                case 2:
                    $('#alternative_board_mc').hide();
                    $('#alternative_board_tf').hide();break;
                default:
                    break;

            }
        });

        $("#showadvanceoption").click(function(){
            showAdvanceOption = !showAdvanceOption;

            if(showAdvanceOption) {
                $('#advancedoption_contents').show();
            } else {
                $('#advancedoption_contents').hide();
            }

        });
        $('.star_input').click(function(){
            var star = parseFloat($(this).val());

            //remove all star
            $("label").each(function() {
                $(this).removeAttr('id');
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('insertrating')}}",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                data: {'star': star, 'questionID': $('#questionID').val(), 'course_id':'{{Session::get('courseID')}}', 'user_id':'{{Auth::id()}}' },
                success:function(response){
//                    $('.comment_area').html(response['comments']);
                    console.log(response);
                }
            });

           $('#thankurating').html('Thank you for your rating!');
        });

        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });

        $(document).ready(function(){
            $('#advancedoption_contents').hide();
            select = $('#choice_type').find(":selected").val();

            $("body").tooltip({ selector: '[data-toggle=tooltip]' });


            switch(parseInt(select)){
                case 0:
                    $('#alternative_board_mc').show();
                    $('#alternative_board_tf').hide();break;
                case 1:
                    $('#alternative_board_mc').hide();
                    $('#alternative_board_tf').show();break;

                case 2:
                    $('#alternative_board_mc').hide();
                    $('#alternative_board_tf').hide();break;
                default:
                    break;
            }
        });

    </script>
    <script type="text/javascript">

        function onAddTag(tag) {
            alert("Added a tag: " + tag);
        }
        function onRemoveTag(tag) {
            alert("Removed a tag: " + tag);
        }

        function onChangeTag(input,tag) {
            alert("Changed a tag: " + tag);
        }

        $(function() {
            $('#tags_1').tagsInput({width:'auto'});

            $("#messageForm").submit(function(e) {
//                console.log($('input#questionID').val());
                $test='';

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('[name="_token"]').val()
                    },
                    method:'POST',
                    url:"{{route('question.insertcomment')}}",
                    beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    data: {'message':$('#detail-message').val(), 'questionID': $('#questionID').val(), 'course_id':'{{Session::get('courseID')}}' },
                    success:function(response){
                        $('.comment_area').html(response['comments']);
                        $test = response['comments'];
                    }
                });

                $('#detail-message').val('');
                console.log($test);

                e.preventDefault(); // avoid to execute the actual submit of the form.
            });

            start = Date.now();

            $("#answerForm").submit(function(e) { // measure time that user spent to asnwer question
                end = Date.now();

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('[name="_token"]').val()
                    },
                    method:'POST',
                    url:"{{route('inserttime')}}",
                    beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    data: {'timeSpent': end - start, 'questionID': $('#questionID').val(), 'course_id':'{{Session::get('courseID')}}', 'user_id':'{{Auth::id()}}' },
                    success:function(response){
//                        console.log(response);
                    }
                });
            });
        });
    </script>

</body>
</html>

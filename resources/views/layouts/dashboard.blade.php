<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="stylesheet" href="{{asset('css/dashboard/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/nprogress.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/daterangepicker.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/dropzone.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/pnotify.buttons.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/pnotify.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/pnotify.nonblock.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/bootstrap-datetimepicker.css')}}" />

    <link rel="stylesheet" href="{{asset('css/dashboard/dataTables.bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/buttons.bootstrap.min.css')}}" />

    <link rel="stylesheet" href="{{asset('css/dashboard/custom.min.css')}}" />
    @yield('css')

    <script src="{{asset('js/dashboard/jquery.min.js')}}"></script>
    <script src="{{asset('js/jquery.tagsinput.js') }}"></script>
    <title>{{config('app.name')}}</title>

    <link rel="icon" href="{!! asset('images/title-icon.png') !!}"/>
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col" style="position: fixed;">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><span>PeerU Admin</span></a>
                    </div>
                    <div class="clearfix"></div>
                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                        <ul class="nav side-menu">
                            <li><a href="{{route('teacher.home')}}"><i class="fa fa-home"></i> Home</a></li>

                            @if(session('courseID_byTeacher'))
                                <li><a><i class="fa fa-graduation-cap"></i> {{session('courseID_byTeacher') }}<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{route('teacher.dashboard',[session('courseID_byTeacher')])}}">Home</a></li>

                                        <li><a href="{{route('teacher.leaderboard')}}" style="color:orange;">Leaderboard</a></li>

                                        <li><a style="color:dodgerblue;"> Question <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <li><a href="{{route('teacher.createquestion')}}">Create Question</a></li>
                                                <li><a href="{{route('teacher.showquestion')}}">Show Question</a></li>
                                            </ul>
                                        </li>

                                        <li><a style="color:yellowgreen;"> Student <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <li><a href="{{route('teacher.mystudents')}}">My Students</a></li>
                                                <li><a href="{{route('teacher.addstudents')}}">Add Students</a></li>
                                            </ul>
                                        </li>

                                        <li><a style="color:mediumvioletred;"> Quiz <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <li><a href="{{route('teacher.createquiz')}}">Create Quiz</a></li>
                                                <li><a href="{{route('teacher.quiz.list')}}">Quiz List</a></li>
                                                <li><a href="{{route('teacher.quizquestions')}}">Quiz Questions</a></li>
                                            </ul>
                                        </li>

                                        <li><a style="color:navajowhite;"> Voting <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <li><a href="{{route('teacher.voting.questions')}}">Questions</a></li>
                                                <li><a href="{{route('teacher.voting.result')}}">Result</a></li>
                                            </ul>
                                        </li>
                                        <li><a style="color:red;"> Assignment <span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <li><a href="{{route('teacher.assignment.show')}}">Show</a></li>
                                                <li><a href="{{route('teacher.assignment.create')}}">Create</a></li>
                                                @if (Session::get('assignment_byTeacher'))
                                                    <li><a href="{{route('teacher.assignment.question.create',['id'=>Session::get('assignment_byTeacher')])}}">Create Questions</a></li>
                                                    <li><a href="{{route('teacher.assignment.question.list',['id'=>Session::get('assignment_byTeacher')])}}">Show Questions</a></li>
                                                @endif

                                            </ul>
                                        </li>

                                        <li><a href="{{route('teacher.statistics')}}">Course Statistics</a></li>
                                    </ul>
                                </li>
                            @endif

                            @if(Auth::id() == 1)
                                <li><a><i class="fa fa-desktop"></i> System <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{route('teacher.log')}}">Log</a></li>
                                        <li><a href="{{route('admin.create.sysannounce')}}">System Announcement</a></li>
                                    </ul>
                                </li>
                            @endif

                        </ul>
                    </div>
                        <div class="menu_section">
                            <h3>Accouncement</h3>
                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-envelope-o"></i> Mail<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{route('teacher.showannounce')}}">Show</a></li>
                                        <li><a href="{{route('teacher.announceform')}}">Send</a></li>
                                    </ul>
                                </li>

                                <li><a href="#"><i class="fa fa-bell"></i> Notification <span class="label label-success pull-right">Coming Soon</span></a></li>
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->
                </div>
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                {{Auth::user()->nickname}}

                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="#"> Profile</a></li>
                                <li>
                                    <a data-toggle="tooltip" data-placement="top" data-placement="top" href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out pull-right"></i> Log Out
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;" class="dropdown-item">
                                            {{ csrf_field() }}
                                        </form>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        @yield('content')
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Created By <a href="#">PeerU</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<script src="{{asset('js/dashboard/bootstrap.min.js')}}"></script>

<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>

<script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>

<script src="{{asset('js/dashboard/dataTables.buttons.min.js')}}"></script>

<script src="{{asset('js/dashboard/buttons.bootstrap.min.js')}}"></script>

<script src="{{asset('js/dashboard/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/dashboard/buttons.print.min.js')}}"></script>

<script src="{{asset('js/dashboard/fastclick.js')}}"></script>
<script src="{{asset('js/dashboard/nprogress.js')}}"></script>
<script src="{{asset('js/dashboard/Chart.min.js')}}"></script>
<script src="{{asset('js/dashboard/jquery.sparkline.min.js')}}"></script>


<script src="{{asset('js/dashboard/jquery.flot.js')}}"></script>
<script src="{{asset('js/dashboard/jquery.flot.pie.js')}}"></script>
<script src="{{asset('js/dashboard/jquery.flot.resize.js')}}"></script>
<script src="{{asset('js/dashboard/jquery.flot.stack.js')}}"></script>
<script src="{{asset('js/dashboard/jquery.flot.time.js')}}"></script>


<script src="{{asset('js/dashboard/jquery.flot.orderBars.js')}}"></script>
<script src="{{asset('js/dashboard/jquery.flot.spline.min.js')}}"></script>
<script src="{{asset('js/dashboard/curvedLines.js')}}"></script>

<script src="{{asset('js/dashboard/date.js')}}"></script>
<script src="{{asset('js/dashboard/moment.min.js')}}"></script>
<script src="{{asset('js/dashboard/daterangepicker.js')}}"></script>
<script src="{{asset('js/dashboard/jquery.smartWizard.js')}}"></script>
<script src="{{asset('js/dashboard/dropzone.min.js')}}"></script>

<script src="{{asset('js/dashboard/pnotify.js')}}"></script>
<script src="{{asset('js/dashboard/pnotify.buttons.js')}}"></script>
<script src="{{asset('js/dashboard/pnotify.nonblock.js')}}"></script>
<script src="http://d3js.org/d3.v3.min.js"></script>
<script src="http://labratrevenge.com/d3-tip/javascripts/d3.tip.v0.6.3.js"></script>


<script src="{{asset('js/dashboard/morris.min.js')}}"></script>
<script src="{{asset('js/dashboard/raphael.min.js')}}"></script>

<script src="http://malsup.github.com/jquery.form.js"></script>
@yield('javascript')
<script src="{{asset('js/dashboard/custom.js')}}"></script>

@if(session('success'))
    <script type='text/javascript'>
        $(document).ready(function(e) {
            generate_PNotify_success('{{session('success')}}');
        });
    </script>
@endif

@if(session('similar'))
    <script type='text/javascript'>
        $(document).ready(function(e) {
            generate_PNotify('Your question is very similar with other question(s). Please create different question.');
        });
    </script>
@endif

@if(session('fail'))
    <script type='text/javascript'>
        $(document).ready(function(e) {
            generate_PNotify('{{session('fail')}}');
        });
    </script>
@endif

<script type="text/javascript">
    function onAddTag(tag) {
        alert("Added a tag: " + tag);
    }
    function onRemoveTag(tag) {
        alert("Removed a tag: " + tag);
    }

    function onChangeTag(input,tag) {
        alert("Changed a tag: " + tag);
    }

    $(function() {
        $('#tags_1').tagsInput({width:'auto'});

        $(document).on('click', '.removemycomment', function(e){
            e.preventDefault();
            var comment_id = $(this).data('filter');
            console.log(comment_id);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('question.removemycomment_question')}}",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                data: {'comment_id':comment_id, 'questionID': $('#questionID').val(), 'course_id':'{{Session::get('courseID')}}' },
                success:function(response){
                    $('div').find('#messages_ajax').html(response['comments']);
                }
            });
        });

        $("#messageForm").submit(function(e) {
            //console.log($('input#questionID').val());
            $test='';
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('question.insertcomment')}}",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                data: {'message':$('#detail-message').val(), 'questionID': $('#questionID').val(), 'course_id':'{{Session::get('courseID')}}' },
                success:function(response){
                    $('div').find('#messages_ajax').html(response['comments']);
                }
            });

            $('#detail-message').val('');
            console.log($test);

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });


    });
</script>
</body>
</html>
<div class="modal fade" id="nav_setting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">Setting</h4>
            </div>
            <div class="modal-body">
                <?php $user = App\User::where('id','=',Auth::id())->first(); ?>

                @if(!empty($user))

                {!! Form::open(['route' => 'student.setting']) !!}
                    <div class="form-group">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">[Notification]</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="">
                                <label>
                                    <input type="checkbox" name= 'notify_rated' class="" @if($user->notify_rated ==1) checked @endif /> Notify Rated
                                </label>
                            </div>
                            <div class="">
                                <label>
                                    <input type="checkbox" name= 'notify_answered' class="" @if($user->notify_answered ==1) checked @endif /> Notify Answered
                                </label>
                            </div>
                            <div class="">
                                <label>
                                    <input type="checkbox" name= 'notify_viewed' class="" @if($user->notify_viewed ==1) checked @endif/> Notify No. Views Daily
                                </label>
                            </div>
                        </div>
                    </div><br><br>

                    <div class="form-group" style="margin-top: 5%;">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">[Announcement]</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="">
                                <label>
                                    <input type="checkbox" name= 'email_announce' class="" @if($user->announce_email ==1) checked @endif /> Send Email
                                </label>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="form-group" style="margin-top: 5%;">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">[Report]</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="">
                                <label>
                                    <input type="checkbox" name='email_report' class="" @if($user->email_report ==1) checked @endif /> Send Email Weekly
                                </label>
                            </div>
                        </div>
                    </div>
                    <br><br><hr>

                    <div class="pull-left">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{Form::submit('Submit', array('class'=>'btn btn-primary'))}}
                    </div>

                {!! Form::close() !!}

                @endif
            </div>
        </div>
    </div>
</div>

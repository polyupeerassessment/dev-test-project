<div class="modal fade" id="nav_mission" tabindex="-1" role="dialog" aria-labelledby="MissionModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="MissionModalLabel">Mission</h4>
            </div>
            <div class="modal-body">
                <ul class="list-unstyled" role="menu">
                    <li>
                        <div class="text-center">
                            <strong>Coming Soon!</strong>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

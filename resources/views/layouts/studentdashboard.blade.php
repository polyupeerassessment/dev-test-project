<?php

if(Auth::check()) { //Update last Activity date for checking if user is in online or offline
    $db = new App\Http\Controllers\DatabaseCapture();
    $db->updateUserLastActivityDate(Auth::id());
}

?>

<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<link rel="stylesheet" href="{{asset('css/dashboard/bootstrap.min.css')}}" />
	<link rel="stylesheet" href="{{asset('css/dashboard/font-awesome.min.css')}}" />
	<link rel="stylesheet" href="{{asset('css/dashboard/nprogress.css')}}" />
	<link rel="stylesheet" href="{{asset('css/dashboard/daterangepicker.css')}}" />
	<link rel="stylesheet" href="{{asset('css/dashboard/dropzone.min.css')}}" />
	<link rel="stylesheet" href="{{asset('css/dashboard/pnotify.buttons.css')}}" />
	<link rel="stylesheet" href="{{asset('css/dashboard/pnotify.css')}}" />
	<link rel="stylesheet" href="{{asset('css/dashboard/pnotify.nonblock.css')}}" />
	<link rel="stylesheet" href="{{asset('css/dashboard/bootstrap-datetimepicker.css')}}" />

    <link rel="stylesheet" href="{{asset('css/dashboard/dataTables.bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/buttons.bootstrap.min.css')}}" />
    @yield('css')
	<link rel="stylesheet" href="{{asset('css/dashboard/custom.min.css')}}" />

	<style>
		.breadcrumb {
			/*padding: 8px 15px;*/
			/*margin-bottom: 20px;*/
			/*list-style: none;*/
			background-color: transparent;
			/*border-radius: 4px;*/
		}
		.breadcrumb > .active {
			font-weight:bold;
		}
		.question_content {
			word-wrap: break-word;
		}
		.question_content h1 {
			font-size:	40px;
		}
		.question_content h2 {
			font-size:	30px;
		}
		.question_content h3 {
			/*font-size:	30px;*/
		}
		.question_content a {
			text-decoration: none;
			color:cornflowerblue;
			border-bottom: 1px solid black;
		}
		.question_content img{
			max-width:100%;
			max-height:400px;
		}
	</style>

	<script src="{{asset('js/dashboard/jquery.min.js')}}"></script>

    <script src="{{asset('js/jquery.tagsinput.js') }}"></script>
	<title>{{config('app.name')}}</title>

	<link rel="icon" href="{!! asset('images/title-icon.png') !!}"/>
</head>

<body class="nav-md">

	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col" style="position: fixed;">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a  class="site_title">
							<span>PeerU Student</span>
						</a>
					</div>
					<div class="clearfix"></div>
					<br />

					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<ul class="nav side-menu">
								<li><a href="{{route('student.home')}}"><i class="fa fa-home"></i> Home</a></li>

								@if(session('courseID'))
								<li>
									<a>
										<i class="fa fa-graduation-cap"></i> {{session('courseID') }}
										<span class="fa fa-chevron-down"></span>
									</a>
									<ul class="nav child_menu">
										<li>
											<a href="{{route('student.dashboard', ['courseID'=>session('courseID')])}}">Home</a>
										</li>
										<li>
											<a href="{{ route('question.voting.questions') }}"><span >Voting </span></a>
										</li>
										<li>
											<a href="{{ route('question.create') }}">Create Question</a>
										</li>
										<li>
											<a href="{{ route('question.showquestion') }}">Show Question</a>
										</li>
										<li>
											<a href="{{ route('question.my') }}">My Question</a>
										</li>
                                        <li>
											<a href="{{ route('question.answered') }}">Answered Question</a>
										</li>
										<li>
											<a href="{{ route('discussion.index') }}">Forum</a>
										</li>
										<li>
											<a href="{{ route('question.exercise') }}" style="color:orange">Exercise</a>
										</li>
										<li>
											<a href="{{ route('question.quiz') }}" style="color:greenyellow">Quiz</a>
										</li>
										<li>
											<a href="{{ route('question.assignment.show') }}" style="color:orangered">Assignment<span class="label label-success pull-right">New</span></a>
										</li>
                                        <li>
											<a href="{{ route('student.exitCourse') }}" style="">Exit Course</a>
										</li>
									</ul>
								</li>
								@endif

								<li>
									<a>
										<i class="fa fa-desktop"></i> System
										<span class="fa fa-chevron-down"></span>
									</a>
									<ul class="nav child_menu">
										<li>
											<a href="{{ route('student.info') }}">Student Info</a>
										</li>
										<li>
											<a href="{{ route('student.enroll') }}">Student Enroll Course</a>
										</li>
										<li>
											<a href="{{ route('student.announcement') }}">Mail Box</a>
										</li>
									</ul>
								</li>

								<li>
										<a href="{{ route('student.usermanual') }}">
											<i class="fa fa-question"></i> User Manual
										</a>
	
											{{--<li>--}}
												{{--<a href="{{ route('student.feedback') }}">Feedback</a>--}}
											{{--</li>--}}
										</ul>
									</li>
								{{--<li><a><i class="fa fa-clone"></i>About<span class="fa fa-chevron-down"></span></a>--}}
									{{--<ul class="nav child_menu">--}}
										{{--<li><a href="#">Features</a></li>--}}
									  {{--<li><a href="#">User Guide</a></li>--}}
									  {{--<li><a href="#">Contact</a></li>--}}
									{{--</ul>--}}
								  {{--</li>--}}

							</ul>
						</div>
						{{--  <div class="menu_section">
							<h3>Accouncement</h3>
							<ul class="nav side-menu">
								<li>
									<a>
										<i class="fa fa-envelope-o"></i> Mail
										<span class="fa fa-chevron-down"></span>
										<span class="label label-success pull-right">Coming Soon</span>
									</a>
									<ul class="nav child_menu">
										<li>
											<a href="#">Show</a>
										</li>
										<li>
											<a href="#">Send</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="#">
										<i class="fa fa-bell"></i> Notification
										<span class="label label-success pull-right">Coming Soon</span>
									</a>
								</li>
							</ul>
						</div>  --}}
					</div>
					<!-- /sidebar menu -->

					<!-- /menu footer buttons -->
					<div class="sidebar-footer hidden-small">
						<a data-toggle="modal" data-placement="top" title="Settings" data-target="#nav_setting">
							<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="User Manual" href="{{ route('student.usermanual') }}">
							<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Mail" href="mailto:comp.peerassessment@gmail.com">
							<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Log Out" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
							<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
						</a>
					</div>
					<!-- /menu footer buttons -->
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav>
						<div class="nav toggle">
							<a id="menu_toggle">
								<i class="fa fa-bars"></i>
							</a>
						</div>

						<ul class="nav navbar-nav navbar-right">
							<li class="">
								<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									{{Auth::user()->nickname}}
									<span class=" fa fa-angle-down"></span>
								</a>
								<ul class="dropdown-menu dropdown-usermenu pull-right">
									<li>
										<a href="{{ route('student.info') }}"> Profile</a>
									</li>
									<li>
										<a data-toggle="modal" data-target="#nav_setting">
											<span>Settings</span>
										</a>
									</li>
									<li>
										<a data-toggle="tooltip" data-placement="top" data-placement="top" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
											<i class="fa fa-sign-out pull-right"></i> Log Out
										</a>
										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;" class="dropdown-item">
											{{ csrf_field() }}
										</form>
									</li>
								</ul>
							</li>

							<li role="presentation" class="dropdown">
								<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
									<i class="fa fa-envelope-o"></i>
									<span class="badge bg-green">{{count(auth()->user()->unreadNotifications) }}</span>
								</a>
								<?php $count_notification = 0; $max_notification = 5; ?>
                                @if (sizeof(auth()->user()->unreadNotifications) > 0)
								<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                    @foreach(auth()->user()->unreadNotifications as $notification)

										<?php $count_notification+=1;?>
										@if($count_notification>$max_notification)
											@break
										@endif

										<li>
                                        	{{--@include('student.notification.'.snake_case(class_basename($notification->type)))--}}
                                        	@include('student.notification.notify_student')
                                    	</li>
									@endforeach
									@if($count_notification>=$max_notification)
									<li>
										<div class="text-center">
											<a href="{{ route('student.announcement') }}">
												<strong>See More</strong>
												<i class="fa fa-angle-right"></i>
											</a>
										</div>
									</li>
									@endif
								</ul>
                                @endif
							</li>

							{{--  <li role="presentation">
								<a data-toggle="modal" data-target="#nav_mission">
									<i class="fa fa-list-alt"></i>
								</a>
							</li>  --}}
							
						</ul>
					</nav>
				</div>
			</div>

			<!-- /top navigation -->

			<!-- page content -->
			<div>
				@include('layouts.partials.setting')
				@include('layouts.partials.mission')
			</div>
			@yield('content')
			<!-- /page content -->
		</div>

		<!-- footer content -->
		<footer class="footer">
			<div class="pull-right">
				Created By
				<a href="#">PeerU</a>
			</div>
			<div class="clearfix"></div>
		</footer>
		<!-- /footer content -->
	</div>


	{{-- <script src="{{asset('js/dashboard/jquery.min.js')}}"></script>--}}
	<script src="{{asset('js/dashboard/bootstrap.min.js')}}"></script>

	<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>

	<script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>

	<script src="{{asset('js/dashboard/dataTables.buttons.min.js')}}"></script>

	<script src="{{asset('js/dashboard/buttons.bootstrap.min.js')}}"></script>


	<script src="{{asset('js/dashboard/buttons.html5.min.js')}}"></script>
	<script src="{{asset('js/dashboard/buttons.print.min.js')}}"></script>

	<script src="{{asset('js/dashboard/fastclick.js')}}"></script>
	<script src="{{asset('js/dashboard/nprogress.js')}}"></script>
	<script src="{{asset('js/dashboard/Chart.min.js')}}"></script>
	<script src="{{asset('js/dashboard/jquery.sparkline.min.js')}}"></script>


	<script src="{{asset('js/dashboard/jquery.flot.js')}}"></script>
	<script src="{{asset('js/dashboard/jquery.flot.pie.js')}}"></script>
	<script src="{{asset('js/dashboard/jquery.flot.resize.js')}}"></script>
	<script src="{{asset('js/dashboard/jquery.flot.stack.js')}}"></script>
	<script src="{{asset('js/dashboard/jquery.flot.time.js')}}"></script>

	<script src="{{asset('js/dashboard/jquery.flot.orderBars.js')}}"></script>
	<script src="{{asset('js/dashboard/jquery.flot.spline.min.js')}}"></script>
	<script src="{{asset('js/dashboard/curvedLines.js')}}"></script>

	<script src="{{asset('js/dashboard/date.js')}}"></script>
	<script src="{{asset('js/dashboard/moment.min.js')}}"></script>
	<script src="{{asset('js/dashboard/daterangepicker.js')}}"></script>
	<script src="{{asset('js/dashboard/jquery.smartWizard.js')}}"></script>
	<script src="{{asset('js/dashboard/dropzone.min.js')}}"></script>

	<script src="{{asset('js/dashboard/pnotify.js')}}"></script>
	<script src="{{asset('js/dashboard/pnotify.buttons.js')}}"></script>
	<script src="{{asset('js/dashboard/pnotify.nonblock.js')}}"></script>


	<script src="{{asset('js/dashboard/morris.min.js')}}"></script>
	<script src="{{asset('js/dashboard/raphael.min.js')}}"></script>

	<script src="http://malsup.github.com/jquery.form.js"></script>

	@yield('javascript')

	<script src="{{asset('js/dashboard/custom.js')}}"></script>

	<script type="text/javascript">
		showAdvanceOption = false;
        //bkLib.onDomLoaded(function() {
        //    new nicEditor().panelInstance('area1');
        //});

        //$( "#choice_type" ).change(function()
        //{
        //    select = $(this).val();
        //    switch(parseInt(select)){
        //        case 0:
        //            $('#alternative_board_mc').show();
        //            $('#alternative_board_tf').hide();
        //            break;
        //        case 1:
        //            $('#alternative_board_mc').hide();
        //            $('#alternative_board_tf').show();
        //            break;
        //        case 2:
        //            $('#alternative_board_mc').hide();
        //            $('#alternative_board_tf').hide();
        //            break;
        //        default:
        //            break;
        //    }
        //});

        //$("#showadvanceoption").click(function(){
        //    showAdvanceOption = !showAdvanceOption;
        //    if(showAdvanceOption) {
        //        $('#advancedoption_contents').show();
        //    } else {
        //        $('#advancedoption_contents').hide();
        //    }
        //});
        $('.star_input_modal').click(function(){
            var star = parseFloat($(this).val());

            //remove all star
            $("label").each(function() {
                $(this).removeAttr('id');
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('insertrating')}}",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                data: {'star': star, 'questionID': $('#questionID').val(), 'course_id':'{{Session::get('courseID')}}', 'user_id':'{{Auth::id()}}' },
                success:function(response){
                    setTimeout(function(){
                        $('.rating').html('<h1>Thank you!</h1><p>Processing....</p>');
					}, 1000);
                    location.reload();
                }
            });


        });

        $('.star_input').click(function(){
            var star = parseFloat($(this).val());

            //remove all star
            $("label").each(function() {
                $(this).removeAttr('id');
            });


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('insertrating')}}",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                data: {'star': star, 'questionID': $('#questionID').val(), 'course_id':'{{Session::get('courseID')}}', 'user_id':'{{Auth::id()}}' },
                success:function(response){
                    //console.log(response);
                }
            });

			$('#pleaseRatetxt').text('');
            $('#thankurating').html('<h4>Thank you!</h4>');
            $('.rating').html('<i class="fa fa-check-circle-o fa-4x" style="color:green;" aria-hidden="true"></i>')
        });

        start = Date.now();
        $('#answerForm').submit(function(e){
            end = Date.now();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('inserttime')}}",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                data: {'timeSpent': end - start, 'questionID': $('#questionID').val(), 'course_id':'{{Session::get('courseID')}}', 'user_id':'{{Auth::id()}}' },
                success:function(response){
                    console.log(response);
                }
            });
        });

        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });

        $(document).ready(function(){
            //$('#advancedoption_contents').hide();
            //select = $('#choice_type').find(":selected").val();

            $("body").tooltip({ selector: '[data-toggle=tooltip]' });

            //switch(parseInt(select)){
            //    case 0:
            //        $('#alternative_board_mc').show();
            //       $('#alternative_board_tf').hide();
            //        break;
            //    case 1:
            //        $('#alternative_board_mc').hide();
            //        $('#alternative_board_tf').show();
            //        break;
            //
            //    case 2:
            //        $('#alternative_board_mc').hide();
            //        $('#alternative_board_tf').hide();
            //        break;
            //    default:
            //        break;
            //}
        });
	</script>

	@if(session('success'))
		<script type='text/javascript'>
            $(document).ready(function(e) {
                generate_PNotify_success('{{session('success')}}');
            });
		</script>
	@endif

	@if(session('similar'))
		<script type='text/javascript'>
            $(document).ready(function(e) {
                generate_PNotify('Your question is very similar with other question(s). Please create different question.');
            });
		</script>
	@endif

	@if(session('fail'))
		<script type='text/javascript'>
            $(document).ready(function(e) {
                generate_PNotify('{{session('fail')}}');
            });
		</script>
	@endif


	<script type="text/javascript">
		function onAddTag(tag) {
            alert("Added a tag: " + tag);
        }
        function onRemoveTag(tag) {
            alert("Removed a tag: " + tag);
        }

        function onChangeTag(input,tag) {
            alert("Changed a tag: " + tag);
        }

        $(function() {
            $('#tags_1').tagsInput({width:'auto'});

            $(document).on('click', '.removemycomment', function(e){
                e.preventDefault();
                var comment_id = $(this).data('filter');
                console.log(comment_id);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('[name="_token"]').val()
                    },
                    method:'POST',
                    url:"{{route('question.removemycomment_question')}}",
                    beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    data: {'comment_id':comment_id, 'questionID': $('#questionID').val(), 'course_id':'{{Session::get('courseID')}}' },
                    success:function(response){
                        $('div').find('#messages_ajax').html(response['comments']);
                    }
                });
            });

            $("#messageForm").submit(function(e) {
                //console.log($('input#questionID').val());
                $test='';
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('[name="_token"]').val()
                    },
                    method:'POST',
                    url:"{{route('question.insertcomment')}}",
                    beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    data: {'message':$('#detail-message').val(), 'questionID': $('#questionID').val(), 'course_id':'{{Session::get('courseID')}}' },
                    success:function(response){
                        $('div').find('#messages_ajax').html(response['comments']);
                    }
                });

                $('#detail-message').val('');
                console.log($test);

                e.preventDefault(); // avoid to execute the actual submit of the form.
            });


        });
	</script>

</body>

</html>
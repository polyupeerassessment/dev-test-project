<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="{{asset('css/home/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/home/stylish-portfolio.min.css')}}" />
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <title>{{config('app.name')}}</title>
    <link rel="icon" href="{!! asset('images/title-icon.png') !!}"/>
    <style>
        .masthead{
            background-image: url("{{asset('images/banner.jpg ')}}");
            padding: 3rem;
        }

        #head-btn{
            margin-left: 1%;
            margin-right: 1%;
        }

        .content-section{
            padding: 5rem;
        }

        #contact{
            margin: 0;
        }

        #profile{
            padding: 3rem;
        }

        #teacher-description{
            text-align: right;
        }

        @media only screen and (max-width: 992px){
            #student-description{
                text-align: center;
            }
            #teacher-description{
                text-align: center;
            }
        }

        #sidebar-red{
            background: #5491B7;
        }

        .callout{
            padding: 0;
        }

        #forgot{
            text-align: right;
        }

        #sidebar-wrapper{
            z-index: 100;
        }

        #anchor-text{
            color:#5A3529;
        }

        .stafflogin{
            background-color: #B2A5A0;
            border-color: #B2A5A0;
            color: #F7EBE7;
        }

        .stafflogin:hover{
            background-color: #928A88;
            border-color: #928A88;
            color: #F7EBE7;
        }

        .head{
            color: #495057;
        }

        .register{
            background-color: #464a4e;
            border-color: #464a4e;
        }

        .std-slide{
            background-color: #4fabc7;
        }

        .stafflogin{
            background-color: #688b96;
        }

    </style>
</head>

<body>
<!-- Navigation -->
<a class="menu-toggle rounded" href="#">
    <i class="fa fa-bars"></i>
</a>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
            <a class="js-scroll-trigger" href="#page-top">PeerU</a>
        </li>
        <li class="sidebar-nav-item">
            <a class="js-scroll-trigger" href="#page-top">Home</a>
        </li>
        <li class="sidebar-nav-item">
            <a class="js-scroll-trigger" href="#about">About</a>
        </li>
        <li class="sidebar-nav-item">
            <a class="js-scroll-trigger" href="#features">Features</a>
        </li>
        <li class="sidebar-nav-item">
            <a class="js-scroll-trigger" href="#userguide">User Guide</a>
        </li>
        <li class="sidebar-nav-item">
            <a class="js-scroll-trigger" href="#contact">Contact</a>
        </li>
        <li class="sidebar-nav-item">
            <a class="std-slide js-scroll-trigger" href="{{ route('login') }}">Student Login</a>
        </li>
        {{--<li class="sidebar-nav-item">--}}
            {{--<a class="std-slide js-scroll-trigger" href="{{ route('register') }}">Register</a>--}}
        {{--</li>--}}
        <li class="sidebar-nav-item">
            <a class="stafflogin js-scroll-trigger" href="{{ route('teacher-login') }}">Staff Login</a>
        </li>
    </ul>
</nav>

<!-- Header -->
<header class="masthead d-flex " id="page-top">
    <div class="container text-center my-auto">
        <h1 class="mb-1 head">PeerU</h1>
        <h5 class="mb-5 head">
            <em>Peer Assessment Platform</em>
        </h5>
<div class="row">
<div class="col-lg-3 col-md-3 mb-3 mb-lg-3"></div>
<div class="col-lg-6 col-md-6 mb-6 mb-lg-6 login_wrapper">
		<div class="animate form login_form">
			<section class="login_content">
				<form class="form-horizontal" method="POST" action="{{ route('login') }}">
					{{ csrf_field() }}

					{{--  <div class="form-group">
						@if ($errors->has('fail'))
						<div class="alert alert-danger">
							<span class="help-block">
								<strong>{{ $errors->first('fail') }}</strong>
							</span>
						</div>
						@endif
					</div>  --}}

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<div class="col-md-12">
							@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
							<div class="input-group">
								<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required autofocus>
							</div>
						</div>
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<div class="col-md-12">
							<div class="input-group">
								<input id="password" type="password" class="form-control" name="password" placeholder="Password" required> 
                                @if ($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
								@endif
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class='col-md-6'>
                            <div style='font-size:90%'>
								<label>
									<input type="checkbox" name="remember" {{ old( 'remember') ? 'checked' : '' }}> Remember Me
								</label>
							</div>
                            </div>

                            <div class='col-md-6'>
                            <small>
								<a id="anchor-text" href="{{ route('password.request') }}">Forgot Your Password?</a><br/>
							</small>

                            </div>
						</div>
					</div>
					<br />
					<div class="form-group">
						<div class="">
							<button type="submit" class="btn btn-light btn-lg">Login</button>
                            {{--<a id="head-btn" class="register btn btn-dark btn-lg js-scroll-trigger" href="{{ route('register') }}">Register</a>--}}
						</div>
					</div>
				</form>

                
			</section>
		</div>
        </div>
    <div class="col-lg-3 col-md-3 mb-3 mb-lg-3"></div>
    </div>


    </div>
    <div class="overlay"></div>



</header>

<!-- About -->
<section class="content-section bg-light" id="about">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-10 mx-auto">
                <h2>PeerU - Peer Assessment Platform</h2>
                <p class="lead mb-5">PeerU is a e-Learning platform designed to help students create effective learning environment with numerous materials and tools. </p>
            </div>
        </div>
    </div>
</section>

<!-- Services -->
<section class="content-section bg-primary text-white text-center" id="features">
    <div class="container">
        <div class="content-section-heading">
            <h2 class="mb-5">Features</h2>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
                <img src="{{asset('images/collaborative-icon.png ')}}" class="service-icon rounded-circle mx-auto mb-3">
                <h4>
                    <strong>Collaborative Learning</strong>
                </h4>
                <p class="text-faded mb-0">Students learn from each other through the platform while contributing to the course at the same time.</p>
            </div>
            <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
            <img src="{{asset('images/recommender-icon.png ')}}" class="service-icon rounded-circle mx-auto mb-3">
              <i class="icon-pencil"></i>
                <h4>
                    <strong>Recommender System</strong>
                </h4>
                <p class="text-faded mb-0">PeerU suggests sets of questions designed for each student based on their strengths and weaknesses on different topics related to the course.</p>
            </div>
            <div class="col-lg-3 col-md-6 mb-5 mb-md-0">
            <img src="{{asset('images/progress-icon.png ')}}" class="service-icon rounded-circle mx-auto mb-3">
              <i class="icon-like"></i>
                <h4>
                    <strong>Progress and Statistics</strong>
                </h4>
                <p class="text-faded mb-0">With statistical analysis, Students can know more about their learning progress, and also evaluate their learning outcomes toward their goal.</p>
            </div>
            <div class="col-lg-3 col-md-6">
            <img src="{{asset('images/discussion-icon.png ')}}" class="service-icon rounded-circle mx-auto mb-3">
              <i class="icon-mustache"></i>
                <h4>
                    <strong>Discussion and Share</strong>
                </h4>
                <p class="text-faded mb-0">PeerU provides platform for learners to share their idea and discuss different topics with their peers. They can learn and get some insights from others.</p>
            </div>
        </div>
    </div>
</section>

<!-- Callout -->
<section class="callout" id="userguide">
        <video width="100%" height="auto" controls>
        <source src="{{asset('images/userguide.mp4 ')}}">
        </video>
</section>

<!-- Call to Action -->
<section class="content-section bg-primary text-white">
    <div class="container text-center">
        <h2 class="mb-4">Find out more on the features of PeerU!</h2>
        <a href="#" class="btn btn-xl btn-dark">Click Me!</a>
    </div>
</section>

<!-- Portfolio -->
<section class="content-section" id="contact">
    <div class="container">
        <div class="content-section-heading text-center">
            <h2 class="mb-5">Contact</h2>
            <br/>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-6" id="student-description">
              <span class="caption">
                <span class="caption-content">
                  <h2>Developers</h2>
                  <p class="mb-0">
                  Administrator: <a href="mailto:omp.peerassessment@gmail.com">comp.peerassessment@gmail.com</a><br/>
                  Masamichi Tanaka: <a href="mailto:masamichi.tanaka@connect.polyu.hk">masamichi.tanaka@connect.polyu.hk</a><br/>
                  Alfred, Poon Chun Long: <a href="mailto:chunlong0429@gmail.com">14005104d@connect.polyu.hk</a><br/>
                  Leo, Lee Ho Yin: <a href="mailto:yin.08.lee@connect.polyu.hk">yin.08.lee@connect.polyu.hk</a><br/>
                  </p>
                </span>
              </span>
              <br/>
              <br/>
            </div>
            <div class="col-lg-6" id="teacher-description">
              <span class="caption">
                <span class="caption-content">
                  <h2>Supervisor and Staff</h2>
                  <p class="mb-0">
                  Dr. Ng To Yee, Vincent: <a href="mailto:cstyng@comp.polyu.edu.hk">cstyng@comp.polyu.edu.hk</a>
                  <br/>
                  Rufus, Huang Li Feng: <a href="mailto:lifeng.huang@polyu.edu.hk">lifeng.huang@polyu.edu.hk</a>
                  
                  </p>
                </span>
              </span>
            </div>
        </div>
    </div>
</section>


<!-- Footer -->
<footer class="footer text-center bg-primary text-white">
    <div class="container">
        {{--  <ul class="list-inline mb-5">
            <li class="list-inline-item">
                <a class="social-link rounded-circle text-white mr-3" href="#">
                    <i class="icon-social-facebook"></i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="social-link rounded-circle text-white mr-3" href="#">
                    <i class="icon-social-twitter"></i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="social-link rounded-circle text-white" href="#">
                    <i class="icon-social-github"></i>
                </a>
            </li>
        </ul>  --}}
        <p class="small mb-0">Copyright &copy; PeerU 2017-2018</p>
    </div>
</footer>

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>

<!-- jQuery -->
<script src="{{asset('js/home/jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="{{asset('js/home/bootstrap.min.js')}}"></script>
<script src="{{asset('js/home/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('js/home/jquery.easing.min.js')}}"></script>
<script src="{{asset('js/home/stylish-portfolio.min.js')}}"></script>

</body>

{{--  Style  --}}

</html>

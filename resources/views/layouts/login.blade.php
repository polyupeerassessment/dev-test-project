<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link rel="stylesheet" href="{{asset('css/home/bootstrap.min.css')}}" />
	<link rel="stylesheet" href="{{asset('css/dashboard/font-awesome.min.css')}}" />
	<link rel="stylesheet" href="{{asset('css/dashboard/custom.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/peer-assessment.css')}}" />
	<title>{{config('app.name')}}</title>
</head>

<body class="login">
	<main role="main" style="flex-grow:1;">
		@yield('content')
	</main>
</body>	

</html>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link rel="stylesheet" href="{{asset('css/home/bootstrap.min.css')}}" />
	<link rel="stylesheet" href="{{asset('css/dashboard/font-awesome.min.css')}}" />
	<link rel="stylesheet" href="{{asset('css/dashboard/custom.min.css')}}" />
	<link rel="stylesheet" href="{{asset('css/animate.min.css')}}" />
	<title>{{config('app.name')}}</title>
	<link rel="icon" href="{!! asset('images/title-icon.png') !!}"/>
</head>

<body class="login">
	<main role="main" style="flex-grow:1;">
		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
					<form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
						{{ csrf_field() }}

						<div class="form-group">
							<h1>Reset Password</h1>
						</div>
						@if (session('status'))
						<div class="alert alert-success" style="text-shadow: 0 0px 0 white">
							{{ session('status') }}
						</div>
						@endif

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="row">
							<div class="col-md-12">
								<div class="input-group">
									<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address"
									 required>
								</div>
								@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
								@endif
							</div>
                            </div>
						</div>

						<div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-success btn-block"> Send Password Reset Link</button>
                                </div>
                            </div>
						</div>
                    
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('login') }}" class="pull-right">Back to login</a>
                                </div>
                            </div>
                        </div>

					</form>
				</section>
			</div>
		</div>
	</main>
</body>

</html>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link rel="stylesheet" href="{{asset('css/home/bootstrap.min.css')}}" />
	<link rel="stylesheet" href="{{asset('css/dashboard/font-awesome.min.css')}}" />
	<link rel="stylesheet" href="{{asset('css/dashboard/custom.min.css')}}" />
	<link rel="stylesheet" href="{{asset('css/animate.min.css')}}" />
	<title>{{config('app.name')}}</title>
	<link rel="icon" href="{!! asset('images/title-icon.png') !!}"/>
</head>

<body class="login">
	<main role="main" style="flex-grow:1;">
		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
					<form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
						{{ csrf_field() }}

						<div class="form-group">
							<h1>Reset Password</h1>
						</div>

                        <input type="hidden" name="token" value="{{ $token }}">
                        
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label>E-Mail Address</label>
							<input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
                      
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        
						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<label for="password">Password</label>
							<input id="password" type="password" class="form-control" name="password" required>
                        
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>

						<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
							<label for="password-confirm">Confirm Password</label>
							<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        
                            @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                            @endif
                        </div>
	
						<div class="form-group">
							<button type="submit" class="btn btn-success">Reset Password</button>
                        </div>
					</form>
				</section>
			</div>
		</div>
	</main>
</body>

</html>

@extends('layouts.studentdashboard') 

@section('content')
<?php use Carbon\Carbon ?>
<!-- page content -->
<div class="right_col" role="main">
	{!! Breadcrumbs::render('discussion') !!}
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>Discussion Room</h2>
					<a href="{{route('discussion.createpage')}}" class="btn btn-primary pull-right navbar-right">Create Thread</a>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="list-group">
						@forelse($threads as $thread) 
							@if($thread->author_id == Auth::id())
								<div class="panel panel-default">
							@else
								<div class="panel panel-info">
							@endif
								<div class="panel-heading">
									<h3 class="panel-title">
										<a href="{{route('discussion.show',$thread->id)}}">{{$thread->topic}}</a>
										<small>By {{$thread->nickname}}</small>
										<div class="pull-right">
											<small>{{ Carbon::parse($thread->created_at)->diffForHumans() }}</small>
										</div>
									</h3>
								</div>
								<div class="panel-body">
									<p>{{str_limit($thread->contents,100)}}
										<br>

									</p>
								</div>
							</div>
						@empty
							<h5>No threads</h5>
						@endforelse
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@endsection
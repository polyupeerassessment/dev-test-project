@extends('layouts.studentdashboard')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <h3>Create Thread</h3>

        <div class="clearfix"></div>

        <div class="row">
            {{--My Courses--}}
            <div class="col-md-13 col-sm-13 col-xs-13">
                <div class="x_panel">
                    <br />
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            {!! Form::open(['route' => 'discussion.insertthread']) !!}

                                {{--SUBJECT--}}
                                <div class="form-group">
                                    {{Form::label('topic', 'Topic: ', array('class'=>'form-createquestion-label'))}}
                                    <i class="fa fa fa-question-circle-o" data-toggle="tooltip" data-placement="right" title=""></i>
                                    {{Form::text('topic', null, array('class' => 'form-control'))}}
                                    @if ($errors->has('topic'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('topic') }}</strong>
                                </span>
                                    @endif
                                </div>

                                {{--CONTENTS--}}
                                <div class="form-group">
                                    {{Form::label('contents', "Contents:" , array('class'=>'form-createquestion-label'))}}
                                    <i class="fa fa fa-question-circle-o" data-toggle="tooltip" data-placement="right" title=""></i>
                                    {{Form::textarea('contents', null, array('class' => 'form-control'))}}
                                    @if ($errors->has('contents'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('contents') }}</strong>
                                </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    {{ Form::submit('Create', array('class'=>'btn btn-success btn-md btn-black'))}}
                                    {{ Form::reset('Reset',array('class'=>'btn btn-md btn-default'))}}
                                </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

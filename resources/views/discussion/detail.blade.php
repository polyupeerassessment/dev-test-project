@extends('layouts.studentdashboard')
<?php use Carbon\Carbon;?>
@section('content')
    @if ($errors->has('message'))
        <script type='text/javascript'>
            $(document).ready(function(e) {
                generate_PNotify('Message is empty');
            });
        </script>
    @endif

    @if(session('fail'))

    @endif

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="row" style="margin-top:5%;">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="">

                <div class="content-wrap well">
                    <h1>{{$thread->topic}}</h1>

                    <div class="thread-details">
                        {{--{!! \Michelf\Markdown::defaultTransform($thread->thread)  !!}--}}
                        {{$thread->contents}}
                    </div>
                    <br>

                    @if(Auth::id() == $thread->author_id)
                        {{--@can('update',$thread)--}}
                        <div class="actions">

                            {{--<a href="#" class="btn btn-info btn-xs">Edit</a>--}}

                            {{--//delete form--}}
                            {!! Form::open(['route' => 'discussion.threaddelete']) !!}
                                {{csrf_field()}}
                                <input  type="hidden" name= 'id' value="{{$thread->id}}">
                                <input class="btn btn-xs btn-danger" type="submit" value="Delete">
                            {!! Form::close() !!}

                        </div>
                        {{--@endcan--}}
                    @endif
                </div></div>
            </div>

            <div  class="col-md-13 col-sm-13 col-xs-13">

                <div class="list-group">
                    @foreach($messages as $msg)
                        @if($thread->author_id == Auth::id())
                            <div class="panel panel-default">
                        @else
                            <div class="panel panel-primary">
                        @endif
                                <div class="panel-body">
                                    {{--<p class="pull-right">{{$msg->nickname}}</p>--}}
                                    <p>{{$msg->messages}}
                                        <br> {{(new Carbon($msg->created_at))->diffForHumans() }}
                                    </p>
                                </div>
                            </div>
                    @endforeach
                </div>

            </div>

            <div  class="col-md-13 col-sm-13 col-xs-13">
                {!! Form::open(['route' => 'discussion.insertmessage']) !!}
                    <h2>Message</h2>
                    {{Form::textarea('message', null, array('class' => 'form-control', 'rows'=>'2'))}}
                    {{Form::hidden('thread_id', $thread->id) }}
                    <br>
                    {{Form::submit('Send', array('class'=>'btn btn-success btn-black'))}}
                    {{ Form::reset('Reset',array('class'=>'btn btn-default'))}}
                {!! Form::close() !!}
                <br>

            </div>
        </div>
    </div>



@endsection

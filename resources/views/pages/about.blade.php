@extends('layouts.app')

@section('content')

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="container-fluid">
        <div class="jumbotron" style="margin-top:10px;">
            <h1 class="display-2">{{$title}}</h1>
            <p>PolyU Peer Assessment aims to increase student learning engagement. The objectives of the platform are:<br/>
            1.	To provides simple and easy-to-use user interface.<br/>
            2.	Support different types of questions<br/>
            3.	To implements better flow control on student’s peer assessment to increase assessment reliability.<br/>
            4.	To introduces communication features to support collaborative learning.<br/>
            5.	To provides comprehensive and precise evaluation of student’s performance.
            </p>
        </div>
    </div>

    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-4">
                <h2>About Us</h2>
                <p>Masamichi Tanaka<br/>Alfred Poon<br/>Leo Lee</p>
            </div>
            <div class="col-md-4">
                <h2>Our Supervisor</h2>
                <p></p>
            </div>
            <div class="col-md-4">
                <h2>Contact Us</h2>
                <p></p>
            </div>
        </div>
    </div> <!-- /container -->

@endsection

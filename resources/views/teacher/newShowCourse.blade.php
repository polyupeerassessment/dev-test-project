@extends('layouts.dashboard')

@section('css')
    <link rel="stylesheet" href="{{asset('css/dashboard/pnotify.buttons.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/pnotify.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/pnotify.nonblock.css')}}" />
@endsection

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>My Courses</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">

                        <div class="x_content">
                            <!-- start project list -->
                            <table class="table table-striped projects">
                                <thead>
                                <tr>
                                    <th style="width: 15%">Course Code</th>
                                    <th style="width: 20%">Course Name</th>

                                    <th style="width: 15%">Status</th>
                                    <th style="width: 20%">Default</th>
                                    <th style="width: 20%">Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($courses as $course)
                                    <tr>
                                        <td>{{$course->course_code}}</td>
                                        <td>
                                            <p>{{$course->course_name}}</p>
                                            <br />
                                            <small>Created {{$course->created_at}}</small>
                                        </td>
                                        <td>
                                            <p>Running</p>
                                        </td>
                                        <td>
                                            @if($course->default_session == 1)
                                                Default Course
                                            @else
                                                <button type="button" id ='default_button' class="btn btn-success btn-xs" value="{{$course->course_code}}"><i class="fa fa-pencil fa-1x"></i> Set Default</button>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{route('teacher.coursedetail', ['courseID' => $course->course_code])}}" class="btn btn-primary btn-xs"><i class="fa fa-folder fa-2x"></i> View </a>
                                            <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target=".bs-edit-{{$course->course_code}}"><i class="fa fa-pencil fa-2x"></i> Edit </button>
                                            {{--<a href="{{route('teacher.removecourse', ['id' => $course->course_code])}}" class="btn btn-danger btn-xs"></a>--}}

                                            <!-- Small modal -->
                                            <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target=".bs-{{$course->course_code}}"><i class="fa fa-trash-o fa-2x"></i> Delete </button>

                                            <div class="modal fade bs-example-modal-sm bs-{{$course->course_code}}" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">

                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                                            </button>
                                                            <h4 class="modal-title" id="myModalLabel2">Really?</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4>Are you sure to delete this course?</h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <a href="{{route('teacher.removecourse', ['id' => $course->course_code])}}" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /modals -->

                                            <div class="modal fade bs-example-modal-sm bs-edit-{{$course->course_code}}" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">

                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                                            </button>
                                                            <h3 class="modal-title" id="myModalLabel2">Course Information</h3>
                                                        </div>
                                                        <div class="modal-body">
                                                            {!! Form::open(['route' => 'teacher.updatecourse', 'class'=>"form-horizontal", 'name'=>'submit-form']) !!}


                                                            <div class="form-group">
                                                                <label class="control-label col-md-2">Course Name</label>
                                                                <div class="col-md-8">
                                                                    {{Form::text('course_name', $course->course_name, array('class'=>'form-control'))}}
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label col-md-2">Course Code</label>
                                                                <div class="col-md-8">
                                                                    {{Form::text('course_code', $course->course_code, array('class'=>'form-control'))}}
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label col-md-2">Course Description</label>
                                                                <div class="col-md-8">
                                                                    {{Form::textarea('description', $course->description, array('class'=>'form-control'))}}
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label col-md-2">Course Enroll Code</label>
                                                                <div class="col-md-8">
                                                                    {{Form::text('enroll', $course->enroll_code, array('class'=>'form-control'))}}
                                                                </div>
                                                            </div>

                                                            {{ Form::hidden('previous_course_code', $course->course_code) }}

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            {{Form::submit('Update', array('class'=>'btn btn-success'))}}
                                                            {{ Form::reset('Reset',array('class'=>'btn btn-primary'))}}

                                                            {!! Form::close() !!}
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>
                            <!-- end project list -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection


@section('javascript')
    <script src="{{asset('js/dashboard/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{asset('js/dashboard/pnotify.buttons.js')}}"></script>
    <script src="{{asset('js/dashboard/pnotify.js')}}"></script>
    <script src="{{asset('js/dashboard/pnotify.nonblock.js')}}"></script>

    <script>
        $(function() {
            $("#default_button").click(function(e) {
                {{--$test='';--}}

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('[name="_token"]').val()
                    },
                    method:'POST',
                    url:"{{route('setDefaultCourseByTeacher')}}",
                    beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    data: {'course_code':$(this).attr("value"), 'instructor_id':'{{Auth::id()}}'},
                    success:function(response){
                        console.log(response);
                        location.reload();
                    }
                });

                {{--$('#detail-message').val('');--}}


                e.preventDefault(); // avoid to execute the actual submit of the form.
            });
        });
    </script>
@endsection

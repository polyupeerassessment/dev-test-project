@extends('layouts.dashboard')

@section('css')
    <link rel="stylesheet" href="{{asset('css/dashboard/dataTables.bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/buttons.bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/fixedHeader.bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/responsive.bootstrap.min.css')}}" />
@endsection

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Your Students <small></small></h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="x_panel">
                    <div class="x_content">
                        <div class="x_content table-responsive borderless">
                            <table id="datatable-buttons-students" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                {{--id,email,num_answer,num_questions,num_answered,score,avg_rating,avg_difficulty,num_mc,num_tf,num_tips,num_explain,num_views,num_discussions,num_comments,num_thread_messages--}}
                                <tr>
                                    <th>Id</th>
                                    <th>Email</th>
                                    <th>No. Answers</th>
                                    <th>No. Questions Created</th>
                                    <th>No. Peers Answered</th>
                                    <th>Score (Correct Answer)</th>
                                    <th>Avg Rating Received</th>
                                    <th>Avg Difficulty of Questions</th>
                                    <th>No. MC Created</th>
                                    <th>No. TF Created</th>
                                    <th>No. Tips Created</th>
                                    <th>No. Explanation Created</th>
                                    <th>No. Views By Peers</th>
                                    <th>No. Thread Topic Created</th>
                                    <th>No. Comments</th>
                                    <th>No. Thread Messages</th>
                                    <th>Online Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($students as $student)
                                    <tr @if($student[16] == 1) style="color:blueviolet" @endif>
                                        @foreach($student as $i => $data)
                                            <td>{{$data}}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        if ($("#datatable-buttons-students").length) {
            $("#datatable-buttons-students").DataTable({
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
                order: [ [ 2, 'desc' ]],
                ordering: true,
                bSort : false,
            });
        }
    </script>
@endsection
@extends('layouts.dashboard')

@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Create New Quiz</h3>
            </div>
            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        {!! Form::open(['route' => 'teacher.quiz.insert', 'class'=>"form-horizontal", 'name'=>'submit-form', 'files' => true, 'enctype'=>"multipart/form-data"]) !!}
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> Schedule <span class="required">*</span></label>

                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                    <input type="text" name="schedule"  id="reportrange" value="December 30, 2014 - January 28, 2015" style="padding: 5px 10px;" />
                                    <b class="caret"></b>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Quiz Topic <span class="required">*</span>
                                </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    {{Form::text('topic', null, array('class' => 'form-control col-md-7 col-xs-12','required'))}}
                                    @if ($errors->has('topic'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('topic') }}</strong>
                                        </span>
                                    @endif

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Quiz Duration (In Mins) <span class="required">*</span>
                                </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    {{Form::number('duration', null, array('min' => '1', 'class' => 'form-control col-md-7 col-xs-12', 'required'))}}
                                    @if ($errors->has('duration'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('duration') }}</strong>
                                            </span>
                                    @endif

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Number of Questions <span class="required">*</span>
                                </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    {{Form::number('number_questions', null, array('min' => '1', 'max' => '40', 'class' => 'form-control col-md-7 col-xs-12', 'required'))}}
                                    @if ($errors->has('number_questions'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('no_questions') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Quiz Type<span class="required">*</span>
                                </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <select name="content_type" id ='content_type' class="form-control col-md-7 col-xs-12" required>
                                        <option value="auto" selected >Automatically </option>
                                        <option value="manually" >Manually </option>
                                    </select>
                                    @if ($errors->has('content_type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('content_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div><br>

                            @if ($errors->has('question_select'))
                                <div class="table-responsive" >
                                    <span class="help-block">
                                        <strong>{{ $errors->first('question_select') }}</strong>
                                    </span>
                            @else
                                <div class="table-responsive" style="display:none;" id="question_selector">
                            @endif

                                <div  style="display:block">
                                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <td>Id</td>
                                                <td>Topic</td>
                                                <td>Difficulty</td>
                                                <td>Rating</td>
                                                <td>Created At</td>
                                                <td>Type</td>
                                                <td>Author</td>
                                                <td>Action</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($questions as $question)
                                                <tr>
                                                    <td>{{$question->id}}</td>
                                                    <td>{{$question->topic}}</td>
                                                    <td @if($question->difficulty>0) style="color:orangered;" @endif>
                                                        @if($question->difficulty==0) Easy
                                                        @elseif($question->difficulty == 1) Normal
                                                        @else Hard @endif
                                                    </td>
                                                    <td>{{$question->rating}}</td>
                                                    <td>{{$question->created_at}}</td>
                                                    <td>
                                                        @if($question->type==0) MC
                                                        @else TF @endif
                                                    </td>
                                                    <td>{{$question->name}}</td>
                                                    <td><input type="checkbox" value="{{$question->id}}" name="question_select[]" /></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <br><hr>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    {{Form::submit('Create', array('class'=>'btn btn-success'))}}
                                    {{ Form::reset('Reset',array('class'=>'btn btn-primary'))}}
                                </div>
                            </div>


                        {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('js/dashboard/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/dashboard/moment.min.js') }}"></script>
    <script>
      $('.myDatepicker').datetimepicker({
          format: 'H:mm'
      });


      $(function(){
          $('#content_type').on('change', function() {
              if(this.value === 'manually') {
                $('#question_selector').show();
              } else {
                  $('#question_selector').hide();
              }
          })
      });


    </script>
@endsection


@extends('layouts.dashboard')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>Student Records</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive borderless">
                        <table id="datatable-buttons-users" class="table table-striped table-bordered ">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Nickname</th>
                               <!-- <th>Score</th>
                                <th>No. of Answers</th>
                                <th>Total No. of Views</th>
                                <th>Total No. of Questions</th>
                                <th>Last Activity Date</th> -->
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $rank = 0; $pre_value = -1; ?>
                            @foreach($students as $user)
                                <?php
                                if($pre_value != $user['score']) {
                                    $pre_value = $user['score'];
                                    $rank++;
                                }
                                ?>
                                <tr>
                                    <td>{{$user['id']}}</td>
                                    <td>{{$user['name']}}</td>
                                    <td>{{$user['nickname']}}</td>
                                   <!-- <td>{{$user['score']}}</td>
                                    <td>{{$user['num_attempts']}}</td>
                                    <td>{{$user['num_views']}}</td>
                                    <td>{{$user['num_questions']}}</td>
                                    <td>{{$user['last_activity_date']}}</td> -->
                                    <td><a href="{{route('teacher.stuCourseStat', ['std_id' => $user['id']])}}" class="btn btn-xs btn-info">View</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
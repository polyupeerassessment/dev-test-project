@extends('layouts.dashboard')
<?php use Illuminate\Support\Facades\Input; use Illuminate\Support\Facades\Log;

$placeholder_html_contents =
'<h3>Example</h3>
<br>
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/1200px-PHP-logo.svg.png" />
<br>
<p>What is an ouput of following code?</p>
<code> echo "Hello World"; </code>
<br><br><br>
<p>Please choose an answer!</p>';

$placeholder_normal_contents =
'Example

What is an ouput of following code?
echo "Hello World?"

Please choose an anseer

'; ?>

@section('css')
<link rel="stylesheet" href="{{asset('css/dashboard/green.css')}}" />
	<style>
		strong {
			color:#d43f3a;;
		}
		.fa-close {
			color:orangered;
		}
	</style>
@endsection

@section('content')

<style>
	#showadvanceoption:hover {
		background-color: lightgrey;
	}
	.form-createquestion-label {
		font-size:120%;
	}
	.choice_createquestion_form {
		height: 70px;
	}
	.white_space_form {
		white-space: pre-line
	}
</style>
@if ($errors->any())
	<script type='text/javascript'>
        $(document).ready(function(e) {
            generate_PNotify('Please complete all.');
        });
	</script>

	@php
		Log::info('[Question.Insert] [ '.Session::get('courseID').' ] ['.Auth::id().'] Insert question. User failed due to incomplete input in create question page');
	@endphp
@endif
<!-- page content -->
<div class="right_col" role="main">
	{!! Breadcrumbs::render('teacher_question_create') !!}
	<h3>Create Your Question</h3>
	<div class="clearfix"></div>
	<div class="row">
		{{--My Courses--}}
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<br />
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">

						{!! Form::open(['route' => 'teacher.insertquestion']) !!}
						<div class="form-group">
							{{Form::label('topic', 'Topic ', array('class'=>'form-createquestion-label'))}}<strong style="font-size: 120%;">*</strong>
							<i class="fa fa fa-question-circle-o" data-toggle="tooltip" data-placement="right" title="Please write the topic or title of your question."></i>
							{{Form::text('topic', null, array('class' => 'form-control','required'))}}
							@if ($errors->has('topic'))
							<span class="help-block">
								<strong>{{ $errors->first('topic') }}</strong>
							</span>
							@endif
						</div>
						<div class="form-group">
							<label class = 'form-createquestion-label'>Keywords</label><strong style="font-size: 120%;">*</strong>
							<i class="fa fa fa-question-circle-o" data-toggle="tooltip" data-placement="right" title="You can add keyword related to the question, for example, the topic covered in this question."></i>
							{{Form::text('tags', null, array('class'=>'tags', 'id'=>'tags_1'))}}
							@if ($errors->has('tags'))
								<span class="help-block">
									<strong>The keyword should fill</strong>
								</span>
							@endif
                            <div id="famous_keywords"></div>
						</div>
						<br>
						<div class="form-group" >
							{{Form::label('contents', "Contents:" , array('class'=>'form-createquestion-label'))}}<strong style="font-size: 120%;">*</strong>
							<i class="fa fa fa-question-circle-o" data-toggle="tooltip" data-placement="right" title="Please write the explaination or the question that you want people to answer.You can type HTML programming here."></i>
							{{ Form::textarea('contents','',array('rows'=>'20','id' => 'content_editor', 'class'=>'form-control','placeholder'=>$placeholder_normal_contents)) }}
							@if ($errors->has('contents'))
							<span class="help-block">
								<strong>{{ $errors->first('contents') }}</strong>
							</span>
							@endif

						</div>
						
						<input type='button' class="btn btn-default" value='Normal' id='content_type' />

						<button type="button" class="btn btn-primary content_type_1" data-toggle="collapse" data-target="#html_output_div" >Content Output</button>

						<div id="html_output_div" class="collapse">
							<br>
							<div class="text-center"><i class="fa fa-arrow-down fa-3x" aria-hidden="true"></i></div>
							<br>
							<div class='question_content white_space_form' id="go" style="min-height:250px; border:1px #ccc solid; padding: 6px 12px 6px 12px;" >
							</div>
						</div>

						<div class='content_type_1'>[<a href="https://jsfiddle.net/" style="text-decoration: underline;color:cornflowerblue;" target="_blank" >You Can Also Test Your Code Here</a>]</div><br><br>

						<br />
						<br />
						{{--ALTERNATIVES--}}
						<div class="x_title">
							<h3><i class="fa fa-bars"></i> Alternatives <strong style="font-size: 120%;">*</strong> <small style="font-size: 60%;">Please select one of the two types</small></h3>
							<div class="clearfix"></div>
						</div>
						<div class="">
							<br>
							<div class="" role="tabpanel" data-example-id="togglable-tabs">
								<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
									<li role="presentation" class="active"><a href="#tab_content1" id="mc_tab" role="tab" data-toggle="tab" aria-expanded="true">Multiple Choice</a>
									</li>
									<li role="presentation" class=""><a href="#tab_content2" role="tab" id="tf_tab" data-toggle="tab" aria-expanded="false">True / False</a>
									</li>
								</ul>
								<div id="myTabContent" class="tab-content">
									<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="mc_tab">
										@if ($errors->has('answer'))
											<span class="help-block">
												<strong>Answer fields are required</strong>
											</span>
										@endif
										<div id="mc_content">
										<?php if(!empty(Input::old('choice'))) {$len = sizeof(Input::old('choice'));} else {$len = 3;}  ?>
										@for ($i = 1; $i <=$len ; $i++)
											<div class="input-group inputmc">
												<div class="input-group-addon">
													<input type="checkbox" name="answer[]" class="flat" value='{{"answer".$i}}'>
												</div>
												<div class="">
													{{Form::text('choice[]', null, array('class' => 'form-control choice_createquestion_form', 'id'=>'mc_question'.$i,'placeholder'=>'Choice', 'required'=>'required'))}}
												</div>
											</div>
										@endfor
									</div>
									<div class="form-group">
										<button type="button" class="btn btn-link" id="addAnswerMCBtn" onclick="addAnswerMC()"><i class="fa fa-plus"></i> Add More Answer</button>
										<button type="button" class="btn btn-link" id="removeAnswerMCBtn" onclick="removeAnswerMC()" style="display: none;"><i class="fa fa-close"></i> Remove More Answer</button>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="tf_tab">
									<div class="form-group">
										<div id="tf_content">
                                            <div class="row inputtf" style="margin-bottom:5px;">
                                                <br>
                                                <div class="col-md-7 col-xs-12 col-sm-12">
                                                    <select name="tf_answer" class="form-control">
                                                        <option value="0">False</option>
                                                        <option value="1">True</option>
                                                    </select>
                                                </div>
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<br>
						<hr>
						<br>
						<br>
						<div class="accordion" id="accordionAdvancedOption" role="tablist" aria-multiselectable="true">
							<div class="panel">
								<a class="panel-heading collapsed" role="tab" data-toggle="collapse" data-parent="#accordionAdvancedOption" href="#collapseAdvancedOption"
								 aria-expanded="true" aria-controls="collapseAdvancedOption">
									<h3 class="panel-title">Advanced Option</h3>
								</a>
								<div id="collapseAdvancedOption" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="AdvancedOption">
									<div class="panel-body">
										<div id="advancedoption_contents">
											<div class="form-group">
												<div class="row">
													<div class="col-md-12 col-sm-12 col-xs-12">
													{{Form::label('number_attempts', 'Number Of Attempts: ')}}
													<i class="fa fa fa-question-circle-o" data-toggle="tooltip" data-placement="right" title="The number of attempt that a student can try to answer the question."></i>
													{{Form::number('number_attempts', 2, array('id'=>'num_attempts_id','class' => 'form-control','min'=>2, 'max'=>10))}}
													</div>
												</div>
											</div>
											<br>
											<div class="form-group">
												<div class="row">
													<div class="col-md-12 col-sm-12 col-xs-12">
														{{Form::label('tips', 'Tips ')}}
														<i class="fa fa fa-question-circle-o" data-toggle="tooltip" data-placement="right" title="When the student wrongly answer the question, tips will be displayed as hints for them to attempt the question again. Example of hints: The related topics covered in the question."></i>
														{{Form::textarea('tips', null, array('class' => 'form-control','rows'=>4))}}
													</div>
												</div>
											</div>
											<br>
											<div class="form-group">
												<div class="row">
													<div class="col-md-12 col-sm-12 col-xs-12">
													{{Form::label('explaination', 'Explanation ')}}
													<i class="fa fa fa-question-circle-o" data-toggle="tooltip" data-placement="right" title="Explain about the correct answer(s) once user answered this question"></i>
													{{Form::textarea('explaination', null, array('class' => 'form-control','rows'=>4))}}
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<div id="choice_type">{{Form::hidden('choice_type', 0) }}</div>
							<div id="content_type_hidden_value">{{Form::hidden('content_type', 1) }}</div>
							{{ Form::submit('Create', array('class'=>'btn btn-success btn-md btn-black'))}}
							{{ Form::reset('Reset',array('class'=>'btn btn-md btn-default'))}}
						</div>
						<br>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('javascript')
	<script>

        function validateContents(contents) {
            contents = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('validateQuestionContents')}}",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                data: {'contents':contents },
                success:function(response){
                    $('#go').html(response);
                }
            });


        }
        $(document).ready(function() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('famouse_keywords')}}",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                data: {'course_id':'{{Session::get('courseID')}}' },
                success:function(response){
                    $('#famous_keywords').html(response);
                }
            });


			$('#content_type').click(function(){
				if ($(this).val() == 'Normal') {
					$(this).val('HTML');
					if ($('#content_editor').val() == '') {
						$('#go').html($('#content_editor').attr('placeholder'));
					} else {
                        validateContents($('#content_editor').val());
					}

                    $('#go').removeClass('white_space_form');
                    document.getElementById("content_type_hidden_value").innerHTML = '{{Form::hidden('content_type', 1) }}';
					
				} else {
					$(this).val('Normal');
					if ($('#content_editor').val() == '') {
						$('#go').text($('#content_editor').attr('placeholder'));
					} else {
						$('#go').text($('#content_editor').val());
					}
                    $('#go').addClass('white_space_form');
                    document.getElementById("content_type_hidden_value").innerHTML = '{{Form::hidden('content_type', 0) }}';
					
				}
			});

            $('#go').text($('#content_editor').attr('placeholder'));

            $('#myNicEditor_formgroup').hide();
            $('#content_editor').keyup(function() {
				if($('#content_type').val() =='Normal') {
					$('#go').text($(this).val());
				} else {
                    validateContents($('#content_editor').val());
				}
            });

            $("select#content_type").change(function(){
                //console.log($(this).val());
                if ($(this).val() === 'normal') {
					$('#myNicEditor_formgroup').show();
                    $('#content_editor').hide();
                    $('#go').hide();
            	} else {
                    $('#myNicEditor_formgroup').hide();
                    $('#content_editor').show();
                    $('#go').show();
				}
            });

            $('#mc_tab').click(function(){
                document.getElementById("choice_type").innerHTML = '{{Form::hidden('choice_type', 0) }}';
                if ($('#num_attempts_id').val() == 1){
                    $('#num_attempts_id').val(2);
                }
                $("#num_attempts_id").attr({
                    "max" : 10,        // substitute your own
                    "min" : 2          // values (or variables) here
				});

				$('[name="choice[]"]').attr("required", "true");
            });

            $('#tf_tab').click(function(){
                document.getElementById("choice_type").innerHTML = '{{Form::hidden('choice_type', 1) }}';
                $("#num_attempts_id").val(1);
                $("#num_attempts_id").attr({
                    "max" : 1,        // substitute your own
                    "min" : 1         // values (or variables) here
				});

				$('[name="choice[]"]').removeAttr("required");
            });

        });
	</script>
	<script src="{{ asset('js/dashboard/icheck.min.js') }}"></script>
	<script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>

	<script type="text/javascript">
		function addAnswerMC() {
			var numItems = $('.inputmc').length;
			if(numItems <6)
			{
				var div = document.createElement('div');	
				div.className = '';
				div.innerHTML =
					'<div class="input-group inputmc">\
						<div class="input-group-addon">\
							<input type="checkbox" name="answer[]" class="flat" value="answer'+(numItems+1)+'">\
						</div>\
						<div class="">\
							<input name="choice[]" class="form-control choice_createquestion_form" id="mc_question' + (numItems + 1 ) + '"placeholder="Choice" type="text" required>\
						</div>\
					</div>';
	
				document.getElementById('mc_content').appendChild(div);
				$('input.flat').iCheck({
					checkboxClass: 'icheckbox_flat-green',
					radioClass: 'iradio_flat-green'
				});
			}

			if(numItems + 1 > 3){
				$("#removeAnswerMCBtn").show();
			}

			if(numItems + 1 == 6){
				$("#addAnswerMCBtn").hide();
			}
		}
		
		function removeAnswerMC() {
			var numItems = $('.inputmc').length;
			if(numItems > 3) {
				var i = 1;
				$( ".inputmc" ).last().remove();
				$('.inputmc').each(function(){
					$(this).find('input:checkbox').val('answer'+i);
					i++;
				});
				$("#addAnswerMCBtn").show();
			}

			if(numItems - 1 <= 3) {
				$("#removeAnswerMCBtn").hide();
			}
		}

		function addAnswerTF(){
			var numItems = $('.inputtf').length;
			if(numItems < 3)
			{
				var div = document.createElement('div');	
				div.className = '';
				div.innerHTML =
					'<div class="row inputtf" style="margin-bottom:5px;">\
						<div class="col-md-6">\
							<input name="tf_question[]" class="form-control" id="tf_question' + (numItems + 1 ) + '" placeholder="Question" type="text" required>\
						</div>\
						<div class="col-md-6">\
							<select name="tf_answer[]" class="form-control">\
								<option value="0">False</option>\
								<option value="1">True</option>\
							</select>\
						</div>\
					</div>';	

				document.getElementById('tf_content').appendChild(div);
			}

			if(numItems + 1 > 1){
				$("#removeAnswerTFBtn").show();
			}

			if(numItems + 1 == 3){
				$("#addAnswerTFBtn").hide();
			}
		}

		function removeAnswerTF(){
			var numItems = $('.inputtf').length;
			if(numItems > 1)
			{
				$( ".inputtf" ).last().remove();
				$("#addAnswerTFBtn").show();
			}

			if(numItems - 1 <= 1) {
				$("#removeAnswerTFBtn").hide();
			}
		}
	</script>
	
@endsection
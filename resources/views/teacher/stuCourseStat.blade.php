@extends('layouts.dashboard')

@section('css')
<style>

    .axis path,
    .axis line {
        fill: none;
        stroke: #000;
        shape-rendering: crispEdges;
    }

    .bar {
        fill: orange;
    }

    .bar:hover {
        fill: orangered ;
    }

    .x.axis path {
        display: none;
    }

    .d3-tip {
        line-height: 1;
        font-weight: bold;
        padding: 12px;
        background: rgba(0, 0, 0, 0.8);
        color: #fff;
        border-radius: 2px;
    }

    /* Creates a small triangle extender for the tooltip */
    .d3-tip:after {
        box-sizing: border-box;
        display: inline;
        font-size: 10px;
        width: 100%;
        line-height: 1;
        color: rgba(0, 0, 0, 0.8);
        content: "\25BC";
        position: absolute;
        text-align: center;
    }

    /* Style northward tooltips differently */
    .d3-tip.n:after {
        margin: -1px 0 0 0;
        top: 100%;
        left: 0;
    }

    .tile-stats .icon{
        top:35px !important;
    }

    .tile-stats .icon i{
        vertical-align: top;
    }
</style>
@endsection

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">

        {{--Total Statistic--}}
        <div class="row top_tiles">
            <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-eye"></i></div>
                    <div class="count">{{$numViews}}</div>
                    <h3>Page Views</h3>
                </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-comments-o"></i></div>
                    <div class="count">{{$numComments}}</div>
                    <h3>Comments</h3>
                </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-question-circle"></i></div>
                    <div class="count">{{$numQuestions}}</div>
                    <h3>Questions</h3>
                </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-paper-plane"></i></div>
                    <div class="count">{{$numAnswereds}}</div>
                    <h3>Answers</h3>
                </div>
            </div>
            <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-line-chart"></i></div>
                    <div class="count">{{$avgScore}}</div>
                    <h3>Score</h3>
                </div>
            </div>

            <div class="animated flipInY col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-star-half-empty"></i></div>
                    <div class="count">{{$avgRating}}</div>
                    <h3>No. Ratings</h3>
                </div>
            </div>
        </div>

        {{--End Total Statistic--}}

        {{-- Page Views Bar Chart --}}
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>Page Views</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive borderless">
                       <div id="student_page_views_stat"></div>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Page Views Bar Chart --}}

        {{-- Question Created Line Chart --}}
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>Questions Created</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive borderless">
                        <div id="student_qc_stat"></div>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Question Created Line Chart --}}

        {{-- Question Answered Line Chart --}}
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>Questions Answered</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive borderless">
                        <div id="student_qa_stat"></div>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Question Answered Line Chart --}}

        {{-- Ratings Line Chart --}}
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>Ratings</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive borderless">
                        <div id="student_rating_stat"></div>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Ratings Line Chart --}}

        {{-- Comments Line Chart --}}
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>Comments</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive borderless">
                        <div id="student_comment_stat"></div>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Comments Line Chart --}}
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        var margin = {top: 40, right: 60, bottom: 30, left: 60},
            width = 960 - margin.left - margin.right,
            height = 500 - margin.top - margin.bottom;

        //var formatPercent = d3.format(".0%");

        var x = d3.scale.ordinal()
            .rangeRoundBands([0, width], .1);

        var y = d3.scale.linear()
            .range([height, 0]);

        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom");

        var yAxis = d3.svg.axis()
            .scale(y)
            .orient("left")
            .ticks(10);

        var tip = d3.tip()
            .attr('class', 'd3-tip')
            .offset([-10, 0])
            .html(function(d) {
                return "<strong>Views:</strong> <span style='color:red'>" + d.page_views + "</span>";
            })

        var svg = d3.select("#student_page_views_stat").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        svg.call(tip);

        function buildChart(data) {
            x.domain(data.map(function(d) { return d.date; }));
            y.domain([0, d3.max(data, function(d) { return d.page_views; })]);

            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);

            svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text("Page Views");

            svg.selectAll(".bar")
                .data(data)
                .enter().append("rect")
                .attr("class", "bar")
                .attr("x", function(d) { return x(d.date); })
                .attr("width", x.rangeBand())
                .attr("y", function(d) { return y(d.page_views); })
                .attr("height", function(d) { return height - y(d.page_views); })
                .on('mouseover', tip.show)
                .on('mouseout', tip.hide)
        }

        function type(d) {
            d.frequency = +d.page_views;
            return d;
        }

        var viewData = {!! json_encode($std_veiws) !!};

        buildChart(viewData);

        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        // questions created chart
        var qc_data = {!! json_encode($qc_stat) !!};
        if(qc_data.length>0){
            Morris.Line({
                element: 'student_qc_stat',
                xkey: 'date',
                ykeys: ['q_created'],
                labels: ['Created'],
                hideHover: 'auto',
                lineColors:['#96CA59'],
                data: qc_data,
                yLabelFormat: function (y) {
                    var parts = y.toString().split(".");
                    return parts[0];
                },
                xLabelFormat: function (x) {
                    var d = new Date(x.label);
                    return d.getDate() + ' ' + months[d.getMonth()];
                },
                parseTime: false,
                resize: true
            });

        }else{
            $('#student_qc_stat').html('User has not created any question(s)');
        }

        // questions answered chart
        var qa_data = {!! json_encode($qa_stat) !!};
        if(qa_data.length>0){
            Morris.Line({
                element: 'student_qa_stat',
                xkey: 'date',
                ykeys: ['q_answered'],
                labels: ['Answered'],
                hideHover: 'auto',
                lineColors:['#96CA59'],
                data: qa_data,
                yLabelFormat: function (y) {
                    var parts = y.toString().split(".");
                    return parts[0];
                },
                xLabelFormat: function (x) {
                    var d = new Date(x.label);
                    return d.getDate() + ' ' + months[d.getMonth()];
                },
                parseTime: false,
                resize: true
            });

        }else{
            $('#student_qa_stat').html('User has not answered any question(s)');
        }

        //ratings chart
        var ratings_data = {!! json_encode($ratings_stat) !!};
        if(ratings_data.length>0){
            Morris.Line({
                element: 'student_rating_stat',
                xkey: 'date',
                ykeys: ['ratings'],
                labels: ['Ratings'],
                hideHover: 'auto',
                lineColors:['#96CA59'],
                data: ratings_data,
                xLabelFormat: function (x) {
                    var d = new Date(x.label);
                    return d.getDate() + ' ' + months[d.getMonth()];
                },
                parseTime: false,
                resize: true
            });
        }else{
            $('#student_rating_stat').html('User has not received any ratings');
        }

        //comments charts
        var comments_data = {!! json_encode($comments_stat) !!};
        if(comments_data.length>0){

            Morris.Line({
                element: 'student_comment_stat',
                xkey: 'date',
                ykeys: ['comments'],
                labels: ['Comments'],
                hideHover: 'auto',
                lineColors:['#96CA59'],
                data: comments_data,
                yLabelFormat: function (y) {
                    var parts = y.toString().split(".");
                    return parts[0];
                },
                xLabelFormat: function (x) {
                    var d = new Date(x.label);
                    return d.getDate() + ' ' + months[d.getMonth()];
                },
                parseTime: false,
                resize: true
            });
        }else{
            $('#student_comment_stat').html('User has no comments');
        }
    </script>
@endsection

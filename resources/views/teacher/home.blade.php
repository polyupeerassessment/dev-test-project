@extends('layouts.dashboard')

@section('content')

<style>
	.borderless {
		border: none;
	}
</style>
<!-- page content -->
<div class="right_col" role="main">
	<div class="page-title">
		<div class="title_left">
		</div>
	</div>

	@if(!empty($sysannounce))
		@foreach($sysannounce as $announce)
			<div class="row "  style="margin-top: 1%;margin-bottom: 1%; padding:0px; ">
				<div class="" >
					<div class="x_panel " style="border:dashed 1px  mediumvioletred">
						<div class="x_content">
							<div class="text-center">
								<i class="fa fa-exclamation-triangle fa-4x" aria-hidden="true" style="color:orangered"></i>
								<h3>{{$announce->topic}}</h3>
								<p>{{$announce->contents}}</p>
								<br>
								<p>This announcement is displayed during <br>{{date('Y:m:d', strtotime($announce->start_at))}} ~ {{date('Y:m:d', strtotime($announce->end_at))}}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endforeach
	@endif

	<div class="row">
		<div class="col-xs-12 col-md-12">
			<div class="x_panel" >
				<div class="x_title">
					<h2>Course Information</h2>
					<div class="nav navbar-right panel_toolbox">
						<button type="button"  class="btn btn-success" data-toggle="modal" data-target="#create_course_modal">Create Course</button>
						<div>
							@include('teacher.partials.CreateCourse')
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<br>
				<div class="x_content table-responsive borderless">
					<div class="row">

						@if($created == true)
							<table  id="datatable-buttons-home" class="table table-hover dt-responsive wrap  " >
								<thead>
									<tr>
										<th>Course Code</th>
										<th>Course Name</th>
										<th>Department</th>
										<th>Created At</th>
										<th>Enroll Code</th>
										{{--<th>Default Selection</th>--}}
										<th>Status</th>
										<th>Actions</th>

									</tr>
								</thead>
								<tbody>
									@foreach($courses as $course)
										<tr >
											<td>{{$course->course_code}}</td>
											<td>{{$course->course_name}}</td>
											<td>{{$course->department}}</td>
											<td>{{$course->created_at}}</td>
											<td>{{$course->enroll_code}}</td>
											{{--<td>--}}
												{{--@if($course->default_session == 1)--}}
													{{--Default Course--}}
												{{--@else--}}
													{{--<button type="button" id ='default_button' class="btn btn-success btn-xs" value="{{$course->course_code}}"><i class="fa fa-pencil fa-1x"></i> Set Default</button>--}}
												{{--@endif--}}
											{{--</td>--}}
											<td>Running</td>
											<td>
												<a href="{{route('teacher.dashboard', [$course->course_code])}}" class="btn btn-primary btn-xs"><i class="fa fa-folder fa-1x"></i> View </a>
												<button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target=".bs-edit-{{$course->course_code}}"><i class="fa fa-pencil fa-1x"></i> Edit </button>
											{{--<a href="{{route('teacher.removecourse', ['id' => $course->course_code])}}" class="btn btn-danger btn-xs"></a>--}}

											<!-- Small modal -->
												<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target=".bs-{{$course->course_code}}"><i class="fa fa-trash-o fa-1x"></i> Delete </button>

												<div class="modal fade bs-example-modal-sm bs-{{$course->course_code}}" tabindex="-1" role="dialog" aria-hidden="true">
													<div class="modal-dialog modal-sm">
														<div class="modal-content">

															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
																</button>
																<h4 class="modal-title" id="myModalLabel2">Really?</h4>
															</div>
															<div class="modal-body">
																<h4>Are you sure to delete this course?</h4>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																<a href="{{route('teacher.removecourse', ['id' => $course->course_code])}}" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete </a>
															</div>

														</div>
													</div>
												</div>
												<!-- /modals -->

												<div class="modal fade bs-example-modal-sm bs-edit-{{$course->course_code}}" tabindex="-1" role="dialog" aria-hidden="true">
													<div class="modal-dialog modal-lg">
														<div class="modal-content">

															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
																</button>
																<h3 class="modal-title" id="myModalLabel2">Course Information</h3>
															</div>
															<div class="modal-body">
																{!! Form::open(['route' => 'teacher.updatecourse', 'class'=>"form-horizontal", 'name'=>'submit-form']) !!}


																<div class="form-group">
																	<label class="control-label col-md-2">Course Name</label>
																	<div class="col-md-8">
																		{{Form::text('course_name', $course->course_name, array('class'=>'form-control'))}}
																	</div>
																</div>

																<div class="form-group">
																	<label class="control-label col-md-2">Course Code</label>
																	<div class="col-md-8">
																		{{Form::text('course_code', $course->course_code, array('class'=>'form-control'))}}
																	</div>
																</div>

																<div class="form-group">
																	<label class="control-label col-md-2">Course Description</label>
																	<div class="col-md-8">
																		{{Form::textarea('description', $course->description, array('class'=>'form-control'))}}
																	</div>
																</div>

																<div class="form-group">
																	<label class="control-label col-md-2">Course Enroll Code</label>
																	<div class="col-md-8">
																		{{Form::text('enroll', $course->enroll_code, array('class'=>'form-control'))}}
																	</div>
																</div>

																{{ Form::hidden('previous_course_code', $course->course_code) }}

															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																{{Form::submit('Update', array('class'=>'btn btn-success'))}}
																{{ Form::reset('Reset',array('class'=>'btn btn-primary'))}}

																{!! Form::close() !!}
															</div>

														</div>
													</div>
												</div>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						@endif

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
			<div class="col-xs-12 col-md-6">
				@if ($created == true)
					<div class="">
						<div class="x_panel">
							<h2>Announcement</h2>
							<div class="x_title">
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<ul class="to_do">
                                    <?php $is_announce_empty = true; $is_notify_empty = true; $count_announce = 0; $max_announce = 5;?>
									@foreach(auth()->user()->unreadNotifications as $notification)
										@if($notification->data['type'] == 1)
                                            <?php $count_announce++; $is_announce_empty=false ?>
											@if($count_announce>=$max_announce)
												<li>
													<div class="text-center">
														<a href="{{ route('student.announcement') }}">
															<strong>See More</strong>
															<i class="fa fa-angle-right"></i>
														</a>
													</div>
												</li>
												@break
											@endif
											<li>
												<a href="{{route('student.announcement')}}"><strong>{{$notification->data['content']['topic']}}</strong> :
													<small>{{date('Y/m/d H:m',strtotime($notification->data['repliedTime']['date']))}}</small>
												</a>
											</li>
										@endif
									@endforeach
									@if($is_announce_empty)
										<p>There is no new announcement</p>
									@endif
								</ul>
							</div>
						</div>
					</div>
				@endif
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="x_panel">
					<h2>Notification</h2>
					<div class="x_title">
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="">
							<ul class="to_do">
                                <?php $is_notify_empty = true; $count_notification = 0; $max_notification = 5; ?>
								@foreach(auth()->user()->unreadNotifications as $notification)
									@if($notification->data['type'] == 0)
                                        <?php $is_notify_empty = false; $count_notification+=1; ?>
										@if($count_notification>=$max_notification)
											<li>
												<div class="text-center">
													<a href="{{ route('student.announcement') }}">
														<strong>See More</strong>
														<i class="fa fa-angle-right"></i>
													</a>
												</div>
											</li>
											@break
										@endif
										<li>
											<a href="{{route('question.my_withSession',['sessionID'=>$notification->data['content']['course_id']])}}">{{$notification->data['content']['topic']}}:
												<small>{{date('Y/m/d H:m',strtotime($notification->data['repliedTime']['date']))}}</small>
											</a>
										</li>
									@endif
								@endforeach

								@if($is_notify_empty)
									<p>There is no new notification</p>
								@endif
							</ul>
						</div>
					</div>
				</div>
			</div>

	</div>
	@if(config('app.env') != 'production')
	<div class="row" id="cute_animation">
		<div class="container">
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
		</div>
	</div>
	@endif
</div>
<style>

	#cute_animation .container {
		width: 200px;
		height: 100px;
		padding-top: 100px;
		margin: 0 auto;
	}

	#cute_animation .ball {
		width: 10px;
		height: 10px;
		margin: 10px auto;
		border-radius: 50px;
	}

	#cute_animation .ball:nth-child(1) {
		background: navy;
		-webkit-animation: right 1s infinite ease-in-out;
		-moz-animation: right 1s infinite ease-in-out;
		animation: right 1s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(2) {
		background: navy;
		-webkit-animation: left 1.1s infinite ease-in-out;
		-moz-animation: left 1.1s infinite ease-in-out;
		animation: left 1.1s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(3) {
		background: navy;
		-webkit-animation: right 1.05s infinite ease-in-out;
		-moz-animation: right 1.05s infinite ease-in-out;
		animation: right 1.05s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(4) {
		background: navy;
		-webkit-animation: left 1.15s infinite ease-in-out;
		-moz-animation: left 1.15s infinite ease-in-out;
		animation: left 1.15s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(5) {
		background: navy;
		-webkit-animation: right 1.1s infinite ease-in-out;
		-moz-animation: right 1.1s infinite ease-in-out;
		animation: right 1.1s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(6) {
		background: navy;
		-webkit-animation: left 1.05s infinite ease-in-out;
		-moz-animation: left 1.05s infinite ease-in-out;
		animation: left 1.05s infinite ease-in-out;
	}

	#cute_animation .ball:nth-child(7) {
		background: navy;
		-webkit-animation: right 1s infinite ease-in-out;
		-moz-animation: right 1s infinite ease-in-out;
		animation: right 1s infinite ease-in-out;
	}

	@-webkit-keyframes right {
		0% {
			-webkit-transform: translate(-15px);
		}
		50% {
			-webkit-transform: translate(15px);
		}
		100% {
			-webkit-transform: translate(-15px);
		}
	}

	@-webkit-keyframes left {
		0% {
			-webkit-transform: translate(15px);
		}
		50% {
			-webkit-transform: translate(-15px);
		}
		100% {
			-webkit-transform: translate(15px);
		}
	}

	@-moz-keyframes right {
		0% {
			-moz-transform: translate(-15px);
		}
		50% {
			-moz-transform: translate(15px);
		}
		100% {
			-moz-transform: translate(-15px);
		}
	}

	@-moz-keyframes left {
		0% {
			-moz-transform: translate(15px);
		}
		50% {
			-moz-transform: translate(-15px);
		}
		100% {
			-moz-transform: translate(15px);
		}
	}

	@keyframes right {
		0% {
			transform: translate(-15px);
		}
		50% {
			transform: translate(15px);
		}
		100% {
			transform: translate(-15px);
		}
	}

	@keyframes left {
		0% {
			transform: translate(15px);
		}
		50% {
			transform: translate(-15px);
		}
		100% {
			transform: translate(15px);
		}
	}
</style>

<script>
	$(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
    $('.myDatepicker').datetimepicker({
        format: 'H:mm'
    });

</script>
<script src="{{ asset('js/dashboard/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('js/dashboard/moment.min.js') }}"></script>

@endsection

@section('javascript')
	<script src="{{ asset('js/dashboard/bootstrap-datetimepicker.min.js') }}"></script>
	<script src="{{ asset('js/dashboard/moment.min.js') }}"></script>
	<script>
        $('.myDatepicker').datetimepicker({
            format: 'H:mm'
        });
        $(function() {
            $("#default_button").click(function(e) {
				{{--$test='';--}}

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('[name="_token"]').val()
                    },
                    method:'POST',
                    url:"{{route('setDefaultCourseByTeacher')}}",
                    beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    data: {'course_code':$(this).attr("value"), 'instructor_id':'{{Auth::id()}}'},
                    success:function(response){
                        console.log(response);
                        location.reload();
                    }
                });

				{{--$('#detail-message').val('');--}}


                e.preventDefault(); // avoid to execute the actual submit of the form.
            });
        });

	</script>
@endsection
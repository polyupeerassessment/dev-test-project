@extends('layouts.dashboard')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">

        <div class="page-title">
            <div class="title_left">
                <h3>Edit Announcement</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::open(['route' => 'teacher.success.edit.announcement', 'class'=>"form-horizontal", 'name'=>'submit-form']) !!}

                {{--topic--}}
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Topic <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{Form::text('topic', null, array('class' => 'form-control col-md-7 col-xs-12'))}}
                        @if ($errors->has('topic'))
                            <span class="help-block">
                                <strong>{{ $errors->first('topic') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Contents <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">

                        {{Form::textarea('contents', null, array('class' => 'form-control col-md-7 col-xs-12'))}}
                        @if ($errors->has('contents'))
                            <span class="help-block">
                                <strong>{{ $errors->first('contents') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"> Schedule <span class="required">*</span></label>

                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" name="schedule"  id="reportrange" value="December 30, 2014 - January 28, 2015" style="padding: 5px 10px;" />
                        <b class="caret"></b>
                    </div>
                </div>


                <div class="ln_solid"></div>

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        {{Form::submit('Send', array('class'=>'btn btn-success'))}}
                        {{ Form::reset('Reset',array('class'=>'btn btn-primary'))}}
                    </div>

                </div>

                {!! Form::close() !!}
            </div>
        </div>



    </div>
@endsection
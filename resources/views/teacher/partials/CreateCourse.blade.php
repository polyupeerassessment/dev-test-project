
<div class="modal fade bs-example-modal-sm" id="create_course_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="x_panel">
                            {{--<div id="wizard" class="form_wizard wizard_horizontal">--}}
                            <div id="" class="">
                                <div id="step-1" class="">
                                    {{--<h2 class="StepTitle">Course Basic Information</h2>--}}
                                    {!! Form::open(['route' => 'teacher.insertcourse', 'class'=>"form-horizontal", 'name'=>'submit-form', 'files' => true, 'enctype'=>"multipart/form-data"]) !!}
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Range <span class="required">*</span></label>

                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                                    <span>January 1, 2018 - March 28, 2018</span> <b class="caret"></b>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Weekly Schedule <span class="required">*</span></label>

                                            <div class="col-md-8 col-sm-7 col-xs-12">
                                                @if ($errors->has('from[]') || $errors->has('to[]'))
                                                    <span class="help-block">
                                                        <strong>Course time should be completed</strong>
                                                    </span>
                                                    <br><br>
                                                @endif

                                                @for ($i = 1; $i <= 7; $i++)



                                                    <div class="row" style="margin-left:2%;">

                                                        <div class="checkbox col-md-2">
                                                            <?php
                                                                if($i==1){
                                                                    $week_value = 'Monday';
                                                                } elseif($i==2) {
                                                                    $week_value = 'Tuesday';
                                                                } elseif($i==3) {
                                                                    $week_value = 'Wednesday';
                                                                } elseif($i==4) {
                                                                    $week_value = 'Thursday';
                                                                } elseif($i==5) {
                                                                    $week_value = 'Friday';
                                                                } elseif($i==6) {
                                                                    $week_value = 'Saturday';
                                                                } else {
                                                                    $week_value = 'Sunday';
                                                                }
                                                            ?>

                                                            <input type="checkbox" name="week[]" value="{{$week_value}}"/> {{$week_value}}

                                                        </div>

                                                        <div class="checkbox col-md-4">
                                                            <div class='input-group date myDatepicker'>
                                                                <input type='text' class="form-control" name="from[]"/>
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </div>
                                                        </div>

                                                        <div class="checkbox col-md-1">~</div>

                                                        <div class="checkbox col-md-4">
                                                            <div class='input-group date myDatepicker'>
                                                                <input type='text' class="form-control"name="to[]" />
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endfor
                                            </div>
                                        </div>
                                    <hr>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Course Code <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-6 col-xs-12">
                                                {{Form::text('code', null, array('class' => 'form-control col-md-7 col-xs-12' , 'required'))}}
                                                @if ($errors->has('code'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('code') }}</strong>
                                                    </span>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Course Name <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-6 col-xs-12">

                                                {{Form::text('name', null, array('class' => 'form-control col-md-7 col-xs-12' , 'required'))}}
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Department <span class="required">*</span></label>
                                            <div class="col-md-7 col-sm-6 col-xs-12">
                                                {{Form::text('department', null, array('class' => 'form-control col-md-7 col-xs-12' , 'required'))}}
                                                @if ($errors->has('department'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('department') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Enroll Code <span class="required">*</span></label>
                                            <div class="col-md-7 col-sm-6 col-xs-12">
                                                {{Form::text('enrollcode', null, array('class' => 'form-control col-md-7 col-xs-12' , 'required'))}}
                                                @if ($errors->has('enrollcode'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('enrollcode') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Description <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-6 col-xs-12">
                                                {{Form::textarea('description', null, array('class' => 'form-control col-md-7 col-xs-12', 'required'))}}
                                                @if ($errors->has('description'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('description') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div><br><hr>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="csv_file">Enroll Students
                                            </label>
                                            <div class="col-md-7 col-sm-6 col-xs-12">
                                                <input type="file" name="csv_file" class="form-control col-md-7 col-xs-12" style="width:100%;" >
                                                @if ($errors->has('csv_file'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('csv_file') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    <hr>

                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            {{Form::submit('Create', array('class'=>'btn btn-success'))}}
                                            {{ Form::reset('Reset',array('class'=>'btn btn-primary'))}}
                                        </div>

                                    </div>


                                    {!! Form::close() !!}

                                </div>

                                {{--<div id="step-2">--}}
                                    {{--<h2 class="StepTitle"></h2>--}}

                                    {{--<div class="col-md-12 col-sm-12 col-xs-12">--}}
                                        {{--<div class="x_panel">--}}
                                            {{--<div class="x_title">--}}
                                                {{--<h2>Add Students</h2>--}}
                                                {{--<ul class="nav navbar-right panel_toolbox">--}}
                                                    {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                                                    {{--</li>--}}
                                                    {{--<li class="dropdown">--}}
                                                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                                        {{--<ul class="dropdown-menu" role="menu">--}}
                                                            {{--<li><a href="#">Settings 1</a>--}}
                                                            {{--</li>--}}
                                                            {{--<li><a href="#">Settings 2</a>--}}
                                                            {{--</li>--}}
                                                        {{--</ul>--}}
                                                    {{--</li>--}}
                                                    {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                                                    {{--</li>--}}
                                                {{--</ul>--}}
                                                {{--<div class="clearfix"></div>--}}
                                            {{--</div>--}}
                                            {{--<div class="x_content" >--}}
                                                {{--<p>Drag multiple files to the box below for multi upload or click to select files.</p>--}}

{{--                                                {!! Form::open(['route' => 'teacher.enrollstudent_newcourse', 'class'=>"dropzone", 'name'=>'submit-form']) !!}--}}
{{--                                                {!! Form::open(['route' => 'teacher.enrollstudent_newcourse', 'class'=>"dropzone", 'name'=>'submit-form2']) !!}--}}
                                                {{--<div style="width:100%; border:grey solid 1px;" class="dropzone">--}}
                                                    {{--<p style="text-align: center; font-size: 110%;" class="col-middle" >Drop file to here</p>--}}

                                                {{--</div>--}}
                                                {{--{!! Form::close() !!}--}}


                                                {{--<br />--}}
                                                {{--<br />--}}
                                                {{--<br />--}}
                                                {{--<br />--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                {{--</div>--}}
                            </div>

                            <!-- End SmartWizard Content -->

                        </div>
    </div>
</div>
@extends('layouts.dashboard')

@section('content')
    <div class="right_col" role="main">

        <!-- Changelogs -->
        <div class="block-header">
            <h2>CHANGELOGS</h2>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            v1.0.5
                            <small>27th June 2017</small>
                        </h2>
                    </div>
                    <div class="body">
                        <p>- Fixed bugs</p>
                        <p>- Button with icon & text added</p>
                    </div>
                </div>
            </div>

        </div>

    </div>

@endsection

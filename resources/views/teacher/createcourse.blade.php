@extends('layouts.app')

@section('content')

    <style>
        #showadvanceoption:hover {
            background-color:lightgrey;
        }
    </style>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <br>
                <h2>Create New Course</h2>
                <br>

                {!! Form::open(['route' => 'teacher.insertcourse']) !!}


                {{--Course Code--}}
                {{Form::label('code', 'Course Code: ')}}
                {{Form::text('code', null, array('class' => 'form-control'))}}
                @if ($errors->has('code'))
                    <span class="help-block">
                                <strong>{{ $errors->first('code') }}</strong>
                            </span>
                    <br><br>
                @endif<br>

                {{--Course Name--}}
                {{Form::label('name', 'Course Name: ')}}
                {{Form::text('name', null, array('class' => 'form-control'))}}
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    <br><br>
                @endif<br>

                {{--Department--}}
                {{Form::label('department', 'Department: ')}}
                {{Form::text('department', null, array('class' => 'form-control'))}}
                @if ($errors->has('department'))
                    <span class="help-block">
                        <strong>{{ $errors->first('department') }}</strong>
                    </span>
                    <br><br>
                @endif<br>


                {{--Course Enroll Code--}}
                {{Form::label('enrollcode', 'Enroll Code: ')}}
                {{Form::text('enrollcode', null, array('class' => 'form-control'))}}
                @if ($errors->has('enrollcode'))
                    <span class="help-block">
                                <strong>{{ $errors->first('enrollcode') }}</strong>
                            </span>
                    <br><br>
                @endif

                <br><br>

                {{Form::label('description', "Description:")}}
                {{Form::textarea('description', null, array('class' => 'form-control'))}}
                @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif

                <br>
                {{Form::submit('Create', array('class'=>'btn btn-custom btn-md btn-black'))}}
                {{ Form::reset('Reset',array('class'=>'btn btn-md btn-outline-secondary'))}}

                {!! Form::close() !!}<br>
            </div>
        </div>

    </div>



@endsection

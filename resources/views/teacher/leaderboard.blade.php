@extends('layouts.dashboard')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>Top Students Scored</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive borderless">
                        <table id="datatable-buttons-users" class="table table-striped table-bordered ">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Nickname</th>
                                <th>Email</th>
                                <th>Score</th>
                                <th>No. of Answers</th>
                                <th>Average Rating Received</th>
                                <th>Total No. of Views</th>
                                <th>Total No. of Questions</th>
                                <th>Last Activity Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $rank = 0; $pre_value = -1; ?>
                                @foreach($top_users as $user)
                                    <?php
                                    if($pre_value != $user['score']) {
                                        $pre_value = $user['score'];
                                        $rank++;
                                    }
                                    ?>
                                    <tr>
                                        <td>{{$user['id']}}</td>
                                        <td>{{$user['name']}}</td>
                                        <td>{{$user['nickname']}}</td>
                                        <td>{{$user['email']}}</td>
                                        <td>{{$user['score']}}</td>
                                        <td>{{$user['num_attempts']}}</td>
                                        <td>{{number_format($user['avg_rating'],1)}}</td>
                                        <td>{{$user['num_views']}}</td>
                                        <td>{{$user['num_questions']}}</td>
                                        <td>{{$user['last_activity_date']}}</td>
                                        <td>
                                            <a href="#" class="btn btn-xs btn-info">View</a>
                                            {{--<a href="#" class="btn btn-xs btn-warning">Edit</a>--}}
                                            {{--<a href="#" class="btn btn-xs btn-danger">Delete</a>--}}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>Top Questions</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive borderless">
                        <table id="datatable-buttons-questions" class="table table-striped table-bordered table-custom-by-masa">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Date</th>
                                <th>Topic</th>
                                <th>Difficulty</th>
                                <th>Type</th>
                                <th>Content Mode</th>
                                <th>Average Rating</th>
                                <th>Average Score</th>
                                <th>Average Time Spent <br>(in seconds)</th>
                                <th>No. of Corrects</th>
                                <th>No. of Answers</th>
                                <th>No. of Views</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($top_questions as $question)
                                <tr>
                                    <td>{{$question['id']}}</td>
                                    <td>{{$question['created_at']}}</td>
                                    <td>{{$question['topic']}}</td>
                                    <td>
                                        @if($question['difficulty'] == 0)
                                            Easy
                                        @elseif($question['difficulty'] == 1)
                                            Normal
                                        @else
                                            Hard
                                        @endif
                                    </td>
                                    <td>
                                        @if($question['type'] == 0)
                                            MC
                                        @else
                                            T/F
                                        @endif
                                    </td>
                                    <td>
                                        @if($question['mode'] == 0)
                                            Normal
                                        @else
                                            HTML
                                        @endif
                                    </td>
                                    <td>{{$question['rating']}}</td>
                                    <td>{{number_format($question['avg_score'],1)}}</td>
                                    <td>{{$question['avg_spent']}}</td>
                                    <td>{{$question['num_corrects']}}</td>
                                    <td>{{$question['num_answers']}}</td>
                                    <td>{{$question['num_views']}}</td>
                                    <td>
                                        <a href="#" class="btn btn-xs btn-info">View</a>
                                        {{--<a href="#" class="btn btn-xs btn-warning">Edit</a>--}}
                                        {{--<a href="#" class="btn btn-xs btn-danger">Delete</a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>Top Keywords</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive borderless">
                        <table id="datatable-buttons-keywords" class="table table-striped table-bordered table-custom-by-masa">
                            <thead>
                            <tr>
                                <th>Keyword</th>
                                <th>Conut</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($top_keywords as $keyword)
                                <tr>
                                    <td>{{$keyword->keyword}}</td>
                                    <td>{{$keyword->rank}}</td>
                                    <td>
                                        <a href="#" class="btn btn-xs btn-info">View</a>
                                        {{--<a href="#" class="btn btn-xs btn-warning">Edit</a>--}}
                                        {{--<a href="#" class="btn btn-xs btn-danger">Delete</a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

@section('javascript')

    <script>
        if ($("#datatable-buttons-questions").length) {
            $("#datatable-buttons-questions").DataTable({
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
                order: [ [ 10, 'desc' ]],
                ordering: true,
                bSort : false,
            });
        }

        if ($("#datatable-buttons-users").length) {
            $("#datatable-buttons-users").DataTable({
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
                order: [ [ 4, 'desc' ]],
                ordering: true,
                bSort : false,
            });
        }

        if ($("#datatable-buttons-keywords").length) {
            $("#datatable-buttons-keywords").DataTable({
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
                order: [ [ 2, 'desc' ]],
                ordering: true,
                bSort : false,
            });
        }
    </script>
@endsection

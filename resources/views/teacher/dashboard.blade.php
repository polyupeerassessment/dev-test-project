@extends('layouts.dashboard')

@section('profile_name')
    {{$teacher->name}}
@endsection

@section('content')
    <style>
        .count {
            font-size:25px !important;
        }
        .teacher-btn {
            height:100px;
        }
        .teacher-btn-box {
            margin:5px;


            /*padding:5px;*/
        }
        .tile-stats:hover {
            border:1px orangered solid;
        }

        .teacher-btn-box.col-md-2 {
            /*width:24%;*/
        }
        @media (min-width: 992px)
        {
            .col-md-2 {
                width: 24%;
            }
        }

        .top-titles-statistics {
            padding:5px;
            /*margin:10px;*/
        }
        .top-titles-statistics-box {
            height:100px;
        }

    </style>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            {{--Total Statistic--}}
            <div class="row top_tiles top-titles-statistics">
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile-stats top-titles-statistics-box">
                        <div class="icon"><i class="fa fa-user"></i></div>
                        <div class="count">{{$numActive}}/{{sizeof($students)}}</div>
                        <h3>Online</h3>
                    </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile-stats top-titles-statistics-box">
                        <div class="icon"><i class="fa fa-comments-o"></i></div>
                        <div class="count">{{$numChat}}</div>
                        <h3>Chats</h3>
                    </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile-stats top-titles-statistics-box">
                        <div class="icon"><i class="fa fa-question-circle"></i></div>
                        <div class="count">{{sizeof($questions)}}</div>
                        <h3>Questions</h3>
                    </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile-stats top-titles-statistics-box">
                        <div class="icon"><i class="fa fa-paper-plane"></i></div>
                        <div class="count">{{sizeof($answers)}}</div>
                        <h3>Answers</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="dashboard_graph">

                        <div class="row top_tiles">
                            <div id="box-answer-question"  >
                                <a class="animated flipInY  col-md-2  col-xs-12 teacher-btn-box" href="{{route('teacher.mystudents')}}">
                                    <div class="tile-stats" id= 'answer_box_button'>
                                        <div class="count teacher-btn">My Students
                                            <i class="fa fa-users pull-right " style="color:lightseagreen;font-size:32px; margin:10px;"></i>
                                        </div>
                                        <p style="margin-left:2%;">Your student list here</p>
                                    </div>
                                </a>
                            </div>
                            <div id="box-create-question" >
                                <a class="animated flipInY  col-md-2  col-xs-12 teacher-btn-box" href="{{route('teacher.showquestion')}}">
                                    <div class="tile-stats" id="create_box_button">
                                        <div class="count teacher-btn">Questions
                                            <i class="fa fa-question pull-right " style="color:palevioletred;font-size:32px; margin:10px;"></i>
                                        </div>
                                        <p style="margin-left:2%;">Questions here</p>
                                    </div>
                                </a>
                            </div>
                            <div id="box-show-created"  >
                                <a class="animated flipInY  col-md-2  col-xs-12 teacher-btn-box" href="{{ route('teacher.leaderboard') }}">
                                    <div class="tile-stats" id="my_box_button">
                                        <div class="count teacher-btn">Leaderboard
                                            <i class="fa fa-trophy pull-right" style="color:yellowgreen; font-size:32px; margin:10px;"></i>
                                        </div>
                                        <p style="margin-left:2%;">Student Leaderboard here</p>
                                    </div>
                                </a>
                            </div>
                            <div id="box-show-answered" >
                                <a class="animated flipInY col-md-2 col-xs-12 teacher-btn-box" href="{{route('teacher.quiz.list')}}">
                                    <div class="tile-stats" id="my_answer_box_button">
                                        <div class="count teacher-btn">Quiz
                                            <i class="fa fa-file pull-right" style="color:darkslategrey; font-size:32px;margin:10px;"></i>
                                        </div>
                                        <p style="margin-left:2%;">Show the quiz</p>
                                    </div>
                                </a>
                            </div>
                            <div id="box-forum" >
                                <a class="animated flipInY col-md-2 col-xs-12 teacher-btn-box" href="{{route('teacher.voting.result')}}">
                                    <div class="tile-stats" id="forum_box_button">
                                        <div class="icon"></div>
                                        <div class="count teacher-btn">Voting
                                            <i class="fa fa-thumbs-up pull-right" style="color:#DCDCDC; font-size:32px;margin:10px;"></i>
                                        </div>
                                        <p style="margin-left:2%;">Voting system for students to vote</p>
                                    </div>
                                </a>
                            </div>
                            <div id="box-exercise" >
                                <a class="animated flipInY col-md-2 col-xs-12 teacher-btn-box" href="{{route('teacher.assignment.show')}}">
                                    <div class="tile-stats" id="exercise_box_button">
                                        <div class="icon"></div>
                                        <div class="count teacher-btn">Assignment
                                            <i class="fa fa-pencil pull-right" style="color:#DC143C; font-size:32px;margin:10px;"></i>
                                        </div>
                                        <p style="margin-left:2%;">Show your assignment here</p>
                                    </div>
                                </a>
                            </div>
                            <div id="box-quiz">
                                <a class="animated flipInY col-md-2 col-xs-12 teacher-btn-box" href="{{route('teacher.statistics')}}">
                                    <div class="tile-stats" id="quiz_box_button">
                                        <div class="icon"></div>
                                        <div class="count teacher-btn">Statistics
                                            <i class="fa fa-database pull-right" style="color:#483D8B; font-size:32px;margin:10px;"></i>
                                        </div>
                                        <p style="margin-left:2%;">Breif statistics about your course</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Overall Student Participation <small>Weekly progress</small></h2>
                            <div class="filter">
                                <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                    <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="" style="">
                            <div class="col-md-9 col-sm-12 col-xs-12">
                                <div class="demo-container" style="height:280px">
                                    <div id="graph_line" style="width:100%; height:300px;"></div>
                                </div>
                                <br>
                                {{--<div class="tiles">--}}
                                    {{--<div class="col-md-4 tile">--}}
                                        {{--<span>Page Views</span>--}}
                                        {{--<h2>231,809</h2>--}}
                                        {{--<span class="sparkline11 graph" style="height: 160px;">--}}
                                            {{--<canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>--}}
                                        {{--</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4 tile">--}}
                                        {{--<span>Exercise</span>--}}
                                        {{--<h2>$231,809</h2>--}}
                                        {{--<span class="sparkline22 graph" style="height: 160px;">--}}
                                              {{--<canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>--}}
                                        {{--</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4 tile">--}}
                                        {{--<span>Rating</span>--}}
                                        {{--<h2>231,809</h2>--}}
                                        {{--<span class="sparkline11 graph" style="height: 160px;">--}}
                                               {{--<canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>--}}
                                        {{--</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                            </div>

                            <div class="col-md-3 col-sm-12 col-x">
                                {{--<div>--}}
                                    {{--<div class="x_title">--}}
                                        {{--<h2>Top 5 Profiles</h2>--}}
                                        {{--<ul class="nav navbar-right panel_toolbox">--}}
                                            {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                                            {{--</li>--}}
                                            {{--<li class="dropdown">--}}
                                                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                                {{--<ul class="dropdown-menu" role="menu">--}}
                                                    {{--<li><a href="#">Settings 1</a>--}}
                                                    {{--</li>--}}
                                                    {{--<li><a href="#">Settings 2</a>--}}
                                                    {{--</li>--}}
                                                {{--</ul>--}}
                                            {{--</li>--}}
                                            {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                                            {{--</li>--}}
                                        {{--</ul>--}}
                                        {{--<div class="clearfix"></div>--}}
                                    {{--</div>--}}
                                    {{--<ul class="list-unstyled top_profiles scroll-view">--}}
                                      {{--@foreach($rank_users as $user)--}}
                                      {{--<li class="media event">--}}
                                          {{--<a class="pull-left border-aero profile_thumb">--}}
                                              {{--<i class="fa fa-user aero"></i>--}}
                                          {{--</a>--}}
                                          {{--<div class="media-body">--}}
                                              {{--<a class="title" href="#">{{$user->Nickname}}</a> | {{$user->Course}}--}}

                                              {{--<p>Score: <strong>{{$user->Score}}</strong></p>--}}
                                              {{--<p> <small>Create: 1 </small> | <small>Answer:2 </small>| <small>Rating:0 </small></p>--}}
                                          {{--</div>--}}
                                      {{--</li>--}}
                                      {{--@endforeach--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /page content -->
@endsection

@section('javascript')
    <script>
        var timeline_type = 0;

        function drawTimeline(offset)
        {
            timeline_type = 0;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                },
                method:'POST',
                url:"{{route('statistic.getTimelineOverall')}}",
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                    if (token) {
                        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },
                data: {"_token": "{{ csrf_token() }}",'offset': offset, 'courseID':"{{$course->course_code}}" , 'studentID':'41'},
                success:function(response){
                    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                    $('#graph_line').html('');
                    $('#graph_title').html('Timeline: Overall');
                    console.log(response['error']);

                    tmpdata_answer = response['data_answer'];
                    tmpdata_answered = response['data_chats'];
                    tmpdata_create = response['data_create'];
                    tmpweek = response['week'];

                    len = tmpdata_answer.length;
                    data=[];

                    for(i=0;i<7;i++) {
                        data[i] = {period:tmpweek[i], answer:tmpdata_answer[tmpweek[i]], create:tmpdata_create[tmpweek[i]], chats: tmpdata_answered[tmpweek[i]]};
                    }

                    Morris.Line({
                        element: 'graph_line',
                        xkey: 'period',
                        ykeys: ['answer', 'create','chats'],
                        labels: ['Answer', "Create", 'Chats'],
                        hideHover: 'auto',
                        lineColors:['#96CA59', 'red', '#3498DB', '#F08080', '#f7cb38', '#5a8022', '#2c7282'],
                        data: data,
                        xLabelFormat: function (x) {
                            // return (d.getMonth()+1).toString()+'/'+(d.getDate()).toString();
                            // console.log(d.getDate());
                            var d = new Date(x.label);
                            // console.log(x.label);
                            return d.getDate() + ' ' + months[d.getMonth()];
                        },
                        parseTime: false,
                        resize: true
                    });

                    $MENU_TOGGLE.on('click', function() {
                        $(window).resize();
                    });
                }, error:function(e){
                    $('footer').html(e.responseText);
                }
            });
        }

        $(document).ready(function() {
            // chart_plot_02_data = [];
            // showOveralStatistic();

            // window.setInterval(function(){
            //     showOveralStatistic();
            // }, 10000);
            drawTimeline(0);


        });

        function showOveralStatistic() {
          $.ajax({
              headers: {
                  'X-CSRF-TOKEN': $('[name="_token"]').val()
              },
              method:'POST',
              url:"{{route('getOverallStudent')}}",
              beforeSend: function (xhr) {
                  var token = $('meta[name="csrf_token"]').attr('content');

                  if (token) {
                      return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                  }
              },
              data: {'id':'{{Auth::id()}}'},
              success:function(response) {
                  chart_plot_02_data = response;
                  //console.log(chart_plot_02_data);
                  var chart_plot_02_settings = {
                      grid: {
                          show: true,
                          aboveData: true,
                          color: "#3f3f3f",
                          labelMargin: 10,
                          axisMargin: 0,
                          borderWidth: 0,
                          borderColor: null,
                          minBorderMargin: 5,
                          clickable: true,
                          hoverable: true,
                          autoHighlight: true,
                          mouseActiveRadius: 100
                      },
                      series: {
                          lines: {
                              show: true,
                              fill: true,
                              lineWidth: 2,
                              steps: false
                          },
                          points: {
                              show: true,
                              radius: 4.5,
                              symbol: "circle",
                              lineWidth: 3.0
                          }
                      },
                      legend: {
                          position: "ne",
                          margin: [0, -25],
                          noColumns: 0,
                          labelBoxBorderColor: null,
                          labelFormatter: function(label, series) {
                              return label + '&nbsp;&nbsp;';
                          },
                          width: 40,
                          height: 1
                      },
                      colors: ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'],
                      shadowSize: 0,
                      tooltip: true,
                      tooltipOpts: {
                          content: "%s: %y.0",
                          xDateFormat: "%d/%m",
                          shifts: {
                              x: -30,
                              y: -50
                          },
                          defaultTheme: false
                      },
                      yaxis: {
                          min: 0
                      },
                      xaxis: {
                          mode: "time",
                          minTickSize: [1, "day"],
                          timeformat: "%d/%m/%y",
                          min: chart_plot_02_data[0][0],
                          max: chart_plot_02_data[6][0]
                      }
                  };
                   if ($("#chart_plot_02").length){

                       $.plot( $("#chart_plot_02"),
                           [{
                               label: "Student Engaged",
                               data: chart_plot_02_data,
                               lines: {
                                   fillColor: "rgba(150, 202, 202, 0.12)"
                               },
                               points: {
                                   fillColor: "#fff" }
                           }], chart_plot_02_settings);

                   }

              }
          });
        }
    </script>
@endsection

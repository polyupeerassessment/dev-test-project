@extends('layouts.dashboard')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>Quiz</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive borderless">
                        <table id="datatable-buttons-quiz-results" class="table table-striped table-bordered ">
                            <thead>
                            <tr>
                                <th>Student ID</th>
                                <th>Student Nickname</th>
                                <th>Student Name</th>
                                <th>Student Email</th>
                                <th>Student Score</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($quiz_results as $q)
                                    <tr>
                                        <td>{{$q->user_id}}</td>
                                        <td>{{$q->nickname}}</td>
                                        <td>{{$q->name}}</td>
                                        <td>{{$q->email}}</td>
                                        <td>{{$q->score}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

@section('javascript')

    <script>
        if ($("#datatable-buttons-quiz-results").length) {
            $("#datatable-buttons-quiz-results").DataTable({
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
            });
        }


    </script>
@endsection
@extends('layouts.dashboard')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title" style="padding: 0px;">
            <div class="title_left"></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <h1>Quiz Questions</h1><br><br>
                    <div class="">
                        {{--Total Statistic--}}
                        <div class="row top_tiles">
                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="tile-stats">
                                    <div class="count">{{sizeof($questions)}}</div>
                                    <h3>No. Questions</h3>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="tile-stats">
                                    <?php
                                        $mc_count = 0;
                                        foreach($questions as $q) {
                                            if($q->type == 0)
                                            {
                                                $mc_count++;
                                            }
                                        }
                                    ?>
                                    <div class="count">{{$mc_count}}</div>
                                    <h3>No. MC</h3>
                                </div>
                            </div>
                            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="tile-stats">
                                    <?php
                                    $tf_count = 0;
                                    foreach($questions as $q) {
                                        if($q->type == 1)
                                        {
                                            $tf_count++;
                                        }
                                    }
                                    ?>
                                    <div class="count">{{$tf_count}}</div>
                                    <h3>No. TF</h3>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="x_content">

                        @foreach($questions as $question)
                            @if(empty($question))
                                @continue
                            @endif
                            <div class="row">
                                <hr><div class="clearfix"></div>
                                <div class="col-md-3 col-xs-3" >
                                    <h1><small>ID: </small>{{$question->id}}</h1>
                                    <a href="{{route('teacher.detailquestion', ['id'=>$question->id])}}" class="btn btn-xs btn-info">View</a>
                                    <button class="btn btn-danger btn-xs" data-toggle="modal" data-target=".delete_q_{{$question->id}}"><i class="fa fa-trash-o fa-1x"></i> Delete </button>

                                    <div class="modal fade bs-example-modal-sm delete_q_{{$question->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel2">Really?</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure to delete this question?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <a href="{{route('teacher.removequestion', ['id' => $question->id])}}" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-9 col-xs-9">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <h3 style="word-wrap: break-word;">{{$question->topic}}</h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 question_content" @if($question->mode == 0) style='white-space: pre-line;' @endif>
                                            <p style="max-width:100%;">
                                                @if($question->mode == 0)
                                                    {{ $question->contents }}
                                                @else
                                                    <?php
                                                    echo html_entity_decode($question_filter_content[$question->id]);
                                                    ?>
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                    <br>
                                    @if(empty($choices[$question->id]))
                                        @continue
                                    @endif
                                    <div>
                                        @foreach($choices[$question->id] as $choice)
                                            {{--Contents HERE! --}}
                                            @if ($question->type==0)
                                                {{--MC Contents! --}}
                                                @if ($choice->answer == 1)
                                                    <div class='row'>
                                                        <div class="col-md-1 col-sm-1 col-xs-1">
                                                            <i class="fa fa-check fa-2x" style="color:#24b999; min-height:35px;"></i>
                                                        </div>
                                                        <div class="col-md-11 col-sm-11 col-xs-11">
                                                            <p style="word-wrap:break-word; min-height:35px; font-size:120%;">{{$choice->choice}}</p>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class='row'>
                                                        <div class="col-md-1 col-sm-1 col-xs-1">
                                                            <i class="fa fa-close fa-2x" style="color:#d43f3a;min-height:35px;"></i>
                                                        </div>
                                                        <div class="col-md-11 col-sm-11 col-xs-11">
                                                            <p style="word-wrap:break-word; min-height:35px; font-size:120%;">{{$choice->choice}}</p>
                                                        </div>
                                                    </div>
                                                @endif
                                            @else
                                                <b>Answers</b>:<br>
                                                {{--TF Contents! --}}
                                                @if($choice->answer == 1)
                                                    <?php $ans = "True" ?>
                                                    <div class="input-group">
                                                        <div>True</div>
                                                    </div>
                                                @else
                                                    <?php $ans = "False" ?>
                                                    <div class="input-group">
                                                        <div>False</div>
                                                    </div>
                                                @endif
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div><div class="clearfix"></div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

@section('javascript')
@endsection
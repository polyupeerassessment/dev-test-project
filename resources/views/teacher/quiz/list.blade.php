@extends('layouts.dashboard')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>Quiz</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive borderless">
                        <table id="datatable-buttons-quiz-list" class="table table-striped table-bordered ">
                            <thead>
                            <tr>
                                <th>Quiz ID</th>
                                <th>Title</th>
                                <th>Created Date</th>
                                <th>Start Quiz Date</th>
                                <th>Quiz Deadline</th>
                                <th>Number of Questions</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($quiz as $q)
                                    <tr>
                                        <td>{{$q->id}}</td>
                                        <td>{{$q->topic}}</td>
                                        <td>{{$q->created_at}}</td>
                                        <td>{{$q->start_at}}</td>
                                        <td>{{$q->end_at}}</td>
                                        <td>0</td>
                                        <td>
                                            @if($q->type == 1)
                                                Auto
                                            @else
                                                Manual
                                            @endif
                                        </td>
                                        <td><a href="{{route('teacher.quiz.result',['id'=>$q->id])}}" class="btn btn-success">Result</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

@section('javascript')

    <script>
        if ($("#datatable-buttons-quiz-list").length) {
            $("#datatable-buttons-quiz-list").DataTable({
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
            });
        }


    </script>
@endsection
@extends('layouts.dashboard')

@section('content')
    @if(session('fail'))
        <script type='text/javascript'>
            $(document).ready(function(e) {
                generate_PNotify('{{session('fail')}}');
            });
        </script>
    @endif

    @if(session('success'))
        <script type='text/javascript'>
            $(document).ready(function(e) {
                generate_PNotify_success('{{session('success')}}');
            });
        </script>
    @endif

    <!-- page content -->
    <div class="right_col" role="main">

        <div class="page-title">
            <div class="title_left">
                <h3>Send Announcement</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::open(['route' => 'teacher.sendannounce', 'class'=>"form-horizontal", 'name'=>'submit-form']) !!}

                {{--topic--}}
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Topic <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {{Form::text('topic', null, array('class' => 'form-control col-md-7 col-xs-12'))}}
                        @if ($errors->has('topic'))
                            <span class="help-block">
                                <strong>{{ $errors->first('topic') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Contents <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">

                        {{Form::textarea('contents', null, array('class' => 'form-control col-md-7 col-xs-12'))}}
                        @if ($errors->has('contents'))
                            <span class="help-block">
                                <strong>{{ $errors->first('contents') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Select Course <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-8 col-xs-12">

                        @foreach($courses as $course)
                                <input type="checkbox" name="courses[]" class=' flat' value='{{$course->course_code}}'>
                                <label for="middle-name" class="control-label ">{{$course->course_code}}</label>
                                <br>
                        @endforeach
                        @if ($errors->has('courses'))
                            <span class="help-block">
                                <strong>{{ $errors->first('courses') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>

                <div class="ln_solid"></div>

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        {{Form::submit('Send', array('class'=>'btn btn-success'))}}
                        {{ Form::reset('Reset',array('class'=>'btn btn-primary'))}}
                    </div>

                </div>

                {!! Form::close() !!}
            </div>
        </div>



    </div>
@endsection
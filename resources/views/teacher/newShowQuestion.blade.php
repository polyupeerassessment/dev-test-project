@extends('layouts.dashboard')

@section('css')
    <style>
        tr{
            cursor: pointer;
        }
    </style>
@endsection


@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        @if (empty($questions[0]) && empty($questions_byTeacher[0]))
            <hr>
            <p>Question not found</p>
        @else
            <h1>Questions</h1>
            <div class="table-responsive" >
                <div id="question_selector" style="display:block">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <td>Id</td>
                            <td>Topic</td>
                            <td>Difficulty</td>
                            <td>Rating</td>
                            <td>Created At</td>
                            <td>Type</td>
                            <td>Author</td>
                            <td>Action</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($questions as $question)
                            <tr class='clickable-row' data-href="{{route('teacher.detailquestion', ['id'=>$question->id])}}">
                                <td class="click_to_link">{{$question->id}}</td>
                                <td class="click_to_link">{{$question->topic}}</td>
                                <td @if($question->difficulty>0) style="color:orangered;" @endif class="click_to_link" >
                                    @if($question->difficulty==0) Easy
                                    @elseif($question->difficulty == 1) Normal
                                    @else Hard @endif
                                </td>
                                <td class="click_to_link">{{$question->rating}}</td>
                                <td class="click_to_link">{{$question->created_at}}</td>
                                <td class="click_to_link">
                                    @if($question->type==0) MC
                                    @else TF @endif
                                </td>
                                <td class="click_to_link">{{$question->name}}</td>
                                <td>
                                    <button class="btn btn-danger btn-xs" data-toggle="modal" data-target=".delete_q_{{$question->id}}"><i class="fa fa-trash-o fa-1x"></i> Delete </button>

                                    <div class="modal fade bs-example-modal-sm delete_q_{{$question->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">

                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel2">Really?</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure to delete this question?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <a href="{{route('teacher.removequestion', ['id' => $question->id])}}" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        @endif
    </div>
    <!-- /page content -->
@endsection


@section('javascript')
    <script src="{{asset('js/dashboard/bootstrap-progressbar.min.js')}}"></script>
    <script>
        $(".click_to_link").click(function() {
            window.location = $(this).parent().closest('tr').data("href");
        });
    </script>
@endsection

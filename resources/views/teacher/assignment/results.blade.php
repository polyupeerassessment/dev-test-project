@extends('layouts.dashboard')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>Your Assignment Results</h3>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content table-responsive borderless">
                        <table id="datatable-buttons-quiz-list" class="table table-striped table-bordered ">
                            <thead>
                            <tr>
                                <th>Student Name</th>
                                <th>Student Email</th>
                                <th>Number of Attempts</th>
                                <th>Results</th>
                                {{--<th>Last Activity Date</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($results as $r)
                                <tr>
                                    <td>{{$r->name}}</td>
                                    <td>{{$r->email}}</td>
                                    <td>
                                        @if(!empty($num_attempts[$r->id]))
                                            {{$num_attempts[$r->id]}}
                                        @endif

                                    </td>
                                    <td>{{$r->result}}</td>
                                    {{--<td>{{$question->created_at}}</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

@section('javascript')

    <script>
        if ($("#datatable-buttons-quiz-list").length) {
            $("#datatable-buttons-quiz-list").DataTable({
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
            });
        }


    </script>
@endsection
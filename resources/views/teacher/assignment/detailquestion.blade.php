@extends('layouts.dashboard')

@section('css')
    <link rel="stylesheet" href="{{asset('css/dashboard/green.css')}}" />
@endsection

@section('content')

    <style>
        @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

        .rating {
            padding: 1%;
        }

        .rating {
            border: none;
            float: left;
        }

        .rating>input {
            display: none;
        }

        .rating>label:before {
            margin: 5px;
            font-size: 1.25em;
            font-family: FontAwesome;
            display: inline-block;
            content: "\f005";
        }

        .rating>.half:before {
            content: "\f089";
            position: absolute;
        }

        .rating>label {
            color: #ddd;
            float: right;
        }

        #custom-rating-click {
            color: #FFD700;
        }
        /***** CSS Magic to Highlight Stars on Hover *****/

        .rating>input:checked~label,
            /* show gold star when clicked */

        .rating:not(:checked)>label:hover,
            /* hover current star */

        .rating:not(:checked)>label:hover~label {
            color: #FFD700;
        }
        /* hover previous stars in list */

        .rating>input:checked+label:hover,
            /* hover current star when changing rating */

        .rating>input:checked~label:hover,
        .rating>label:hover~input:checked~label,
            /* lighten current selection */

        .rating>input:checked~label:hover~label {
            color: #FFED85;
        }
    </style>
    <!-- page content -->
    <div class="right_col" role="main">
        {!! Breadcrumbs::render('teacher_assignment_question_detail', $question) !!}
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 style="overflow-wrap: break-word;">{{$question->topic}}</h2>
                        <div class="checkmark-circle">
                            <div class="background"></div>
                            <div class="checkmark draw"></div>
                        </div>
                        <div class="nav navbar-right">
                            Difficulty:
                            @if ($question->difficulty == 90)
                                Very Easy
                            @elseif($question->difficulty == 70)
                                Easy
                            @elseif($question->difficulty == 50)
                                Normal
                            @elseif($question->difficulty == 30)
                                Hard
                            @else
                                Very Hard
                            @endif
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        {{--Question contents--}}
                        <div class='row'>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 question_content" @if($question->mode == 0) style='white-space: pre-line;' @endif>
                                            <p style="max-width:100%;" id="question_content_detail">
                                                @if($question->mode == 0)
                                                    {{$question->contents }}
                                                @else
                                                    <?php echo html_entity_decode($contents); ?>
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        {{--Answer--}}
	    <div class="row aligned-row ">
		    <div class="col-md-12 col-sm-12 col-xs-12">
			    <div class="x_panel">
			        <div class="x_content">
                        <div class="col-xs-12 col-lg-7" >
					        <div class="x_title">
						        <h2>Answer</h2>
						        <div class="clearfix"></div>
					        </div>
					        <div class="">
						        {{--Answer Choice--}}
						        <div class='row'>
							        <div class="col-md-12 col-sm-12 col-xs-12">
					    				<!-- MC -->
						    			@if ($question->type==0)
							    			@foreach($choices as $i=>$choice)
								    			@if(empty($choice->choice))
									    			@continue;
										    	@endif
											    <?php
											        $prefix_character = '';
                                                    if ($i == 0) {
                                                        $prefix_character = 'A: ';
                                                    } elseif ($i == 1) {
                                                        $prefix_character = 'B: ';
                                                    } elseif ($i == 2) {
                                                        $prefix_character = 'C: ';
                                                    } elseif ($i == 3) {
                                                        $prefix_character = 'D: ';
                                                    } elseif ($i == 4) {
                                                        $prefix_character = 'E: ';
                                                    } elseif ($i == 5) {
                                                        $prefix_character = 'F: ';
                                                    }

                                                ?>

                                                @if($choice->answer == 1)
                                                    <div class='row'>
                                                        <div class="col-md-1 col-sm-1 col-xs-1">
                                                            <i class="fa fa-check fa-2x" style="color:#24b999; min-height:35px;"></i>
                                                        </div>
                                                        <div class="col-md-11 col-sm-11 col-xs-11">
                                                            <p style="word-wrap:break-word; min-height:35px; font-size:120%;">{{$prefix_character.' '.$choice->choice}}</p>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class='row'>
                                                        <div class="col-md-1 col-sm-1 col-xs-1">
                                                            <i class="fa fa-close fa-2x" style="color:#d43f3a;min-height:35px;"></i>
                                                        </div>
                                                        <div class="col-md-11 col-sm-11 col-xs-11">
                                                            <p style="word-wrap:break-word; min-height:35px; font-size:120%;">{{$prefix_character.$choice->choice}}</p>
                                                        </div>
                                                    </div>
                                                @endif
										    @endforeach

									    @elseif ($question->type==1)
                                            @foreach($choices as $choice)
                                                <div class="input-group">
                                                    <div>
                                                        @if($choice->answer == 1)
                                                            True
                                                        @else
                                                            False
                                                        @endif
                                                    </div>
                                                </div>
                                            @endforeach
									    @endif
							        </div>
						        </div>
						        <br>
						        <br>
						        <br>
				            </div>
			            </div>

	                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('js/dashboard/icheck.min.js') }}"></script>
<script>
$('.rating-modal').modal('show');
$(function(){

    function htmlDecode(input){
        var e = document.createElement('div');
        e.innerHTML = input;
        // handle case of empty input
        return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
    }

    if ($('#graph_donut').length ){
        choicedata = [];

        @if($total_num!=0)
			@foreach($choices as $i=>$choice)
				<?php
					$prefix_character = '';
					if ($i == 0) {
						$prefix_character = 'A';
					} elseif ($i == 1) {
						$prefix_character = 'B';
					} elseif ($i == 2) {
						$prefix_character = 'C';
					} elseif ($i == 3) {
						$prefix_character = 'D';
					} elseif ($i == 4) {
						$prefix_character = 'E';
					} elseif ($i == 5) {
						$prefix_character = 'F';
					}
				?>

				@if ($question->type == 0 )
					choicedata.push({label:htmlDecode("{{($prefix_character)}}"), value: "{{round(($choice->num_attempts / $total_num) * 100, 2)}}" });
				@else
					@if($choice->answer == 1)
						choicedata.push({label:"False", value: "{{ round((($total_num - $choice->num_attempts) / $total_num) * 100 , 2)}}" });
						choicedata.push({label:"True", value: "{{round(($choice->num_attempts / $total_num)*100, 2)}}" });
					@else
						choicedata.push({label:"True", value: "{{ round((($total_num - $choice->num_attempts) / $total_num) * 100 ,2)}}" });
						choicedata.push({label:"False", value: "{{round(($choice->num_attempts / $total_num)*100, 2)}}" });
					@endif
				@endif
			@endforeach
        @endif
        Morris.Donut({
            element: 'graph_donut',
            data: choicedata,
            colors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB', '#FF1493', '#DAA520'],
            formatter: function (y) {
                return y + "%";
            },
            resize: true
        });
    }
});
</script>
@endsection
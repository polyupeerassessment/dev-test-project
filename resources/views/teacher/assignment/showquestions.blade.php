@extends('layouts.dashboard')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>Your Assignment Questions</h3>
                        <a href="{{route('teacher.assignment.question.create', ['id'=>$assignment_id])}}" class="btn btn-success pull-right">Create Question</a>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content table-responsive borderless">
                        <table id="datatable-buttons-quiz-list" class="table table-striped table-bordered ">
                            <thead>
                            <tr>
                                <th>Question ID</th>
                                <th>Topic</th>
                                <th>Difficulty</th>
                                <th>Mode</th>
                                <th>Created Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($questions as $question)
                                <tr>
                                    <td>{{$question->id}}</td>
                                    <td>{{$question->topic}}</td>
                                    <td>
                                        @if ($question->difficulty == 90)
                                            Very Easy
                                        @elseif($question->difficulty == 70)
                                            Easy
                                        @elseif($question->difficulty == 50)
                                            Normal
                                        @elseif($question->difficulty == 30)
                                            Hard
                                        @else
                                            Very Hard
                                        @endif

                                    </td>
                                    <td>
                                        @if($question->mode == 0)
                                            Normal Contents
                                        @else
                                            HTML Contents
                                        @endif
                                    </td>
                                    <td>{{$question->created_at}}</td>
                                    <td>
                                        <a href="{{route('teacher.assignment.question.detail',['id'=>$question->id])}}" class="btn btn-md btn-primary ">View</a>
                                        <a href="{{route('teacher.assignment.question.delete',['id'=>$question->id])}}" class="btn btn-md btn-danger ">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

@section('javascript')

    <script>
        if ($("#datatable-buttons-quiz-list").length) {
            $("#datatable-buttons-quiz-list").DataTable({
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
            });
        }


    </script>
@endsection
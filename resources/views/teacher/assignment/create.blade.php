@extends('layouts.dashboard')

@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Create New Assignment</h3>
            </div>
            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        {!! Form::open(['route' => 'teacher.assignment.insert', 'class'=>"form-horizontal", 'name'=>'submit-form', 'files' => true, 'enctype'=>"multipart/form-data"]) !!}
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> Schedule <span class="required">*</span></label>

                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                    <input type="text" name="schedule"  id="reportrange" value="December 30, 2014 - January 28, 2015" style="padding: 5px 10px;" />
                                    <b class="caret"></b>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Assignment Topic <span class="required">*</span>
                                </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    {{Form::text('topic', null, array('class' => 'form-control col-md-7 col-xs-12','required'))}}
                                    @if ($errors->has('topic'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('topic') }}</strong>
                                        </span>
                                    @endif

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Student Max Attempts <span class="required">*</span>
                                </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    {{Form::number('max_attempts', null, array('min' => '1', 'class' => 'form-control col-md-7 col-xs-12', 'required'))}}
                                    @if ($errors->has('max_attempts'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('max_attempts') }}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Student Score <span class="required">*</span>
                                </label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    {{Form::number('score', null, array('min' => '1', 'max' => '40', 'class' => 'form-control col-md-7 col-xs-12', 'required'))}}
                                    @if ($errors->has('score'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('score') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <br><hr>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    {{Form::submit('Create', array('class'=>'btn btn-success'))}}
                                    {{ Form::reset('Reset',array('class'=>'btn btn-primary'))}}
                                </div>
                            </div>


                        {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('js/dashboard/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/dashboard/moment.min.js') }}"></script>
    <script>
      $('.myDatepicker').datetimepicker({
          format: 'H:mm'
      });


      $(function(){
          $('#content_type').on('change', function() {
              if(this.value === 'manually') {
                $('#question_selector').show();
              } else {
                  $('#question_selector').hide();
              }
          })
      });


    </script>
@endsection

@extends('layouts.dashboard')

@section('content')
    <style>
        .label {
            font-size:90%;
        }
    </style>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h3>Your Assignments</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content table-responsive borderless">
                        <table id="datatable-buttons-quiz-list" class="table table-striped table-bordered ">
                            <thead>
                            <tr>
                                <th>Assignment ID</th>
                                <th>Topic</th>
                                <th>Created Date</th>
                                <th>Start Quiz Date</th>
                                <th>Quiz Deadline</th>
                                <th>Maximum Attempts</th>
                                <th>Student Score</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assignments as $assignment)
                                <tr>
                                    <td>{{$assignment->id}}</td>
                                    <td>{{$assignment->topic}}</td>
                                    <td>{{$assignment->created_at}}</td>
                                    <td>{{$assignment->start_at}}</td>
                                    <td>{{$assignment->end_at}}</td>
                                    <td>{{$assignment->max_attempts}}</td>
                                    <td>{{$assignment->score}}</td>
                                    <td>
                                        <?php $today = date("Y-m-d H:i:s"); ?>
                                        @if($today > $assignment->end_at )
                                                <span class="label label-danger">Closed</span>
                                        @elseif($today <= $assignment->end_at && $today >= $assignment->start_at)
                                                <span class="label label-success">Active</span>
                                        @else
                                                <span class="label label-info">Pending</span>
                                        @endif
                                    </td>
                                    {{--<td><a href="{{route('teacher.quiz.result',['id'=>$assignment->id])}}" class="btn btn-success">Result</a></td>--}}
                                    <td>
                                        <a href="{{route('teacher.assignment.question.create',['id'=>$assignment->id])}}" class="btn btn-xs btn-primary ">Create Questions</a>
                                        <a href="{{route('teacher.assignment.question.list',['id'=>$assignment->id])}}" class="btn btn-xs btn-success">Show Questions</a>
                                        <a href="{{route('teacher.assignment.question.result',['id'=>$assignment->id])}}" class="btn btn-xs btn-warning">Results</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

@section('javascript')

    <script>
        if ($("#datatable-buttons-quiz-list").length) {
            $("#datatable-buttons-quiz-list").DataTable({
                dom: "Blfrtip",
                buttons: [
                    {
                        extend: "copy",
                        className: "btn-sm"
                    },
                    {
                        extend: "csv",
                        className: "btn-sm"
                    },
                    {
                        extend: "excel",
                        className: "btn-sm"
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm"
                    },
                    {
                        extend: "print",
                        className: "btn-sm"
                    },
                ],
                responsive: true,
            });
        }


    </script>
@endsection
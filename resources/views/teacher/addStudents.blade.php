@extends('layouts.dashboard')

@section('content')

    <div class="right_col" role="main">
            <div class="page-title">
                <div class="title_left">
                    <h3>Add Students</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">

                        {!! Form::open(['route' => 'teacher.insertstudents', 'class'=>"form-horizontal", 'name'=>'submit-form', 'files' => true, 'enctype'=>"multipart/form-data"]) !!}
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="csv_file">Enroll Students <span class="required">*</span>
                                </label>
                                <div class="col-md-7 col-sm-6 col-xs-12 ">
                                    <div class="form-control">
                                        <input type="file" name="csv_file" class=" col-md-7 col-xs-12" required>
                                        @if ($errors->has('csv_file'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('csv_file') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
                                <div class="col-md-7 col-sm-6 col-xs-12 ">
                                    <p>Please submit excel file with only one column which has list of new students' email. We will send invitation to the students once you submitted.</p>
                                </div>
                            </div>
                        <hr>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                {{Form::submit('Submit', array('class'=>'btn btn-success'))}}
                                {{ Form::reset('Reset',array('class'=>'btn btn-primary'))}}
                            </div>

                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
    </div>
    <!-- /page content -->
@endsection

@section('javascript')
    <script src="{{ asset('js/dashboard/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/dashboard/moment.min.js') }}"></script>
    <script>
      $('.myDatepicker').datetimepicker({
          format: 'H:mm'
      });

    </script>
@endsection

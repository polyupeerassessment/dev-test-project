@extends('layouts.dashboard')

@section('css')
    <style>
        .vote_modal_box {
            margin-top:15%;
        }
    </style>
@endsection
@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title" style="padding: 0px;">
            <div class="title_left"></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <h1>Voting Questions</h1><br><br>

                    <div class="x_content">

                        @foreach($questions as $question)
                            @if(empty($question))
                                @continue
                            @endif
                            <div class="row">
                                <hr><div class="clearfix"></div>
                                <div class="col-md-9 col-xs-9">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <h3 style="word-wrap: break-word;">{{$question->topic}}</h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 question_content" @if($question->mode == 0) style='white-space: pre-line;' @endif>
                                            <p style="max-width:100%;">
                                                @if($question->mode == 0)
                                                    {{ $question->contents }}
                                                @else
                                                    <?php
                                                    echo html_entity_decode($question_filter_content[$question->id]);
                                                    ?>
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-3" >
                                    {{--<a href="{{route('question.detail', ['id'=>$question->id])}}" class="btn btn-md btn-danger">Vote</a>--}}
                                    @if ($errors->has('vote'))
                                        <script type='text/javascript'>
                                            $(document).ready(function(e) {
                                                generate_PNotify('Failed to vote!');
                                            });
                                        </script>
                                    @endif

                                    <a href="{{route('teacher.detailquestion', ['id'=>$question->id])}}" class="btn btn-md btn-primary">View</a>

                                </div>
                            </div><div class="clearfix"></div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

@section('javascript')
@endsection
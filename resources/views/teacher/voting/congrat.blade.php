<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>{{config('app.name')}}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{asset('css/dashboard/bootstrap.min.css')}}" />
    <style>
        @import url(https://fonts.googleapis.com/css?family=Sigmar+One);

        body {
            background: #3da1d1;
            color: #fff;
            overflow: hidden;
        }
        .congrats {
            position: absolute;
            top: 15%;
            /*width: 550px;*/
            height: 100px;
            padding: 20px 10px;
            text-align: center;
            margin: 0 auto;
            left: 0;
            right: 0;
        }

        .congrats_name {
            position: absolute;
            top: 20%;
            padding: 20px 10px;
            margin: 0 auto;
            left: 210px;
            right: 0;
        }

        h1 {
            transform-origin: 50% 50%;
            font-size: 50px;
            font-family: 'Sigmar One', cursive;
            cursor: pointer;
            z-index: 2;
            position: absolute;
            top: 0;
            text-align: center;
            width: 100%;
        }

        .blob {
            height: 50px;
            width: 50px;
            color: #ffcc00;
            position: absolute;
            top: 45%;
            left: 45%;
            z-index: 1;
            font-size: 30px;
            display: none;
        }
    </style>

    <link rel="icon" href="{!! asset('images/title-icon.png') !!}"/>

    <!-- /page content -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.2/underscore-min.js"></script>
    <script type="text/javascript" src="http://s3.amazonaws.com/codecademy-content/courses/hour-of-code/js/alphabet.js"></script>
    <script>
        $(function() {
            var numberOfStars = 200;

            for (var i = 0; i < numberOfStars; i++) {
                $('.congrats').append('<div class="blob fa fa-star ' + i + '"></div>');
            }

            animateText();

            animateBlobs();
        });

        $('.congrats').click(function() {
            reset();

            animateText();

            animateBlobs();
        });

        function reset() {
            $.each($('.blob'), function(i) {
                TweenMax.set($(this), { x: 0, y: 0, opacity: 1 });
            });

            TweenMax.set($('h1'), { scale: 1, opacity: 1, rotation: 0 });
        }

        function animateText() {
            TweenMax.from($('h1'), 0.8, {
                scale: 0.4,
                opacity: 0,
                rotation: 15,
                ease: Back.easeOut.config(4),
            });
        }

        function animateBlobs() {

            var xSeed = _.random(350, 380);
            var ySeed = _.random(120, 170);

            $.each($('.blob'), function(i) {
                var $blob = $(this);
                var speed = _.random(1, 5);
                var rotation = _.random(5, 100);
                var scale = _.random(0.8, 1.5);
                var x = _.random(-xSeed, xSeed);
                var y = _.random(-ySeed, ySeed);

                TweenMax.to($blob, speed, {
                    x: x,
                    y: y,
                    ease: Power1.easeOut,
                    opacity: 0,
                    rotation: rotation,
                    scale: scale,
                    onStartParams: [$blob],
                    onStart: function($element) {
                        $element.css('display', 'block');
                    },
                    onCompleteParams: [$blob],
                    onComplete: function($element) {
                        $element.css('display', 'none');
                    }
                });
            });
        }
    </script>

</head>
<body>
    <div>
        <div class="congrats">
            <h1>Congratulations!</h1>
        </div>
        <img src="https://bit.ly/20qKWK0" style="display:none">
    </div>

    <div class="congrats_name">
        <canvas id="myCanvas"></canvas>
        <h3>

            <b>Number of Votes:</b> <span style="font-size: 170%;">{{$num}}</span> <i class="fa fa-trophy fa-2x" style="color:gold"></i>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <b>Topic:</b> {{$question->topic}}
            &nbsp;&nbsp;&nbsp;&nbsp;
            <b>Rating:</b> {{$question->rating}}
            &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="{{route('teacher.detailquestion',['id'=>$question->id])}}" class="btn btn-md btn-success">View</a>
        </h3>


        <script type="text/javascript" src="http://s3.amazonaws.com/codecademy-content/courses/hour-of-code/js/bubbles.js"></script>
        <script type="text/javascript" src="main.js"></script>
    </div>

    <script>
        $(function(){
            var myName = "{{$user->name}}";

            var red = [0, 100, 63];
            var orange = [40, 100, 60];
            var green = [75, 100, 40];
            var blue = [196, 77, 55];
            var purple = [280, 50, 60];
            var letterColors = [red, orange, green, blue, purple];

            drawName(myName, letterColors);

            if(10 < 3)
            {
                bubbleShape = 'square';
            }
            else
            {
                bubbleShape = 'circle';
            }

            bounceBubbles();
        });
    </script>

</body>
</html>

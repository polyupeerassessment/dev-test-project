@extends('layouts.dashboard')

@section('css')
    <link rel="stylesheet" href="{{asset('css/dashboard/dataTables.bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/buttons.bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/fixedHeader.bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/dashboard/responsive.bootstrap.min.css')}}" />
@endsection

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Your Announcements <small></small></h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">


                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        {{--<div class="x_title">--}}
                            {{--<h2>Responsive example<small>Users</small></h2>--}}
                            {{--<ul class="nav navbar-right panel_toolbox">--}}
                                {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                                {{--</li>--}}

                                {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                            {{--<div class="clearfix"></div>--}}
                        {{--</div>--}}
                        <div class="x_content">


                            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Topic</th>
                                    <th>Contents</th>
                                    <th>Course Code</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($mails as $mail)
                                    <tr>
                                        <td>{{$mail->topic}}</td>
                                        <td>{{$mail->contents}}</td>
                                        <td>{{$mail->course_id}}</td>
                                        <td>{{$mail->created_at}}</td>
                                        <td>
                                            <a href="{{route('teacher.cancelannounce', ['id'=>$mail->id])}}" class="btn btn-xs btn-danger">Delete</a>
                                            <a href="{{route('teacher.editannounce', ['id'=>$mail->id])}}" class="btn btn-xs btn-warning">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>

                            </table>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{asset('js/dashboard/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dashboard/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/dashboard/dataTables.buttons.min.js')}}"></script>
@endsection
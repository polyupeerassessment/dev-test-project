<?php

return [

    'create_question' => 'Create 3 questions',

    'answer_7' => 'Answer 4 Questions',
    'answer_8' => 'Answer 5 Questions',
    'answer_9' => 'Answer 6 Questions',

    'chat_7' => 'Comment 3 Question',
    'chat_8' => 'Comment 3 Question',
    'chat_9' => 'Comment 3 Question',

    'rating_7' => 'Rate 4 Question',
    'rating_8' => 'Rate 5 Question',
    'rating_9' => 'Rate 6 Question',
];

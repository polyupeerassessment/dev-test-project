<?php

return [

    'create_question' => 'Create 1 question',

    'answer_1' => 'Answer 1 Question',
    'answer_2' => 'Answer 2 Questions',
    'answer_3' => 'Answer 3 Questions',

    'chat_1' => 'Comment 1 Question',
    'chat_2' => 'Comment 2 Question',
    'chat_3' => 'Comment 3 Question',

    'rating_1' => 'Rate 1 Question',
    'rating_2' => 'Rate 2 Question',
    'rating_3' => 'Rate 3 Question',
];

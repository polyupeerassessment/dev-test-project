<?php

return [

    'create_question' => 'Create 2 questions',

    'answer_4' => 'Answer 4 Questions',
    'answer_5' => 'Answer 5 Questions',
    'answer_6' => 'Answer 6 Questions',

    'chat_4' => 'Comment 3 Question',
    'chat_5' => 'Comment 3 Question',
    'chat_6' => 'Comment 3 Question',

    'rating_4' => 'Rate 4 Question',
    'rating_5' => 'Rate 5 Question',
    'rating_6' => 'Rate 6 Question',
];

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignmentUserAnswered extends Model
{
    protected $table = 'assignment_user_answers';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignmentQuestionChoice extends Model
{
    protected $table = 'assignment_question_choices';
}

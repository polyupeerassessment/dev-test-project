<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignmentUserAttempt extends Model
{
    protected $table = 'assignment_user_attempts';
}

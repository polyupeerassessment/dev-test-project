<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAnswerSpent extends Model
{
    protected $table = 'user_question_spent';
}

import pandas as pd
import numpy as np
import math
import csv
from scipy.spatial.distance import cosine

# path
filename_irt_input = '/root/dev-test-project/storage/app/csv/irt_input.csv'
filename_irt_output = '/root/dev-test-project/storage/app/csv/irt_output.csv'
filename_question_difficulty = '/root/dev-test-project/storage/app/csv/question_difficulty.csv'


# results = pd.read_csv('storage/app/csv/irt_test.csv')




results = pd.read_csv(filename_irt_input)
M=results.pivot_table(index=['user_id'],columns=['question_id'],values='result')

difficulty_arr = []
question_list = M.columns
user_list = []

# COLUMN
for column in question_list:
    total_count = 0
    correct_count = 0

    for result in M[column].values:
        if ~np.isnan(result):
            total_count += 1
            if result == 1.0:
                correct_count += 1

    # nobody answered yet
    if total_count == 0:
        difficulty_arr.append(0)
        continue

    p = correct_count / total_count

    # question is too hard
    if p == 0:
        difficulty_arr.append(2)
        continue

    odds = (1 - p) / p

    # question is too easy
    if odds == 0:
        difficulty_arr.append(-2)
        continue

    # calculate difficulty of the question
    difficulty_arr.append(math.log(odds, math.e))

avg_diff_arr = np.mean(difficulty_arr)

original_difficulty_arr = np.subtract(difficulty_arr,avg_diff_arr)
num_questions = len(M.columns)

U = np.sum(np.multiply(original_difficulty_arr,original_difficulty_arr)) / (num_questions)


#ROW
user_level_arr =[]
num_users = 0;

for index, row in M.iterrows():
    user_list.append(index)
    total_count = 0
    correct_count = 0
    num_users += 1

    for result in row:
        if ~np.isnan(result):
            total_count += 1
            if result == 1.0:
                correct_count += 1

    # user does not do any actions
    if total_count == 0:
        user_level_arr.append(0)
        continue

    p = correct_count / total_count


    # user has no correct answer yet
    if p == 0:
        user_level_arr.append(-2)
        continue

    # user has all correct
    if p == 1:
        user_level_arr.append(2)
        continue

    odds = p / (1 - p)

    # calculate user's study level
    user_level_arr.append(math.log(odds, math.e))

V = np.sum(np.multiply(user_level_arr,user_level_arr)) / (num_users)

final_extend_value_question = math.sqrt( (1+(V/2.89)) /  (1-(U*V)/8.35) )
final_extend_value_student = math.sqrt( (1+(U/2.89)) /  (1-(U*V)/8.35) )

final_difficulty_arr = np.multiply(original_difficulty_arr, final_extend_value_question)
final_user_level_arr = np.multiply(user_level_arr, final_extend_value_student)


# Matrix: [user_id, question_id, P]
newM = []

for i, user_level in enumerate(final_user_level_arr):
    newTmp=[]
    question_level_tmp = []
    for j, question_level in enumerate(final_difficulty_arr):
        diff = user_level - question_level

        final_result_p = 100 - 1 / (1 + math.pow(math.e, diff)) * 100

        newTmp.append([user_list[i],question_list[j],final_result_p])

    newM.append(newTmp)


question_M_difficulty = []

for i, q_level in enumerate(final_difficulty_arr):
    question_M_difficulty.append([question_list[i], q_level])


#STORE IRT RESULT
df = pd.DataFrame(0, index=user_list, columns=question_list)
i = 0
for index, row in df.iterrows():
    j = 0
    for q in question_list:
        df.loc[index][q] = newM[i][j][2]
        j += 1
    i += 1
# df.to_csv("storage/app/csv/file_path.csv")
df.to_csv(filename_irt_output)

#STORE QUESTION LEVEL RESULT
df_q = pd.DataFrame(question_M_difficulty)
df_q.to_csv(filename_question_difficulty)


<?php
/**
 * Created by PhpStorm.
 * User: darshan
 * Date: 15/02/18
 * Time: 11:30 PM
 */

namespace App\Http\Controllers;

use Khill\Lavacharts\Lavacharts;


class ChartsController extends Controller
{

    //Lava:: Lavacharts;

    private $lava;

    private $finances;

    public function __construct()
    {
        //$this->middleware('auth');
        $this->lava = new Lavacharts;


    }

    public function createData(){
        $this->finances = $this->lava->DataTable();

        $this->finances->addDateColumn('Year')
            ->addNumberColumn('Sales')
            ->addNumberColumn('Expenses')
            ->setDateTimeFormat('Y')
            ->addRow(['2004', 1000, 400])
            ->addRow(['2005', 1170, 460])
            ->addRow(['2006', 660, 1120])
            ->addRow(['2007', 1030, 54]);

        $this->lava->ColumnChart('Finances', $this->finances, [
         'title' => 'Company Performance',
         'titleTextStyle' => [
             'color'    => '#eb6b2c',
            'fontSize' => 14

       ]
]); }
}
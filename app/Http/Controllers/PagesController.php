<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enrollment;
use Symfony\Component\VarDumper\Cloner\Data;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
    public function __construct()
    {
//        $this->middleware('');

    }
    public function index(Request $request) {
        $title = 'Welcome to PolyU Peer Assessment';

        if( Auth::guard('web')->check()) {

            return redirect()->route('student.home');

        } elseif (Auth::guard('teacher')->check()) {
            return redirect()->route('teacher.home');

        } else {
            Log::info('[Home Page] [Guest] From IP Addr: '.$request->ip());
            return view('pages.index', compact('title'));
        }

        
    }

    public function accessCode()
    {
        return view('pages.access');
    }

    public function uploadFile() {
        return view('upload');
    }

    public function successUpload(Request $request) {
        $this->validate($request, array(
            'enroll_course_id' => 'required',
        ));

        $filePath = $request->file('upload_file')->getRealPath();

        $file = fopen($filePath,'r');
        $header= fgetcsv($file); // headers

        $successFlag = -1;
        $db = new DatabaseCapture();

        $course_id = $request->enroll_course_id;
//        print_r($header);

        while($line=fgetcsv($file)) {
            if(empty($line[0]) || empty($line))
            {
                continue;
            }

            $student_id = $line[0];

            //check if the student registered
            if(empty($db->getStudentById($student_id))) {
                continue;
            }

            //check if the student already registered
            if (!empty($db->getCourseInformationById($course_id, $student_id)[0])) {
                continue;
            }

            //check if the course_id exists


            $enroll = new Enrollment();
            $enroll->student_id = (int)$student_id;
            $enroll->course_id = trim((string)$course_id);

            $enroll->save();

            $successFlag = 1;
        }


        return redirect()->route('upload')->with('success', $successFlag);

    }

    public function about() {
        $title = 'About PolyU Peer Assessment';

        return view('pages.about', compact('title'));
    }

    public function usermanual() {
        $title = 'About PolyU Peer Assessment';

        return view('pages.usermanual', compact('title'));
    }

}

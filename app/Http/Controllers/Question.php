<?php

namespace App\Http\Controllers;

use Session;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use \Exception;


class Question
{
    public function __construct() {}

    public function getQuestionsByKeywords($keyword)
    {
        $db = new DatabaseCapture();

        $questions = $db->searchEngine($keyword, Session::get('courseID'));

        return $questions;
    }

    public function getQuestion ($id)
    {
        $question = DB::table('questions')->where('id', $id)->first();
        return $question;
    }

    public function getQuestions($mode = 1, $submode = -1)
    {
        $quetions = null;

        switch ($mode) {
            case 1:
                $questions = $this->getQuestionsOrderByDate($submode); break;
            case 2:
                $questions = $this->getQuestionsOrderByPopularity($submode); break;
            case 3:
                $questions = $this->getQuestionsOrderByRating($submode); break;
            default:
                break;
        }

        return $questions;
    }

    private function getQuestionsOrderByPopularity($order_popularity_mode)
    {
        $sql_query = 'SELECT * from questions as q where q.course_id = ? and q.author_id != ? order by case ';
        $sql_argument = [Session::get('courseID'), Auth::id()];

        $mining = new MiningController();
        $question_list = $mining->getQuestionIdList_Popularity_Order(Session::get('courseID'));

        foreach ($question_list as $i => $q_id ) {
            $sql_query .= ' when id = '.$q_id.' then '.($i+1);
        }
        $sql_query .= ' else id end ';

        if ($order_popularity_mode == 1 ) {
            $sql_query .= 'ASC';
        } else {
            $sql_query .= 'DESC';
        }

        $questions = DB::select($sql_query,$sql_argument );

        return $questions;
    }

    private function getQuestionsOrderByDate($order_date_mode)
    {
        $sql_query = 'select * from questions where course_id =? order by created_at ';

        if ($order_date_mode == 1 ) {
            $sql_query .= 'ASC';
        } else {
            $sql_query .= 'DESC';
        }

        $questions = DB::select($sql_query,[Session::get('courseID')]);

        return $questions;
    }

    private function getQuestionsOrderByRating ($order_rating_mode)
    {
        $sql_query = 'select * from questions where course_id =?  order by rating ';
        if($order_rating_mode == 1) {
            $sql_query .=  'DESC';
        } else {
            $sql_query .=  'ASC';
        }

        $questions = DB::select($sql_query, [Session::get('courseID')]);

        return $questions;
    }

    public function getQuestionSurroundings ($id, $isMadeByTeacherFlag = 0)
    {
        $questions = DB::table('questions')->where('course_id',Session::get('courseID'))->orderBy('created_at','desc')->get();
        $next_flag = false;
        $next_question_id = -1;
        $pre_question_id = -1;

        foreach($questions as $q) {
            if($q->isMadeByTeacher != $isMadeByTeacherFlag || $q->author_id == Auth::id()) {
                continue;
            }

            if($next_flag) {
                $next_question_id = $q->id;
                break;
            }

            if($q->id == $id) {
                $next_flag = true;
                continue;
            }

            $pre_question_id = $q->id;
        }

        return [$pre_question_id, $next_question_id];
    }

    public function getChoices ($id)
    {
        $choices = DB::table('question_choices')->where('question_id','=',$id)->get();

        return $choices;
    }


}
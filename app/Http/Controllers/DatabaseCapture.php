<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use \Exception;

use App\User;
use Carbon\Carbon;

class DatabaseCapture {


    public function searchEngine($keyword, $courseID) {
        $keyword = "%$keyword%";
        $result_query = DB::select("select *
from questions as Q
where Q.course_id = ? and CONCAT_WS(Q.id, Q.topic, Q.contents, Q.rating, Q.created_at,
    case when (Q.type = 0) then 'Multiple Choice Question MC MCQ' else 'True False Question TF T/F ' end,
    case when (Q.difficulty = 0) then 'Easy' when (Q.difficulty = 1) then 'Normal' else 'Hard' end
  )  Like ? OR exists
  (
    select * from keywords as K
    where K.question_id = Q.id and 
    K.keyword like ? and Q.course_id = ? 
  )
;",[$courseID, $keyword, $keyword,$courseID]);

        return $result_query;
    }

    public function updateUserLastActivityDate($id) {
        User::where('id',$id)->update(['last_activity_date'=>Carbon::now()]);
    }

    public function getCourseByTeacherId($id = -1) {

        if ($id == -1) {
            $id = Auth::id();
        }

        return DB::table('courses')->where('instructor_id', $id)->select('course_code', 'course_name', 'department'
        , 'enroll_code', 'created_at','description', 'default_session')->get();
    }

    public function getStudentEmailByQuestionID($questionID, $courseID) {
        $result_query = DB::select('select email from users as S where exists 
                                    (select * from questions as Q where Q.author_id = S.id and Q.id = ? and Q.course_id = ?);',[$questionID,$courseID]);

        return $result_query;
    }

    public function getTeacherById($id = -1) {

        if ($id == -1) {
            $id = Auth::id();
        }
        return DB::table('teachers')->where('id', $id)->first();
    }

    //admin
    public function getDefaultCourseCodeByTeacherId($id = -1) {

        if ($id == -1) {
            $id = Auth::id();
        }

        $result_query = DB::select('select course_code from courses where instructor_id = ? and default_session = 1',[$id]);
        return $result_query;
    }

    //admin
    public function getStudentsByTeacherId($id = -1) {

        if ($id == -1) {
            $id = Auth::id();
        }
        return DB::select('select * from users where exists
            (select student_id from enrollments as e, courses as c, teachers as t where e.course_id = c.course_code and c.instructor_id = ?) order by updated_at DESC ', [$id]);
    }

    //admin
    public function getMailsByTeacherId($id = -1) {

        if ($id == -1) {
            $id = Auth::id();
        }
        return DB::select('select * from announcements where author_id = ?', [$id]);
    }
    //admin
    public function getActiveNumStudentsByTeacherId($id = -1) {

        if ($id == -1) {
            $id = Auth::id();
        }

        $current = Carbon::now();
        $current = $current->subMinutes(10);

        $result_query = DB::select('select count(id) as num from users where status = 1 and exists
            (select student_id from enrollments as e, courses as c, teachers as t where e.course_id = c.course_code and c.instructor_id = ?) and last_activity_date >= ?;', [$id, $current]);

        if(empty($result_query[0])) {
            return 0;
        } else {
            return $result_query[0]->num;
        }
    }

    //admin
    public function getActiveNumStudentsByCourseId($courseID) {
        $num = 0;
        $current = Carbon::now();

        $result_query = DB::select('select id, last_activity_date, count(*) as num from users where status = 1 and exists
            (select student_id from enrollments as e where e.course_id = ?) group by id, last_activity_date', [$courseID]);

        foreach($result_query as $r) {

            if(!empty($r->last_activity_date)) {
                $d = new Carbon($r->last_activity_date);
                if ($current->diffInMinutes($d)<10) {
                    $num++;
                }
            }
        }

        return $num;
    }

    //admin
    public function getActiveStudentsByCourseId($courseID) {
        $students = [];
        $current = Carbon::now();

        $result_query = DB::select('select id, last_activity_date from users where status = 1 and exists
            (select student_id from enrollments as e where e.course_id = ?) group by id, last_activity_date', [$courseID]);

        foreach($result_query as $r) {

            if(!empty($r->last_activity_date)) {
                $d = new Carbon($r->last_activity_date);
                if ($current->diffInMinutes($d)<10) {
                    $students[$r->id] = 1;
                } else {
                    $students[$r->id] = -1;
                }
            }
        }

//        print_r($students);

        return $students;
    }

    //admin
    public function getChatsByCourseId($courseID) {

        $num1 = 0;
        $num2 = 0;

        $result_query_comments = DB::select('select count(comment) as num from comments, questions as q, courses where
                                comments.question_id = q.id and q.course_id = courses.course_code and courses.course_code = ?', [$courseID]);

        $result_query_discussion = DB::select('select count(contents) as num from discussions, courses where
                                discussions.course_id = courses.course_code and courses.course_code = ?', [$courseID]);

        if (!empty($result_query_comments[0])) {
            $num1 = $result_query_comments[0]->num;
        }

        if (!empty($result_query_discussion[0])) {
            $num2 = $result_query_discussion[0]->num;
        }


        return $num1 + $num2;
    }

    //admin
    public function getAnswersCourseId($courseID) {

        $result_query = DB::select('select u.user_id, u.updated_at as updated from user_answereds as u, courses as c, questions as q where q.course_id = c.course_code and c.course_code = ? and q.id = u.question_id order by u.updated_at DESC', [$courseID]);

        return $result_query;
    }

    //admin
    public function getQuestionsCourseId($courseID) {

        $result_query = DB::select('select topic, contents, difficulty,rating, q.updated_at as updated, author_id, course_id, course_name, department from questions as q, courses as c where q.course_id = c.course_code and c.course_code = ? order by updated DESC', [$courseID]);

        return $result_query;
    }

    //admin
    public function getChatByTeacherId($id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }
        $num1 = 0;
        $num2 = 0;

        $result_query_comments = DB::select('select count(comment) as num from comments, courses, questions as q where courses.instructor_id = ? and q.course_id = courses.course_code
and comments.question_id = q.id;', [$id]);

        $result_query_discussion = DB::select('select count(contents) as num from discussions, courses where courses.instructor_id = ?
and discussions.course_id = courses.course_code;', [$id]);

        if (!empty($result_query_comments[0])) {
            $num1 = $result_query_comments[0]->num;
        }

        if (!empty($result_query_discussion[0])) {
            $num2 = $result_query_discussion[0]->num;
        }


        return $num1 + $num2;
    }

    //admin
    public function getQuestionsTeacherId($id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }
        $result_query = DB::select('select topic, contents, difficulty,rating, q.updated_at as updated, author_id, course_id, course_name, department from questions as q, courses as c where q.course_id = c.course_code and c.instructor_id = ? order by updated DESC', [$id]);

        return $result_query;
    }

    //admin
    public function getChatsByTeacherId($id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }
        $result_query = DB::select('select comment,c.updated_at as updated from comments as c, courses as co, questions as q where c.question_id = q.course_id and q.course_id = co.course_code and co.instructor_id = ? order by c.updated_at DESC', [$id]);

        return $result_query;
    }

    //admin
    public function getAnswersTeacherId($id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }
        $result_query = DB::select('select u.user_id, u.updated_at as updated from user_answereds as u, courses as c, questions as q where q.course_id = c.course_code and c.instructor_id = ? and q.id = u.question_id order by u.updated_at DESC', [$id]);

        return $result_query;
    }

    //admin
    public function getCourseByTeacher($courseID, $id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }

        $result_query = DB::select('select * from courses where instructor_id = ? and course_code = ?', [$id, $courseID]);
        return $result_query;
    }

    //admin
    public function getStudentsByCourseId($courseID)
    {
        $result_query = DB::select('select * from users as S where 
                                    exists (select * from enrollments as E, courses as C where C.course_code = ?
                                    and C.course_code = E.course_id and E.student_id = S.id)', [$courseID]);
        return $result_query;
    }

    public function getStudents_NumAnswers_ByCourseId($courseID)
    {
        $result_query = DB::select('select A.user_id as user_id,
                                    case
                                        when result is null then 0
                                        else count(A.result)
                                    end as num
                                    from user_answereds as A, questions as q where A.question_id = q.id and q.course_id = ? and    
                                    exists (select * from enrollments as E, courses as C where C.course_code = q.course_id
                                    and C.course_code = E.course_id and E.student_id = A.user_id) group by user_id, result', [$courseID]);

        return $result_query;
    }

    public function getStudents_NumQuestions_ByCourseId($courseID)
    {
        $result_query = DB::select('select U.id, count(Q.id) as num from enrollments as E
join users as U on U.id = E.student_id and E.course_id =?
left join questions as Q on Q.isMadeByTeacher = 0 and Q.author_id = E.student_id and Q.course_id = E.course_id group by U.id', [$courseID]);

        return $result_query;
    }

    public function getStudents_NumQuestions_ByCourseId_ByStudentID($courseID, $studentID)
    {
        $result_query = DB::select('select U.name, U.nickname, Q.id, Q.topic, Q.created_at from users as U, questions as Q where Q.author_id =U.id and Q.isMadeByTeacher = 0 and course_id = ? and U.id = ?;', [$courseID, $studentID]);

        return $result_query;
    }

    public function getStudents_NumAnswers_ByCourseId_ByStudentID($courseID, $studentID)
    {
        $result_query = DB::select('
        select ua.user_id as user_id, ua.created_at,
        sum(ua.num_attempts) as num
        from user_answereds as ua, questions as q where q.course_id = ? and user_id = ? and q.id = ua.question_id group by created_at, user_id
        ', [$courseID, $studentID]);

        return $result_query;
    }

    public function getStudents_NumAnswered_ByCourseId_ByStudentID($courseID, $studentID)
    {
        try{
            $result_query = DB::select(' 
            select Q.author_id, UA.created_at, count(UA.id) as num from user_answereds as UA, questions as Q where UA.question_id = Q.id and Q.course_id = ?  and Q.author_id = ?
            group by UA.created_at, Q.author_id ', [$courseID, $studentID]);
        } catch(Exception $e) {
            return $e;
        }


        return $result_query;
    }

    public function getStudents_NumCorrect_ByCourseId($courseID)
    {
        $result_query = DB::select('select user_id, count(result) as num
                                      from user_answereds as A, questions as q where A.result= 1 and q.id = A.question_id  
                                        and q.course_id = ? group by user_id', [$courseID]);

        return $result_query;
    }

    public function getStudentById($id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }
        return DB::table('users')->where('id', $id)->first();
    }

    public function getStudentByEmail($email) {

        return DB::table('users')->where('email', $email)->first();
    }

    public function checkQuestionSimilarity($content, $courseID, $except_question_id = -1, $topic ='', $choices, $choice_string = '') {
        if($except_question_id != -1 ) {
            $result_query = DB::select('select id, contents, topic from questions where course_id = ? and id != ? ', [$courseID,$except_question_id]);
        } else {
            $result_query = DB::select('select id, contents, topic from questions where course_id = ?', [$courseID]);
        }

        $questions = [];

        //combine all together
        foreach ($result_query as $question) {
            $questions[$question->id] = $question->topic.'/'.$question->contents;
        }

        $result_query_for_choices = DB::table('question_choices')->get();
        foreach($result_query_for_choices as $choice ){
            if(empty($questions[$choice->question_id])) {
                $questions[$choice->question_id] = $choice->choice;
            } else {
                $questions[$choice->question_id] .= '/'.$choice->choice;
            }
        }

        //combine user's question
        $question_user = $topic.'/'.$content;

        foreach($choices as $choice) {
            $question_user .= '/'.$choice;
        }
        if(!empty($choice_string)){
            $question_user .= '/'.$choice_string;
        }

        $max_percent = 0;
        foreach($questions as $db) {
            similar_text($question_user, $db, $percent);
            if ($max_percent < $percent) {
                $max_percent = $percent;
            }
        }
        return $max_percent;
    }

    public function getStudents() {
        return DB::table('users')->get();
    }

    public function getStudentNameById($id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }
        $user =  DB::table('users')->where('id', $id)->first();

        return $user->name;

    }

    public function getStudentNicknameById($id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }
        $user =  DB::table('users')->where('id', $id)->first();

        return $user->nickname;
    }

    public function getDiscussions() {
        return DB::table('discussions')->get();
    }

    public function getNameWithDiscussion() {
        $sql_statement = 'select * from users, discussions where author_id = users.id order by discussions.created_at ASC ;';

        $result= DB::select($sql_statement);

        return $result;
    }

    public function getNameWithComment($id) {
        $sql_statement = 'select * from users, comments where author_id = users.id and question_id = ? order by comments.created_at ASC ;';

        $result= DB::select($sql_statement,[$id]);

        return $result;
    }

    public function getNumAttemptsQuestions($questionID, $id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }
        $sql_result = DB::select('select num_attempts from user_answereds where user_id = ? and question_id = ? ;',[$id, $questionID]);

        if(empty($sql_result)) {
            return 0;
        }

        return $sql_result[0]->num_attempts;
    }

    public function addNumAttemptsQuestions($questionID, $id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }

        $sql_result = DB::table('user_answereds')->where([['user_id', $id], ['question_id', $questionID]])->first();
        if (empty($sql_result)) {
            return 0;
        }

        DB::table('user_answereds')->where([['user_id', $id], ['question_id', $questionID]])->increment('num_attempts');

        return 1;

        //DB::table('user_answereds')->where([['user_id', $id], ['question_id', $questionID]])->update(['num_attempts' => DB::raw('num_attempts+1')]);
    }

    public function compareAttemptsQuestions($questionID, $id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }

        $limit_attempts = DB::select('select num_attempts from questions where id = ? ;',[$questionID]);
        $current_attempts = DB::select('select num_attempts from user_answereds where user_id = ? and question_id = ? ;',[$id, $questionID]);

        if(empty($limit_attempts) || empty($current_attempts)) {
            return 1;
        }

        return ($current_attempts[0]->num_attempts < $limit_attempts[0]->num_attempts)?1:0;
    }

    public function updateUserAttemptsResult($questionID, $user_result_flag, $id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }

        DB::table('user_answereds')
            ->where([['user_id', $id],['question_id',$questionID]])
            ->update(['result' => $user_result_flag]);
    }

    public function checkQuestionsId($questionID) {
        $sql_result = DB::select('select id from questions where id = ?;',[$questionID]);

        if(empty($sql_result)) return true; //no duplicated
        return false; //cannot use this question id due to already existed
    }

    public function checkAssignmentQuestionsId($questionID) {
        $sql_result = DB::select('select id from assignment_questions where id = ?;',[$questionID]);

        if(empty($sql_result)) return true; //no duplicated
        return false; //cannot use this question id due to already existed
    }

    public function getCourseByStudentId($id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }

        $sql_result = DB::select('select C.course_code, C.course_name, T.name from enrollments as E, courses as C,teachers as T where T.id = C.instructor_id and E.course_id = C.course_code and E.student_id = ?;',[$id]);

        return $sql_result;

    }

    public function getCourseInformationById($courseID, $id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }

        $sql_result = DB::select('select * from enrollments as E, courses as C where E.course_id = C.course_code and E.student_id = ? and C.course_code = ?;',[$id,$courseID]);

        return $sql_result;

    }

    public function getRatingByIds($courseID, $questionID, $id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }

        $sql_result = DB::select('select rating from user_answereds where question_id = ?  and user_id = ? ',[$questionID, $id]);

        if (empty($sql_result[0]) || is_null($sql_result[0]->rating)) {
            return -1;
        }

        return $sql_result[0]->rating;

    }

    public function getCourses() {
        return DB::select('select course_code, course_name, department, description, instructor_id, created_at  from courses;');
    }

    public function getEnrollCodeByCourseId($courseID) {
        $sql_result = DB::select('select enroll_code from courses where course_code = ?;',[$courseID]);

        return $sql_result;
    }

    // Assignment
    public function addNumAssignmentAttemptsQuestions($questionID, $id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }

        $sql_result = DB::table('assignment_user_answers')->where([['user_id', $id], ['assignment_question_id', $questionID]])->first();
        if (empty($sql_result)) {
            return 0;
        }
        return 1;
    }

    public function updateAssignmentUserAttemptsResult($questionID, $user_result_flag, $id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }

        DB::table('assignment_user_answers')
            ->where([['user_id', $id],['assignment_question_id',$questionID]])
            ->update(['result' => $user_result_flag]);
    }

    public function addAssignmentUserAttemptsRecord($assignmentID, $id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }

        $sql_result = DB::table('assignment_user_attempts')->where([['user_id', $id], ['assignment_id', $assignmentID]])->first();
        if (empty($sql_result)) {
            return 0;
        }
        return 1;
    }

    public function updateAssignmentUserAttemptsRecordResult($assignmentID, $isCorrectAllAns, $id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }

        $sql_result = DB::table('assignment_user_attempts')->where([['user_id', $id], ['assignment_id', $assignmentID]])->first();
        $updatedTotalCount = $sql_result->total_count;
        $updatedTotalScore = $sql_result->total_score;

        if ($isCorrectAllAns)
        {
            $updatedTotalCount++;
            $updatedTotalScore++;
        }
        else
        {
            $updatedTotalCount++;
        }

        DB::table('assignment_user_attempts')->where([['user_id', $id],['assignment_id',$assignmentID]])->update(['total_count' => $updatedTotalCount]);
        DB::table('assignment_user_attempts')->where([['user_id', $id],['assignment_id',$assignmentID]])->update(['total_score' => $updatedTotalScore]);
    }

    public function resetAssignmentUserAttemptsRecord($assignmentID, $id = -1) {
        if ($id == -1) {
            $id = Auth::id();
        }

        DB::table('assignment_user_attempts')->where([['user_id', $id],['assignment_id',$assignmentID]])->update(['total_count' => 0]);
        DB::table('assignment_user_attempts')->where([['user_id', $id],['assignment_id',$assignmentID]])->update(['total_score' => 0]);   
    }

}

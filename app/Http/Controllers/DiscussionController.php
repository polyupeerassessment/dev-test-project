<?php

namespace App\Http\Controllers;

use App\ThreadMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Discussion;
use Session;

class DiscussionController extends Controller
{
    private $db;
    private $json;

    public function __construct()
    {
        $this->middleware('auth');
        $this->db = new DatabaseCapture();
        $this->json = new StorageController();
    }

    public function discussion()
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

//        $threads = Discussion::where('course_id','=', Session::get('courseID'))->paginate(15);
        $threads = DB::select("select D.id,D.author_id, D.contents, D.created_at, U.nickname, D.topic, D.course_id
        from discussions as D, users as U where U.id = D.author_id and course_id = ?", [Session::get('courseID')]);

        return view('discussion.discussion',['threads'=>$threads]);
    }

    public function createPage() //For new thread
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        return view('discussion.createpage');
    }

    public function showDetail($id) {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        $thread = Discussion::where('course_id','=', Session::get('courseID'))->where('id','=', $id)->first();
        $messages = DB::select('select nickname, messages, t.created_at from thread_messages as t, users as u where u.id = t.author_id and t.thread_id = ? ;',[$thread->id]);


        return view('discussion.detail',['thread'=>$thread,'messages'=>$messages]);
    }

    public function threadDelete(Request $request) {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        $deletedRows = Discussion::where('id', $request->id)->delete();
        return redirect()->route('discussion.index')->with('success', "Successfully Deleted Thread!");
    }

    public function insertMessage(Request $request) {
        $this->validate($request, array(
            'message' => 'required',
        ));

        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        $msg = new ThreadMessage();
        $msg->messages = $request->message;
        $msg->thread_id = $request->thread_id;
        $msg->author_id = Auth::id();
        $msg->save();

        //store into json file
//        $tmpdate = array('author_id'=>Auth::id(), 'text'=>$request->message, 'time'=>date("Y-m-d H:i:s"), 'location'=>'course_discussion');
//        $this->json->addLog($tmpdate, 'course_discussion');

        return redirect()->route('discussion.show',$request->thread_id)->with('success', 'Successfully posted!');
    }

    public function insertThread(Request $request) {
        $this->validate($request, array(
            'topic' => 'required',
            'contents' => 'required',
        ));

        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        $model = new Discussion();
        $model->topic = $request->topic;
        $model->contents = $request->contents;
        $model->course_id = Session::get('courseID');
        $model->author_id = Auth::id();

        $model->save();


        return redirect()->route('discussion.index')->with('success', "Successfully Created Thread!");
    }

    public function insertdiscussion(Request $request)
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        $discussion = new Discussion();
        $discussion->text = $request->message;
        $discussion->author_id = Auth::id();
        $discussion->course_id = Session::get('courseID');

        $discussion->save();

        //store into json file
        $tmpdate = array('author_id'=>Auth::id(), 'text'=>$request->message, 'time'=>date("Y-m-d H:i:s"), 'location'=>'course_discussion');
        $this->json->addLog($tmpdate, 'course_discussion');

        return redirect()->route('discussion.index')->with('success', true);
    }



}

<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

use Session;
use App\Enrollment;
use App\Notifications\notifyStudent;
use Notification;
use \Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;


class StudentController extends Controller
{

    private $db;
    private $json;
    private $student;

    public function __construct()
    {
        $this->middleware('auth');
        $this->db = new DatabaseCapture();
        $this->json = new StorageController();
        $this->student = new Student();
    }

    public function showAnnouncement() {
        $announcements = auth()->user()->Notifications;

        //Laravel Log
        Log::info('[Announcement] ['.Auth::id().'] Enter Announcement Page.');

        return view('student.announce', ['announcements'=>$announcements]);
    }

    public function exitCourse()
    {
        //Laravel Log
        Log::info('[Course.Exit] ['.Auth::id().'] Exit Course. course id: '. Session::get('courseID'));

        //Json log
        $log = array('user_id'=>Auth::id(), 'course_id'=>Session::get('courseID'), 'status'=>0, 'time'=>date("Y-m-d H:i:s"));
        $this->json->addLog($log, 'exit_course');

        Session::forget('courseID');

        return redirect()->route('student.home')->with('status', "Successfully Exit");

    }

    public function index()
    {
        $questions_num = DB::select('select count(*) as num_question from questions');
        $courses = $this->db->getCourseByStudentId();

        $today = Carbon::today();
        $sysannounce = DB::table('announcements')->where('type',1)->where('start_at', '<=', $today)->where('end_at', '>=', $today)->get();

        $enrolled = false;

        if(!empty($courses[0])) {
            $enrolled = true;
        }

        //Laravel Log
        Log::info('[Home] ['.Auth::id().'] Enter home dashboard. ');
        
        return view('student.home',['sysannounce'=>$sysannounce,'questions_num'=>$questions_num, 'enrolled'=>$enrolled, 'courses'=>$courses]);
    }

    public function top5Questions($courseID) {
        $questions = DB::select('select * from questions where course_id = ? order by rating DESC limit 5', [$courseID]);

        return $questions;
    }

    public function course_dashboard($courseID)
    {
        $courseInformation = $this->db->getCourseInformationById($courseID);

        if(empty($courseInformation[0])) {
            return redirect()->route('student.home')->with('fail', "You are not enrolled this course");
        }

        Session::put('courseID', $courseID);

        $log = array('student_id'=>Auth::id(), 'course_id'=>$courseID, 'status'=>1, 'time'=>date("Y-m-d H:i:s"));
        $this->json->addLog($log, 'enter_course_dashboard');

        $leaderboard = new LeaderboardController();

        $questions_num = DB::select('select count(*) as num_question from questions where course_id = ?', [$courseID]);
        $questions_created = DB::select('select count(topic) as num_question from questions where author_id =?', [Auth::id()]);
        $num_answered = DB::select('select count(result) as num_answered from user_answereds, questions as q where user_id =? and q.course_id = ? and q.id = user_answereds.question_id = q.id group by user_id', [Auth::id(),$courseID]);
        $num_answered_correct = DB::select('select count(user_id) as num_correct from 
                                (select distinct user_id, question_id from user_answereds, questions as q where user_id =? AND result = 1 and q.id = user_answereds.question_id and q.course_id = ? ) as T', [Auth::id(), $courseID]);

        $students_ranking = $leaderboard->score_ranking(5,$courseID,8);
        $top_keywords = $leaderboard->keyword_ranking(5, $courseID);
        $top_questions = $leaderboard->question_ranking(5,$courseID);
        $top_created_questions = $leaderboard->students_created_ranking(5,$courseID,8);

        $user_performance = $leaderboard->student_performance($courseID);

        $mining = new MiningController();
        $hot_topics =  $mining->keywords_detection($courseID);

        //Store into JSON
        $path = '/json/'.$courseID.'/hot_topic.json';
        $new_hot_topics = [];
        $total_num = 0;

        if (file_exists(storage_path().'/app/'.$path)===false) {
            foreach($hot_topics as $k=>$num) {
                $new_hot_topics[$k] = [$num, 2];
                $total_num += $num;
            }

            $myJSON = json_encode($new_hot_topics,JSON_PRETTY_PRINT);
            Storage::put($path, $myJSON);
        } else {
            $pre_hot_topics = json_decode(file_get_contents(storage_path().'/app/'.$path), true);

            //check if anything need to change
            $pre_hot_topics_tmp = [];
            foreach ($pre_hot_topics as $v=>$arr) {
                $pre_hot_topics_tmp[$v] = $arr[0];
                $total_num += $arr[0];
            }

            if (array_slice($pre_hot_topics_tmp, 0, 3, true) == array_slice($hot_topics, 0, 3, true)) {
                $new_hot_topics = $pre_hot_topics;
            } else {
                $total_num = 0;
                $i = 1;
                foreach($hot_topics as $cur_v=>$cur_num) {
                    $j = 1;
                    $found_in_prev_flag = false;
                    foreach($pre_hot_topics as $pre_v=>$pre_num) {
                        if($pre_v == $cur_v) {
                            $found_in_prev_flag = true;
                            $diff = $i - $j;
                            if ($diff > 0) {
                                $new_hot_topics[$cur_v] = [$cur_num, 0];
                            } elseif($diff == 0 ) {
                                $new_hot_topics[$cur_v] = [$cur_num, 1];
                            } else {
                                $new_hot_topics[$cur_v] = [$cur_num, 2];
                            }

                            $total_num += $cur_num;
                            break;
                        }
                        $j++;
                    }
                    if (!$found_in_prev_flag) {
                        $total_num += $cur_num;
                        $new_hot_topics[$cur_v] = [$cur_num, 2];
                    }
                    $i++;
                }

                $myJSON = json_encode($new_hot_topics,JSON_PRETTY_PRINT);
                Storage::put($path, $myJSON);
            }
        }
        $irt = new IRTController($courseID, Auth::id());
        $recommend_questions = $irt->getQuestions(7, true, [], 30,40,false);

        //Laravel Log
        Log::info('[Course] ['.Auth::id().'] Enter course dashboard. Course Id: '.$courseInformation[0]->course_code);

        //return
        return view('student.student',['recommend_questions'=>$recommend_questions,'total_num'=>$total_num,'hot_topics'=>$new_hot_topics,'courseInformation'=>$courseInformation[0],'top_keywords'=>$top_keywords, 'questions_num'=>$questions_num, 'questions_created'=>$questions_created ,'num_answered' => $num_answered,
            'num_answered_correct'=> $num_answered_correct, 'top5_ranking' =>$students_ranking, 'color_cluster'=>"white",'courseID'=>$courseID, 'top_questions'=>$top_questions, 'top_created_questions'=>$top_created_questions, 'user_performance'=>$user_performance]);

    }

    public function personalinformation()
    {
//        $users = DB::select('select id, name, nickname, email from users where id =?', [Auth::id()]);
        $user = $this->student->getStudentById(Auth::id());

        if( is_null($user) ) {
            return redirect()->back()->with('fail', 'Failed to retrieve student data');
        }

        //Laravel Log
        Log::info('[Personal.Info] ['.Auth::id().'] Enter Personal Info Page.');

        return view('student.info',['user'=>$user]);
    }

    public function updateinfo(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required',
            'nickname' => 'required',
        ));

        $result = $this->student->updateInfo(Auth::id(), $request->name, $request->nickname);

        if($result == -1) {
            return redirect()->back()->withInput()->with('fail', 'Failed to update due to wrong information');
        }

        //Laravel Log
        Log::info('[Personal.Info] ['.Auth::id().'] Update Personal Info. name: '.$request->name.', nickname: '.$request->nickname. ', email: '.$request->email);

        return redirect()->route('student.info')->with('success', 'Successfully Updated');
    }

    public function newpass (Request $request)
    {
        $this->validate($request, array(
            'pass' => 'required|min:6',
            'oldpass' => 'required|min:6',
            'confirm' => 'required|min:6',
        ));

        if(strcmp($request->pass, $request->confirm) != 0) {
            return redirect()->back()->withInput()->with('fail', 'Two new passwords do not match');
        }

        $result = $this->student->changePassword(Auth::id(), $request->oldpass, $request->pass);

        if ($result == -1) {
            return redirect()->back()->withInput()->with('fail', 'Sorry we failed to update your password. Please contact your teacher for your help');
        } elseif ($result == -2) {
            return redirect()->back()->withInput()->with('fail', 'Your old password did not match with our record. Please try again.');
        } elseif ($result == -3) {
            return redirect()->back()->withInput()->with('fail', 'Failed to update due to wrong information.');
        }

        return redirect()->route('student.info')->with('success', 'Successfully Updated');
    }

    public function ranking()
    {
        $students = $this->calculate_ranking(true, Session::get('courseID'));
        return view('student.ranking',['students'=>$students]);
    }

    public function showfeedback() {
        return view('student.feedback');
    }

    private function calculate_ranking($top5 =false, $courseID)
    {
        $sql_statement = '
        select users.nickname as Nickname, users.id,
        case
            when result is null then 0
            else count(result)
        end as Score from enrollments as E
        join users on E.student_id = users.id and E.course_id = ? and users.id >= 8
        join questions as q on E.course_id = q.course_id
        join user_answereds as UA on E.student_id = UA.user_id and q.id = UA.question_id and
                                    result = 1  and users.id >= 8 group by nickname, id, result order by Score DESC ';

        if ($top5) {
            $sql_statement .= 'limit 5';
        }

        $students= DB::select($sql_statement, [$courseID]);

        return $students;
    }

    private function topStudentCreatedQuestions($courseID)
    {
        $sql_statement = '
        select U.nickname, count(Q.id) as num from enrollments as E
join users as U on U.id = E.student_id and E.course_id = ? and E.student_id >= 8
join questions as Q on Q.isMadeByTeacher = 0 and Q.author_id = E.student_id and Q.course_id = E.course_id  group by U.nickname order by num  DESC limit 5;';

        $students= DB::select($sql_statement, [$courseID]);

        return $students;
    }

    private function getTop5Keywords($courseID)
    {
        $sql_statement = 'select keyword, count(keyword) as rank from keywords, questions as q where q.id = keywords.question_id and q.course_id = ? group by keyword order by rank DESC limit 5 ';

        $result= DB::select($sql_statement,[$courseID]);

        return $result;
    }

    public function enrollCourse()
    {
        $courses = $this->db->getCourses();
        $tmpenrolled = DB::select('select * from enrollments as E where E.student_id = ?',[Auth::id()]);

        $enrolled=[];
        foreach($tmpenrolled as $enroll) {
            $enrolled[] = $enroll->course_id;
        }

        //Laravel Log
        Log::info('[Enroll] ['.Auth::id().'] Enter Enroll Page.');

        return view('student.enroll',['courses'=>$courses, 'enrolled'=>$enrolled]);
    }

    public function enrollCourseSucess(Request $request) {

        if (!empty($this->db->getCourseInformationById($request->course_id)[0])) {

            //Laravel Log
            Log::warning('[Enroll.Success] ['.Auth::id().'] Failed to enroll course [ '.$request->course_id.' ] due to already enrolled, which should not be appeared');

            return redirect()->route('student.home')->with('status', "You have already enrolled");
        }

        if(empty($request->enrollcode)) {
            //Laravel Log
            Log::info('[Enroll.Success] ['.Auth::id().'] Failed to enroll Course [ '.$request->course_id.' ] due to empty enroll code received');

            return redirect()->route('student.enroll')->with('fail', "Please Enter Code");
        }

        $code = $this->db->getEnrollCodeByCourseId($request->course_id)[0]->enroll_code;

        if ($code != $request->enrollcode) {
            //Laravel Log
            Log::info('[Enroll.Success] ['.Auth::id().'] Failed to enroll course [ '.$request->course_id.' ] due to unmatching enroll code [ '.$request->enrollcode.' ]');

            return redirect()->route('student.enroll')->with('fail', "Your Enroll Code Is Not Correct");
        } elseif (empty($code)) {
            //Laravel Log
            Log::warning('[Enroll.Success] ['.Auth::id().'] Failed to enroll course [ '.$request->course_id.' ] due to empty enroll code variable got from DB');

            return redirect()->route('student.enroll')->with('fail', "Your Course Is Not Available");
        }

        $enroll = new Enrollment();
        $enroll->student_id = Auth::id();
        $enroll->course_id = $request->course_id;

        $enroll->save();

        //Laravel Log
        Log::info('[Enroll.Success] ['.Auth::id().'] Enrolled Course [ '.$request->course_id.' ] with Enroll Code: [ '.$code.' ]');

        return redirect()->route('student.home')->with('status', "Successfully Enrolled");
    }

    public function setting (Request $request) {
        $notify_answered = 0;
        $notify_viewed = 0;
        $notify_rated = 0;
        $email_announce = 0;
        $email_report = 0;

        if($request->notify_answered == 'on') {
            $notify_answered = 1;
        }
        if($request->notify_viewed == 'on') {
            $notify_viewed = 1;
        }
        if($request->notify_rated == 'on') {
            $notify_rated = 1;
        }
        if($request->email_announce == 'on') {
            $email_announce = 1;
        }
        if($request->email_report == 'on') {
            $email_report = 1;
        }

        DB::table('users')
            ->where('id', Auth::id())
            ->update(['notify_answered' => $notify_answered,'notify_rated' => $notify_rated,'notify_viewed' => $notify_viewed
            ,'announce_email' => $email_announce, 'email_report' => $email_report ]);

        //Laravel Log
        Log::info('[Setting] ['.Auth::id().'] Update Setting. Notify answer: [ '.$notify_answered.' ] Notify View: [ '.$notify_viewed.' ] Notify Rate: [ '.$notify_rated.' ] Email Announcement: [ '.$email_announce.' ] Email Report [ '.$email_report.' ]');

        return back()->with('success', "Successfully Updated");

    }

    public function usermanual() {
        $title = 'About PolyU Peer Assessment';

        Log::info('[User Manual] ['.Auth::id().'] Enter User Manual Page.');
        return view('student.usermanual', compact('title'));
    }

}

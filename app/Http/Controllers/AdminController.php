<?php

namespace App\Http\Controllers;
use App\Quiz;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Course;
use App\QuizQuestion;
use \Datetime;
use \Exception;
use Session;
use App\Question;
use Mail;
use App\Mail\Announce;
use App\Announcement;
use App\Enrollment;
use App\User;
use App\CourseSchedule;
use Carbon\Carbon;

use App\Notifications\notifyStudent;

class AdminController extends Controller
{
    private $db;

    public function __construct()
    {
        $this->middleware('auth:teacher');
        $this->db = new DatabaseCapture();

    }

    public function createSystemAnnounce()
    {
        $courses = DB::table('courses')->get();
        return view('admin.createSystemAnnounce',['courses'=>$courses]);
    }

    public function sendSystemAnnounce(Request $request)
    {
        $this->validate($request, array(
            'topic' => 'required|max:255',
            'contents' => 'required|min:1',
            'schedule' =>'required'
        ));

        $dates = explode('-', $request->schedule);

        $start_at = Carbon::parse($dates[0]);
        $end_at = Carbon::parse($dates[1]);

        //save to DB
        $announce = new Announcement();
        $announce->topic = $request->topic;
        $announce->contents = $request->contents;
        $announce->author_id = Auth::id();
        $announce->course_id = '';
        $announce->start_at = $start_at;
        $announce->end_at = $end_at;
        $announce->type = 1;

        $announce->save();

        return redirect()->back()->with('success','Successfully sent to students');

    }
}

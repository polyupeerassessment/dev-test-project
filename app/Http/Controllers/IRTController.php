<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Phpml\Clustering\KMeans;
use Phpml\Classification\KNearestNeighbors;
use Phpml\Regression\LeastSquares;
use Phpml\Regression\SVR;
use Phpml\SupportVectorMachine\Kernel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\UserAttempt;
use Illuminate\Support\Facades\Storage;
use App\Question;
use App\UserAnswered;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class IRTController extends Controller
{
    private $course_code;
    private $author_id;

    public function __construct($course_code, $author_id)
    {
        $this->course_code = $course_code;
        $this->author_id = $author_id;
    }

    public function updateIRT_test($assignment_id) {
//        $results = DB::select('select u.id, assignment_question_id as question_id, result
//                                  from users as u join assignment_user_attempts as ua on ua.user_id = u.id and u.id = ?;',[$this->author_id]);

        $results = DB::table('assignment_user_attempts')->where('assignment_id', $assignment_id)->where('user_id', Auth::id())->get();

        if (empty($results[0])) {
            Log::warning('[IRT.update.assignment] Failed to update IRT since there is no questions and results found from DB ');
            return -3;
        }
        $total_count = 0;
        $correct_count = 0;

        foreach( $results as $r ) {
            $total_count++;
            if ( $r->result == 1 ) {
                $correct_count++;
            }
        }

        if ($total_count == 0) {
            return 0;
        }

        $p = $correct_count / $total_count;

        if ($p == 0) {
            return -2;
        }

        if ($p == 1) {
            return 2;
        }

        $odds = $p / (1 - $p);
        return log($odds,M_E);
    }

    public function updateAssignmentIRT() {
        $results = DB::select('select u.id, assignment_question_id as question_id, result 
                                  from users as u join assignment_user_answers as ua on ua.user_id = u.id;');

        if (empty($results)) {
            Log::warning('[IRT.update.assignment] Failed to update IRT since there is no questions and results found from DB ');
            return -1;
        }

        $filename = storage_path() . '/app/csv/irt_assignment_input.csv';

        $fp = fopen($filename, 'w');
        fputcsv($fp, ['user_id', 'question_id', 'result']);

        foreach ($results as $result) {
            $question_value = (is_null($result->question_id)?-1:$result->question_id);
            fputcsv($fp, [$result->id,$question_value ,$result->result ]);
        }

        Log::info('[IRT.update.assignment] ['.$this->course_code.'] ['.$this->author_id.']  Updated IRT successfully ');

        fclose($fp);
    }

    public function updateIRT() {
        $results = DB::select('select u.id, question_id, result from users as u join user_answereds as ua on ua.user_id = u.id;');

        if (empty($results)) {
            Log::warning('[IRT.update] Failed to update IRT since there is no questions and results found from DB ');
            return -1;
        }

        $filename = storage_path() . '/app/csv/irt_input.csv';

        $fp = fopen($filename, 'w');
        fputcsv($fp, ['user_id', 'question_id', 'result']);

        foreach ($results as $result) {
            $question_value = (is_null($result->question_id)?-1:$result->question_id);
            fputcsv($fp, [$result->id,$question_value ,$result->result ]);
        }

        Log::info('[IRT.update] ['.$this->course_code.'] ['.$this->author_id.']  Updated IRT successfully ');

        fclose($fp);
    }

    public function getQuestions($number_of_questions, $isQuizFlag = false, $pre_questions = [], $min_p = -1, $max_p = -1, $exactnum = true, $isAssignment = false) {
        $r = $this->getLastUpdateIRT();
        if ( $r == 1) {
            if($this->updateIRT() == -1) {
                return -1;
            }
        } elseif ($r == -1) {
            Log::warning('[IRT.update] ['.$this->course_code.'] ['.$this->author_id.']  Failed to update IRT since not found irt_output csv file. ');
            return -2;
        }

        $question_id_list = $this->readIRT();

        $questions = [];
        if ($min_p == -1) {
            $min_p = 10;
        }
        if ($max_p == -1) {
            $max_p = 30;
        }


        while(sizeof($questions) < $number_of_questions ) {
            if($max_p == 110) {
                break;
            }
            $questions = $this->manageQuestions($question_id_list, $min_p, $max_p, $number_of_questions,$isQuizFlag,$pre_questions,$questions, $isAssignment);
            $max_p += 10;
        }

        if ($exactnum) {
            if (sizeof($questions) < $number_of_questions && $isQuizFlag == true) {
                Log::warning('[IRT.update] ['.$this->course_code.'] ['.$this->author_id.'] Failed to update IRT since student does not match any of the questions ');
                return -3;
            }
        }

        return $questions;
    }

    private function manageQuestions($question_id_list, $min,$max, $number_of_questions, $isQuizFlag, $pre_questions, $ready_questions, $isAssignment)
    {
        if (!$isAssignment) {
            $sql_query = 'SELECT * from questions as q where q.course_id = ? and q.rating >= 2 and q.id not in (select q2.id from questions as q2 where q2.author_id = ? and q2.isMadeByTeacher = 0) ';
        } else {
            $sql_query = 'SELECT * from assignment_user_questions as q where q.course_id = ? ; ';
        }

        $sql_argument = [$this->course_code, $this->author_id];
        $if_first_time_then_no_or = true;

        if (!$isQuizFlag) {
            $sql_query .= ' and not exists ( select * from user_answereds ua where ua.question_id = q.id and ua.user_id = ? ) ';
            $sql_argument[] = $this->author_id;
        }

        $alrady_taken_question_ids = [];
        foreach($pre_questions as $q) {
            $alrady_taken_question_ids[] = $q->question_id;
        }

        foreach($ready_questions as $q) {
            $alrady_taken_question_ids[] = $q->id;
        }

        foreach($question_id_list as $question_id => $p) {
            if ($isQuizFlag == true && ($p >= $max || $p <= $min)) {
                continue;
            }

            if (in_array($question_id, $alrady_taken_question_ids)) {
                continue;
            }

            if($if_first_time_then_no_or) {
                $sql_query .= ' and ( id = ? ';
                $if_first_time_then_no_or = false;
            } else {
                $sql_query .= ' or id = ? ';
            }

            $sql_argument[] = $question_id;
        }

        foreach($ready_questions as $q) {

            if($if_first_time_then_no_or) {
                $sql_query .= ' and ( id = ? ';
                $if_first_time_then_no_or = false;
            } else {
                $sql_query .= ' or id = ? ';
            }

            $sql_argument[] = $q->id;
        }

        if($if_first_time_then_no_or) {
            $questions = array();
        } else {
            $sql_query .= ' ) order by rating DESC,created_at DESC limit '.$number_of_questions;
            $questions = DB::select($sql_query,$sql_argument);
        }

        return $questions;
    }

    private function readIRT($isAssignment = false) {

        if (!$isAssignment) {
            $filename = storage_path().'/app/csv/irt_output.csv';
        } else {
            $filename = storage_path().'/app/csv/irt_assignment_output.csv';
        }

        $all_questions = DB::select('select * from questions');
        $question_list_output= [];

        foreach($all_questions as $question) {
            $question_list_output[$question->id] = 99;
        }

        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            if (($question_list = fgetcsv($handle, 1000, ",")) !== FALSE) {

                //get index from header
                while (($row = fgetcsv($handle, 1000, ",")) !== FALSE)
                {
                    if($row[0]==Auth::id()) {
                        $remove_first_flag = true;

                        foreach($row as $index=>$p) {
                            if($remove_first_flag == true) {
                                $remove_first_flag = false;
                                continue;
                            }
                            if (empty($question_list[$index])) {
                                continue;
                            }
                            $question_list_output[$question_list[$index]]= $p;
                        }

                    }
                }
            }

        }

        return $question_list_output;
    }

    public function getLastUpdateIRT() {
        $filename = storage_path().'/app/csv/irt_output.csv';
        if (file_exists($filename)) {
            $d = Carbon::parse(date ("F d Y H:i:s.", filemtime($filename)));
            $now = Carbon::now();
            if ($now->diffInMinutes($d) >= 10) {
//                $this->updateIRT();
                return 1;
            } else {
                return 0;
            }
        }
        return -1;

    }

    public function updateQuestionLevel() {
        $filename = storage_path().'/app/csv/question_difficulty.csv';
        $all_questions = DB::select('select * from questions');
        $question_list_output= [];

        foreach($all_questions as $question) {//make all the questions easy
            $question_list_output[$question->id] = 99;
        }

        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            if (($question_list = fgetcsv($handle, 1000, ",")) !== FALSE) {

                //get index from header
                while (($row = fgetcsv($handle, 1000, ",")) !== FALSE)
                {
                    $remove_first_flag = true;

                    $difficulty = 0;
                    if($row[2] >1) {
                        $difficulty = 2;
                    } elseif($row[2]<=1 && $row[2] >=-1) {
                        $difficulty =1;
                    }

                    DB::table('questions')
                        ->where([['id',$row[1]]])
                        ->update(['difficulty' => $difficulty]);
                }
            }

        }
    }

    public function updateCSVQuestionRating() {

        $questions = Question::get(['id','topic']);
        $ratings =UserAnswered::get(['user_id','question_id','rating']);

        $filename = storage_path() . '/app/csv/question.csv';

        $fp = fopen($filename, 'w');
        fputcsv($fp, ['id', 'topic']);

        foreach ($questions as $question) {
            fputcsv($fp, [$question->id, $question->topic]);
        }

        $filename = storage_path() . '/app/csv/rating.csv';

        $fp = fopen($filename, 'w');
        fputcsv($fp, ['user_id', 'question_id', 'rating']);

        foreach ($ratings as $rating) {
            if(is_null($rating->rating)) {
                continue;
            }
            fputcsv($fp, [$rating->user_id, $rating->question_id, $rating->rating]);
        }

        fclose($fp);

        echo "$filename";
    }

    public function updateQuestionsPopularityRate($course_code) {
        $num_views = DB::select('select questions.id, count(a.id) as num from questions left join page_views a ON questions.id = a.question_id and questions.course_id = ? group by questions.id',[$course_code]);
        $avg_rating = DB::table('questions')->where('course_id','=',$course_code)->get();
        $num_answers = DB::select('select question_id, sum(u.num_attempts) as num from user_answereds u, questions q where u.question_id = q.id and q.course_id = ? group by question_id; ',[$course_code]);

        $data = [];
        $target_tmp = [];
        foreach ($avg_rating as $r) {
            $data[$r->id][] = $r->rating;
            $target_tmp[$r->id] = 0;
        }

        foreach ($num_views as $v) {
            if (empty($data[$v->id])) {
                continue;
            }
            $data[$v->id][] = $v->num;
        }

        foreach ($num_answers as $a) {
            $target_tmp[$a->question_id] = $a->num;
        }


        $target = array_replace($data, $target_tmp);

        $regression = new LeastSquares();

        $regression->train($data, $target);

        //output result
        $output_filename = storage_path() . '/app/csv/'.$course_code.'/question_popularity_rate.csv';

        if (!is_dir(storage_path() . '/app/csv/'.$course_code)) {
            $old = umask(0);
            mkdir(storage_path() . '/app/csv/'.$course_code, 0777, true);
            umask($old);
        }

        $fp = fopen($output_filename, 'w');

        //contents
        $content2put = [];
        $content2put[] =$regression->getIntercept();

        foreach($regression->getCoefficients() as $c) {
            $content2put[] = $c;
        }

        fputcsv($fp, $content2put);

        fclose($fp);

//        print_r($data);
//        echo '<br>';echo '<br>';
//        print_r($target);
//
//        echo '<br>';echo '<br>';
//        print_r($regression->predict([4.5,2])); echo '<br>';
//        print_r($regression->getCoefficients());
//        echo '<br>';
//        print_r($regression->getIntercept());

    }

    public function getQuestionIdList_Popularity_Order($course_code) {
        $filename = storage_path() . '/app/csv/'.$course_code.'/question_popularity_rate.csv';

        $list = [];

        $questions = DB::table('questions')->where('course_id','=',$course_code)->get();
        $num_views = DB::select('select questions.id, count(a.id) as num from questions left join page_views a ON questions.id = a.question_id and questions.course_id = ? group by questions.id',[$course_code]);
        foreach($questions as $q) {
            $list[$q->id] = 0;
        }

        if (!is_dir(storage_path() . '/app/csv/'.$course_code)) {
            return $list;
        }

        $coefficients = [];
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            if (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {

                foreach($row as $v) {
                    $coefficients[] = $v;
                }
            }
        } else {

            return $list;
        }

        foreach($num_views as $view) {
            $list[$view->id] = $view->num;
        }

        foreach($questions as $q) {
//            echo $q->rating.'<br>';
//            echo $list[$q->id].'<br>';
//            echo $q->rating.'<br>';
            $list[$q->id] = $coefficients[0] + $coefficients[1] * $q->rating +  $coefficients[2] * $list[$q->id];
        }

        arsort($list);
//        print_r($list);
//        exit;
        $list = array_keys($list);
//        print_r($list);
        return $list;

    }
}
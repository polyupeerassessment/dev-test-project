<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Phpml\Clustering\KMeans;
use Phpml\Classification\KNearestNeighbors;
use Phpml\Regression\LeastSquares;
use Phpml\Regression\SVR;
use Phpml\SupportVectorMachine\Kernel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\UserAttempt;
use Illuminate\Support\Facades\Storage;
use App\Question;
use App\UserAnswered;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class LeaderboardController extends Controller
{

    public function __construct() {}

    public function student_performance($courseID)
    {
        //User performance
        $user_performance['rank_num_questions'] = [0,0];
        $user_performance['rank_scores'] = [0,0];
        $user_performance['rank_rating'] = [0,-1];
        $user_performance['rank_answers'] = [0,0];

        //rank: num_questions
        $result = DB::select('select author_id, count(id) as num from questions q where course_id = ? and isMadeByTeacher = 0 and author_id >= 8 group by author_id order by num DESC',[$courseID]);
        $rank_num_questions = 0;
        $user_num = 0;
        $found_flag = false;

        if(!empty($result) ) {
            $previous_num = -1;

            foreach($result as $r) {

                if ($r->num != $previous_num) {
                    $previous_num = $r->num;
                    $rank_num_questions++;
                }

                if($r->author_id == Auth::id()) {
                    $user_num = $r->num;
                    $found_flag = true;
                    break;
                }
            }

            if (!$found_flag) {
                $rank_num_questions = 0;
            }

            $user_performance['rank_num_questions'] = [$rank_num_questions, $user_num];
        }


        //rank: scores
        $result = $this->score_ranking(5,$courseID, 8);
        $rank_scores = 0;
        $user_score = 0;
        $found_flag = false;

        if(!empty($result) || $rank_scores!=0) {
            $previous_num = -1;

            foreach($result as $r) {

                if ($r->Score != $previous_num) {
                    $previous_num = $r->Score;
                    $rank_scores++;
                }

                if($r->id == Auth::id()) {
                    $found_flag = true;
                    $user_score = $r->Score;
                    break;
                }

            }
            if (!$found_flag) {
                $rank_scores = 0;
            }

            $user_performance['rank_scores'] = [$rank_scores, $user_score];
        }

        //rank: rating
        $result_total = DB::select('select author_id, avg(rating) as rating from questions where course_id = ? and author_id >= 8 group by author_id order by rating DESC;', [$courseID]);
        $rank = 1;
        $user_rating = 0;
        $found_flag = false;

        if(!empty($result_total[0])) {
            $previous_num = -1;

            foreach($result_total as $r) {

                if (empty($r->rating)) {
                    continue;
                }

                if ($r->author_id == Auth::id()) {
                    $found_flag = true;
                    $user_rating = $r->rating;
                    break;
                }

                if ($previous_num != $r->rating) {

                    $previous_num = $r->rating;
                    $rank++;
                }
            }

            if (!$found_flag) {
                $rank = 0;
            }

            $user_performance['rank_rating'] = [$rank, $user_rating];
        }

        //rank: number of answers
        $result_total = DB::select('select user_id,sum(user_answereds.num_attempts) as num from user_answereds, questions as q where q.id = user_answereds.question_id and q.course_id = ? and user_id >= 8 group by user_id order by num DESC ', [$courseID]);
        $rank = 0;
        $previous_num = -1;
        $user_num_answers = 0;

        if(!empty($result_total)) {

            foreach($result_total as $r) {
                if($r->num != $previous_num) {

                    $previous_num = $r->num;
                    $rank++;

                }

                if($r->user_id == Auth::id()) {

                    $user_num_answers = $r->num;
                    break;

                }

            }
            if($user_num_answers == 0) {
                $rank = 0;
            }

            $user_performance['rank_answers'] = [$rank,$user_num_answers];
        }

        return $user_performance;
    }

    public function score_ranking($sqllimit = -1, $courseID, $ignore_developer_id = -1)
    {
        $sql_statement = '
        select users.nickname as Nickname, users.id,
        case
            when result is null then 0
            else count(result)
        end as Score from enrollments as E
        join users on E.student_id = users.id and E.course_id = ? and users.id >= ?
        join questions as q on E.course_id = q.course_id
        join user_answereds as UA on E.student_id = UA.user_id and q.id = UA.question_id and
                                    result = 1  and users.id >= ? group by nickname, id, result order by Score DESC ';

        if ($sqllimit >= 1) {
            $sql_statement .= 'limit '.$sqllimit;
        }

        $students= DB::select($sql_statement, [$courseID,$ignore_developer_id, $ignore_developer_id]);
        return $students;
    }

    public function question_ranking ($sqllimit = -1, $courseID, $ignore_developer_id = -1) {
        $sql_statement = 'select * from questions where course_id = ? and author_id >= ? order by rating DESC ';

        if ($sqllimit >= 1) {
            $sql_statement .= ' limit '.$sqllimit;
        }

        $questions = DB::select($sql_statement, [$courseID, $ignore_developer_id]);

        return $questions;
    }

    public function keyword_ranking ($sqllimit = -1,$courseID)
    {
        $sql_statement = 'select keyword, count(keyword) as rank from keywords, questions as q where q.id = keywords.question_id and q.course_id = ? group by keyword order by rank DESC ';

        if ($sqllimit >= 1) {
            $sql_statement .= ' limit '.$sqllimit;
        }

        $result= DB::select($sql_statement,[$courseID]);

        return $result;
    }

    public function students_created_ranking ($sqllimit = -1, $courseID, $ignore_developer_id = -1)
    {
        $sql_statement = '
        select U.nickname, count(Q.id) as num from enrollments as E
join users as U on U.id = E.student_id and E.course_id = ? and E.student_id >= ? 
join questions as Q on Q.isMadeByTeacher = 0 and Q.author_id = E.student_id and Q.course_id = E.course_id  group by U.nickname order by num  DESC ';

        if ($sqllimit >= 1) {
            $sql_statement .= ' limit '.$sqllimit;
        }

        $students= DB::select($sql_statement, [$courseID,$ignore_developer_id]);

        return $students;
    }


    public function students_avg_rating ($sqllimit = -1, $courseID, $ignore_developer_id = -1)
    {
        $sql_statement = 'select nickname, avg(rating) as rating from users as u, questions as q where course_id = ? and q.author_id = u.id and u.id >= ? group by nickname order by rating DESC ';

        if ($sqllimit >= 1) {
            $sql_statement .= ' limit '.$sqllimit;
        }

        $students= DB::select($sql_statement, [$courseID,$ignore_developer_id]);

        return $students;
    }


    public function students_answers_ranking($sqllimit = -1, $courseID, $ignore_developer_id = -1)
    {
        $sql_statement = 'select nickname,sum(user_answereds.num_attempts) as num from user_answereds, questions as q, users as u where u.id = user_answereds.user_id and q.id = user_answereds.question_id and q.course_id = ? and u.id >= ? group by nickname order by num DESC ';

        if ($sqllimit >= 1) {
            $sql_statement .= ' limit '.$sqllimit;
        }

        $students= DB::select($sql_statement, [$courseID,$ignore_developer_id]);

        return $students;
    }


    public function getQuestionIDsByNumAnswers ($course_id, $limit = 5)
    {
        if(empty($course_id)){
            return null;
        }

        $ids = DB::select('select question_id, sum(ua.num_attempts) as num from user_answereds ua where exists(select * from questions q where ua.question_id = q.id and q.course_id =? ) group by question_id order by num DESC limit '.$limit, [$course_id]);

        if(empty($ids[0])){
            return null;
        }

        return $ids;
    }


}
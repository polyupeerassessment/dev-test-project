<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

class TeacherLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:teacher');
    }

    public function showlogin() {
        return view('auth.teacher-login');
    }

    public function login(Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        //attempt user to login
        if (Auth::guard('teacher')->attempt(['email'=>$request->email, 'password' => $request->password])){
            return redirect()->intended(route('teacher.home'));
        }

        return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors([
            'fail' => "Failed to Login",
        ]);
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\UserAttempt;
use App\Http\Controllers\StorageController;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/student/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // Function to return error msg if user failed to log in
    protected function sendFailedLoginResponse(\Illuminate\Http\Request $request)
    {
        return redirect()->to('/login')
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'fail' => 'Failed to login',
            ]);
    }

    public function logout(Request $request)
    {
        DB::table('users')->where('id',Auth::id())->update(['status'=> 0]);
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->route('home');
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

//        Log::useFiles(storage_path().'/logs/user-login-info.log');
//        Log::info('User Logged in:ID:'.Auth::id());


        //save into database
        $userattempt = new UserAttempt();
        $userattempt->user_id = Auth::id();
        $userattempt->login_at = date("Y-m-d H:i:s");
        $userattempt->save();

        //save into json file
        $jsonfile = new StorageController();
        $tmpdata = array('id'=>Auth::id(), 'time'=>date("Y-m-d H:i:s"), 'status'=>1);
        $jsonfile->addLog($tmpdata, 'login');

        DB::table('users')
            ->where([['id',Auth::id()]])
            ->update(['status' => 1]);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }
}

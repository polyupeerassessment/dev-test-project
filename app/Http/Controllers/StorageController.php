<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StorageController extends Controller
{
    public function storeJsonUserAuthenticate($users) {

        $this->storeJson($users, 'log.json');
    }

    public function storeJsonQuestions($questions) {

        $this->storeJson($questions, 'log.json');
    }

    public function storeJsonKeywords($keywords) {
        $this->storeJson($keywords, 'log.json');
    }

    public function storeJsonAnswer($answer) {
        $this->storeJson($answer, 'log.json');
    }

    public function storeJsonDiscussion($text) {
        $this->storeJson($text, 'log.json');
    }

    public function addLog($content, $type) {
        $log = $content;
        $log['log_type'] = $type;
        $this->storeJson($log, 'log.json');
    }

    private function storeJson($contents, $filename) {
        $path = storage_path() . '/app/json/'.$filename;
        if (file_exists($path)===false) {
            $data[0] = $contents;
        } else {
            $data = json_decode(file_get_contents($path),true);
            $data[] = $contents;
        }

        $myJSON = json_encode($data,JSON_PRETTY_PRINT);
        Storage::put('json/'.$filename, $myJSON);
    }

    public function readLogAvgTimeSpent() {
        $path = storage_path() . '/app/json/log.json';

        if (file_exists($path)===false) {
            return -1;
        }

        $data = json_decode(file_get_contents($path),true);

        $result = [];
        $count_question_num = [];

        foreach($data as $d) {
            if(!empty($d['log_type']) && $d['log_type'] == 'answer_spent' && !is_null($d['question_id'])) {
                $tmpresult['user_id'] = $d['user_id'];
                $tmpresult['course_id'] = $d['course_id'];
                $tmpresult['question_id'] = $d['question_id'];
                $tmpresult['timeSpent'] = $d['timeSpent'];

                $unique_id =  $tmpresult['question_id'];

                //if already exist or not

                if(!empty($result[$unique_id]) && !empty($result[$unique_id]['timeSpent'])) {
                    $count_question_num[$unique_id] += 1;
                    $result[$unique_id]['timeSpent'] += $tmpresult['timeSpent'];
                } else {
                    $count_question_num[$unique_id] = 0;
                    $result[$unique_id] = $tmpresult;
                }
            }
        }

        foreach ($result as $k =>$r) {
            if (!empty($count_question_num[$k])) {
                $r['timeSpent'] = $r['timeSpent'] / $count_question_num[$k];
            }
        }

        return $result;


    }
}

<?php

namespace App\Http\Controllers;

use App\QuestionChoice;
use App\QuizAttempts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Log;

use App\Question;
use App\QuizTimer;
use App\QuizAnswer;
use App\Keyword;
use App\PageView;
use App\Comment;
use App\UserAnswered;
use App\AssignmentUserAnswered;
use App\AssignmentUserAttempt;
use App\QuizUserQuestion;
use App\UserVote;
use \Exception;
use Session;
use App\Notifications\notifyStudent;
use App\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Input;


class QuestionController extends Controller
{
    private $db; // store into database
    private $json; //store into files
    private $mining; //access data mining controller
    private $question_class;

    public function __construct()
    {
        $this->middleware('auth');

        $this->db = new DatabaseCapture();
        $this->json = new StorageController();
        $this->mining = new MiningController();
        $this->question_class = new \App\Http\Controllers\Question();
        $this->student = new Student();

    }

    public function index()
    {
        return view('student');
    }

    public function showquiz()
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Sorry Session is out");
        }

        // if quiz is available
        $today = new Carbon();
        $quiz_available_tmp = DB::select("select * from quiz where course_id = ? and start_at <= ? and end_at >= ? ", [Session::get('courseID'), $today,$today]);

        $deadline='';
        if (empty($quiz_available_tmp) || empty($quiz_available_tmp[0])) {
            $is_test_now_flag = false;

            //Laravel Log
            Log::info('[Quiz.Show] ['.Auth::id().'] Enter Show Quiz but the quiz is not available in the course: [ '.Session::get('courseID').' ]');
        } else {
            $is_test_now_flag = true;
            Session::put('quizID', $quiz_available_tmp[0]->id);

            $deadline = $quiz_available_tmp[0]->end_at;
            $datetime = new DateTime($deadline);
            $deadline = $datetime->format('Y-m-d');

            //Laravel Log
            Log::info('[Quiz.Show] [ '.Session::get('courseID').' ] ['.Auth::id().'] Enter Show Quiz Page. The deadline is [ '.$deadline.' ]. The student started quiz');
        }

        $pre_quiz = DB::select("select * from quiz where course_id = ? and end_at < ? ", [Session::get('courseID'), $today]);

        return view('questions.showquiz', ['is_test_now_flag'=>$is_test_now_flag, 'deadline' => $deadline, 'pre_quiz'=>$pre_quiz]);
    }

    public function showAssignment()
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Sorry Session is out");
        }

        $today = new Carbon();
        $activeAssignments = DB::table('assignments')->where('course_id',Session::get('courseID'))->where('start_at','<=', $today)->where('end_at','>=', $today)->get();
        $assignmentsTook = DB::table('assignments')->where('course_id',Session::get('courseID'))->Where('end_at', '<', new Carbon())->get();

        $assignmentsTook_scores = [];
       
        $activeAssignmentsStatus = [];
        if (!empty($activeAssignments) && count($activeAssignments) > 0) {
            foreach ($activeAssignments as $assignment) {               
                $status = "Start";
                $userAttemptInfo = DB::table('assignment_user_attempts')->where([['user_id', Auth::id()], ['assignment_id', $assignment->id]])->get();
                $totalCount = 0;
                $totalScore = 0;
                if (!empty($userAttemptInfo[0])) {
                    foreach($userAttemptInfo as $a) {
                        $totalCount++;
                        if($a->result == 1) {
                            $totalScore++;
                        }
                    }
                }


                //User Status on this assignment
                if ($totalCount==0 && $totalScore == 0) {
                        $status = 'Start';
                } else if($totalCount >= $assignment->max_attempts || $totalScore >= $assignment->score) {
                    $status = 'Finish';
                } else {
                    $status = 'Continue';
                }

                //Get User's Score on this assignment
                $assignmentsTook_scores[$assignment->id] = $totalScore;
                $activeAssignmentsStatus[$assignment->id] = $status;
            }
        }

        if (!empty($assignmentsTook)) {
            foreach($assignmentsTook as $assignment) {
                $userAttemptInfo = DB::table('assignment_user_attempts')->where([['user_id', Auth::id()], ['assignment_id', $assignment->id]])->get();
                if (!empty($userAttemptInfo[0])) {
                    $totalScore = 0;
                    foreach($userAttemptInfo as $a) {
                        if($a->result == 1) {
                            $totalScore++;
                        }
                    }
                    $assignmentsTook_scores[$assignment->id] = $totalScore;
                } else {
                    $assignmentsTook_scores[$assignment->id] = 0;
                }
            }
        }

        return view('questions.assignment.show',['assignmentsTook_scores'=>$assignmentsTook_scores,'activeAssignments' => $activeAssignments, 'assignnmentsTook' => $assignmentsTook, 'activeAssignmentsStatus' => $activeAssignmentsStatus]);
    }

    private function getAssignmentQuestion_byDifficulty($assignment_id,$difficulty)
    {
        return DB::select('select * from assignment_questions aq where assignment_id = ? and difficulty = ? and not exists ( select * from assignment_user_answers aqa 
            where aq.id = aqa.assignment_question_id and aqa.user_id = ?); ', [$assignment_id, $difficulty,Auth::id()]);
    }

    private function assignmentQuestion_getNextLevel($difficulty,$tried_diff_arr)
    {
        if ($difficulty == 90) {
            if (!in_array(70, $tried_diff_arr)) {
                return 70;
            } else if (!in_array(50, $tried_diff_arr)) {
                return 50;
            } else if (!in_array(30, $tried_diff_arr)) {
                return 30;
            } else if (!in_array(10, $tried_diff_arr)) {
                return 10;
            }
        } else if ($difficulty == 70) {
            if (!in_array(50, $tried_diff_arr)) {
                return 50;
            } else if (!in_array(90, $tried_diff_arr)) {
                return 90;
            } else if (!in_array(30, $tried_diff_arr)) {
                return 30;
            } else if (!in_array(10, $tried_diff_arr)) {
                return 10;
            }
        } else if ($difficulty == 50) {
            if (!in_array(30, $tried_diff_arr)) {
                return 30;
            } else if (!in_array(70, $tried_diff_arr)) {
                return 70;
            } else if (!in_array(10, $tried_diff_arr)) {
                return 10;
            } else if (!in_array(90, $tried_diff_arr)) {
                return 90;
            }
        } else if ($difficulty == 30) {
            if (!in_array(10, $tried_diff_arr)) {
                return 10;
            } else if (!in_array(50, $tried_diff_arr)) {
                return 50;
            } else if (!in_array(70, $tried_diff_arr)) {
                return 70;
            } else if (!in_array(90, $tried_diff_arr)) {
                return 90;
            }
        } else if ($difficulty == 10) {
            if (!in_array(10, $tried_diff_arr)) {
                return 10;
            } else if (!in_array(30, $tried_diff_arr)) {
                return 30;
            } else if (!in_array(50, $tried_diff_arr)) {
                return 50;
            } else if (!in_array(90, $tried_diff_arr)) {
                return 90;
            }
        }

    }

    public function assignment_detail($id)
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Sorry Session is out");
        }

        // Check Current Status and table information
        $today = new Carbon();
        $assignmentInfo = DB::table('assignments')->where('course_id',Session::get('courseID'))->where('start_at','<=', $today)->where('end_at','>=', $today)->where('id',$id)->first();

        if (empty($assignmentInfo)) {
            return redirect()->back()->with('fail', 'Failed to retrieve the Assignment record');
        }

        $maxAttempts = $assignmentInfo->max_attempts;
        $targetScore = $assignmentInfo->score;
          
        // Check Exist table assignment_user_attempts record
        $totalScore = 0;
        $totalCount = 0;
        $userAttemptInfo = DB::table('assignment_user_attempts')->where('assignment_id', $id)->where('user_id', Auth::id())->get();

        if (!empty($userAttemptInfo[0])) {
            foreach($userAttemptInfo as $a) {
                $totalCount++;
                if($a->result == 1) {
                    $totalScore++;
                }
            }
        }

        //check if user already done all the questions
        if($maxAttempts <= $totalCount) {
            return redirect()->route('question.assignment.show')->with('fail', 'You have already tried maximum attempts');
        }

        // Set UI current score
        $currentResult = $totalScore.'/'.$targetScore;

        $irt = new IRTController(Session::get('courseID'), Auth::id());
        $level = $irt->updateIRT_test($id);

        //check if still have questions
        $tmp_questions = DB::select('select * from assignment_questions aq where assignment_id = ? and not exists ( select * from assignment_user_answers aqa 
            where aq.id = aqa.assignment_question_id and aqa.user_id = ?); ', [$id,Auth::id()]);

        if (empty($tmp_questions[0])) {
            if (!empty($userAnswerInfo[0])) {
                return redirect()->route('question.assignment.show')->with('fail','You have done for all the questions!');
            }
            return redirect()->route('question.assignment.show')->with('fail','No Questions available yet');
        }
        if ( $level == -3 ) {

            $question = $this->getAssignmentQuestion_byDifficulty($id, 90);

            if (empty($question)) {
                $question = $this->getAssignmentQuestion_byDifficulty($id, 70);
            }

            if (empty($question)) {
                $question = $this->getAssignmentQuestion_byDifficulty($id, 50);
            }

            if (empty($question)) {
                $question = $this->getAssignmentQuestion_byDifficulty($id, 30);
            }

            if (empty($question)) {
                $question = $this->getAssignmentQuestion_byDifficulty($id, 10);
            }

            if (empty($question)) {
//                return redirect()->back()->with('fail','Failed to get questions');
                return redirect()->route('question.assignment.show')->with('fail','You have done for all the questions!');
            }

        } else {

            $level = round($level);
            if ($level >-2 && $level <= -1 ) {
                $tmp_difficulty = 70;
            } else if ($level >-1 && $level <= 0 ) {
                $tmp_difficulty = 50;
            } else if ($level >0 && $level <= 1 ) {
                $tmp_difficulty = 30;
            } else if ($level >1) {
                $tmp_difficulty = 10;
            } else {
                $tmp_difficulty = 90;
            }

            $question = $this->getAssignmentQuestion_byDifficulty($id, $tmp_difficulty);
            $tried_diff_arr = [$tmp_difficulty];

            if (empty($question)) {
                for($i=0;$i<=4;$i++) {
                    $next_diff = $this->assignmentQuestion_getNextLevel($tmp_difficulty,$tried_diff_arr);
                    $tried_diff_arr[]=$next_diff;
                    $question = $this->getAssignmentQuestion_byDifficulty($id, $next_diff);

                    if(!empty($question)) {
                        break;
                    }
                }
            }

            if (empty($question)) {
//                return redirect()->back()->with('fail','Failed to get questions');
                return redirect()->route('question.assignment.show')->with('fail','You have done for all the questions!');
            }
        }

        $choices =[];
        $question_num_answers = [];
        $question_filter_content = [];

        if (!empty($question)) {
            //get random question here
            $tmp_size = sizeof($question) -1;
            $randomIndex = rand(0, $tmp_size);

            $question = $question[$randomIndex];

            $tmp_choices = DB::table('assignment_question_choices')->where('assignment_question_id',$question->id)->get();
            $question_num_answers[$question->id] = 0;
            foreach($tmp_choices as $choice ) {
                $choices[$question->id][] = $choice;
                if($choice->answer == 1) {
                    $question_num_answers[$question->id] += 1;
                }
            }

            $questionTable = DB::table('assignment_questions')->where('id','=',$question->id)->get();

            foreach($questionTable as $tmp_question ) {
                $contents = $tmp_question->contents;

                if ($tmp_question->mode == 1) {
                    libxml_use_internal_errors(true);
                    $doc = new \DOMDocument();
                    $doc->loadHTML($contents);
                    $this->removeElementsByTagName('script', $doc);
                    $this->removeElementsByTagName('style', $doc);
                    $this->removeElementsByTagName('form', $doc);

                    foreach ($doc->getElementsByTagname('*') as $element)
                    {
                        foreach (iterator_to_array($element->attributes) as $name => $attribute)
                        {
                            if (substr_compare($name, 'on', 0, 2, TRUE) === 0)
                            {
                                $element->removeAttribute($name);
                            }
                        }
                    }

                    libxml_clear_errors();
                    $contents = $doc->saveHTML();

                    $question_filter_content[$question->id] = $contents;
                }
                else
                {
                    $question_filter_content[$question->id] = $tmp_question->contents;
                }
            }
        } else{
            return redirect()->route('question.assignment.show')->with('fail','You have done for all the questions!');
        }

        return view('questions.assignment.detail', ['maxAttempts'=>$maxAttempts,'totalCount'=>$totalCount,'currentResult'=>$currentResult, 'assignmentID'=>$id, 'question'=>$question, 'question_choice'=>$choices, 'question_num_answers'=>$question_num_answers, 'question_filter_content'=>$question_filter_content]);
    }

    public function successAssignmentAnswer(Request $request)
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Session is out");
        }

        $question_id = $request->questionID;
        $assignment_id = $request->assignmentID;

        // Get assignment_question_choices table informartion
        $choices = DB::table('assignment_question_choices')->where('assignment_question_id','=',$question_id)->get();
        $question = DB::table('assignment_questions')->where('id', $question_id)->first();

        // Get assignments table informartion
        $assignmentInfo = DB::table('assignments')->where('id','=',$assignment_id)->first();

        // Check empty
        if (empty($choices) || is_null($choices[0]->answer) ||empty($question)) {
            return redirect()->back()->withInput()->with('fail','Sorry we cannot find the answer from DB or the Question Record');
        }

        // Check empty
        if (empty($assignmentInfo)) {
            return redirect()->back()->withInput()->with('fail','Sorry we cannot find the assignment from DB');
        }
 
        // Check User Answer
        $result = 0;

        if (empty($request->choice2answer)) {
            redirect()->back()->with('Please answer the question');
        }

        if($question->type == 1) {
            if ($choices[0]->answer == $request->choice2answer[0]) {
                $result = 1;
            }
        } else {
            $real_answers = [];
            foreach($choices as $choice) {
                if($choice->answer == 1) {
                    $real_answers[] = $choice->choice;
                }
            }

            if(empty($real_answers)) {
                Log::warning('Assignment.[Question.Answer] ['.Auth::id().'] Answered Question: ['.$request->questionID.']. Failed Due to not found answer of the question');
                return redirect()->back()->with('fail','Sorry We cannot find the answer of this question');
            }

            if(sizeof($request->choice2answer) == sizeof($real_answers)) {
                if ($request->choice2answer === $real_answers) { //For Both MC & TF Question
                    $result = 1;
                }
            }
        }

        // Store Record to DB
        // Update Assignment User Attempt Table
        $assignment_user_attempts = new AssignmentUserAttempt;
        $assignment_user_attempts->user_id = Auth::id();
        $assignment_user_attempts->assignment_question_id = $question_id;
        $assignment_user_attempts->assignment_id = $assignment_id;
        $assignment_user_attempts->result = $result;
        $assignment_user_attempts->save();

        // Update Assignment User Answer Table
        $user_answer = new AssignmentUserAnswered();
        $user_answer->user_id = Auth::id();
        $user_answer->assignment_question_id = $question_id;
        if (is_array($request->choice2answer)) { // MC
            foreach ($request->choice2answer as $answer) {

                $user_answer->answer = $answer;
                $user_answer->save();
            }
        }
        else { // TF
            $user_answer->answer = $request->choice2answer;
            $user_answer->save();
        }

        // Check attempt count and target 20 break refirect page;
        // Get Assignment User Attempts informartion
        $totalScore = 0;
        $userAnswerInfo = DB::select('select aua.assignment_question_id, aua.result from assignment_questions as aq, assignment_user_attempts as aua
where aq.assignment_id = ? and aua.assignment_question_id = aq.id and aua.user_id = ? ;',[$assignment_id, Auth::id()]);

        if (!empty($userAnswerInfo[0])) {
            $totalCount = 0;

            foreach($userAnswerInfo as $a) {
                $totalCount++;
                if($a->result == 1) {
                    $totalScore++;
                }
            }
        }

        if ($totalScore >= $assignmentInfo->score) {
            return redirect()->route('question.assignment.show')->with('success','Well Done! You have completed this assignment!');
        } else if ($totalCount >= $assignmentInfo->max_attempts) {
            return redirect()->route('question.assignment.show')->with('fail','You have done all the questions!');
        } else {
            if ($result == 1) {
                return redirect()->route('question.assignment.detail',['id'=>$assignment_id])->with('success', 'Correct!');
            } else {
                return redirect()->route('question.assignment.detail',['id'=>$assignment_id])->with('fail', 'Wrong!');
            }

        }


    }

    private function getQuestionsByIds($ids, $course_id, $user_id, $isObjectFlag = false)
    {
        $sql_query = 'SELECT * from questions as q where q.course_id = ? and q.author_id != ? ';
        $sql_argument = [$course_id, $user_id];
        $if_first_time_then_no_or = true;

        foreach($ids as $id) {

            if($if_first_time_then_no_or) {
                $sql_query .= ' and ( id = ? ';
                $if_first_time_then_no_or = false;
            } else {
                $sql_query .= ' or id = ? ';
            }

            if($isObjectFlag){
                $sql_argument[] = $id->question_id;
            } else {
                $sql_argument[] = $id;
            }

        }

        if($if_first_time_then_no_or) {
            $questions = array();
        } else {
            $sql_query .= ' ) order by rating DESC,created_at DESC ';
            $questions = DB::select($sql_query,$sql_argument);
        }

        return $questions;
    }

    public function quizdetail_with_id ($id)
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Sorry Session is out");
        }

        //get questions
        $quiz = DB::table('quiz')->where('id',$id)->first();

        if ( empty($quiz) ) {
            Log::warning('[Quiz.Detail.My] [ '.Session::get('courseID').' ] ['.Auth::id().'] Failed to retrieve quiz information in quiz detail page');
            return redirect()->route('question.quiz')->with('fail','Failed to retrieve quiz. Please contact your teacher for your help');
        }

        $pre_questions = DB::table('quiz_user_questions')->where('quiz_id',$id)->where('user_id',Auth::id())->get();
        $questions = $this->getQuestionsByIds($pre_questions,Session::get('courseID'), Auth::id(),true);

        if (empty($questions)) {
            Log::warning('[Quiz.Detail.My] [ '.Session::get('courseID').' ] ['.Auth::id().'] Failed to retrieve quiz information in quiz detail page');
            return redirect()->route('question.quiz')->with('fail','Failed to retrieve quiz questions. Please try again later');
        }


        $choices = [];
        $question_num_answers = [];
        $question_filter_content = [];


        //Avoid XSS here
        $result_list=[]; //Format: [result, your answer, question_id]

        foreach ($questions as $question) {
            $result_list[$question->id] = [0,[],$question->id];

            $choices[$question->id] = DB::table('question_choices')->where('question_id','=',$question->id)->get();
            // Get answer count
            //$tmp_choices = DB::table('question_choices')->where('question_id','=',$question->id)->get();
            $question_num_answers[$question->id] = 0;
            foreach($choices[$question->id] as $choice ) {
                //$choices[$question->id][] = $choice;
                if ($choice->answer == 1) {
                    $question_num_answers[$question->id] += 1;
                }
            }

            $questionTable = DB::table('questions')->where('id','=',$question->id)->get();

            foreach($questionTable as $tmp_question ) {
                $contents = $tmp_question->contents;

                if ($tmp_question->mode == 1) {
                    libxml_use_internal_errors(true);
                    $doc = new \DOMDocument();
                    $doc->loadHTML($contents);
                    $this->removeElementsByTagName('script', $doc);
                    $this->removeElementsByTagName('style', $doc);
                    $this->removeElementsByTagName('form', $doc);

                    foreach ($doc->getElementsByTagname('*') as $element)
                    {
                        foreach (iterator_to_array($element->attributes) as $name => $attribute)
                        {
                            if (substr_compare($name, 'on', 0, 2, TRUE) === 0)
                            {
                                $element->removeAttribute($name);
                            }
                        }
                    }

                    libxml_clear_errors();
                    $contents = $doc->saveHTML();

                    $question_filter_content[$question->id] = $contents;
                }
                else
                {
                    $question_filter_content[$question->id] = $tmp_question->contents;
                }
            }
        }

        $score = 0;

        $user_attempts = DB::select('select qa.user_id, qa.question_id, qa.result, qa.quiz_id, a.answer from quiz_attempts as qa
JOIN quiz_answers as a on a.question_id = qa.question_id and a.quiz_attempt_id = qa.id and qa.user_id = ? and qa.quiz_id = ?;',[Auth::id() , $id ]);

        if (!empty($user_attempts[0])) { //check if the user already took this quiz
            foreach ($user_attempts as $user_attempt) {
                //Format: [result, your answer, question_id]
                if (!empty($result_list[$user_attempt->question_id][1])) {
                    $result_list[$user_attempt->question_id][1][] = $user_attempt->answer;
                } else {
                    $score += $user_attempt->result;
                    $result_list[$user_attempt->question_id] = [$user_attempt->result, [$user_attempt->answer], $user_attempt->question_id];
                }
            }
        }

        $quiz_attempted = DB::table('quiz_timers')->where([['user_id', Auth::id()], ['quiz_id',$id ]])->first();
        $deadline = Carbon::parse($quiz_attempted->deadline);
	$total_score = $quiz->num_questions;

	if ($total_score != sizeof($questions)) {
            Log::warning('[Quiz.Show] [ '.Session::get('courseID').' ] ['.Auth::id().'] Number of questions do not match. number of questions stored in Quiz and number of questions generated are different');
            $total_score = sizeof($questions);
        }

        return view('questions.showquizdetail', ['total_score'=>$total_score,'topic'=>$quiz->topic,'deadline'=>$deadline,'score'=>$score,'choices'=>$choices,'result_list'=>$result_list,'questions'=>$questions, 'question_num_answers'=>$question_num_answers, 'quiz_done_flag'=>true,'question_filter_content'=>$question_filter_content ]);
    }

    public function quizdetail()
    {
        //check if the quiz is available
        if (!Session::get('courseID') || !Session::get('quizID')) {
            return redirect()->route('student.home')->with('fail', "Sorry Session is out");
        }

        //get questions
        $quiz = DB::table('quiz')->where('id',Session::get('quizID'))->first();

        if ( empty($quiz) ) {
            Log::warning('[Quiz.Detail] [ '.Session::get('courseID').' ] ['.Auth::id().']  [ Quiz ID: '.Session::get('quizID').'] Failed to retrieve quiz information in quiz detail page');
            return redirect()->route('question.quiz')->with('fail','Failed to retrieve quiz. Please contact your teacher for your help');
        }

        $total_score = $quiz->num_questions;

        if ($quiz->type != 1) {
            $question_ids = DB::table('quiz_questions')->where('quiz_id',Session::get('quizID') )->get();

            $questions = $this->getQuestionsByIds($question_ids,Session::get('courseID'), Auth::id(),true);

//            if (sizeof($questions) < 10) {
//                return redirect()->back()->with('fail', 'You have no questions assigned. ');
//            }

        } else {
            //check if already started and stored the questions
            $pre_questions = DB::table('quiz_user_questions')->where('quiz_id',Session::get('quizID'))->where('user_id',Auth::id())->get();
            $quiz_attempts = DB::table('quiz_attempts')->where('quiz_id',Session::get('quizID'))->where('user_id',Auth::id())->get();
            $pre_questions_before_quiz = DB::table('quiz_user_questions')->where('quiz_id','!=',Session::get('quizID'))->where('user_id',Auth::id())->get();

            if (empty($pre_questions[0])) {

                if(!empty($quiz_attempts[0])) {
                    $questions = $this->getQuestionsByIds($quiz_attempts,Session::get('courseID'), Auth::id(),true);
                } else {
                    $irt = new IRTController(Session::get('courseID'), Auth::id());
                    $questions = $irt->getQuestions($quiz->num_questions, true, $pre_questions_before_quiz);
                }

                if (!is_array($questions)) {
                    return redirect()->back()->with('fail','Failed to retrieve quiz information. Please contact your teacher for your help.');
                }

                foreach ($questions as $question) {
                    $quiz_q = new QuizUserQuestion();
                    $quiz_q->quiz_id = Session::get('quizID');
                    $quiz_q->user_id = Auth::id();
                    $quiz_q->question_id = $question->id;
                    $quiz_q->save();
                }
            } else {
                $questions = $this->getQuestionsByIds($pre_questions,Session::get('courseID'), Auth::id(),true);
            }
        }

        $choices = [];
        $question_num_answers = [];
        $question_filter_content = [];


        //Avoid XSS here
        $result_list=[]; //Format: [result, your answer, question_id]

        foreach ($questions as $question) {
            $result_list[$question->id] = [0,[],$question->id];

            $choices[$question->id] = DB::table('question_choices')->where('question_id','=',$question->id)->get();
            // Get answer count
            //$tmp_choices = DB::table('question_choices')->where('question_id','=',$question->id)->get();
            $question_num_answers[$question->id] = 0;
            foreach($choices[$question->id] as $choice ) {
                //$choices[$question->id][] = $choice;
                if ($choice->answer == 1) {
                    $question_num_answers[$question->id] += 1;
                }
            }

            $questionTable = DB::table('questions')->where('id','=',$question->id)->get();

            foreach($questionTable as $tmp_question ) {
                $contents = $tmp_question->contents;

                if ($tmp_question->mode == 1) {
                    libxml_use_internal_errors(true);
                    $doc = new \DOMDocument();
                    $doc->loadHTML($contents);
                    $this->removeElementsByTagName('script', $doc);
                    $this->removeElementsByTagName('style', $doc);
                    $this->removeElementsByTagName('form', $doc);

                    foreach ($doc->getElementsByTagname('*') as $element)
                    {
                        foreach (iterator_to_array($element->attributes) as $name => $attribute)
                        {
                            if (substr_compare($name, 'on', 0, 2, TRUE) === 0)
                            {
                                $element->removeAttribute($name);
                            }
                        }
                    }

                    libxml_clear_errors();
                    $contents = $doc->saveHTML();

                    $question_filter_content[$question->id] = $contents;
                }
                else
                {
                    $question_filter_content[$question->id] = $tmp_question->contents;
                }
            }
        }

        //check if started already
        $quiz_attempted = DB::table('quiz_timers')->where([['user_id', Auth::id()], ['quiz_id',Session::get('quizID') ]])->first();
        $now = Carbon::now()->addSecond();
        $deadline = Carbon::now()->addMinutes($quiz->duration);


        $score = 0;

        if (empty($quiz_attempted)) {

            //insert quiz_attempts model
            $quiz_timer = new QuizTimer;
            $quiz_timer->user_id = Auth::id();
            $quiz_timer->start_at =$now;
            $quiz_timer->deadline = $deadline;
            $quiz_timer->quiz_id =Session::get('quizID');

            $quiz_timer->save();
            $quiz_done_flag = false;

        } else {

            $user_attempts = DB::select('select qa.user_id, qa.question_id, qa.result, qa.quiz_id, a.answer from quiz_attempts as qa
JOIN quiz_answers as a on a.question_id = qa.question_id and a.quiz_attempt_id = qa.id and qa.user_id = ? and qa.quiz_id = ?;',[Auth::id() , Session::get('quizID') ]);

            if (!empty($user_attempts[0])) { //check if the user already took this quiz
                foreach( $user_attempts as $user_attempt) {
                    //Format: [result, your answer, question_id]
                    if(!empty($result_list[$user_attempt->question_id][1])) {
                        $result_list[$user_attempt->question_id][1][] = $user_attempt->answer;
                    } else {
                        $score += $user_attempt->result;
                        $result_list[$user_attempt->question_id] = [$user_attempt->result,[$user_attempt->answer],$user_attempt->question_id];
                    }
                }
                $quiz_done_flag = true;
            } else {
                $now = Carbon::now();
                $deadline = Carbon::parse($quiz_attempted->deadline);

                if ($deadline->lt($now)) {
                    $quiz_done_flag = true;

                    //Store data to DB
                    foreach($questions as $question) {
                        $quiz_attempt = new QuizAttempts;
                        $quiz_attempt->user_id = Auth::id();
                        $quiz_attempt->result = 0;
                        $quiz_attempt->question_id = $question->id;
                        $quiz_attempt->quiz_id = Session::get('quizID');
                        $quiz_attempt->save();

                        $user_answer = DB::table('quiz_attempts')->where([['user_id', Auth::id()],['question_id',$question->id], ['quiz_id',Session::get('quizID') ]])->first();

                        $quiz_answer = new QuizAnswer;
                        $quiz_answer->user_id = Auth::id();
                        $quiz_answer->question_id = $question->id;
                        $quiz_answer->quiz_attempt_id = $user_answer->id;
                        $quiz_answer->answer = '';
                        $quiz_answer->save();
                    }

                } else {
                    $quiz_done_flag = false;
                }
            }
        }
        if ($total_score != sizeof($questions)) {
            Log::warning('[Quiz.Show] [ '.Session::get('courseID').' ] ['.Auth::id().'] Number of questions do not match. number of questions stored in Quiz and number of questions generated are different');
            $total_score = sizeof($questions);
        }
        //Laravel Log
        Log::info('[Quiz.Show] [ '.Session::get('courseID').' ] ['.Auth::id().'] Take Quiz with the quiz id [ '.Session::get('quizID').' ]');
        return view('questions.showquizdetail', ['total_score'=>$total_score,'topic'=>$quiz->topic,'score'=>$score,'choices'=>$choices,'result_list'=>$result_list,'questions'=>$questions, 'question_num_answers'=>$question_num_answers, 'quiz_done_flag'=>$quiz_done_flag,'question_filter_content'=>$question_filter_content, 'deadline'=>$deadline ]);
    }

    public function showexercise()
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Session is out.");
        }

        $sql_query = 'SELECT * from questions as q where q.course_id = ? and q.id not in (select q2.id from questions as q2 where q2.author_id = ? and q2.isMadeByTeacher = 0) and (';

        if (session('result_list')) {
            $i = 1;
            $len = count(session('result_list'));

            foreach(session('result_list') as $r) {
                if (!empty($r[2])) {
                    $sql_query .= ' id = '.$r[2];
                    if ($i != $len) {
                        $sql_query .= ' or ';
                    }
                }
                $i++;
            }
            $sql_query .= ' ) order by rating DESC,created_at DESC';

            $sql_argument = [Session::get('courseID'), Auth::id()];

            $questions = DB::select($sql_query,$sql_argument );

        } else {
            $irt = new IRTController(Session::get('courseID'), Auth::id());
            $questions = $irt->getQuestions(5);
        }
        $choices =[];
        $question_num_answers = [];
        $question_filter_content = [];

        if (!empty($questions)) {
            foreach($questions as $q ) {
                $tmp_choices = DB::table('question_choices')->where('question_id','=',$q->id)->get();
                $question_num_answers[$q->id] = 0;
                foreach($tmp_choices as $choice ) {
                    $choices[$q->id][] = $choice;
                    if($choice->answer == 1) {
                        $question_num_answers[$q->id] += 1;
                    }
                }

                $questionTable = DB::table('questions')->where('id','=',$q->id)->get();
                
                $contents = '';
                foreach($questionTable as $tmp_question ) {
                    $contents = $tmp_question->contents;
                 
                    if ($tmp_question->mode == 1) {
                        libxml_use_internal_errors(true);
                        $doc = new \DOMDocument();
                        $doc->loadHTML($contents);
                        $this->removeElementsByTagName('script', $doc);
                        $this->removeElementsByTagName('style', $doc);
                        $this->removeElementsByTagName('form', $doc);
            
                        foreach ($doc->getElementsByTagname('*') as $element)
                        {
                            foreach (iterator_to_array($element->attributes) as $name => $attribute)
                            {
                                if (substr_compare($name, 'on', 0, 2, TRUE) === 0)
                                {
                                    $element->removeAttribute($name);
                                }
                            }
                        }
            
                        libxml_clear_errors();
                        $contents = $doc->saveHTML();

                        $question_filter_content[$q->id] = $contents;
                    }
                    else
                    {
                        $question_filter_content[$q->id] = $tmp_question->contents;
                    }
                }
            }
        }

        //Laravel Log
        Log::info('[Question.Exercise] [ '.Session::get('courseID').' ] ['.Auth::id().'] Enter Exercise. ');

        return view('questions.showexercise',['questions'=>$questions, 'question_choices'=>$choices, 'question_num_answers'=>$question_num_answers, 'question_filter_content'=>$question_filter_content]);
    }

    public function showquestion_popularity($order)
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        if ($order == 1) {
            return redirect()->route('question.showquestion')->with('order_popularity', 2);
        } elseif ($order == 2 || $order == -1) {
            return redirect()->route('question.showquestion')->with('order_popularity', 1);
        } else {
            return redirect()->back()->with('fail', 'Failed to load Popularity Order Request. Please try again');
        }
    }

    public function showquestion_date($order)
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        if ($order == 1) {
            return redirect()->route('question.showquestion')->with('order_date', 2);
        } elseif ($order == 2 || $order == -1) {
            return redirect()->route('question.showquestion')->with('order_date', 1);
        } else {
            return redirect()->back()->with('fail', 'Failed to load Date Order Request. Please try again');
        }
    }

    public function showquestion_rating($order)
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        if ($order == 1) {
            return redirect()->route('question.showquestion')->with('order_rating', 2);
        } elseif ($order == 2 || $order == -1) {
            return redirect()->route('question.showquestion')->with('order_rating', 1);
        } else {
            return redirect()->back()->with('fail', 'Failed to load Date Order Request. Please try again');
        }
    }

    public function showquestion(Request $request)
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        $keyword_searched='';
        $order_popularity_mode = -1; //-1: none, 1: asc, 2: desc
        $order_date_mode = -1; //-1: none, 1: desc, 2: asc
        $order_rating_mode = -1; //-1: none, 1: desc, 2: asc

        if(!empty($request->search)) {

            $questions = $this->question_class->getQuestionsByKeywords($request->search);

        } elseif (!empty(session('search_keyword_by_tags'))) {

            $questions = $this->question_class->getQuestionsByKeywords(session('search_keyword_by_tags'));

        } elseif (!empty(session('order_popularity'))) {

            if (session('order_popularity') == 1 ) {
                $order_popularity_mode = 1;
            }

            $questions = $this->question_class->getQuestions(2, $order_popularity_mode);

        } elseif (!empty(session('order_date'))) {

            if(session('order_date') == 1) {
                $order_date_mode = 1;
            }

            $questions = $this->question_class->getQuestions(1, $order_date_mode);

        } elseif (!empty(session('order_rating'))) {

            if(session('order_rating') == 1) {
                $order_rating_mode = 1;
            }

            $questions = $this->question_class->getQuestions(3, $order_rating_mode);

        } else {
            $questions = $this->question_class->getQuestions(1);
        }

        if (is_null($questions)) {
            return redirect()->back()->with('fail','Failed to retrieve questions');
        }

        $keywords = [];
        $tmp_keywords = DB::select('select * from keywords where EXISTS (select * from questions as q where q.id = keywords.question_id and course_id =?  ) ;', [Session::get('courseID')]);

        $tmp_attempts = DB::select('select * from user_answereds where EXISTS (select * from questions as q where q.id = user_answereds.question_id and course_id =?)',[Session::get('courseID')]);

        $tmp_comments = DB::select('select question_id, count(id) as num from comments where EXISTS (select * from questions as q where q.id = comments.question_id and course_id =?) group by question_id', [Session::get('courseID')]);

        $attempts = [];
        $comments =[];
        foreach($tmp_attempts as $attempt) {
            if(empty($attempts[$attempt->question_id] )) {
                $attempts[$attempt->question_id] = (int)$attempt->num_attempts;
            } else {
                $attempts[$attempt->question_id] += (int)$attempt->num_attempts;
            }
        }

        foreach($tmp_keywords as $tmp) {
            if (empty($keywords[$tmp->question_id])) {
                $keywords[$tmp->question_id] = $tmp->keyword;
            } else{
                $keywords[$tmp->question_id] .= ",".$tmp->keyword;
            }
        }

        foreach($tmp_comments as $tmp) {
            $comments[$tmp->question_id] = $tmp->num;
        }

        $teachersByQuestionId = [];

        $tmpTeacher = DB::select('select * from teachers;');
        foreach($tmpTeacher as $tmp) {
            $teachersByQuestionId[$tmp->id] = $tmp->nickname;
        }

        //Laravel Log
        Log::info('[Question.Show] [ '.Session::get('courseID').' ] ['.Auth::id().'] Enter show question');

        return view('questions.showquestion', ['order_rating_mode'=>$order_rating_mode,'order_date_mode'=>$order_date_mode,'order_popularity_mode'=>$order_popularity_mode,'comments'=>$comments, 'teachersByQuestionId'=>$teachersByQuestionId,'keyword_searched'=>$keyword_searched,'questions' => $questions,'user_id'=>Auth::id(), 'keywords'=>$keywords, 'attempts'=>$attempts]);
    }

    public function searchquestion($search)
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        if (empty($search)) {
            return redirect()->route('question.showquestion')->with('fail','Failed to search keyword');
        }

        //Laravel Log
        Log::info('[Question.Search] [ '.Session::get('courseID').' ] ['.Auth::id().'] Search question by the keyword: [ '.$search.' ] ');

        return redirect()->route('question.showquestion')->with('search_keyword_by_tags', $search);
    }

    public function clearSearchquestion()
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        return redirect()->route('question.showquestion');
    }

    public function removeElementsByTagName($tagName, $document) {
        $nodeList = $document->getElementsByTagName($tagName);
        for ($nodeIdx = $nodeList->length; --$nodeIdx >= 0; ) {
            $node = $nodeList->item($nodeIdx);
            $node->parentNode->removeChild($node);
        }
    }

    public function insertquestion(Request $request)
    {
        $this->validate($request, array(
            'topic' => 'required|max:95',
            'tags' => 'required',
            'contents' => 'required|min:1'
        ));

        $contents = $request->contents;

        //check if contents contain chinese character
        if (preg_match("/\p{Han}+/u", $contents) || preg_match("/\p{Han}+/u", $request->topic)) {
            return redirect()->back()->withInput()->with('fail','Please write English');
        }

        if(!empty($request->tips)) {
            //check if contents contain chinese character
            if (preg_match("/\p{Han}+/u", $request->tips)) {
                return redirect()->back()->withInput()->with('fail','Please write English');
            }
        }

        if(!empty($request->explaination)) {
            //check if contents contain chinese character
            if (preg_match("/\p{Han}+/u", $request->explaination) ) {
                return redirect()->back()->withInput()->with('fail','Please write English');
            }
        }

        $question = new Question;

        $question_id = 1;

        while ($this->db->checkQuestionsId($question_id)!=true) {
            $question_id = rand(1, 5000);
        }

        $question->id = $question_id;
        $question->topic = $request->topic;
        $question->contents = $contents;
        $question->difficulty = 0;
        $question->explaination = $request->explaination;
        $question->rating = 0;
        $question->course_id = Session::get('courseID');
        $question->author_id = Auth::id();
        $question->tips = (empty($request->tips))?'':$request->tips;
        $question->num_attempts = $request->number_attempts;
        $question->mode = $request->content_type;

        switch ((int)$request->choice_type) {
            case 0:
                $this->validate($request, array(
                    'answer'=>'required',
                    'choice' => 'required',
                ));

                $choices = array_filter($request->choice);
                if (sizeof($choices) <3) {
                    Log::info('[Question.Insert] [ '.Session::get('courseID').' ] ['.Auth::id().'] Insert question. User failed to insert question due to less number of MC choices created which is less than 3');
                    return redirect()->back()->withInput()->with('fail','Please create at least three choices');
                }

                //avoid duplicate value
                if (count($choices) !== count(array_unique($choices)) ) {
                    Log::info('[Question.Insert] [ '.Session::get('courseID').' ] ['.Auth::id().'] Insert question. User failed due to duplicate choices');
                    return redirect()->back()->withInput()->with('fail','There are duplicate choices. ');
                }

                //check Similarity Here
                $similar_score = $this->db->checkQuestionSimilarity($contents, Session::get('courseID'),-1, $request->topic, $choices);
                if ($similar_score>80) {
                    Log::info('[Question.Insert] [ '.Session::get('courseID').' ] ['.Auth::id().'] Insert question. User failed due to the similarity score greater than 80. Similarity Score: [ '.$similar_score.' ]');
                    return redirect()->back()->withInput()->with('similar',$similar_score);
                }

                $question->type = 0;

                //Run the sql
                $question->save();

                foreach($choices as $i =>$choice) {
                    //check if contents contain chinese character
                    if (preg_match("/\p{Han}+/u", $choice)) {
                        return redirect()->back()->withInput()->with('fail','Please write English');
                    }

                    $question_choice = new QuestionChoice();
                    $question_choice->question_id = $question_id;
                    $question_choice->choice = $choice;
                    if (in_array('answer'.($i+1) ,$request->answer)) {
                        $question_choice->answer = 1;
                    } else {
                        $question_choice->answer = 0;
                    }
                    $question_choice->type=0;
                    $question_choice->save();
                }

                break;
            case 1:
                $this->validate($request, array(
                    'tf_answer' => 'required',
                ));
                $answer = $request->tf_answer;

                //check Similarity Here
                $similar_score = $this->db->checkQuestionSimilarity($contents, Session::get('courseID'),-1, $request->topic, [],$answer);

                if ($similar_score>85) {
                    Log::info('[Question.Insert] [ '.Session::get('courseID').' ] ['.Auth::id().'] Insert question. User failed due to the similarity score greater than 85. Similarity Score: [ '.$similar_score.' ]');
                    return redirect()->back()->withInput()->with('similar',$similar_score);
                }

                $question->type = 1;
                //Run the sql
                $question->save();

                $question_choice = new QuestionChoice();
                $question_choice->question_id = $question_id;
                $question_choice->choice = '';
                $question_choice->answer =$answer;
                $question_choice->type=1;
                $question_choice->save();

                break;
            default:
                Log::warning('[Question.Insert] [ '.Session::get('courseID').' ] ['.Auth::id().'] Insert question. User failed to insert question due to undefined choice_type which is neither 0 nor 1');
                return redirect()->back()->withInput()->with('fail','Please check the choice type for your question');
        }

        $tags = '';

        if(!empty($request->tags)) {
            $tags = $request->tags;
//            $tags = filter_var($tags, FILTER_SANITIZE_STRING);
            $keywords = explode(',',$request->tags);
            $keyword_data = [];

            foreach ($keywords as $keyword) {
                $keyword_data[] = array('question_id'=>$question_id, 'keyword' =>$this->xss_clean($keyword));
            }

            DB::table('keywords')->insert($keyword_data);

            //store into json files
            $this->json->addLog($keyword_data, 'keyword');

        } else {
            Log::warning('[Question.Insert] [ '.Session::get('courseID').' ] ['.Auth::id().'] Insert question. User failed to insert question due to empty keyword tags');
        }


        // after run sql, add it into log file
        $this->json->addLog($question, 'question');

        //Laravel Log
        $answers = null;
        if(!empty($request->answer)) {
            $answers = implode(' ',  $request->answer);
        } elseif (!empty($request->tf_answer)) {
            $answers = $request->tf_answer;
        }
        Log::info('[Question.Insert] [ '.Session::get('courseID').' ] ['.Auth::id().'] Insert question: id -> [ '.$question_id.' ], topic->[ '.$request->topic.' ], contents->[ '.$contents.' ], difficulty->[ Easy ], explaination->[ '.$request->explaination.' ], tips->[ '.$request->tips.' ], rating->[ 0 ],
         choice type->[ '.$request->choice_type.' ], number of attempts-> [ '.$request->number_attempts.' ], keywords [ '.$tags.' ], answers [ '.$answers. ' ]');

        return redirect()->route('question.my')->with('justcreated_id',$question->id )->with('success', 'Successfully Created!');

    }

    private function xss_clean($data)
    {
        // Fix &entity\n;
        $data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);
        $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
        $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
        $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

        // Remove any attribute starting with "on" or xmlns
        $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

        // Remove javascript: and vbscript: protocols
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

        // Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

        // Remove namespaced elements (we do not need them)
        $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);
        $data = preg_replace('/;+/', '', $data);
        $data = preg_replace('/--+/', '', $data);
        $data = preg_replace('/!+/', '', $data);
        $data = preg_replace('/{+/', '', $data);
        $data = preg_replace('/}+/', '', $data);
        $data = preg_replace('/\'/', '', $data);
        $data = preg_replace('/&/', '', $data);
        $data = preg_replace('/=/', '', $data);
        $data = preg_replace('/\"/', '', $data);
        $data = preg_replace('/‘’/', '', $data);
        $data = preg_replace('/“/', '', $data);
        $data = preg_replace('/\'/', '', $data);
        $data = preg_replace('@\([^\)]*\)$@', '', $data);
        $data = preg_replace("/<!--.*?-->/", "", $data);
        $data = preg_replace("/\/\/.*?\n/", "\n", $data);
        $data= preg_replace("/\/*.*?\*\//", "", $data);
        $data = preg_replace("/#.*?\n/", "\n", $data);

        str_replace('"', "", $data);
        str_replace("'", "", $data);

        do
        {
            // Remove really unwanted tags
            $old_data = $data;
            $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
        }
        while ($old_data !== $data);

        return $data;
    }

    public function createquestion(Request $request)
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        //Laravel Log
        Log::info('[Question.Create] [ '.Session::get('courseID').' ] ['.Auth::id().'] Enter create question page');

        return view('questions.create');
    }

    public function showmyquestion ($sessionID = null)
    {
        if(!is_null($sessionID)){
            Session::put('courseID', $sessionID);
        }

        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        //mark read on notifications
        if(sizeof(auth()->user()->unreadNotifications)) {
            foreach(auth()->user()->unreadNotifications as $notification) {
                if($notification->data['type'] == 0) {
                    $notification->markAsRead();
                }
            }
        }

        $questions = DB::select('select id, topic, mode, difficulty, type, rating, created_at from questions where author_id =? and course_id = ? and isMadeByTeacher =0 order by created_at DESC ', [Auth::id(), Session::get('courseID')]);

        $questions_arr = [];

        foreach ($questions as $question)
        {
            $questions_arr["$question->id"] = ['id'=>$question->id, 'topic'=>$question->topic, 'difficulty'=>$question->difficulty,'rating'=>$question->rating,
                'created_at'=>$question->created_at, 'type'=>$question->type,'mode'=>$question->mode, 'num_answers'=>0, 'num_corrects'=> 0, 'avg_score'=>0, 'num_views'=>0,'avg_spent' =>0];
        }


        $num_answers = DB::select('
        select Q.id, sum(UA.num_attempts) as num from questions as Q
        join user_answereds as UA on UA.question_id = Q.id AND
        Q.isMadeByTeacher = 0 and Q.author_id = ? and Q.course_id = ? group by Q.id;
        ', [Auth::id(), Session::get('courseID')]);
//        $num_answers = DB::select('');

        foreach ($num_answers as $each_answer) {
            $questions_arr["$each_answer->id"]['num_answers'] = $each_answer->num;
        }

        $num_corrects = DB::select('
        select Q.id, count(UA.result) as num from questions as Q
        join user_answereds as UA on UA.question_id = Q.id AND UA.result = 1 and
        Q.isMadeByTeacher = 0 and Q.course_id = ? and Q.author_id = ? group by Q.id;
        ', [Session::get('courseID'),Auth::id()]);

        foreach ($num_corrects as $each_answer) {
            $questions_arr["$each_answer->id"]['num_corrects'] = $each_answer->num;
        }

        $avg_score = DB::select('
        select Q.id, avg(UA.result) as num from questions as Q
        join user_answereds as UA on UA.question_id = Q.id AND
        Q.isMadeByTeacher = 0 and Q.course_id = ? and Q.author_id = ? group by Q.id;
        ', [Session::get('courseID'), Auth::id()]);

        foreach ($avg_score as $each_answer) {
            $questions_arr["$each_answer->id"]['avg_score'] = $each_answer->num;
        }

        $num_views = DB::select('select Q.id, count(P.id) as num from questions as Q
                    join page_views as P on P.question_id = Q.id and Q.course_id = ? and Q.author_id = ? and isMadeByTeacher =0 group by Q.id;',[Session::get('courseID'), Auth::id()]);

        foreach ($num_views as $each_answer) {
            $questions_arr["$each_answer->id"]['num_views'] = $each_answer->num;
        }

        $avg_spent = $this->json->readLogAvgTimeSpent();
        if(is_array($avg_spent)) {
            foreach($avg_spent as $time) {
                if(!empty($questions_arr[$time['question_id']])) {
                    $questions_arr[$time['question_id']]['avg_spent'] = $time['timeSpent'];
                }
            }
        }

        //Laravel Log
        Log::info('[Question.ShowMy] [ '.Session::get('courseID').' ] ['.Auth::id().'] Enter show my question page');


        return view('questions.showmyquestion', ['questions' => $questions_arr]);

    }

    private function checkUserAnswer($question_id, $user_answer)
    {
        $question = DB::table('questions')->where('id','=',$question_id)->first();
        $choices = DB::table('question_choices')->where('question_id','=',$question_id)->get();
        $result = 0;

        if(empty($question)) {
            return -1; //not found this question.....
        }

        if (empty($choices) || is_null($choices[0]->answer)) {
            return -2; //Sorry we cannot find the answer from DB
        }

        if ($question->type == 1 ) { //TF
            if (!is_null($user_answer)) {
                if ($user_answer == $choices[0]->answer) {
                    $result = 1;
                }
            }
        } else { //MC
            if (!empty($user_answer)) {
                $real_answers = [];
                foreach($choices as $choice) {
                    if($choice->answer == 1) {
                        $real_answers[] = $choice->choice;
                    }
                }

                if(!is_array($user_answer)) {
                    return -3; //parameter is wrong
                }

                if(empty($real_answers)) {
                    return -2;
                }

                if(sizeof($user_answer) == sizeof($real_answers) && $user_answer === $real_answers) {
                    $result = 1;
                }
            }
        }
        return $result;
    }

    public function successQuizAnswer(Request $request)
    {
        if (!Session::get('courseID') || !Session::get('quizID')) {
            return redirect()->route('student.home')->with('fail', "Sorry Session is out");
        }
        //check if user already answered
        $user_answer = DB::table('quiz_attempts')->where([['user_id', Auth::id()], ['quiz_id',Session::get('quizID') ]])->first();
        if(!empty($user_answer)) {
            return redirect()->route('question.quiz')->with('fail', "You have already answered");
        }

        $quiz = DB::table('quiz')->where('id',Session::get('quizID'))->first();
        if (empty($quiz) || $quiz->num_questions <=0) {
            return redirect()->route('question.quiz')->with('fail', "Failed to retrieve quiz data. Please try again!");
        }

        for ($k=0;$k < $quiz->num_questions; $k++) {
            $index = 'questionID_'.$k;
            if(empty($request->$index)) {
                continue;
            }

            $question_id = $request->$index;
            $answer_index = 'choice2answer_'.($k);

            //compare user answer and real answer
            $result = $this->checkUserAnswer($question_id, $request->$answer_index);
            if ($result == -1) {
                return redirect()->route('question.quiz')->with('fail', "Sorry we failed to access to DB");
            } elseif ($result == -2) {
                return redirect()->route('question.quiz')->with('fail', "Sorry we cannot find answer from DB");
            } elseif ($result == -3) {
                return redirect()->route('question.quiz')->with('fail', "There is something wrong with the input. Please try again later");
            }

            //Format: [result, your answer, question_id]
            $result_list[] = [$result,$request->$answer_index,$question_id];

            //Store data to DB
            $quiz_attempt = new QuizAttempts;
            $quiz_attempt->user_id = Auth::id();
            $quiz_attempt->result = $result;
            $quiz_attempt->question_id = $question_id;
            $quiz_attempt->quiz_id = Session::get('quizID');
            $quiz_attempt->save();


            $user_answer = DB::table('quiz_attempts')->where([['user_id', Auth::id()],['question_id',$question_id], ['quiz_id',Session::get('quizID') ]])->first();

            //store answer to db (quiz_answers)
            if (!is_null($request->$answer_index)) {
                if (is_array($request->$answer_index)) { // MC
                    foreach ($request->$answer_index as $answer) {

                        $quiz_answer = new QuizAnswer;
                        $quiz_answer->user_id = Auth::id();
                        $quiz_answer->question_id = $question_id;
                        $quiz_answer->quiz_attempt_id = $user_answer->id;
                        $quiz_answer->answer = $answer;
                        $quiz_answer->save();
                    }
                }
                else { // TF
                    $quiz_answer = new QuizAnswer;
                    $quiz_answer->user_id = Auth::id();
                    $quiz_answer->question_id = $question_id;
                    $quiz_answer->quiz_attempt_id = $user_answer->id;
                    $quiz_answer->answer = $request->$answer_index;
                    $quiz_answer->save();
                }
            } else {

                $quiz_answer = new QuizAnswer;
                $quiz_answer->user_id = Auth::id();
                $quiz_answer->question_id = $question_id;
                $quiz_answer->quiz_attempt_id = $user_answer->id;
                $quiz_answer->answer = '';
                $quiz_answer->save();
            }
        }

        //Laravel Log
        Log::info('[Question.Create] [ '.Session::get('courseID').' ] ['.Auth::id().'] Enter create question page');

        return redirect()->route('question.quizdetail');

    }

    public function successExerciseAnswer(Request $request)
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Session is out");
        }

        $question_index_list = ['questionID_0','questionID_1','questionID_2','questionID_3','questionID_4'];
        $result_list = [];
        $num_correct = 0;

        foreach($question_index_list as $k=>$index) {
            if(empty($request->$index)) {
                continue;
            }

            $question_id = $request->$index;
            $answer_index = 'choice2answer_'.($k);

            $questions = DB::select('select * from questions where id =?', [$question_id]);
            $choices = DB::table('question_choices')->where('question_id','=',$question_id)->get();
            $result = 0;

            if (empty($choices) || is_null($choices[0]->answer)) {
                return redirect()->back()->withInput()->with('fail','Sorry we cannot find the answer from DB');
            }

            if ($questions[0]->type == 1 ) {
//                if (!empty($request->$answer_index)) {
                    if ($request->$answer_index == $choices[0]->answer) {
                        $result = 1; $num_correct++;
                    }
//                }
            } else {
                if (!empty($request->$answer_index)) {
                    $real_answers = [];
                    foreach($choices as $choice) {
                        if($choice->answer == 1) {
                            $real_answers[] = $choice->choice;
                        }
                    }

                    if(empty($real_answers)) {
                        return redirect()->back()->with('fail','Sorry we cannot find the answer from DB');
                    }

                    if(sizeof($request->$answer_index) == sizeof($real_answers) && $request->$answer_index === $real_answers) {
                        $result = 1; $num_correct++;
                    }
                }
            }

            //Format: [result, your answer, question_id]
            $result_list[] = [$result,$request->$answer_index,$question_id];

            //Store data to DB (User_Answered)
            $user_answered = new UserAnswered;
            $user_answered->user_id = Auth::id();
            $user_answered->question_id = $question_id;
            $user_answered->result = $result;

            if ($this->db->addNumAttemptsQuestions($question_id) ==0 ) {
                $user_answered->save();
                $this->db->addNumAttemptsQuestions($question_id);
            } else {
                $this->db->updateUserAttemptsResult($question_id, $result);
                $user_answered = null;
            }

            if($questions[0]->isMadeByTeacher ==0) {
                $user = User::where('notify_answered', '=', 1)->where('id', '=', $questions[0]->author_id)->first();

                if(!empty($user)) {
                    $content['content'] ="Somebody answered your question!";
                    $content['topic'] = "Answered Your Question!";
                    $content['course_id'] = $questions[0]->course_id;

                    $user->notify(new notifyStudent($content, 0));
                }
            }

        }
        //for session flush
        sleep(1);

        //overall performance
        if ($num_correct == 5) {
            return redirect()->route('question.exercise')->with('success','Well Done! All Correct!')->with('result_list',$result_list);
        } else {
            return redirect()->route('question.exercise')->with('result_list',$result_list);
        }
    }

    public function detailquestion_email($id, $course_id)
    {
        $courseInformation = $this->db->getCourseInformationById($course_id);

        if(empty($courseInformation[0])) {
            return redirect()->route('student.home')->with('fail', "You are not enrolled this course");
        }

        if(!is_numeric($id)) {
            return redirect()->route('student.home')->with('fail', "a Question Not Found and string detected instead of number");
        }

        $question = (DB::select('select * from questions where id =? and course_id =?; ', [$id,$course_id]));
        if(empty($question)) {
            return redirect()->route('student.home')->with('fail', "Question Not Found");
        }

        Session::put('courseID', $course_id);

        return redirect()->route('question.detail',['id'=>$id]);

    }

    public function showvoting()
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        $votes = DB::select('select question_id, count(user_id) as num from user_votes uv 
          where exists (select * from questions q where q.id = uv.question_id and q.course_id = ? ) 
          group by question_id order by num DESC limit 10 ', [Session::get('courseID')]);

        $questions = $this->getQuestionsByIds($votes,Session::get('courseID'), -1, true);

        return view('questions.showvoting',['votes'=>$votes]);
    }

    public function detailquestion ($id)
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        if(!is_numeric($id)) {
            Log::warning('[Question.detail] ['.Auth::id().'] See Question: ['.$id.'] Failed due to not found record');
            return redirect()->back()->with('fail', "aQuestion Not Found and string detected instead of number");
        }

        $question = $this->question_class->getQuestion($id);

        if(empty($question)) {
            Log::warning('[Question.detail] ['.Auth::id().'] See Question: ['.$id.'] Failed due to not found record');
            return redirect()->back()->with('fail',  "Question Not Found");
        }

        $surroundings = $this->question_class->getQuestionSurroundings($id,$question->isMadeByTeacher);
        $pre_question_id = $surroundings[0];
        $next_question_id = $surroundings[1];

        $comments = $this->db->getNameWithComment($id);

        $rating = $this->db->getRatingByIds(Session::get('courseID'), $id);

        $result = session('result');

        $toShowChatFlag = true;
        $showAnswer = false;
        if ( $this->db->compareAttemptsQuestions($id) == 0 ) { //user attempt is less than the limit
            $showAnswer = true;
        }

        if ($this->db->getNumAttemptsQuestions($id)==0) { // get number of attempts that this user tried on this question
            $toShowChatFlag = false;
        }

        if ($this->student->getScore(Auth::id(), $id) == 1) {
            $showAnswer = true;
        }

        if ($result==1) {
            $showAnswer = true;
        }

        if ($question->isMadeByTeacher == 0 && $question->author_id == Auth::id()) {
            $showAnswer = true;
        }

        //record views into DB
        if($question->author_id !=Auth::id()) {
            $view_table = new PageView();
            $view_table->question_id = $id;
            $view_table->user_id = Auth::id();
            $view_table->save();
        }

        //if we need to show small box displayed on the middle of screen to let users to give rating or not
        $show_rating_popup_box = false;
        if(Auth::user()->tried_first_answer_flag == 0 && $showAnswer == true && ($question->author_id != Auth::id() || $question->isMadeByTeacher == 1  ) ) {
            DB::table('users')
                ->where([['id', Auth::id()]])
                ->update(['tried_first_answer_flag' => 1]);
            $show_rating_popup_box = true;
        }

        //get Choices
        $choices = DB::table('question_choices')->where('question_id',$id)->get();

        //get totoal number of answers
        $num_answers_total = 0;
        if ($question->type == 0) {
            foreach($choices as $choice) {
                $num_answers_total += $choice->num_attempts;
            }
        }  else {
            $tmp_result = DB::select('select question_id, count(id) as num from user_answereds where question_id = ? group by question_id', [$id]);
            if (empty($tmp_result)) {
                $num_answers_total = 0;
            } else {
                $num_answers_total = $tmp_result[0]->num;
            }
        }

        $contents = $question->contents;
        if ($question->mode == 1) {
            libxml_use_internal_errors(true);
            $doc = new \DOMDocument();
            $doc->loadHTML($contents);
            $this->removeElementsByTagName('script', $doc);
            $this->removeElementsByTagName('style', $doc);
            $this->removeElementsByTagName('form', $doc);

            foreach ($doc->getElementsByTagname('*') as $element)
            {
                foreach (iterator_to_array($element->attributes) as $name => $attribute)
                {
                    if (substr_compare($name, 'on', 0, 2, TRUE) === 0)
                    {
                        $element->removeAttribute($name);
                    }
                }
            }

            libxml_clear_errors();
            $contents = $doc->saveHTML();
        }

        Log::info('[Question.detail] ['.Auth::id().'] See Question: ['.$id.'] ');

        return view('questions.detailquestion', ['contents'=>$contents,'total_num'=>$num_answers_total,'choices'=>$choices,'show_rating_popup_box'=>$show_rating_popup_box,'rating'=>$rating, 'question' => $question,
            'comments'=>$comments,'toShowChatFlag'=>$toShowChatFlag, 'showAnswer'=>$showAnswer,
            'next_question_id'=>$next_question_id, 'pre_question_id'=>$pre_question_id]);
    }

    public function insertcomment(Request $request)
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        $this->validate($request, array(
            'message' => 'required',
        ));

        $comment = new Comment();
        $comment->comment = $request->message;
        $comment->author_id = Auth::id();
        $comment->question_id = $request->questionID;
        $comment->course_id = Session::get('courseID');

        $comment->save();

        return redirect()->route('question.detail',['id'=>$request->questionID]);
    }

    public function successanswer (Request $request) //validator for answer
    {
        $this->validate($request, array(
            'choice2answer' => 'required',
        ));

        Log::info('[Question.Answer] ['.Auth::id().'] Answered Question: ['.$request->questionID.']');

        //get question
        $question = DB::table('questions')->where('id','=',$request->questionID)->first();
        if(empty($question)) {
            Log::warning('[Question.Answer] ['.Auth::id().'] Answered Question: ['.$request->questionID.']. Failed Due to not found the question record');
            return redirect()->back()->with('fail','Sorry We cannot find the question');
        }

        //get answer
        $choices = $this->question_class->getChoices($request->questionID);
        if(empty($choices)) {
            Log::warning('[Question.Answer] ['.Auth::id().'] Answered Question: ['.$request->questionID.']. Failed Due to not found the answer of this question');
            return redirect()->back()->with('fail','Sorry We cannot find the answer of this question. Please refresh and try again');
        }

        $result = 0;

        if($question->type == 1) {
            if ($choices[0]->answer == $request->choice2answer) {
                $result = 1;
            }
        } else {
            $real_answers = [];
            foreach($choices as $choice) {
                if($choice->answer == 1) {
                    $real_answers[] = $choice->choice;
                }
            }

            if(empty($real_answers)) {
                Log::warning('[Question.Answer] ['.Auth::id().'] Answered Question: ['.$request->questionID.']. Failed Due to not found answer of the question');
                return redirect()->back()->with('fail','Sorry We cannot find the answer of this question');
            }

            if(sizeof($request->choice2answer) == sizeof($real_answers)) {
                if ($request->choice2answer === $real_answers) { //For Both MC & TF Question
                    $result = 1;
                }
            }
        }

        //store answer into log
        $tmpdata = array('user_id'=>Auth::id(), 'question_id'=>$question->id, 'answer'=>$request->choice2answer, 'result'=>$result, 'time'=>date("Y-m-d H:i:s"));
        $this->json->addLog($tmpdata, 'answer');


        //Laravel Log
        if (!is_array($request->choice2answer)) {
            Log::info('[Question.Answer] ['.Auth::id().'] Answered Question: ['.$request->questionID.']. Result:  ['.$result.']. User Answer:  ['.$request->choice2answer.'] ');
        } else {
            Log::info('[Question.Answer] ['.Auth::id().'] Answered Question: ['.$request->questionID.']. Result:  ['.$result.']. User Answer:  ['.implode(',',$request->choice2answer).'] ');
        }

        //Store data to User_Answered
        $user_answered = new UserAnswered;
        $user_answered->user_id = Auth::id();
        $user_answered->question_id = (int)$question->id;
        $user_answered->result = $result;

        if ($this->db->addNumAttemptsQuestions($request->questionID) ==0 ) {
            $user_answered->save();
            $this->db->addNumAttemptsQuestions($request->questionID);
        } else {
            $this->db->updateUserAttemptsResult($request->questionID, $result);
            $user_answered = null;
        }

        if($question->isMadeByTeacher ==0) {
            $user = User::where('notify_answered', '=', 1)->where('id', '=', $question->author_id)->first();

            if(!empty($user)) {
                $content['content'] ="Somebody answered your question!";
                $content['topic'] = "Answered Your Question!";
                $content['course_id'] = $question->course_id;

                $user->notify(new notifyStudent($content, 0));
            }
        }

        //update number of choice be selected to question_choice DB
        if($question->type == 0){
            foreach($request->choice2answer as $user_choice) {
                DB::table('question_choices')->where([['question_id',$question->id ], ['type', $question->type], ['choice',$user_choice]])->increment('num_attempts');
            }
        } else {
            if ($result == 1) {
                DB::table('question_choices')->where([['question_id',$question->id ], ['type', $question->type]])->increment('num_attempts');
            }
        }

        //update question level && irt data
        $this->mining->updateCsv();
        $this->mining->updateQuestionLevel();

        if ($result==0) {
            $result = -1;
            $userattempts = DB::select('select * from user_answereds where question_id =? AND user_id =?', [$question->id, Auth::id()]);
            $remaining = $question->num_attempts - $userattempts[0]->num_attempts;
            return redirect()->route('question.detail',['id'=>(int)$question->id] )->with('result', $result)->with('fail','Wrong! Attempt remaining: '.$remaining);
        } else {
            $result = 1;
            return redirect()->route('question.detail',['id'=>(int)$question->id] )->with('result', $result)->with('success', "Correct");
        }

    }

    public function showanswered()
    {
        $questions = DB::select('select Q.id, Q.topic, Q.difficulty, UA.created_at, UA.result, UA.num_attempts as num_attempts , Q.num_attempts as max_attempts,  UA.rating as rating, Q.rating as avg_rating from user_answereds as UA, questions as Q
where Q.id = UA.question_id  and Q.course_id = ? and UA.user_id = ? and Q.isMadeByTeacher = 0;', [Session::get('courseID'), Auth::id()]);

        return view('questions.answered', ['questions' => $questions]);
    }

    public function editquestion($id)
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        $question = DB::table('questions')->where([['author_id', Auth::id()], ['id',$id],['isMadeByTeacher', 0]])->first();

        if( empty($question) ) {
            return redirect()->back()->with('fail', "Sorry Not Found");
        }

        $keywords = DB::select('select keyword from keywords where question_id = ?;',[$question->id]);
        $keywords_to_string = '';
        foreach($keywords as $k) {
            $keywords_to_string .= $k->keyword.',';
        }

        $choices = DB::table('question_choices')->where('question_id',$id)->get();

        $contents = $question->contents;
        if ($question->mode == 1) {
            libxml_use_internal_errors(true);
            $doc = new \DOMDocument();
            $doc->loadHTML($contents);
            $this->removeElementsByTagName('script', $doc);
            $this->removeElementsByTagName('style', $doc);

            libxml_clear_errors();
            $contents = $doc->saveHTML();
        }

        return view('questions.edit',['contents'=>$contents,'choices'=>$choices,'question'=>$question,'keywords'=>$keywords_to_string]);
    }

    public function updatequestion(Request $request)
    {
        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        $this->validate($request, array(
            'topic' => 'required|max:255',
            'tags' => 'required',
            'contents' => 'required|min:1',
            'answer' => 'required'
        ));

        $contents = $request->contents;

        if(!empty($request->tips)) {
            //check if contents contain chinese character
            if (preg_match("/\p{Han}+/u", $request->tips)) {
                return redirect()->back()->withInput()->with('fail','Please write English');
            }
        }

        if(!empty($request->explaination)) {
            //check if contents contain chinese character
            if (preg_match("/\p{Han}+/u", $request->explaination) ) {
                return redirect()->back()->withInput()->with('fail','Please write English');
            }
        }

        //Update Question Info
        $tips='';
        $explaination = '';

        if(!empty($request->tips)) {$tips = $request->tips;}
        if(!empty($request->explaination)) {$explaination = $request->explaination;}

        DB::table('questions')
            ->where([['id', $request->question_id]])
            ->update(['topic' => $request->topic, 'contents'=>$contents,'tips'=>$tips,'mode'=>$request->content_type, 'explaination'=>$explaination, 'num_attempts'=>$request->number_attempts]);

        //Update Keyword
        $keywords = explode(',',$request->tags);
        foreach ($keywords as $keyword) {
            DB::table('keywords')->where(['question_id'=>$request->question_id])->update(['keyword'=>$keyword]);
        }

        if ($request->choice_type ==0 ) {
            //Update Choices and Answers
            $choices = array_filter($request->choice);
            $answers = array_filter($request->answer);

            $this->validate($request, array(
                'choice' => 'required'
            ));


            if (sizeof($choices) <3) {
                Log::info('[Question.Update] [ '.Session::get('courseID').' ] ['.Auth::id().'] Update question. User failed to update question due to less number of MC choices created which is less than 3');
                return redirect()->back()->withInput()->with('fail','Please create at least three choices');
            }

            //check Similarity Here
            $similar_score = $this->db->checkQuestionSimilarity($contents, Session::get('courseID'),$request->question_id, $request->topic, $choices);
            if ($similar_score>80) {
                Log::info('[Question.Update] [ '.Session::get('courseID').' ] ['.Auth::id().'] Update question. User failed due to the similarity score greater than 80. Similarity Score: [ '.$similar_score.' ]');
                return redirect()->back()->withInput()->with('similar',$similar_score);
            }

            DB::table('question_choices')->where(['question_id'=>$request->question_id])->delete();
            foreach($choices as $i =>$choice) {
                $question_choice = new QuestionChoice();
                $question_choice->question_id = $request->question_id;
                $question_choice->choice = $choice;
                if (in_array(($i+1),$answers)) {
                    $question_choice->answer = 1;
                } else {
                    $question_choice->answer = 0;
                }
                $question_choice->type=0;
                $question_choice->save();
            }
        } else {

            //check Similarity Here
            $similar_score = $this->db->checkQuestionSimilarity($contents, Session::get('courseID'),$request->question_id, $request->topic, [],$request->answer);

            if ($similar_score>85) {
                Log::info('[Question.Update] [ '.Session::get('courseID').' ] ['.Auth::id().'] Update question. User failed due to the similarity score greater than 85. Similarity Score: [ '.$similar_score.' ]');
                return redirect()->back()->withInput()->with('similar',$similar_score);
            }

            DB::table('question_choices')->where(['question_id'=>$request->question_id])->delete();
            $question_choice = new QuestionChoice();
            $question_choice->question_id = $request->question_id;
            $question_choice->choice = '';
            $question_choice->answer =$request->answer;
            $question_choice->type=1;
            $question_choice->save();
        }

        if(!empty($request->tags)) {
            DB::table('keywords')->where(['question_id'=>$request->question_id])->delete();

            $tags = $request->tags;
            $keywords = explode(',',$tags);

            foreach ($keywords as $keyword) {
                $keyword_data = array('question_id'=>$request->question_id, 'keyword' =>$this->xss_clean($keyword));
                DB::table('keywords')->insert($keyword_data);
                Log::info('[Question.Update] [ '.Session::get('courseID').' ] ['.Auth::id().'] Inserted new keyword: [ '.$keyword.' ]');

                //store into json files
                $this->json->addLog($keyword_data, 'keyword');
            }

        } else {
            Log::warning('[Question.Insert] [ '.Session::get('courseID').' ] ['.Auth::id().'] Insert question. User failed to insert question due to empty keyword tags');
        }

        Log::info('[Question.Update] [ '.Session::get('courseID').' ] ['.Auth::id().'] Update question: id -> [ '.$request->question_id.' ], topic->[ '.$request->topic.' ], contents->[ '.$contents.' ],  explaination->[ '.$request->explaination.' ], tips->[ '.$request->tips.' ],
         choice type->[ '.$request->choice_type.' ],  number of attempts-> [ '.$request->number_attempts.' ], keywords [ '.$request->tags.' ]');

        return redirect()->route('question.detail', ['id' => $request->question_id])->with('success', 'Successfully Updated!');
    }

    public function showquestion_for_voting ()
    {
        if ( !Session::get('courseID') ) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        $questions = DB::table('questions')->where('rating','>=', 3)->where('author_id','>',8)->limit(10)->where('course_id',Session::get('courseID') )->get();
        $question_filter_content = [];

        foreach ($questions as $question) {
            $contents = $question->contents;

            if ($question->mode == 1) {

                libxml_use_internal_errors(true);
                $doc = new \DOMDocument();
                $doc->loadHTML($contents);
                $this->removeElementsByTagName('script', $doc);
                $this->removeElementsByTagName('style', $doc);
                $this->removeElementsByTagName('form', $doc);

                foreach ($doc->getElementsByTagname('*') as $element) {
                    foreach (iterator_to_array($element->attributes) as $name => $attribute) {
                        if (substr_compare($name, 'on', 0, 2, TRUE) === 0) {
                            $element->removeAttribute($name);
                        }
                    }
                }

                libxml_clear_errors();
                $contents = $doc->saveHTML();

                $question_filter_content[$question->id] = $contents;
            } else {
                $question_filter_content[$question->id] = $question->contents;
            }
        }

        return view('questions.voting.questions',['questions'=>$questions,'question_filter_content'=>$question_filter_content]);

    }

    public function insertvoting(Request $request)
    {
        $this->validate($request, array(
            'vote' => 'required',
            'question_id'=>'required'
        ));

        if (!Session::get('courseID')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        if($request->vote != 1 || empty($request->question_id)) {
            return redirect()->back()->with('fail', 'Failed to vote since the system received unexpected value from the request');
        }

        //check if already voted
        $tmp = DB::table('user_votes')->where('user_id', Auth::id())->first();
        if(!empty($tmp)){
            return redirect()->back()->with('fail', 'You have already voted! Please wait the vote result. Thank you!');
        }

        $user_vote = new UserVote();
        $user_vote->user_id = Auth::id();
        $user_vote->question_id = $request->question_id;
        $user_vote->save();

        return redirect()->back()->with('success', 'Successfully voted! Thank you!');

    }

}

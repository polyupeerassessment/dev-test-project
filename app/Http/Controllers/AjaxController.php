<?php

namespace App\Http\Controllers;

use App\QuizAttempts;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Comment;
//use App\Question;
use \Datetime;
use \DateInterval;
use Mail;
use App\Mail\Announce;
use Session;
use App\User;
use App\Notifications\notifyStudent;
use Carbon\Carbon;
use App\UserAnswerSpent;

class AjaxController extends Controller
{
    private $json;
    private $db;
    public function __construct()
    {
        $this->json = new StorageController();
        $this->db = new DatabaseCapture();
    }

    public function index(){
        $msg = "This is a simple message.";
        $response = array(
            'status' => 'success',
            'msg'    => 'Setting created successfully',
        );

        return \Response::json($response);
    }

    public function getOverallStudent(Request $request) {
        $id = (int)$request->id;
        $questions = $this->db->getQuestionsTeacherId($id);
        $answers = $this->db->getAnswersTeacherId($id);
        $chats = $this->db->getChatsByTeacherId($id);

        $week = $this->getOneWeek();
//        print_r($week);

        $data = [];
        foreach ($week as $date) {
            $data[$date] = 0;
        }

        //Actions on Questions
        foreach($questions as $question) {
            $d = new DateTime($question->updated);
            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data[$date] += 1;
                }
            }
        }
//        print_r($data);

        //Actions on answers
        foreach($answers as $answer) {
            $d = new DateTime($answer->updated);
            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data[$date] += 1;
                }
            }
        }

        //Actions on Chats
        foreach($chats as $comment) {
            $d = new DateTime($comment->updated);
            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data[$date] += 1;
                }
            }
        }

        //$chart_plot_02_data.push([new Date(Date.today().add(i).days()).getTime(), 10]);

        $result = [];
        $i = 0;
        foreach($data as $k => $v) {
            $result[$i++] = [strtotime($k)*1000, $v];
        }

        return $result;
    }

    private function getOneWeek($offset = 0, $today = null)
    {
        if (is_null($today)) {
            $today = (new DateTime());
        }

        if ($offset < 0 ) {
            $offset_argument = 'P'.(abs($offset)*7).'D';
            $today->sub(new DateInterval($offset_argument));
        } elseif ($offset > 0) {
            $offset_argument = '+'.$offset.' week';
            $today->modify($offset_argument);
        }

        $week[0]=$today->format('Y-m-d');
        for($i=6;$i>0;$i--) {
            $week[7-$i] = ($today->modify('-1 day'))->format('Y-m-d');
        }

        return array_reverse($week);
    }

    public function removemycomment_question(Request $request)
    {
        DB::table('comments')->where([['author_id',Auth::id()],['id', $request->comment_id ]])->delete();

        $comments = $this->db->getNameWithComment($request->questionID);

        $newStr = '';

        foreach($comments as $post) {
            $date = date_create($post->created_at);
            $date = date_format($date,"Y/m/d H:i");

            if($post->author_id != Auth::id())
            {
                $newStr .= '
                <div class="row"> 
                    <h4 class="heading">'.$post->nickname.' : <small>'.$date.'</small></h4> 
                </div>
                <div class="row">
                    <blockquote class="message" style="border-color:#3597db;">'.$post->comment.'</blockquote>
                </div>';
            }

            if(!empty($post->comment) && $post->author_id == Auth::id()) {

                $newStr .= '
                <div class="row">
                    <h4 class="heading pull-right">You : <small>'.$date.'</small></h4>
                </div>											
                <div class="row">
                    <blockquote class="message blockquote-reverse pull-right" style="border-color:#28b999;">
                        <a href="#" class="removemycomment" data-filter="'.$post->id.'"><i style="color:palevioletred; font-size:90%;" class="fa fa-close fa-1x"></i></a>
                        '.$post->comment.'
                    </blockquote>
                </div>
                ';
            }
        }

        $response = array(
            'status' => 'success',
            'comments'    => $newStr,
        );

        //store into json file
        $tmpdate = array('author_id'=>Auth::id(), 'text'=>$request->message, 'time'=>date("Y-m-d H:i:s"), 'location'=>'question', 'question_id'=>$request->questionID);
        $this->json->storeJsonDiscussion($tmpdate, 'question_discussion');

        return \Response::json($response);
    }

    public function insertcomment(Request $request) 
    {
        $comment = new Comment();
        $comment->comment = $request->message;
        $comment->author_id = Auth::id();
        $comment->question_id = $request->questionID;
        $comment->save();

        //notify author here
        $user = DB::table('users')->where('id', Auth::id())->first();
        $question = DB::table('questions')->where('id', $request->questionID)->first();
        $author = User::where('notify_answered', '=', 1)->where('id', '=', $question->author_id)->first();

        if(!empty($author) && !empty($user)) {
            $content['content'] =$user->nickname." Commented your question!";
            $content['topic'] = "Commented Your Question!";
            $content['course_id'] = $question->course_id;
            $content['question_id'] = $question->id;
            $content['comment'] = $request->message;

            $author->notify(new notifyStudent($content, 0));
        }

        $comments = $this->db->getNameWithComment($request->questionID);

        $str = '';
        $newStr = '';

        foreach($comments as $post) {
            $date = date_create($post->created_at);
            $date = date_format($date,"Y/m/d H:i");

            if($post->author_id != Auth::id())
            {
                $newStr .= '
                <div class="row"> 
                    <h4 class="heading">'.$post->nickname.' : <small>'.$date.'</small></h4> 
                </div>
                <div class="row">
                    <blockquote class="message" style="border-color:#3597db;">'.htmlentities($post->comment).'</blockquote>
                </div>';
            }

            if(!empty($post->comment) && $post->author_id == Auth::id()) {
                
                $newStr .= '
                <div class="row">
                    <h4 class="heading pull-right">You : <small>'.$date.'</small></h4>
                </div>											
                <div class="row">
                    <blockquote class="message blockquote-reverse pull-right" style="border-color:#28b999;">
                    <a href="#" class="removemycomment" data-filter="'.$post->id.'"><i style="color:palevioletred; font-size:90%;" class="fa fa-close fa-1x"></i></a>
                    '.htmlentities($post->comment).'</blockquote>
                </div>
                ';
            }
        }

        $response = array(
            'status' => 'success',
            'comments'    => $newStr,
        );

        //store into json file
        $tmpdate = array('author_id'=>Auth::id(), 'text'=>$request->message, 'time'=>date("Y-m-d H:i:s"), 'location'=>'question', 'question_id'=>$request->questionID);
        $this->json->storeJsonDiscussion($tmpdate, 'question_discussion');

        return \Response::json($response);
    }

    public function insertRating(Request $request) {
        DB::table('user_answereds')
            ->where([['question_id',$request->questionID], ['user_id', Auth::id()]])
            ->update(['rating' => $request->star]);

        $question_rating = DB::select('select AVG(rating) as rating from user_answereds where question_id= ?',[$request->questionID]);

        DB::table('questions')
            ->where([['course_id', $request->course_id],['id',$request->questionID]])
            ->update(['rating' => $question_rating[0]->rating]);

        $log = array('question_id'=>$request->questionID, 'course_id'=>$request->course_id, 'star'=>$request->star, 'user_id'=>$request->user_id, 'time'=>date("Y-m-d H:i:s"));
        $this->json->addLog($log, 'rating');

        //send notify to question's author
        $questions = DB::select('select * from questions where id =?', [$request->questionID]);

        if(!empty($questions[0]) ) {
            $user = User::where('notify_rated', '=', 1)->where('id', '=', $questions[0]->author_id)->first();

            if(!empty($user)) {
                $content['content'] ="Somebody rated your questions! Come back and see details.";
                $content['topic'] = "Your Peer Rated Yours!";
                $content['course_id'] = $request->course_id;

                $user->notify(new notifyStudent($content, 0));
            }
        }

        //send author email
//        $email_addr = $this->db->getStudentEmailByQuestionID($request->questionID, $request->course_id);

//        if(!empty($email_addr)) {
//            $addr = $email_addr[0]->email;
//
//            Mail::to($addr)->send(new Announce('You Have Received Rating!', 'Well Done! Your Peers Answered and Rated Your Questions!
//            Come back and see how many stars you got!'));
//
//            return $addr;
//        }

        return "OK";
    }

    public function insertTimeSpent(Request $request) {
        $log = array('question_id'=>$request->questionID, 'course_id'=>$request->course_id, 'timeSpent'=>floor($request->timeSpent/1000), 'user_id'=>$request->user_id, 'time'=>date("Y-m-d H:i:s"));

        $this->json->addLog($log, 'answer_spent');

        //store to DB
//        return $request->questionID;
//        $user_spent = new UserAnswerSpent();
//
//        $user_spent->user_id = $request->user_id;
//        $user_spent->question_id = $request->questionID;
//        $user_spent->time = 1;
//        $user_spent->save();

        return 'ok';

    }

    public function getOffsetTimeline($offset) {
        if($offset == 0) {
            Session::put('timeline_offset', 0);

            return 0;

        } else {

            if(Session::has('timeline_type') && Session::has('timeline_offset')) {
                if ($offset == 1000) {
                    $offset = Session::get('timeline_offset');
                } else {
                    $offset = Session::get('timeline_offset')+$offset;
                    Session::put('timeline_offset', $offset);
                }
            }

            return $offset;
        }
    }

    public function getTimelineOverall(Request $request) {
        Session::put('timeline_type', 0);

        $offset = $this->getOffsetTimeline($request->offset);

        $week = $this->getOneWeek($offset);

        //initialize values
        $data_answer = [];
        $data_answered = [];
        $data_create = [];
        foreach ($week as $date) {
            $data_create[$date] = 0;
        }
        foreach ($week as $date) {
            $data_answer[$date] = 0;
        }
        foreach ($week as $date) {
            $data_answered[$date] = 0;
        }

        //get values from DB
        $questions = $this->db->getStudents_NumQuestions_ByCourseId_ByStudentID($request->courseID, $request->studentID);
        $answers = $this->db->getStudents_NumAnswers_ByCourseId_ByStudentID($request->courseID, $request->studentID);
        $answered = $this->db->getStudents_NumAnswered_ByCourseId_ByStudentID($request->courseID, $request->studentID);



        //Actions on Questions
        foreach($questions as $question) {

            $d = new DateTime($question->created_at);

            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data_create[$date] += 1;
                }
            }
        }

        //Actions on Answers
        foreach($answers as $answer) {
            $d = new DateTime($answer->created_at);

            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data_answer[$date] += $answer->num;
                }
            }
        }

        //Actions on Answered by others
        foreach($answered as $answer) {
            $d = new DateTime($answer->created_at);

            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data_answered[$date] += $answer->num;
                }
            }
        }

        $response = array(
            'week' =>$week,
            'data_answer' =>$data_answer,
            'data_answered' =>$data_answered,
            'data_create' =>$data_create,
            'error' => $answered,
        );

        return \Response::json($response);
    }

    public function getTimelineAnswers(Request $request) {
        Session::put('timeline_type', 1);
        $offset = $this->getOffsetTimeline($request->offset);

        $week = $this->getOneWeek($offset);

        $data = [];

        foreach ($week as $date) {
            $data[$date] = 0;
        }

        $answers = $this->db->getStudents_NumAnswers_ByCourseId_ByStudentID($request->courseID, $request->studentID);

        //Actions on Questions
        foreach($answers as $answer) {
            $d = new DateTime($answer->created_at);

            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data[$date] += $answer->num;
                }
            }
        }

        $response = array(
            'week' =>$week,
            'data' =>$data
        );
        return \Response::json($response);
    }

    public function getTimelinePeersAnswered(Request $request) {
        Session::put('timeline_type', 3);
        $offset = $this->getOffsetTimeline($request->offset);

        $week = $this->getOneWeek($offset);

        $data = [];

        foreach ($week as $date) {
            $data[$date] = 0;
        }

        $answered = $this->db->getStudents_NumAnswered_ByCourseId_ByStudentID($request->courseID, $request->studentID);

        foreach($answered as $answer) {
            $d = new DateTime($answer->created_at);

            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data[$date] += $answer->num;
                }
            }
        }

        $response = array(
            'week' =>$week,
            'data' =>$data
        );
        return \Response::json($response);
    }

    public function getTimelineQuestions(Request $request) {
        $offset = $this->getOffsetTimeline($request->offset);

        $week = $this->getOneWeek($offset);
        Session::put('timeline_type', 2);

        $data = [];

        foreach ($week as $date) {
            $data[$date] = 0;
        }

        $questions = $this->db->getStudents_NumQuestions_ByCourseId_ByStudentID($request->courseID, $request->studentID);

        //Actions on Questions
        foreach($questions as $question) {

            $d = new DateTime($question->created_at);

            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data[$date] += 1;
                }
            }
        }

        $response = array(
            'week' =>$week,
            'data' =>$data
        );
        return \Response::json($response);
    }

    public function setDefaultCourseByTeacher(Request $request) {
        DB::table('courses')
            ->where([['instructor_id',$request->instructor_id]])
            ->update(['default_session' => 0]);

        DB::table('courses')
            ->where([['course_code', $request->course_code],['instructor_id',$request->instructor_id]])
            ->update(['default_session' => 1]);

        redirect()->route('teacher.showcourse')->with('success','Successfully Set Default Course');
        return 'ok';
    }

    public function user_setting (Request $request) {
        DB::table('users')
            ->where('id', Auth::id())
            ->update(['notify_answered' => $request->notify_rated], ['notify_viewed' => $request->notify_viewed]
                , ['notify_rated' => $request->notify_rated], ['announce_email' => $request->email_announce]
                ,['email_report' => $request->email_report]);

        return 'ok';
    }

    public function getUserPerformance_Num_Questions(Request $request) {
        if(!empty($request->id)) {
            $rank = 1;
            $result = DB::select('select author_id, count(id) as num from questions q group by author_id order by num DESC');
            foreach($result as $r) {
                if($r->author_id == $request->id) {
                    return $rank;
                }
                $rank++;
            }
        }
    }

    public function getQuizClockTimer(Request $request) {
        //get timer
        $result_query = DB::select("select * from quiz_attempts where user_id = ?", [$request->user_id]);
        $minutes_to_add = 15;

        if(empty($result_query) || empty($result_query[0])) {

            $time_for_quiz = new DateTime();
            $time_for_quiz->add(new DateInterval('PT' . $minutes_to_add . 'M'));

        } else {

            $time_for_quiz = new Datetime($result_query[0]->deadline);
        }

        $start = new DateTime($request->start_time);

        $diff = $time_for_quiz->diff($start);
        return $diff->format("%H:%i:%s") ;
    }

    public function famouse_keywords(Request $request) {
        $keywords = DB::select('select keyword, count(keyword) as rank from keywords, questions as q where q.id = keywords.question_id and q.course_id = ? group by keyword order by rank DESC limit 3',[$request->course_id]);
        $html = '';
        foreach ($keywords as $k) {
            $html .= '<span class="label label-info">'.htmlentities($k->keyword).'</span>&nbsp;';
        }

        if (empty($html)) {
            return '';
        }
        return 'Example: '.$html;

    }

    public function removeElementsByTagName($tagName, $document) {
        $nodeList = $document->getElementsByTagName($tagName);
        for ($nodeIdx = $nodeList->length; --$nodeIdx >= 0; ) {
            $node = $nodeList->item($nodeIdx);
            $node->parentNode->removeChild($node);
        }
    }

    public function validateQuestionContents(Request $request) {

        try {
            if (!empty($request->contents)) {
                // create a new DomDocument object
                libxml_use_internal_errors(true);
                $doc = new \DOMDocument();

                // load the HTML into the DomDocument object (this would be your source HTML)
                $doc->loadHTML($request->contents);

                $this->removeElementsByTagName('script', $doc);
                $this->removeElementsByTagName('style', $doc);
//                $this->removeElementsByTagName('link', $doc);
                //            $dom = new DOMDocument;
                //            $doc->loadXML($request->contents);
//                $doc->removeChild($doc->getElementsByTagName('script')->item(0));

                // output cleaned html
//                $output = $doc->saveHTML();

//                $script = $dom->getElementsByTagName('script');
//
//                $remove = [];
//                foreach($script as $item)
//                {
//                    $remove[] = $item;
//                }
//
//                foreach ($remove as $item)
//                {
//                    $item->parentNode->removeChild($item);
//                }

                $this->removeElementsByTagName('form', $doc);

                foreach ($doc->getElementsByTagname('*') as $element)
                {
                    foreach (iterator_to_array($element->attributes) as $name => $attribute)
                    {
                        if (substr_compare($name, 'on', 0, 2, TRUE) === 0)
                        {
                            $element->removeAttribute($name);
                        }
                    }
                }

                $output = $doc->saveHTML();

                libxml_clear_errors();
                //            $output = preg_replace('/(<[^>]+) style=".*?"/i', ' ', $request->contents);
                return html_entity_decode($output);
            } else {
                return '';
            }
        } catch (\Exception $e) {
            return $e;
        }

    }

    public function getCommentsReceived (Request $request)
    {
        if (empty($request->courseID)) {
            return -1;
        } elseif (empty($request->studentID)) {
            return -1;
        }
        $html = '';
        $comments = DB::select('select c.created_at, c.comment, u.nickname, c.question_id from comments c, questions q, users u 
        where c.question_id = q.id and q.author_id = ? and q.course_id = ? and u.id = c.author_id order by c.created_at DESC;',[$request->studentID, $request->courseID]);

        foreach($comments as $index=>$comment) {
            if ($index >=4) {
                $html .= '
                <br><br>
                <div class="text-center">
                    <a href="'.route('student.announcement').'">
                        <strong>See More</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
                ';
                break;
            }
            $str = $comment->comment;
            if (strlen($str) >= 30) {
                $str = substr($str, 0, 10). " ... " . substr($str, -5);
            }

            $html .= '
                    <a href="'.route("question.detail", ["id" => $comment->question_id]).'" style="margin:2%;">
                        <div class="mail_list">
                            <div class="left">
                                <i class="fa fa-eye"></i>
                            </div>
                            <div class="right">
                                <h3>'.$comment->nickname.'<small>'.$comment->created_at.'</small></h3>
                                <p>'.htmlentities($str).'</p>
                            </div>
                        </div>
                    </a>
            ';
        }

        $response = array(
            'status' => 'success',
            'comments'    => $html,
        );

        return \Response::json($response);

    }

    public function keywords_frequency(Request $request)
    {
        if (empty($request->courseID) || empty($request->studentID)) {
            return -1;
        }

        $mining = new MiningController();
        $arr = $mining->keywords_detection($request->courseID);

        $total_num = 0;

        foreach($arr as $v) {
            $total_num += $v;
        }


        $response = array(
            'total_num' => $total_num,
            'contents'    => $arr,
        );

        return \Response::json($response);
    }
}

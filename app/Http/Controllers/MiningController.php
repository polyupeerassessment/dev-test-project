<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Phpml\Clustering\KMeans;
use Phpml\Classification\KNearestNeighbors;
use Phpml\Regression\LeastSquares;
use Phpml\Regression\SVR;
use Phpml\SupportVectorMachine\Kernel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\UserAttempt;
use Illuminate\Support\Facades\Storage;
use App\Question;
use App\UserAnswered;

class MiningController extends Controller
{
    public function printKMeansGroup() {
        echo "<h1>Clustering Result</h1>";
//        $group = $this->KMeansGroup();
        $group = $this->knnGroup();
        print_r($group);

    }
    public function KMeansGroup() {

        if ($this->kmeans()==true) {

            $path_kmeans = storage_path() . "/app/json/kmeans.json"; // ie: /var/www/laravel/app/storage/json/filename.json
            $path_kmeans_data = storage_path() . "/app/json/kmeans_data.json"; // ie: /var/www/laravel/app/storage/json/filename.json

            $json_kmeans_result = json_decode(file_get_contents($path_kmeans),true);
            $json_kmeans_data = json_decode(file_get_contents($path_kmeans_data), true);

            //Create Group Name
            $tmpgroup = [];
            $i = 0;

            foreach($json_kmeans_result as $kmeans_group) {
                $tmpSum = 0;
                foreach ($kmeans_group as $j=> $student) {
                    $tmpSum += array_sum($student);
                }
                $tmpgroup[$i] = $tmpSum / ($j+1);
                $i++;
            }

            //sorting without changing key
            asort($tmpgroup);
//            print_r($tmpgroup);echo "<br>";

            $i = 1;

            $result = [];
            foreach($tmpgroup as $group_i => $group) {
                $closed = [];
                foreach ($json_kmeans_result[$group_i] as $student) {
                    //find student id from kmeans_data.json

                    foreach ($json_kmeans_data as $id=>$data) {
                        if (in_array($id, $closed)) {
                            continue;
                        }

                        if ($data['score'] == $student[0]
                            && $data['prob_complete'] == $student[1]
                            && $data['attendance'] == $student[2]
                            && $data['prob_correct_answer'] == $student[3]
                            && $data['num_questions_created'] == $student[4])
                        {
                            $closed[] =$id;

                            switch($i) {
                                case 1: $result['Bad'][] = $id;break;
                                case 2: $result['Normal'][] = $id;break;
                                case 3: $result['Good'][] = $id;break;
                                default:break;
                            }

                        }

                    }

                }
                $i++;
            }
            return $result;
        } else {
            return [];
        }
    }

    public function kmeans() {

        $data = $this->getData();

        $kmeans = new KMeans((int)3);
        $tmp_kmeans = $kmeans->cluster($data);

        $myJSON = json_encode($tmp_kmeans,JSON_PRETTY_PRINT);
        Storage::put('json/kmeans.json', $myJSON);

        return true;

    }

    public function getData() {

        $scores = $this->getStudentScore();
        $attendences = $this->getStudentAttendance();
        $students = $this->getStudents();
        $prob = $this->getStudentGoodScorePossibility();
        $prob_complete = $this->getStudentsCompletenessProbability();
        $num_questions_created = $this->getStudentCreatedQuestions();

        //print_r($num_questions_created);
        $data = [];
        $data2store = [];
        $data_i = 0;

        foreach($students as $student) {
            $user_id = $student->id;
            $tmpdata = [0,0,0, 0,0];
            $tmpdata2store = [
                'score'=>0,
                'prob_complete'=>0,
                'attendance'=>0,
                'prob_correct_answer'=>0,
                'num_questions_created' =>0

            ];
            foreach($scores as $score) {
                if ($score->user_id == $user_id) {
                    $tmpdata[0] = $score->score;
                    $tmpdata2store['score'] = $score->score;

                }
            }

            foreach($prob_complete as $p) {
                if ($p->user_id == $user_id) {
                    $tmpdata[1] = $p->prob;
                    $tmpdata2store['prob_complete'] = $p->prob;
                }
            }

            foreach($attendences as $attend) {
                if ($attend->user_id == $user_id) {
                    $tmpdata[2] = $attend->num_attempts;
                    $tmpdata2store['attendance'] = $attend->num_attempts;
                }
            }

            foreach($prob as $p) {
                if ($p[0] == $user_id) {
                    $tmpdata[3] = $p[1];
                    $tmpdata2store['prob_correct_answer'] = $p[1];
                }
            }

            foreach ($num_questions_created as $num_questions) {
                if ($num_questions->user_id == $user_id) {
                    $tmpdata[4] = $num_questions->num_questions_created;
                    $tmpdata2store['num_questions_created'] = $num_questions->num_questions_created;
                }
            }
            $data[$data_i] = $tmpdata;
            $data2store[$user_id] = $tmpdata2store;
            $data_i++;

        }

        $myJSON = json_encode($data2store,JSON_PRETTY_PRINT);
        Storage::put('json/kmeans_data.json', $myJSON);

        return $data;
    }

    private function getStudentScore() {
        $sql_statement = 'select user_id, count(user_id) as score from
        (select distinct user_id, question_id from user_answereds as UA where result = 1) as T, users where
        users.id = T.user_id group by user_id order by Score DESC ';

        $result= DB::select($sql_statement);

        return $result;
    }

    private function getStudentAttendance() {
        $sql_statement = 'select user_id, count(user_id) as num_attempts from userattempts group by user_id;';

        $result= DB::select($sql_statement);

        return $result;
    }

    private function getStudentCreatedQuestions() {
        $sql_statement = 'select author_id as user_id, count(id) as num_questions_created from questions group by user_id';

        $result= DB::select($sql_statement);

        return $result;
    }

    private function getStudentGoodScorePossibility() {
        $result = [];
        $num_answered_correct = DB::select('select user_id, count(user_id) as num_correct from 
                                (select distinct user_id, question_id from user_answereds where result = 1) as T group by user_id');

        foreach($num_answered_correct as $user) {
            $num_answered = DB::select('select count(result) as num_answered from user_answereds where user_id=?;', [$user->user_id]);

            if(!empty($num_answered)) {
                $result[] = array($user->user_id,($user->num_correct  / $num_answered[0]->num_answered));
            }
        }

        return $result;
    }

    private function getStudents() {
        $sql_statement = 'select id from users;';
        $result= DB::select($sql_statement);

        return $result;
    }

    private function getStudentsCompletenessProbability() {
        return DB::select('select T.user_id, count(T.user_id) as prob from (select distinct user_id, question_id from user_answereds) as T, questions where questions.id = T.question_id group by user_id;');
    }

    public function updateCsv() {
//        $results =UserAnswered::get(['user_id','question_id','result']);
        $results = DB::select('select u.id, question_id, result from users as u left join user_answereds as ua on ua.user_id = u.id;');

        $filename = storage_path() . '/app/csv/irt_input.csv';

        $fp = fopen($filename, 'w');
        fputcsv($fp, ['user_id', 'question_id', 'result']);

        foreach ($results as $result) {
            $result_value = (is_null($result->result)?-1:$result->result);
            $question_value = (is_null($result->question_id)?-1:$result->question_id);
            fputcsv($fp, [$result->id,$question_value ,$result->result ]);
        }

        fclose($fp);

        //update question_list
//        $results =Question::get(['id']);
//        $filename = storage_path() . '/app/csv/question_list.csv';
//
//        $fp = fopen($filename, 'w');
//        fputcsv($fp, ['question_id']);
//
//        foreach ($results as $result) {
//            if(is_null($result->id)) {
//                continue;
//            }
//            fputcsv($fp, [$result->id]);
//        }
//
//        fclose($fp);

//        exec("/usr/local/bin/python3 ../app/Http/irt_test.py 2>&1",$output, $return);
//        print_r($return); print_r($output);

    }

    public function readcsv_irt() {
        $filename = storage_path().'/app/csv/irt_output.csv';
        $all_questions = DB::select('select * from questions');
        $question_list_output= [];

        foreach($all_questions as $question) {
            $question_list_output[$question->id] = 99;
        }

        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            if (($question_list = fgetcsv($handle, 1000, ",")) !== FALSE) {

                //get index from header
                while (($row = fgetcsv($handle, 1000, ",")) !== FALSE)
                {
                    if($row[0]==Auth::id()) {
                        $remove_first_flag = true;

                        foreach($row as $index=>$p) {
                            if($remove_first_flag == true) {
                                $remove_first_flag = false;
                                continue;
                            }
//                            echo $question_list[$index].'<br>';
                            $question_list_output[$question_list[$index]]= $p;
                        }

                    }
                }
            }

        }



        return $question_list_output;
    }

    public function updateQuestionLevel() {
        $filename = storage_path().'/app/csv/question_difficulty.csv';
        $all_questions = DB::select('select * from questions');
        $question_list_output= [];

        foreach($all_questions as $question) {//make all the questions easy
            $question_list_output[$question->id] = 99;
        }

        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            if (($question_list = fgetcsv($handle, 1000, ",")) !== FALSE) {

                //get index from header
                while (($row = fgetcsv($handle, 1000, ",")) !== FALSE)
                {
                    if(is_array($row) && sizeof($row) == 3) {
                        $difficulty = 0;
                        if($row[2] >1) {
                            $difficulty = 2;
                        } elseif($row[2]<=1 && $row[2] >=-1) {
                            $difficulty =1;
                        }

                        DB::table('questions')
                            ->where([['id',$row[1]]])
                            ->update(['difficulty' => $difficulty]);
                    }

                }
            }

        }
    }

    public function updateCSVQuestionRating() {

        $questions = Question::get(['id','topic']);
        $ratings =UserAnswered::get(['user_id','question_id','rating']);

        $filename = storage_path() . '/app/csv/question.csv';

        $fp = fopen($filename, 'w');
        fputcsv($fp, ['id', 'topic']);

        foreach ($questions as $question) {
            fputcsv($fp, [$question->id, $question->topic]);
        }

        $filename = storage_path() . '/app/csv/rating.csv';

        $fp = fopen($filename, 'w');
        fputcsv($fp, ['user_id', 'question_id', 'rating']);

        foreach ($ratings as $rating) {
            if(is_null($rating->rating)) {
                continue;
            }
            fputcsv($fp, [$rating->user_id, $rating->question_id, $rating->rating]);
        }

        fclose($fp);

        echo "$filename";
    }

    public function update_student_csv_data($course_code)
    {
        $course = DB::table('courses')->where('course_code','=',$course_code)->get();

        if (empty($course)) {
            return -1;
        }

        $students = DB::select('select * from users as u where exists (select * from enrollments as e where e.student_id = u.id and e.course_id = ? )', [$course_code]);
        $answers = DB::select('select * from user_answereds as ua where exists (select * from questions q where q.id = ua.question_id and q.course_id = ?)', [$course_code]);
        $num_answers = DB::select('select user_id, sum(num_attempts) as sum from user_answereds ua where exists (select * from questions q where q.id = ua.question_id and q.course_id = ?) group by user_id;',[$course_code]);
        $questions = DB::select('select * from questions where course_id = ?', [$course_code]);
        $answered_by_peers = DB::select('select Q.author_id, sum(UA.num_attempts) as num from questions as Q join user_answereds as UA on UA.question_id = Q.id AND Q.isMadeByTeacher = 0 and Q.course_id = ? group by Q.author_id;',[$course_code]);
        $avg_rating = DB::select('select author_id, avg(rating) as rating from questions where course_id = ? group by author_id',[$course_code]);
        $avg_difficulty = DB::select('select author_id, avg(difficulty) as num from questions where course_id = ? group by author_id',[$course_code]);
        $viewed = DB::select('select author_id, count(question_id) as num from page_views as pw left join questions as q on q.id = pw.question_id and q.course_id = ? group by author_id;',[$course_code]);
        $discussions = DB::table('discussions')->where('course_id' ,'=', $course_code)->get();
        $comments = DB::select('select c.author_id, count(comment) as num from comments as c, questions as q where c.question_id = q.id and q.course_id = ? group by author_id;',[$course_code]);

        //initialize $students_data
        $students_data = [];
        foreach( $students as $student ) {
            $students_data[$student->id] = [$student->id,$student->email,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
        }

        //num answers
        foreach($num_answers as $answer ) {
            if(empty($students_data[$answer->user_id])) {
                continue;
            }

            $students_data[$answer->user_id][2] += $answer->sum;
        }

        //num questions created & num of mc & num of tf & num of tips & num of explaination
        foreach ($questions as $question) {
            if(empty($students_data[$question->author_id])) {
                continue;
            }

            $students_data[$question->author_id][3] += 1;

            if ($question->type == 0) {
                $students_data[$question->author_id][8] += 1;
            } else {
                $students_data[$question->author_id][9] += 1;
            }

            if (!empty($question->tips)) {
                $students_data[$question->author_id][10] += 1;
            }

            if (!empty($question->explaination)) {
                $students_data[$question->author_id][11] += 1;
            }
        }

        //num questions answered by peers
        foreach($answered_by_peers as $answered ) {
            if(empty($students_data[$answered->author_id])) {
                continue;
            }

            $students_data[$answered->author_id][4] = $answered->num;
        }

        //num corrects answers
        foreach ($answers as $answer) {
            if(empty($students_data[$answer->user_id])) {
                continue;
            }
            if ($answer->result == 1) {
                $students_data[$answer->user_id][5] += 1;
            }
        }

        //average rating
        foreach ($avg_rating as $question) {
            if(empty($students_data[$question->author_id])) {
                continue;
            }

            $students_data[$question->author_id][6] = $question->rating;
        }

        //average question's difficulty
        foreach ($avg_difficulty as $question ) {
            if(empty($students_data[$question->author_id])) {
                continue;
            }

            $students_data[$question->author_id][7] = $question->num;
        }

        //page views
        foreach ($viewed as $v ) {
            if (empty($students_data[$v->author_id])) {
                continue;
            }

            $students_data[$v->author_id][12] = $v->num;
        }

        //num discussion topic && num_messages
        foreach ($discussions as $d) {
            if (empty($students_data[$d->author_id])) {
                continue;
            }

            $students_data[$d->author_id][13] += 1;


            $thread_messages = DB::table('thread_messages')->where('thread_id','=',$d->id)->get();

            foreach($thread_messages as $msg) {
                if (empty($students_data[$msg->author_id])) {
                    continue;
                }

                $students_data[$msg->author_id][15] += 1;
            }
        }

        //num comments
        foreach ($comments as $comment) {
            if (empty($students_data[$comment->author_id])) {
                continue;
            }

            $students_data[$comment->author_id][14] = $comment->num;
        }

        //output result
        $output_filename = storage_path() . '/app/csv/'.$course_code.'/student_data.csv';

        if (!is_dir(storage_path() . '/app/csv/'.$course_code)) {
            $old = umask(0);
            mkdir(storage_path() . '/app/csv/'.$course_code, 0777, true);
            umask($old);
        }

        $fp = fopen($output_filename, 'w');

        //header
        fputcsv($fp, ['id', 'email','num_answer', 'num_questions',
            'num_answered', 'score', 'avg_rating','avg_difficulty','num_mc',
            'num_tf', 'num_tips', 'num_explain', 'num_views','num_discussions', 'num_comments',
            'num_thread_messages']);

        //contents
        foreach ($students_data as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);

//        return storage_path() . '/app/csv/'.$course_code.'/student_data.csv';

        return $students_data;
    }

    public function backup2Csv() {
//        $test = DB::select("SELECT * INTO OUTFILE '/var/www/html/dev-test-project/storage/backup/csv/users.csv' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n' FROM users");
//        $table = 'users';
//        DB::select("SELECT * INTO OUTFILE '/var/www/html/dev-test-project/storage/backup/csv/".$table.".csv' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n' FROM ".$table);
//        DB::select("SELECT * INTO OUTFILE '/var/www/html/dev-test-project/storage/backup/csv/".$table.".csv FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n' FROM ".$table);
//        print_r($test);
        $path = "/var/www/html/dev-test-project/storage/backup/csv/";
        $tables = ['announcements','comments', 'course_schedules', 'courses', 'discussions', 'enrollments','keywords','notifications',
            'page_views','question_choices','questions','quiz','quiz_answers','quiz_attempts', 'quiz_questions','quiz_timers','teachers',
            'thread_messages','user_answereds','userattempts','users'];
        $today = date("Y-m-d");


        if (!is_dir($path.$today)) {
            mkdir($path.$today, 0777, true);
        }
    }

    public function updateQuestionsPopularityRate($course_code='COMP3421') {
        $num_views = DB::select('select questions.id, count(a.id) as num from questions left join page_views a ON questions.id = a.question_id and questions.course_id = ? group by questions.id',[$course_code]);
        $avg_rating = DB::table('questions')->where('course_id','=',$course_code)->get();
        $num_answers = DB::select('select question_id, sum(u.num_attempts) as num from user_answereds u, questions q where u.question_id = q.id and q.course_id = ? group by question_id; ',[$course_code]);

        $data = [];
        $target = [];
        foreach ($avg_rating as $r) {
            $data[$r->id][] = $r->rating;
            $target[$r->id] = 0;
        }

        foreach ($num_views as $v) {
            if (empty($data[$v->id])) {
                continue;
            }
            $data[$v->id][] = $v->num;
        }

        foreach ($num_answers as $a) {
            if (empty($data[$a->question_id]) || is_null($target[$a->question_id])) {
                continue;
            }
            $target[$a->question_id] = $a->num;
        }

        //match data and target array
        if (sizeof($data) != sizeof($target)) {
            return -1;
        }

        ksort($data);
        ksort($target);

        $regression = new LeastSquares();

        $regression->train($data, $target);

        //output result
        $output_filename = storage_path() . '/app/csv/'.$course_code.'/question_popularity_rate.csv';

        if (!is_dir(storage_path() . '/app/csv/'.$course_code)) {
            $old = umask(0);
            mkdir(storage_path() . '/app/csv/'.$course_code, 0777, true);
            umask($old);
        }

        $fp = fopen($output_filename, 'w');

        //contents
        $content2put = [];
        $content2put[] =$regression->getIntercept();

        foreach($regression->getCoefficients() as $c) {
            $content2put[] = $c;
        }

        fputcsv($fp, $content2put);

        fclose($fp);

//        print_r($data);
//        echo '<br>';echo '<br>';
//        print_r($target);
//
//        echo '<br>';echo '<br>';
//        print_r($regression->predict([4.5,2])); echo '<br>';
//        print_r($regression->getCoefficients());
//        echo '<br>';
//        print_r($regression->getIntercept());

    }

    public function getQuestionIdList_Popularity_Order($course_code) {
        $filename = storage_path() . '/app/csv/'.$course_code.'/question_popularity_rate.csv';

        $list = [];

        $questions = DB::table('questions')->where('course_id','=',$course_code)->get();
        $num_views = DB::select('select questions.id, count(a.id) as num from questions left join page_views a ON questions.id = a.question_id and questions.course_id = ? group by questions.id',[$course_code]);
        foreach($questions as $q) {
            $list[$q->id] = 0;
        }

        if (!is_dir(storage_path() . '/app/csv/'.$course_code)) {
            return $list;
        }

        $coefficients = [];
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            if (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {

                foreach($row as $v) {
                    $coefficients[] = $v;
                }
            }
        } else {

            return $list;
        }

        foreach($num_views as $view) {
            $list[$view->id] = $view->num;
        }

        foreach($questions as $q) {
//            echo $q->rating.'<br>';
//            echo $list[$q->id].'<br>';
//            echo $q->rating.'<br>';
            $list[$q->id] = $coefficients[0] + $coefficients[1] * $q->rating +  $coefficients[2] * $list[$q->id];
        }

        arsort($list);
//        print_r($list);
//        exit;
        $list = array_keys($list);
//        print_r($list);
        return $list;

    }

    public function testIRTController() {
        $irt = new IRTController('COMP2222',2);
        $irt->getLastUpdateIRT();
    }

    public function keywords_detection($course_id) {
        $questions = DB::table('questions')->where('course_id', $course_id)->get();
        $keywords = DB::select('select * from keywords k where exists(select * from questions q where q.id = k.question_id and q.course_id = ? )', [$course_id]);
        $text = '';

        $commonWords = array('a','able','about','above','abroad','according','accordingly','across','actually','adj','after','afterwards','again','against','ago','ahead','ain\'t','all','allow','allows','almost','alone','along','alongside','already','also','although','always','am','amid','amidst','among','amongst','an','and','another','any','anybody','anyhow','anyone','anything','anyway','anyways','anywhere','apart','appear','appreciate','appropriate','are','aren\'t','around','as','a\'s','aside','ask','asking','associated','at','available','away','awfully','b','back','backward','backwards','be','became','because','become','becomes','becoming','been','before','beforehand','begin','behind','being','believe','below','beside','besides','best','better','between','beyond','both','brief','but','by','c','came','can','cannot','cant','can\'t','caption','cause','causes','certain','certainly','changes','clearly','c\'mon','co','co.','com','come','comes','concerning','consequently','consider','considering','contain','containing','contains','corresponding','could','couldn\'t','course','c\'s','currently','d','dare','daren\'t','definitely','described','despite','did','didn\'t','different','directly','do','does','doesn\'t','doing','done','don\'t','down','downwards','during','e','each','edu','eg','eight','eighty','either','else','elsewhere','end','ending','enough','entirely','especially','et','etc','even','ever','evermore','every','everybody','everyone','everything','everywhere','ex','exactly','example','except','f','fairly','far','farther','few','fewer','fifth','first','five','followed','following','follows','for','forever','former','formerly','forth','forward','found','four','from','further','furthermore','g','get','gets','getting','given','gives','go','goes','going','gone','got','gotten','greetings','h','had','hadn\'t','half','happens','hardly','has','hasn\'t','have','haven\'t','having','he','he\'d','he\'ll','hello','help','hence','her','here','hereafter','hereby','herein','here\'s','hereupon','hers','herself','he\'s','hi','him','himself','his','hither','hopefully','how','howbeit','however','hundred','i','i\'d','ie','if','ignored','i\'ll','i\'m','immediate','in','inasmuch','inc','inc.','indeed','indicate','indicated','indicates','inner','inside','insofar','instead','into','inward','is','isn\'t','it','it\'d','it\'ll','its','it\'s','itself','i\'ve','j','just','k','keep','keeps','kept','know','known','knows','l','last','lately','later','latter','latterly','least','less','lest','let','let\'s','like','liked','likely','likewise','little','look','looking','looks','low','lower','ltd','m','made','mainly','make','makes','many','may','maybe','mayn\'t','me','mean','meantime','meanwhile','merely','might','mightn\'t','mine','minus','miss','more','moreover','most','mostly','mr','mrs','much','must','mustn\'t','my','myself','n','name','namely','nd','near','nearly','necessary','need','needn\'t','needs','neither','never','neverf','neverless','nevertheless','new','next','nine','ninety','no','nobody','non','none','nonetheless','noone','no-one','nor','normally','not','nothing','notwithstanding','novel','now','nowhere','o','obviously','of','off','often','oh','ok','okay','old','on','once','one','ones','one\'s','only','onto','opposite','or','other','others','otherwise','ought','oughtn\'t','our','ours','ourselves','out','outside','over','overall','own','p','particular','particularly','past','per','perhaps','placed','please','plus','possible','presumably','probably','provided','provides','q','que','quite','qv','r','rather','rd','re','really','reasonably','recent','recently','regarding','regardless','regards','relatively','respectively','right','round','s','said','same','saw','say','saying','says','second','secondly','see','seeing','seem','seemed','seeming','seems','seen','self','selves','sensible','sent','serious','seriously','seven','several','shall','shan\'t','she','she\'d','she\'ll','she\'s','should','shouldn\'t','since','six','so','some','somebody','someday','somehow','someone','something','sometime','sometimes','somewhat','somewhere','soon','sorry','specified','specify','specifying','still','sub','such','sup','sure','t','take','taken','taking','tell','tends','th','than','thank','thanks','thanx','that','that\'ll','thats','that\'s','that\'ve','the','their','theirs','them','themselves','then','thence','there','thereafter','thereby','there\'d','therefore','therein','there\'ll','there\'re','theres','there\'s','thereupon','there\'ve','these','they','they\'d','they\'ll','they\'re','they\'ve','thing','things','think','third','thirty','this','thorough','thoroughly','those','though','three','through','throughout','thru','thus','till','to','together','too','took','toward','towards','tried','tries','truly','try','trying','t\'s','twice','two','u','un','under','underneath','undoing','unfortunately','unless','unlike','unlikely','until','unto','up','upon','upwards','us','use','used','useful','uses','using','usually','v','value','various','versus','very','via','viz','vs','w','want','wants','was','wasn\'t','way','we','we\'d','welcome','well','we\'ll','went','were','we\'re','weren\'t','we\'ve','what','whatever','what\'ll','what\'s','what\'ve','when','whence','whenever','where','whereafter','whereas','whereby','wherein','where\'s','whereupon','wherever','whether','which','whichever','while','whilst','whither','who','who\'d','whoever','whole','who\'ll','whom','whomever','who\'s','whose','why','will','willing','wish','with','within','without','wonder','won\'t','would','wouldn\'t','x','y','yes','yet','you','you\'d','you\'ll','your','you\'re','yours','yourself','yourselves','you\'ve','z','zero');

        $test  = array_map('strtoupper', $commonWords);

        foreach($questions as $question) {
            $str = ' '.strtoupper($question->topic). ' '.strtoupper($question->contents);
            $text .= ' '.$str;
        }

        foreach($keywords as $k) {
            $text .= ' '.strtoupper($k->keyword);
        }

        //filter text
        $text = str_replace("'", "", $text);
        $text = str_replace("-", "", $text);
        $text = str_replace(")", "", $text);
        $text = str_replace("(", "", $text);
        $text = str_replace("=", "", $text);
        $text = str_replace(".", "", $text);
        $text = str_replace(",", "", $text);
        $text = str_replace(":", "", $text);
        $text = str_replace(";", "", $text);
        $text = str_replace("!", "", $text);
        $text = str_replace("?", "", $text);

        $text = preg_replace('/[0-9,\(\)\-\=\.\,\;\!\?]+/', '', $text);
        $text = preg_replace('~\b('.implode('|',$test).')\b~','',$text);

        $arr = array_count_values(str_word_count($text, 1));
        arsort($arr);
        if (sizeof($arr) > 100 ) {
            $arr = array_slice($arr, 0, 80);
        }

        foreach ($arr as $key=>&$value) {
            if (strlen($key) > 11) {
                unset($arr[$key]);
            }
        }

        return $arr;
    }

}
<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\AssignmentQuestion;
use App\AssignmentQuestionChoice;
use App\QuestionChoice;
use App\Quiz;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Course;
use App\QuizQuestion;
use \Datetime;
use \Exception;
use Session;
use App\Question;
use Mail;
use App\Mail\Announce;
use App\Announcement;
use App\Enrollment;
use App\User;
use App\CourseSchedule;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

use App\Notifications\notifyStudent;

class TeacherController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $db;
    private $json;

    public function __construct()
    {
        $this->middleware('auth:teacher');
        $this->db = new DatabaseCapture();
        $this->json = new StorageController();
    }

    public function createquestion(Request $request)
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or Course Session Out");
        }

        $courseInformation = $this->db->getCourseByTeacher(Session::get('courseID_byTeacher'));

        if(empty($courseInformation[0])) {
            Session::forget('courseID_byTeacher');
            return redirect()->route('teacher.showcourse')->with('fail','No Course Found');
        }

        return view('teacher.CreateQuestion');
    }

    public function createCourse()
    {
        return view('teacher.newCreateCourse');
    }

    public function showcourse()
    {
        $courses = $this->db->getCourseByTeacherId();
        return view('teacher.newShowCourse', ['courses'=>$courses]);
    }

    public function stuCourseStat($std_id)
    {

        $numComments= DB::table('comments')->where('author_id', $std_id)->count();
        $numViews = DB::table('page_views')->where('user_id',$std_id)->count();
        $numQuestions= DB::table('questions')->where('author_id', $std_id)->count();
        $numAnswereds = DB::table('user_answereds')->where('user_id',$std_id)->count();
        $avgRating = DB::table('user_answereds')->where('user_id',$std_id)->count('rating');
        $avgScore = DB::table('user_answereds')->where('user_id',$std_id)->where('result',1)->count('result');


        $std_views = DB::table('page_views')->select(DB::raw('count(*) as page_views'), DB::raw('DATE(created_at) as date'))
                        ->groupBy('date')->where('user_id',$std_id)->get();

        $qc_stat = DB::table('questions')->select(DB::raw('count(*) as q_created'), DB::raw('DATE(created_at) as date'))
            ->groupBy('date')->where('author_id', $std_id)->get();

        $qa_stat = DB::table('user_answereds')->select(DB::raw('count(*) as q_answered'), DB::raw('DATE(created_at) as date'))
            ->groupBy('date')->where('user_id', $std_id)->get();


//        $ratings_stat = DB::table('user_answereds')->select(DB::raw('count(rating) as ratings'), DB::raw('DATE(created_at) as date'))
//            ->groupBy('date')->where('user_id', $std_id)->get();

        $ratings_stat = DB::table('user_answereds')->select(DB::raw('Round(IFNULL(avg(rating), 0),2) as ratings'), DB::raw('DATE(created_at) as date'))
            ->groupBy('date')->where('user_id', $std_id)->get();

        $comments_stat = DB::table('comments')->select(DB::raw('count(*) as comments'), DB::raw('DATE(created_at) as date'))
            ->groupBy('date')->where('author_id', $std_id)->get();


        return view('teacher.stuCourseStat', ['std_veiws' => $std_views, 'qc_stat'=>$qc_stat, 'numViews'=>$numViews,
                    'numComments'=>$numComments, 'numQuestions'=>$numQuestions, 'numAnswereds'=>$numAnswereds, 'avgScore'=>$avgScore,
                    'avgRating'=>$avgRating, 'qa_stat'=>$qa_stat,'ratings_stat'=>$ratings_stat,'comments_stat'=>$comments_stat]);


    }

    public function statistics()
    {

        $top_users = [];
        $top_questions = [];

        $top_users_tmp = $this->calculate_ranking(false, Session::get('courseID_byTeacher'));
        $top_users_answers_tmp = $this->students_ranking_num_answers(Session::get('courseID_byTeacher'));
        $top_users_rating_tmp = $this->students_ranking_avg_rating(Session::get('courseID_byTeacher'));
        $top_users_views_tmp = $this->students_ranking_num_views(Session::get('courseID_byTeacher'));
        $top_users_questions_tmp = $this->students_ranking_num_questions(Session::get('courseID_byTeacher'));
        $top_users_avg_score = $this->students_ranking_avg_score(Session::get('courseID_byTeacher'));
        $top_users_question_difficulty = $this->students_ranking_question_difficulty(Session::get('courseID_byTeacher'));
        $top_users_ques_ans = $this->students_ranking_num_ques_ans(Session::get('courseID_byTeacher'));
        $top_keywords = $this->getTopKeywords(Session::get('courseID_byTeacher'));
        $top_questions_tmp = $this->topQuestions(Session::get('courseID_byTeacher'));
        $top_created_questions = $this->topStudentCreatedQuestions(Session::get('courseID_byTeacher'));


        //top users
        foreach ($top_users_tmp as $user) {
            $top_users[$user->id] = ['id' => $user->id, 'name' => $user->name, 'nickname' => $user->nickname, 'email' => $user->email, 'last_activity_date' => $user->last_activity_date, 'score' => $user->score, 'num_attempts' => 0
                , 'avg_rating' => 0, 'num_views' => 0, 'num_questions' => 0];
        }

        foreach ($top_users_answers_tmp as $answer) {
            if (empty($top_users[$answer->user_id])) {
                continue;
            }
            $top_users[$answer->user_id]['num_attempts'] = $answer->num_attempts;
        }

        foreach ($top_users_rating_tmp as $rating) {
            if (empty($top_users[$rating->author_id])) {
                continue;
            }
            $top_users[$rating->author_id]['avg_rating'] = $rating->rating;
        }

        foreach ($top_users_views_tmp as $view) {
            if (empty($top_users[$view->author_id])) {
                continue;
            }
            $top_users[$view->author_id]['num_views'] = $view->num;
        }

        foreach ($top_users_questions_tmp as $q) {
            if (empty($top_users[$q->author_id])) {
                continue;
            }
            $top_users[$q->author_id]['num_questions'] = $q->num;
        }

        foreach ($top_users_avg_score as $avg) {
            if (empty($top_users[$avg->user_id])) {
                continue;
            }
            $top_users[$avg->user_id]['avg_score'] = $avg->avg_score;
        }

        foreach ($top_users_question_difficulty as $diff) {
            if (empty($top_users[$diff->author_id])) {
                continue;
            }
            $top_users[$diff->author_id]['avg_diff'] = $diff->avg_diff;
        }

        foreach ($top_users_ques_ans as $ques) {
            if (empty($top_users[$ques->user_id])) {
                continue;
            }
            $top_users[$ques->user_id]['ques_ans'] = $ques->ques_ans;

        }

        // top questions
        foreach ($top_questions_tmp as $question)
        {
            $top_questions[$question->id] = ['id'=>$question->id, 'topic'=>$question->topic, 'difficulty'=>$question->difficulty,'rating'=>$question->rating,
                'created_at'=>$question->created_at, 'type'=>$question->type,'mode'=>$question->mode, 'num_answers'=>0, 'num_corrects'=> 0, 'avg_score'=>0, 'num_views'=>0,'avg_spent' =>0];
        }

        $num_answers = DB::select('
        select Q.id, sum(UA.num_attempts) as num from questions as Q
        join user_answereds as UA on UA.question_id = Q.id AND
        Q.isMadeByTeacher = 0 and Q.course_id = ? group by Q.id order by num desc;
        ', [Session::get('courseID_byTeacher')]);

        foreach ($num_answers as $each_answer) {
            if (empty($top_questions[$each_answer->id])) {
                continue;
            }
            $top_questions[$each_answer->id]['num_answers'] = $each_answer->num;
        }


        $num_corrects = DB::select('
        select Q.id, count(UA.result) as num from questions as Q
        join user_answereds as UA on UA.question_id = Q.id AND UA.result = 1 and
        Q.isMadeByTeacher = 0 and Q.course_id = ? group by Q.id;
        ', [Session::get('courseID_byTeacher')]);

        foreach ($num_corrects as $each_answer) {
            if (empty($top_questions[$each_answer->id])) {
                continue;
            }

            $top_questions["$each_answer->id"]['num_corrects'] = $each_answer->num;
        }

        $avg_score = DB::select('
        select Q.id, avg(UA.result) as num from questions as Q
        join user_answereds as UA on UA.question_id = Q.id AND
        Q.isMadeByTeacher = 0 and Q.course_id = ?  group by Q.id;
        ', [Session::get('courseID_byTeacher')]);

        foreach ($avg_score as $each_answer) {
            if (empty($top_questions[$each_answer->id])) {
                continue;
            }
            $top_questions["$each_answer->id"]['avg_score'] = $each_answer->num;
        }

        $num_views = DB::select('select Q.id, count(P.id) as num from questions as Q
                    join page_views as P on P.question_id = Q.id and Q.course_id = ?  group by Q.id;',[Session::get('courseID_byTeacher')]);

        foreach ($num_views as $each_answer) {
            if (empty($top_questions[$each_answer->id])) {
                continue;
            }
            $top_questions["$each_answer->id"]['num_views'] = $each_answer->num;
        }

        $avg_spent = $this->json->readLogAvgTimeSpent();

        foreach($avg_spent as $time) {
            if(!empty($questions_arr[$time['question_id']])) {
                $top_questions[$time['question_id']]['avg_spent'] = $time['timeSpent'];
            }
        }

        return view('teacher.statistics', ['students'=>$top_users]);

    }

    public function removecourse($id)
    {
        DB::table('courses')->where('course_code', '=', $id)->delete();
        return redirect()->route('teacher.showcourse')->with('success',"Successfully Removed Course");
    }

    public function announce()
    {
        $students = $this->db->getStudentsByTeacherId();
        $questions = $this->db->getQuestionsTeacherId();
        $answers = $this->db->getAnswersTeacherId();
        $chats = $this->db->getChatsByTeacherId();

        $week = $this->getOneWeek();
//        print_r($week);

        $data = [];
        foreach ($week as $date) {
            $data[$date] = 0;
        }


        //Actions on Questions
        foreach($questions as $question) {

            $d = new DateTime($question->updated);

            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data[$date] += 1;
                }
            }
        }

        //Actions on answers
        foreach($answers as $answer) {

            $d = new DateTime($answer->updated);

            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data[$date] += 1;
                }
            }
        }

        //Actions on Chats
        foreach($chats as $comment) {

            $d = new DateTime($comment->updated);

            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data[$date] += 1;
                }
            }
        }


        print_r($data);
        $result = [];
//        return view('teacher.announce');
    }

    private function getOneWeek($today = null)
    {
        if (is_null($today)) {
            $today = (new DateTime());
//            $today = new DateTime('2000-12-31');
        }

        $week[0]=$today->format('Y-m-d');
        for($i=6;$i>0;$i--) {
            $week[7-$i] = ($today->modify('-1 day'))->format('Y-m-d');
        }

        return array_reverse($week);

    }

    public function insertCourse(Request $request)
    {
        $this->validate($request, array(
            'code' => 'required',
            'name' => 'required',
            'enrollcode' => 'required',
            'description' => 'required',
            'department' => 'required',
            'week' => 'required',
            'from' => 'required',
            'to' => 'required'
        ));

        $time_from = $request->from;
        $time_to = $request->to;

        foreach($request->week as $week) {
            $course_schedule = new CourseSchedule();
            $course_schedule->week = $week;
            $week_num = 0;

            switch($week){
                case 'Monday': $week_num=0;break;
                case 'Tuesday': $week_num=1;break;
                case 'Wednesday': $week_num=2;break;
                case 'Thursday': $week_num=3;break;
                case 'Friday': $week_num=4;break;
                case 'Satusday': $week_num=5;break;
                default: $week_num=6;break;
            }

            if(!empty($time_from[$week_num])) {
                $date = new DateTime($time_from[$week_num]);
//                echo $date->format('Y-m-d H:i:s');
//                $timestamp = strtotime($time_from[$week_num]);
//                echo date("h.i A", $timestamp)."<br>";
//                $date=date('H:i', $time_from[$week_num]);
//                $date=date('Y-m-d H:i:s', $timestamp);
                $course_schedule->time_from = $date->format('Y-m-d H:i:s');

            } else {
                return redirect()->back()->withInput()->with('fail',"Failed to insert. You need to complete time of your course");
            }

            if(!empty($time_to[$week_num])) {
                $date = new DateTime($time_to[$week_num]);
                $course_schedule->time_to = $date->format('Y-m-d H:i:s');
//                echo strtotime($time_to[$week_num])."<br>";
//                $course_schedule->time_to = strtotime($time_to[$week_num]);
            } else {
                return redirect()->back()->withInput()->with('fail',"Failed to insert. You need to complete time of your course");
            }

            $course_schedule->course_id = $request->code;

        }

        try {
            $course = new Course();
            $course->course_code = $request->code;
            $course->course_name = $request->name;
            $course->department = $request->department;
            $course->description = $request->description;
            $course->enroll_code = $request->enrollcode;
            $course->instructor_id = Auth::id();

            $course->save();

        } catch (Exception $e) {
            return redirect()->back()->withInput()->with('fail',"Failed to update. You typed same information with others");
        }

        if (!empty($request->file('csv_file'))) {

            $successFlag = -1;

            $filePath = $request->file('csv_file')->getRealPath();

            $file = fopen($filePath,'r');
            $header= fgetcsv($file); // headers

            $db = new DatabaseCapture();

            $course_id = $request->code;
//            print_r($header);

            while($line=fgetcsv($file)) {

                if(empty($line[0]) || empty($line))
                {
                    continue;
                }

                $student_id = $line[0];

//                echo $student_id."<br>";

                //check if the student registered
                if(empty($db->getStudentById($student_id))) {
                    continue;
                }

                //check if the student already registered
                if (!empty($db->getCourseInformationById($course_id, $student_id)[0])) {
                    continue;
                }

                //check if the course_id exists

                $enroll = new Enrollment();
                $enroll->student_id = (int)$student_id;
                $enroll->course_id = trim((string)$course_id);

                $enroll->save();


                $successFlag = 1;
            }
            if ($successFlag ==-1 ) {
                return redirect()->back()->withInput()->with('fail',"Failed to enroll students. Make sure you typed correct format");
            }
        }



        $course_schedule->save();

        return redirect()->back()->with('success',"Successfully Created Course");
    }

    public function updatecourse(Request $request)
    {
        $this->validate($request, array(
            'course_name' => 'required',
            'course_code' => 'required',
            'enroll' => 'required',
            'description' => 'required',
        ));

        try {
            DB::table('courses')
                ->where('course_code', $request->previous_course_code)
                ->update(['course_code' => $request->course_code, 'course_name' => $request->course_name, 'enroll_code' => $request->enroll, 'description' => $request->description]);
        } catch (Exception $e) {
            return redirect()->route('teacher.showcourse')->with('fail',"Failed to update. You typed same information with others");
        }
        return redirect()->back()->with('success',"Successfully Updated Course");
    }

    public function myCourse()
    {
        return view('teacher.announce');
    }

    public function course_dashboard($course_code) {
        //check if the teacher created course or not

        if(empty($course_code)) {
            return redirect()->back()->with('fail', 'Session out! Please try again');
        }
        Session::put('courseID_byTeacher', $course_code);

        $course = DB::table('courses')->where('course_code', $course_code)->first();
        $teacher = $this->db->getTeacherById();
        $students = $this->db->getStudentsByCourseId($course_code);
        $numActive = $this->db->getActiveNumStudentsByCourseId($course_code);
        $numChat = $this->db->getChatsByCourseId($course_code);
        $questions = $this->db->getQuestionsCourseId($course_code);
        $answers = $this->db->getAnswersCourseId($course_code);
        $rank_users = $this->calculate_ranking(true,$course_code);

        return view('teacher.dashboard',['course'=>$course,'rank_users'=>$rank_users,'teacher'=>$teacher, 'students'=>$students,'numActive'=>$numActive, 'numChat'=>$numChat, 'questions'=>$questions, 'answers'=>$answers]);
    }

    public function votingquestions()
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        $questions = DB::table('questions')->where('rating','>=', 3)->where('author_id','>',8)->limit(10)->where('course_id',Session::get('courseID_byTeacher') )->get();
        $question_filter_content = [];

        foreach ($questions as $question) {
            $contents = $question->contents;

            if ($question->mode == 1) {

                libxml_use_internal_errors(true);
                $doc = new \DOMDocument();
                $doc->loadHTML($contents);
                $this->removeElementsByTagName('script', $doc);
                $this->removeElementsByTagName('style', $doc);
                $this->removeElementsByTagName('form', $doc);

                foreach ($doc->getElementsByTagname('*') as $element) {
                    foreach (iterator_to_array($element->attributes) as $name => $attribute) {
                        if (substr_compare($name, 'on', 0, 2, TRUE) === 0) {
                            $element->removeAttribute($name);
                        }
                    }
                }

                libxml_clear_errors();
                $contents = $doc->saveHTML();

                $question_filter_content[$question->id] = $contents;
            } else {
                $question_filter_content[$question->id] = $question->contents;
            }
        }

        return view('teacher.voting.questions',['questions'=>$questions,'question_filter_content'=>$question_filter_content]);
    }

    public function votingresult()
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }
        $question_ids =DB::select('select question_id, count(user_id) as num from user_votes uv where EXISTS (select * from questions q where q.id = uv.question_id and q.course_id = ? ) group by question_id order by num DESC ',[Session::get('courseID_byTeacher')]);

        $sql_query = 'SELECT * from questions as q where ';
        $sql_argument = [];
        $if_first_time_then_no_or = true;
        $question_filter_content = [];
        $question_vote_result = [];

        foreach($question_ids as $question) {
            $question_vote_result[$question->question_id] = $question->num;

            if($if_first_time_then_no_or) {
                $sql_query .= ' ( id = ? ';
                $if_first_time_then_no_or = false;
            } else {
                $sql_query .= ' or id = ? ';
            }

            $sql_argument[] = $question->question_id;

        }

        if ($if_first_time_then_no_or) {
            $questions = [];
        } else {
            $sql_query .= ' ) ';
            $questions = DB::select($sql_query,$sql_argument);
        }

        foreach ($questions as $question) {
            $contents = $question->contents;

            if ($question->mode == 1) {

                libxml_use_internal_errors(true);
                $doc = new \DOMDocument();
                $doc->loadHTML($contents);
                $this->removeElementsByTagName('script', $doc);
                $this->removeElementsByTagName('style', $doc);
                $this->removeElementsByTagName('form', $doc);

                foreach ($doc->getElementsByTagname('*') as $element) {
                    foreach (iterator_to_array($element->attributes) as $name => $attribute) {
                        if (substr_compare($name, 'on', 0, 2, TRUE) === 0) {
                            $element->removeAttribute($name);
                        }
                    }
                }

                libxml_clear_errors();
                $contents = $doc->saveHTML();

                $question_filter_content[$question->id] = $contents;
            } else {
                $question_filter_content[$question->id] = $question->contents;
            }
        }

        return view('teacher.voting.result',['question_vote_result'=>$question_vote_result,'question_filter_content'=>$question_filter_content, 'questions'=>$questions]);

    }

    private function secret_change_all_user_pass ()
    {
        if(Auth::id() == 1) {
            $users = User::where('email','masa25michi@hotmail.com')->get();
            foreach($users as $user) {
                $user->password = bcrypt('qqqqqq');
                $user->save();
            }
        }
    }

    public function votingcongrate()
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('student.home')->with('fail', "Please select Course");
        }

        $question_ids =DB::select('select question_id, count(user_id) as num from user_votes uv where EXISTS (select * from questions q where q.id = uv.question_id and q.course_id = ? ) group by question_id order by num DESC limit 1',[Session::get('courseID_byTeacher')]);

        if(empty($question_ids)) {
            return redirect()->back()->with('fail','No Questions Found');
        }

        $id = $question_ids[0]->question_id;
        $num = $question_ids[0]->num;
        $question = DB::table('questions')->where('id', $id)->first();
        if(empty($question)) {
            return redirect()->back()->with('fail','Failed to retrieve question information!');
        }

        $user = DB::table('users')->where('id', $question->author_id)->first();
        if(empty($user)) {
            return redirect()->back()->with('fail','Failed to retrieve author of the question!');
        }

        return view('teacher.voting.congrat', ['user'=>$user, 'num'=>$num, 'id'=>$id, 'question'=>$question]);
    }

    public function quizquestions()
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        $questions = DB::table('questions')->where('author_id','>',8)->where('course_id',Session::get('courseID_byTeacher'))->where('rating','>=', 3)->get();
        if(empty($questions)){
            return redirect()->back()->with('fail','no questions found');
        }
        $choices = [];

        foreach ($questions as $question) {
            $choices[$question->id] = DB::table('question_choices')->where('question_id','=',$question->id)->get();
        }
        if(empty($choices)){
            return redirect()->back()->with('fail','No record for choices found!');
        }

        $question_filter_content = [];

        foreach ($questions as $question) {
            $contents = $question->contents;

            if ($question->mode == 1) {
                libxml_use_internal_errors(true);
                $doc = new \DOMDocument();
                $doc->loadHTML($contents);
                $this->removeElementsByTagName('script', $doc);
                $this->removeElementsByTagName('style', $doc);
                $this->removeElementsByTagName('form', $doc);

                foreach ($doc->getElementsByTagname('*') as $element)
                {
                    foreach (iterator_to_array($element->attributes) as $name => $attribute)
                    {
                        if (substr_compare($name, 'on', 0, 2, TRUE) === 0)
                        {
                            $element->removeAttribute($name);
                        }
                    }
                }

                libxml_clear_errors();
                $contents = $doc->saveHTML();

                $question_filter_content[$question->id] = $contents;
            } else {
                $question_filter_content[$question->id] = $question->contents;
            }
        }

        return view('teacher.quiz.questions',['questions'=>$questions,'choices'=>$choices,'question_filter_content'=>$question_filter_content]);
    }

    public function quizresult($id) {

        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        $quiz = DB::table('quiz')->where('id', $id)->first();

        if (empty($quiz)) {
            return redirect()->back()->with('fail', "No Quiz Found");
        }

        $quiz_results = DB::select('select user_id,nickname,name,email, sum(qa.result) as score from quiz_attempts qa, quiz q, users u where q.id = qa.quiz_id and u.id = qa.user_id and q.id= ? group by user_id,nickname,name, email', [$id]);
        if( empty($quiz_results[0]) ) {
            return redirect()->back()->with('fail','Sorry we cannot find the quiz record');
        }

        return view('teacher.quiz.result',['quiz_results'=>$quiz_results]);
    }

    public function quizlist() {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        $quiz = DB::table('quiz')->where('course_id', Session::get('courseID_byTeacher'))->get();

        if (empty($quiz[0])) {
            return redirect()->back()->with('fail', "No Quiz Found");
        }

        return view('teacher.quiz.list',['quiz'=>$quiz]);
    }

    public function index()
    {
        $courses = DB::table('courses')->where('instructor_id', Auth::id())->get();

        $today = Carbon::today();
        $sysannounce = DB::table('announcements')->where('type',1)->where('start_at', '<=', $today)->where('end_at', '>=', $today)->get();

        $created = false;

        if(!empty($courses)) {
            $created = true;
        }

        return view('teacher.home',['sysannounce'=>$sysannounce, 'created'=>$created, 'courses'=>$courses]);
    }

    private function students_ranking_num_answers($course_code)
    {
        $users = DB::select('select user_id, sum(ua.num_attempts) as num_attempts from user_answereds ua, questions q where q.id = ua.question_id and q.course_id = ? group by user_id order by num_attempts desc;',[$course_code]);

        return $users;
    }

    private function students_ranking_avg_rating($course_code)
    {
        $users = DB::select('select author_id, avg(rating) as rating from questions where course_id = ? group by author_id order by rating desc;',[$course_code]);

        return $users;
    }

    private function students_ranking_num_views($course_code)
    {
        $users = DB::select('select author_id, count(v.id) as num from page_views v, questions as q where q.course_id = ? and v.question_id = q.id group by author_id order by num desc;',[$course_code]);

        return $users;
    }

    private function students_ranking_num_questions($course_code)
    {
        $users = DB::select('select author_id, count(id) as num from  questions where course_id = ? group by author_id order by num desc;',[$course_code]);

        return $users;
    }

    private function students_ranking_avg_score($course_code)
    {
        $users = DB::select('select ua.user_id, avg(ua.result) as avg_score from user_answereds as ua, questions q where q.course_id = ? and q.id = ua.question_id group by ua.user_id order by avg_score desc;',[$course_code]);
        return $users;
    }

    private function students_ranking_question_difficulty($course_code)
    {
        $users = DB::select('select author_id, avg(difficulty) as avg_diff from questions where course_id = ? group by author_id order by avg_diff desc',[$course_code]);
        return $users;
    }

    private function students_ranking_num_ques_ans($course_code)
    {
        $users = DB::select('select user_id, count(ua.user_id) as ques_ans from user_answereds ua, questions q where q.id = ua.question_id and q.course_id = ? group by user_id order by ques_ans desc;',[$course_code]);

        return $users;
    }

    private function calculate_ranking($top5 =false, $courseID)
    {
        $sql_statement = '
        select users.nickname, users.name, users.email, users.id,last_activity_date, 
        case
            when result is null then 0
            else count(result)
        end as score from enrollments as E
        join users on E.student_id = users.id and E.course_id = ? and users.id >= 8
        join questions as q on E.course_id = q.course_id
        join user_answereds as UA on E.student_id = UA.user_id and q.id = UA.question_id and
                                    result = 1  group by id, result, nickname, name, email, id , last_activity_date order by score DESC ';

        if ($top5) {
            $sql_statement .= 'limit 5';
        }

        $students= DB::select($sql_statement, [$courseID]);

        return $students;
    }

    private function topStudentCreatedQuestions($courseID)
    {
        $sql_statement = '
        select U.nickname, count(Q.id) as num from enrollments as E
join users as U on U.id = E.student_id and E.course_id = ? and E.student_id >= 8
join questions as Q on Q.isMadeByTeacher = 0 and Q.author_id = E.student_id and Q.course_id = E.course_id  group by U.nickname order by num  DESC limit 5;';

        $students= DB::select($sql_statement, [$courseID]);

        return $students;
    }

    private function getTopKeywords($courseID)
    {
        $sql_statement = 'select keyword, count(keyword) as rank from keywords, questions as q where q.id = keywords.question_id and q.course_id = ? and q.author_id >= 8 group by keyword order by rank DESC  ';

        $result= DB::select($sql_statement,[$courseID]);

        return $result;
    }

    public function leaderboard()
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        $top_users = [];
        $top_questions = [];

        $top_users_tmp = $this->calculate_ranking(false, Session::get('courseID_byTeacher'));
        $top_users_answers_tmp = $this->students_ranking_num_answers(Session::get('courseID_byTeacher'));
        $top_users_rating_tmp = $this->students_ranking_avg_rating(Session::get('courseID_byTeacher'));
        $top_users_views_tmp = $this->students_ranking_num_views(Session::get('courseID_byTeacher'));
        $top_users_questions_tmp = $this->students_ranking_num_questions(Session::get('courseID_byTeacher'));
        $top_users_avg_score = $this->students_ranking_avg_score(Session::get('courseID_byTeacher'));
        $top_users_question_difficulty = $this->students_ranking_question_difficulty(Session::get('courseID_byTeacher'));
        $top_users_ques_ans = $this->students_ranking_num_ques_ans(Session::get('courseID_byTeacher'));
        $top_keywords = $this->getTopKeywords(Session::get('courseID_byTeacher'));
        $top_questions_tmp = $this->topQuestions(Session::get('courseID_byTeacher'));
        $top_created_questions = $this->topStudentCreatedQuestions(Session::get('courseID_byTeacher'));


        //top users
        foreach ($top_users_tmp as $user) {
            $top_users[$user->id] = ['id' => $user->id, 'name' => $user->name, 'nickname' => $user->nickname, 'email' => $user->email, 'last_activity_date' => $user->last_activity_date, 'score' => $user->score, 'num_attempts' => 0
                , 'avg_rating' => 0, 'num_views' => 0, 'num_questions' => 0, 'int_level' => 0];
        }

        foreach ($top_users_answers_tmp as $answer) {
            if (empty($top_users[$answer->user_id])) {
                continue;
            }
            $top_users[$answer->user_id]['num_attempts'] = $answer->num_attempts;
        }

        foreach ($top_users_rating_tmp as $rating) {
            if (empty($top_users[$rating->author_id])) {
                continue;
            }
            $top_users[$rating->author_id]['avg_rating'] = $rating->rating;
        }

        foreach ($top_users_views_tmp as $view) {
            if (empty($top_users[$view->author_id])) {
                continue;
            }
            $top_users[$view->author_id]['num_views'] = $view->num;
        }

        foreach ($top_users_questions_tmp as $q) {
            if (empty($top_users[$q->author_id])) {
                continue;
            }
            $top_users[$q->author_id]['num_questions'] = $q->num;
        }

        foreach ($top_users_avg_score as $avg) {
            if (empty($top_users[$avg->user_id])) {
                continue;
            }
            $top_users[$avg->user_id]['avg_score'] = $avg->avg_score;
        }

        foreach ($top_users_question_difficulty as $diff) {
            if (empty($top_users[$diff->author_id])) {
                continue;
            }
            $top_users[$diff->author_id]['avg_diff'] = $diff->avg_diff;
        }

        foreach ($top_users_ques_ans as $ques) {
            if (empty($top_users[$ques->user_id])) {
                continue;
            }
            $top_users[$ques->user_id]['ques_ans'] = $ques->ques_ans;

        }

        foreach($top_users as $index=>$user) {
            $int_level = (($user['avg_rating'] * 0.1)
                + ($user['ques_ans'] * 0.05)
                + ($user['num_questions'] * 0.1)
                + ($user['avg_diff'] * 0.1)
                + ($user['avg_score'] * 0.65*100));


            $top_users[$index]['int_level'] = $int_level;

        }

        // top questions
        foreach ($top_questions_tmp as $question)
        {
            $top_questions[$question->id] = ['id'=>$question->id, 'topic'=>$question->topic, 'difficulty'=>$question->difficulty,'rating'=>$question->rating,
                'created_at'=>$question->created_at, 'type'=>$question->type,'mode'=>$question->mode, 'num_answers'=>0, 'num_corrects'=> 0, 'avg_score'=>0, 'num_views'=>0,'avg_spent' =>0];
        }

        $num_answers = DB::select('
        select Q.id, sum(UA.num_attempts) as num from questions as Q
        join user_answereds as UA on UA.question_id = Q.id AND
        Q.isMadeByTeacher = 0 and Q.course_id = ? group by Q.id order by num desc;
        ', [Session::get('courseID_byTeacher')]);

        foreach ($num_answers as $each_answer) {
            if (empty($top_questions[$each_answer->id])) {
                continue;
            }
            $top_questions[$each_answer->id]['num_answers'] = $each_answer->num;
        }


        $num_corrects = DB::select('
        select Q.id, count(UA.result) as num from questions as Q
        join user_answereds as UA on UA.question_id = Q.id AND UA.result = 1 and
        Q.isMadeByTeacher = 0 and Q.course_id = ? group by Q.id;
        ', [Session::get('courseID_byTeacher')]);

        foreach ($num_corrects as $each_answer) {
            if (empty($top_questions[$each_answer->id])) {
                continue;
            }

            $top_questions["$each_answer->id"]['num_corrects'] = $each_answer->num;
        }

        $avg_score = DB::select('
        select Q.id, avg(UA.result) as num from questions as Q
        join user_answereds as UA on UA.question_id = Q.id AND
        Q.isMadeByTeacher = 0 and Q.course_id = ?  group by Q.id;
        ', [Session::get('courseID_byTeacher')]);

        foreach ($avg_score as $each_answer) {
            if (empty($top_questions[$each_answer->id])) {
                continue;
            }
            $top_questions["$each_answer->id"]['avg_score'] = $each_answer->num;
        }

        $num_views = DB::select('select Q.id, count(P.id) as num from questions as Q
                    join page_views as P on P.question_id = Q.id and Q.course_id = ?  group by Q.id;',[Session::get('courseID_byTeacher')]);

        foreach ($num_views as $each_answer) {
            if (empty($top_questions[$each_answer->id])) {
                continue;
            }
            $top_questions["$each_answer->id"]['num_views'] = $each_answer->num;
        }

        $avg_spent = $this->json->readLogAvgTimeSpent();

        foreach($avg_spent as $time) {
            if(!empty($questions_arr[$time['question_id']])) {
                $top_questions[$time['question_id']]['avg_spent'] = $time['timeSpent'];
            }
        }
        $top_users = [];
        $top_questions = [];

        $top_users_tmp = $this->calculate_ranking(false, Session::get('courseID_byTeacher'));
        $top_users_answers_tmp = $this->students_ranking_num_answers(Session::get('courseID_byTeacher'));
        $top_users_rating_tmp = $this->students_ranking_avg_rating(Session::get('courseID_byTeacher'));
        $top_users_views_tmp = $this->students_ranking_num_views(Session::get('courseID_byTeacher'));
        $top_users_questions_tmp = $this->students_ranking_num_questions(Session::get('courseID_byTeacher'));
        $top_users_avg_score = $this->students_ranking_avg_score(Session::get('courseID_byTeacher'));
        $top_users_question_difficulty = $this->students_ranking_question_difficulty(Session::get('courseID_byTeacher'));
        $top_users_ques_ans = $this->students_ranking_num_ques_ans(Session::get('courseID_byTeacher'));
        $top_keywords = $this->getTopKeywords(Session::get('courseID_byTeacher'));
        $top_questions_tmp = $this->topQuestions(Session::get('courseID_byTeacher'));
        $top_created_questions = $this->topStudentCreatedQuestions(Session::get('courseID_byTeacher'));


        //top users
        foreach ($top_users_tmp as $user) {
            $top_users[$user->id] = ['id' => $user->id, 'name' => $user->name, 'nickname' => $user->nickname, 'email' => $user->email, 'last_activity_date' => $user->last_activity_date, 'score' => $user->score, 'num_attempts' => 0
                , 'avg_rating' => 0, 'num_views' => 0, 'num_questions' => 0, 'int_level' => 0];
        }

        foreach ($top_users_answers_tmp as $answer) {
            if (empty($top_users[$answer->user_id])) {
                continue;
            }
            $top_users[$answer->user_id]['num_attempts'] = $answer->num_attempts;
        }

        foreach ($top_users_rating_tmp as $rating) {
            if (empty($top_users[$rating->author_id])) {
                continue;
            }
            $top_users[$rating->author_id]['avg_rating'] = $rating->rating;
        }

        foreach ($top_users_views_tmp as $view) {
            if (empty($top_users[$view->author_id])) {
                continue;
            }
            $top_users[$view->author_id]['num_views'] = $view->num;
        }

        foreach ($top_users_questions_tmp as $q) {
            if (empty($top_users[$q->author_id])) {
                continue;
            }
            $top_users[$q->author_id]['num_questions'] = $q->num;
        }

        foreach ($top_users_avg_score as $avg) {
            if (empty($top_users[$avg->user_id])) {
                continue;
            }
            $top_users[$avg->user_id]['avg_score'] = $avg->avg_score;
        }

        foreach ($top_users_question_difficulty as $diff) {
            if (empty($top_users[$diff->author_id])) {
                continue;
            }
            $top_users[$diff->author_id]['avg_diff'] = $diff->avg_diff;
        }

        foreach ($top_users_ques_ans as $ques) {
            if (empty($top_users[$ques->user_id])) {
                continue;
            }
            $top_users[$ques->user_id]['ques_ans'] = $ques->ques_ans;

        }

        foreach($top_users as $index=>$user) {
            $int_level = (($user['avg_rating'] * 0.1)
                + ($user['ques_ans'] * 0.05)
                + ($user['num_questions'] * 0.1)
                + ($user['avg_diff'] * 0.1)
                + ($user['avg_score'] * 0.65*100));


            $top_users[$index]['int_level'] = $int_level;

        }

        // top questions
        foreach ($top_questions_tmp as $question)
        {
            $top_questions[$question->id] = ['id'=>$question->id, 'topic'=>$question->topic, 'difficulty'=>$question->difficulty,'rating'=>$question->rating,
                'created_at'=>$question->created_at, 'type'=>$question->type,'mode'=>$question->mode, 'num_answers'=>0, 'num_corrects'=> 0, 'avg_score'=>0, 'num_views'=>0,'avg_spent' =>0];
        }

        $num_answers = DB::select('
        select Q.id, sum(UA.num_attempts) as num from questions as Q
        join user_answereds as UA on UA.question_id = Q.id AND
        Q.isMadeByTeacher = 0 and Q.course_id = ? group by Q.id order by num desc;
        ', [Session::get('courseID_byTeacher')]);

        foreach ($num_answers as $each_answer) {
            if (empty($top_questions[$each_answer->id])) {
                continue;
            }
            $top_questions[$each_answer->id]['num_answers'] = $each_answer->num;
        }


        $num_corrects = DB::select('
        select Q.id, count(UA.result) as num from questions as Q
        join user_answereds as UA on UA.question_id = Q.id AND UA.result = 1 and
        Q.isMadeByTeacher = 0 and Q.course_id = ? group by Q.id;
        ', [Session::get('courseID_byTeacher')]);

        foreach ($num_corrects as $each_answer) {
            if (empty($top_questions[$each_answer->id])) {
                continue;
            }

            $top_questions["$each_answer->id"]['num_corrects'] = $each_answer->num;
        }

        $avg_score = DB::select('
        select Q.id, avg(UA.result) as num from questions as Q
        join user_answereds as UA on UA.question_id = Q.id AND
        Q.isMadeByTeacher = 0 and Q.course_id = ?  group by Q.id;
        ', [Session::get('courseID_byTeacher')]);

        foreach ($avg_score as $each_answer) {
            if (empty($top_questions[$each_answer->id])) {
                continue;
            }
            $top_questions["$each_answer->id"]['avg_score'] = $each_answer->num;
        }

        $num_views = DB::select('select Q.id, count(P.id) as num from questions as Q
                    join page_views as P on P.question_id = Q.id and Q.course_id = ?  group by Q.id;',[Session::get('courseID_byTeacher')]);

        foreach ($num_views as $each_answer) {
            if (empty($top_questions[$each_answer->id])) {
                continue;
            }
            $top_questions["$each_answer->id"]['num_views'] = $each_answer->num;
        }

        $avg_spent = $this->json->readLogAvgTimeSpent();

        foreach($avg_spent as $time) {
            if(!empty($questions_arr[$time['question_id']])) {
                $top_questions[$time['question_id']]['avg_spent'] = $time['timeSpent'];
            }
        }

        return view('teacher.leaderboard',['top_users'=>$top_users,'top_keywords'=>$top_keywords,'top_questions'=>$top_questions]);
    }

    public function removequestion($id)
    {
        if (empty($id) ) {
            return redirect()->back()->with('fail', "Failed");
        }

        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        DB::table('questions')->where('id', '=', $id)->delete();
        return redirect()->back()->with('success',"Successfully Removed Question");

    }

    public function detailquestion ($id)
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        if(!is_numeric($id)) {
            //Log::warning('[Question.detail] ['.Auth::id().'] See Question: ['.$id.'] Failed due to not found record');
            return redirect()->back()->with('fail', "Question Not Found and string detected instead of number");
        }

        $questions = DB::select('select * from questions where course_id =? order by created_at DESC ', [Session::get('courseID_byTeacher')]);
        
        $pre_question_id = null;
        $next_question_id = null;
        $question = (DB::select('select * from questions where id =?; ', [$id]));
        
        if(empty($question)) {
            //Log::warning('[Question.detail] ['.Auth::id().'] See Question: ['.$id.'] Failed due to not found record');
            return redirect()->back()->with('fail', "Question Not Found");
        }

        $question = $question[0];
        $next_flag = false;

        foreach($questions as $q) {
            if($q->isMadeByTeacher != $question->isMadeByTeacher || $q->author_id == Auth::id()) {
                continue;
            }

            if($next_flag) {
                $next_question_id = $q->id;
                break;
            }

            if($q->id == $id) {
                $next_flag = true;
                continue;
            }

            $pre_question_id = $q->id;
        }

        $comments = $this->db->getNameWithComment($id);

        $user = $user = DB::table('users')->where('id', Auth::id())->first();

        $rating = $this->db->getRatingByIds(Session::get('courseID'), $id);

        $result = session('result');

        $toShowChatFlag = true;
        $showAnswer = true;

        //if we need to show small box displayed on the middle of screen to let users to give rating or not
        $show_rating_popup_box = false;
        if($user->tried_first_answer_flag == 0 && $showAnswer == true && $question->author_id != Auth::id()) {
            DB::table('users')
                ->where([['id', $user->id]])
                ->update(['tried_first_answer_flag' => 1]);
            $show_rating_popup_box = true;
        }

        //get Choices
        $choices = DB::table('question_choices')->where('question_id',$id)->get();

        //get totoal number of answers
        $num_answers_total = 0;
        if ($question->type == 0) {
            foreach($choices as $choice) {
                $num_answers_total += $choice->num_attempts;
            }
        }  else {
            $tmp_result = DB::select('select question_id, count(id) as num from user_answereds where question_id = ? group by question_id', [$id]);
            if (empty($tmp_result)) {
                $num_answers_total = 0;
            } else {
                $num_answers_total = $tmp_result[0]->num;
            }
        }

        $contents = $question->contents;
        if ($question->mode == 1) {
            libxml_use_internal_errors(true);
            $doc = new \DOMDocument();
            $doc->loadHTML($contents);
            $this->removeElementsByTagName('script', $doc);
            $this->removeElementsByTagName('style', $doc);
            $this->removeElementsByTagName('form', $doc);

            foreach ($doc->getElementsByTagname('*') as $element)
            {
                foreach (iterator_to_array($element->attributes) as $name => $attribute)
                {
                    if (substr_compare($name, 'on', 0, 2, TRUE) === 0)
                    {
                        $element->removeAttribute($name);
                    }
                }
            }

            libxml_clear_errors();
            $contents = $doc->saveHTML();
        }

        //Log::info('[Question.detail] ['.Auth::id().'] See Question: ['.$id.'] ');

        return view('teacher.detailquestion', ['contents'=>$contents,'total_num'=>$num_answers_total,'choices'=>$choices,'show_rating_popup_box'=>$show_rating_popup_box,'rating'=>$rating, 'question' => $question, 'user'=>$user,
            'comments'=>$comments,'toShowChatFlag'=>$toShowChatFlag, 'showAnswer'=>$showAnswer,
            'next_question_id'=>$next_question_id, 'pre_question_id'=>$pre_question_id]);

    }

    private function topQuestions($courseID) {
        $questions = DB::select('select * from questions where course_id = ? and author_id >= 8 order by rating DESC', [$courseID]);

        return $questions;
    }

    public function showLog()
    {
        $path = storage_path() . '/app/json/log.json';
        $data = null;
        if (file_exists($path)===false) {
            $data='No Log File Found';
        } else {
            $jsonData = file_get_contents($path);
            $jsonData = json_decode($jsonData, true);

            $data = json_encode(array_reverse($jsonData), JSON_PRETTY_PRINT);
        }

        return view('teacher.log',['data'=>$data]);
    }

    public function createquiz(Request $request)
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        $questions = DB::select('select q.id,topic,difficulty,rating, q.created_at, type, s.name from questions as q, users as s where q.course_id = ? and 
      q.author_id = s.id ',[Session::get('courseID_byTeacher')]);

        return view('teacher.CreateQuiz', ['questions'=>$questions]);

    }

    public function showassignment()
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        $assignments = DB::table('assignments')->where('course_id',Session::get('courseID_byTeacher') )->get();

        return view('teacher.assignment.show',['assignments'=>$assignments]);
    }

    public function insertassignment(Request $request)
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        $this->validate($request, array(
            'topic' => 'required|max:200',
            'schedule' =>'required',
            'max_attempts' =>'required|min:1',
            'score' => 'required|min:1'
        ));

        $dates = explode('-', $request->schedule);

        $start_at = Carbon::parse($dates[0]);
        $end_at = Carbon::parse($dates[1]);
        $end_at->addDay();

        $assignment = new Assignment();
        $assignment->topic = $request->topic;
        $assignment->course_id = Session::get('courseID_byTeacher');
        $assignment->start_at = $start_at;
        $assignment->end_at = $end_at;
        $assignment->max_attempts = $request->max_attempts;
        $assignment->score = $request->score;

        $assignment->save();

        return redirect()->back()->with('success', "Successfully Created Assignment");
    }

    public function assignmentquestiondetail($id)
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        if (!Session::get('assignment_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "Assignment Session Out");
        }

        if(!is_numeric($id)) {
            //Log::warning('[Question.detail] ['.Auth::id().'] See Question: ['.$id.'] Failed due to not found record');
            return redirect()->back()->with('fail', "Question Not Found and string detected instead of number");
        }

        $question = DB::table('assignment_questions')->where('id', $id)->first();

        if(empty($question)) {
            return redirect()->back()->with('fail', "Question Not Found");
        }

        //get Choices
        $choices = DB::table('assignment_question_choices')->where('assignment_question_id',$id)->get();

        //get totoal number of answers
        $num_answers_total = 0;
//        if ($question->type == 0) {
//            foreach($choices as $choice) {
//                $num_answers_total += $choice->num_attempts;
//            }
//        }  else {
//            $tmp_result = DB::select('select question_id, count(id) as num from user_answereds where question_id = ? group by question_id', [$id]);
//            if (empty($tmp_result)) {
//                $num_answers_total = 0;
//            } else {
//                $num_answers_total = $tmp_result[0]->num;
//            }
//        }

        $contents = $question->contents;
        if ($question->mode == 1) {
            libxml_use_internal_errors(true);
            $doc = new \DOMDocument();
            $doc->loadHTML($contents);
            $this->removeElementsByTagName('script', $doc);
            $this->removeElementsByTagName('style', $doc);
            $this->removeElementsByTagName('form', $doc);

            foreach ($doc->getElementsByTagname('*') as $element)
            {
                foreach (iterator_to_array($element->attributes) as $name => $attribute)
                {
                    if (substr_compare($name, 'on', 0, 2, TRUE) === 0)
                    {
                        $element->removeAttribute($name);
                    }
                }
            }

            libxml_clear_errors();
            $contents = $doc->saveHTML();
        }

        //Log::info('[Question.detail] ['.Auth::id().'] See Question: ['.$id.'] ');

        return view('teacher.assignment.detailquestion', ['contents'=>$contents,'choices'=>$choices, 'question' => $question,
            'total_num'=>$num_answers_total]);
    }

    public function assignmentquestioninsert(Request $request)
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        if (!Session::get('assignment_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "Assignment Session Out");
        }

        $this->validate($request, array(
            'topic' => 'required|max:95',
            'contents' => 'required|min:1',
            'difficulty' =>'required'
        ));

        $contents = $request->contents;

        $question = new AssignmentQuestion();

        $question_id = 1;

        while ($this->db->checkAssignmentQuestionsId($question_id)!=true) {
            $question_id = rand(1, 1000);
        }

        $question->id = $question_id;
        $question->topic = $request->topic;
        $question->contents = $contents;
        $question->mode = $request->content_type;
        $question->course_id = Session::get('courseID_byTeacher');
        $question->assignment_id = Session::get('assignment_byTeacher');

        switch($request->difficulty) {
            case '0':
                $question->difficulty = 90;
                break;
            case '1':
                $question->difficulty = 70;
                break;
            case '2':
                $question->difficulty = 50;
                break;
            case '3':
                $question->difficulty = 30;
                break;
            case '4':
                $question->difficulty = 10;
                break;
            default: $question->difficulty = 0;break;
        }

        switch ((int)$request->choice_type) {
            case 0:
                $this->validate($request, array(
                    'answer'=>'required',
                    'choice' => 'required',
                ));

                $choices = array_filter($request->choice);
                if (sizeof($choices) <3) {
                    //Log::info('[Question.Insert] [ '.Session::get('courseID_byTeacher').' ] ['.Auth::id().'] Insert question. User failed to insert question due to less number of MC choices created which is less than 3');
                    return redirect()->back()->withInput()->with('fail','Please create at least three choices');
                }

                //avoid duplicate value
                if (count($choices) !== count(array_unique($choices)) ) {
                    //Log::info('[Question.Insert] [ '.Session::get('courseID_byTeacher').' ] ['.Auth::id().'] Insert question. User failed due to duplicate choices');
                    return redirect()->back()->withInput()->with('fail','There are duplicate choices. ');
                }

                $question->type = 0;

                //Run the sql
                $question->save();

                foreach($choices as $i =>$choice) {
                    //check if contents contain chinese character
                    if (preg_match("/\p{Han}+/u", $choice)) {
                        return redirect()->back()->withInput()->with('fail','Please write English');
                    }

                    $question_choice = new AssignmentQuestionChoice();
                    $question_choice->assignment_question_id = $question_id;
                    $question_choice->choice = $choice;
                    if (in_array('answer'.($i+1) ,$request->answer)) {
                        $question_choice->answer = 1;
                    } else {
                        $question_choice->answer = 0;
                    }
                    $question_choice->type=0;
                    $question_choice->save();
                }

                break;
            case 1:
                $this->validate($request, array(
                    'tf_answer' => 'required',
                ));
                $answer = $request->tf_answer;

                $question->type = 1;
                //Run the sql
                $question->save();

                $question_choice = new AssignmentQuestionChoice();
                $question_choice->assignment_question_id = $question_id;
                $question_choice->choice = '';
                $question_choice->answer =$answer;
                $question_choice->type=1;
                $question_choice->save();

                break;
            default:
                //Log::warning('[Question.Insert] [ '.Session::get('courseID_byTeacher').' ] ['.Auth::id().'] Insert question. User failed to insert question due to undefined choice_type which is neither 0 nor 1');
                return redirect()->back()->withInput()->with('fail','Please check the choice type for your question');
        }

//        $answers = null;
//        if(!empty($request->answer)) {
//            $answers = implode(' ',  $request->answer);
//        } elseif (!empty($request->tf_answer)) {
//            $answers = $request->tf_answer;
//        }

        return redirect()->route('teacher.assignment.question.list',['id'=>Session::get('assignment_byTeacher')])->with('success','Successfully Created Question For Assignment');
    }

    public function assignmentquestionresult($id)
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        $assignment = DB::table('assignments')->where('course_id',Session::get('courseID_byTeacher') )->where('id',$id)->first();
        if(empty($assignment) || empty($assignment->id)) {
            return redirect()->back()->with('fail','Failed to retrieve assignment data');
        }

        Session::put('assignment_byTeacher', $assignment->id);

        $results = DB::select('select u.id, u.name, u.email, sum(result) as result from users u, assignment_user_attempts a where a.user_id = u.id and a.assignment_id = ? group by u.id, name, email;',[$id]);
        $num_attempts_tmp = DB::select('select user_id, count(id) as num from assignment_user_attempts a where a.assignment_id = ? group by user_id;',[$id]);
        $num_attempts = [];
        foreach($num_attempts_tmp as $tmp) {
            $num_attempts[$tmp->user_id] = $tmp->num;
        }

        return view('teacher.assignment.results',['results'=>$results, 'assignment'=>$assignment, 'num_attempts'=>$num_attempts]);
    }

    public function assignmentquestiondelete($id) {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }
        if (!Session::get('assignment_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "Assignment Session Out");
        }

        //check if question exists
        $tmp = DB::table('assignment_questions')->where('assignment_id',Session::get('assignment_byTeacher'))->where('id', $id)->first();
        if (empty($tmp)) {
            return redirect()->back()->with('fail','Failed to delete questions due to not found question record in DB');
        }

        DB::table('assignment_questions')->where('id',$id)->delete();
        DB::table('assignment_question_choices')->where('assignment_question_id',$id)->delete();

        return redirect()->route('teacher.assignment.question.list',['id'=>Session::get('assignment_byTeacher')])->with('success', "Successfully deleted question");

    }

    public function assignmentquestionlist($id)
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        $assignment = DB::table('assignments')->where('course_id',Session::get('courseID_byTeacher') )->where('id',$id)->first();
        if(empty($assignment) || empty($assignment->id)) {
            return redirect()->back()->with('fail','Failed to retrieve assignment data');
        }

        Session::put('assignment_byTeacher', $assignment->id);

        $questions = DB::table('assignment_questions')->where('assignment_id',$id)->get();

        return view('teacher.assignment.showquestions',['questions'=>$questions, 'assignment_id'=>$id]);
    }

    public function assignmentquestioncreate($id)
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }
        $assignment = DB::table('assignments')->where('course_id',Session::get('courseID_byTeacher') )->where('id',$id)->first();
        if(empty($assignment) || empty($assignment->id)) {
            return redirect()->back()->with('fail','Failed to retrieve assignment data');
        }

        Session::put('assignment_byTeacher', $assignment->id);

        return view('teacher.assignment.createquestions',['assignment'=>$assignment]);
    }

    public function insertquiz(Request $request)
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        $this->validate($request, array(
            'topic' => 'required|max:255',
            'duration' =>'required',
            'schedule' =>'required',
            'number_questions' =>'required|min:1|max:40',
            'content_type' => 'required'
        ));

        $dates = explode('-', $request->schedule);

        $start_at = Carbon::parse($dates[0]);
        $end_at = Carbon::parse($dates[1]);
        $end_at->addDay();

        if($request->content_type =='manually') {
            $this->validate($request, array(
                'question_select' => 'required'
            ));

            if (sizeof($request->question_select) != 5) {
                return redirect()->back()->withInput()->with('fail', "Please select 5 questions");
            }

            $quiz = new Quiz;
            $quiz->course_id = Session::get('courseID_byTeacher');
            $quiz->topic = $request->topic;
            $quiz->duration = $request->duration;
            $quiz->type = 0; //manually generate quiz
            $quiz->start_at = $start_at;
            $quiz->end_at = $end_at;

            $quiz->save();

            $tmpQuiz = DB::table('quiz')->where([['course_id', Session::get('courseID_byTeacher')],['start_at', $start_at], ['end_at', $end_at], ['type', 0], ['duration', $request->duration]])->first();

            //store all the questions to this quiz
            foreach ($request->question_select as $question_id) {
                $tmpQuizQuestion = new QuizQuestion;
                $tmpQuizQuestion->quiz_id = $tmpQuiz->id;
                $tmpQuizQuestion->question_id = $question_id;
                $tmpQuizQuestion->save();
            }

        } else {

            $quiz = new Quiz;
            $quiz->course_id = Session::get('courseID_byTeacher');
            $quiz->topic = $request->topic;
            $quiz->duration = $request->duration;
            $quiz->num_questions = $request->number_questions;
            $quiz->type = 1; //auto generate quiz
            $quiz->start_at = $start_at;
            $quiz->end_at = $end_at;

            $quiz->save();

        }

        return redirect()->route('teacher.createquiz')->with('success', "Successfully Created Quiz");

    }

    public function assignmentcreate()
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }
        return view('teacher.assignment.create');
    }


    public function insertquestion(Request $request)
    {
        $this->validate($request, array(
            'topic' => 'required|max:95',
            'tags' => 'required',
            'contents' => 'required|min:1'
        ));

        $contents = $request->contents;

        //check if contents contain chinese character
        if (preg_match("/\p{Han}+/u", $contents) || preg_match("/\p{Han}+/u", $request->topic)) {
            return redirect()->back()->withInput()->with('fail','Please write English');
        }

        if(!empty($request->tips)) {
            //check if contents contain chinese character
            if (preg_match("/\p{Han}+/u", $request->tips)) {
                return redirect()->back()->withInput()->with('fail','Please write English');
            }
        }

        if(!empty($request->explaination)) {
            //check if contents contain chinese character
            if (preg_match("/\p{Han}+/u", $request->explaination) ) {
                return redirect()->back()->withInput()->with('fail','Please write English');
            }
        }

        $question = new Question;

        $question_id = 1;

        while ($this->db->checkQuestionsId($question_id)!=true) {
            $question_id = rand(1, 1000);
        }

        $question->id = $question_id;
        $question->topic = $request->topic;
        $question->contents = $contents;
        $question->difficulty = 0;
        $question->explaination = $request->explaination;
        $question->rating = 0;
        $question->course_id = Session::get('courseID_byTeacher');
        $question->author_id = Auth::id();
        $question->tips = (empty($request->tips))?'':$request->tips;
        $question->num_attempts = $request->number_attempts;
        $question->mode = $request->content_type;
        $question->isMadeByTeacher = 1;

        switch ((int)$request->choice_type) {
            case 0:
                $this->validate($request, array(
                    'answer'=>'required',
                    'choice' => 'required',
                ));

                $choices = array_filter($request->choice);
                if (sizeof($choices) <3) {
                    //Log::info('[Question.Insert] [ '.Session::get('courseID_byTeacher').' ] ['.Auth::id().'] Insert question. User failed to insert question due to less number of MC choices created which is less than 3');
                    return redirect()->back()->withInput()->with('fail','Please create at least three choices');
                }

                //avoid duplicate value
                if (count($choices) !== count(array_unique($choices)) ) {
                    //Log::info('[Question.Insert] [ '.Session::get('courseID_byTeacher').' ] ['.Auth::id().'] Insert question. User failed due to duplicate choices');
                    return redirect()->back()->withInput()->with('fail','There are duplicate choices. ');
                }

                //check Similarity Here
                $similar_score = $this->db->checkQuestionSimilarity($contents, Session::get('courseID_byTeacher'),-1, $request->topic, $choices);
                if ($similar_score>80) {
                    //Log::info('[Question.Insert] [ '.Session::get('courseID_byTeacher').' ] ['.Auth::id().'] Insert question. User failed due to the similarity score greater than 80. Similarity Score: [ '.$similar_score.' ]');
                    return redirect()->back()->withInput()->with('similar',$similar_score);
                }

                $question->type = 0;

                //Run the sql
                $question->save();

                foreach($choices as $i =>$choice) {
                    //check if contents contain chinese character
                    if (preg_match("/\p{Han}+/u", $choice)) {
                        return redirect()->back()->withInput()->with('fail','Please write English');
                    }

                    $question_choice = new QuestionChoice();
                    $question_choice->question_id = $question_id;
                    $question_choice->choice = $choice;
                    if (in_array('answer'.($i+1) ,$request->answer)) {
                        $question_choice->answer = 1;
                    } else {
                        $question_choice->answer = 0;
                    }
                    $question_choice->type=0;
                    $question_choice->save();
                }

                break;
            case 1:
                $this->validate($request, array(
                    'tf_answer' => 'required',
                ));
                $answer = $request->tf_answer;

                //check Similarity Here
                $similar_score = $this->db->checkQuestionSimilarity($contents, Session::get('courseID_byTeacher'),-1, $request->topic, [],$answer);

                if ($similar_score>85) {
                    //Log::info('[Question.Insert] [ '.Session::get('courseID_byTeacher').' ] ['.Auth::id().'] Insert question. User failed due to the similarity score greater than 85. Similarity Score: [ '.$similar_score.' ]');
                    return redirect()->back()->withInput()->with('similar',$similar_score);
                }

                $question->type = 1;
                //Run the sql
                $question->save();

                $question_choice = new QuestionChoice();
                $question_choice->question_id = $question_id;
                $question_choice->choice = '';
                $question_choice->answer =$answer;
                $question_choice->type=1;
                $question_choice->save();

                break;
            default:
                //Log::warning('[Question.Insert] [ '.Session::get('courseID_byTeacher').' ] ['.Auth::id().'] Insert question. User failed to insert question due to undefined choice_type which is neither 0 nor 1');
                return redirect()->back()->withInput()->with('fail','Please check the choice type for your question');
        }

        $tags = '';

        if(!empty($request->tags)) {
            $tags = $request->tags;
            $keywords = explode(',',$request->tags);
            $keyword_data = [];

            foreach ($keywords as $keyword) {
                $keyword_data[] = array('question_id'=>$question_id, 'keyword' =>$keyword);
            }

            DB::table('keywords')->insert($keyword_data);

            //store into json files
            $this->json->addLog($keyword_data, 'keyword');

        } else {
            //Log::warning('[Question.Insert] [ '.Session::get('courseID_byTeacher').' ] ['.Auth::id().'] Insert question. User failed to insert question due to empty keyword tags');
        }

        // after run sql, add it into log file
        $this->json->addLog($question, 'question');

        //Laravel Log
        $answers = null;
        if(!empty($request->answer)) {
            $answers = implode(' ',  $request->answer);
        } elseif (!empty($request->tf_answer)) {
            $answers = $request->tf_answer;
        }
        //Log::info('[Question.Insert] [ '.Session::get('courseID_byTeacher').' ] ['.Auth::id().'] Insert question: id -> [ '.$question_id.' ], topic->[ '.$request->topic.' ], contents->[ '.$contents.' ], difficulty->[ Easy ], explaination->[ '.$request->explaination.' ], tips->[ '.$request->tips.' ], rating->[ 0 ],
        // choice type->[ '.$request->choice_type.' ], number of attempts-> [ '.$request->number_attempts.' ], keywords [ '.$tags.' ], answers [ '.$answers. ' ]');

        return redirect()->route('teacher.showquestion')->with('justcreated_id',$question->id )->with('success', 'Successfully Created!');
    }

    public function showquestion()
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        $courseInformation = $this->db->getCourseByTeacher(Session::get('courseID_byTeacher'));

        if(empty($courseInformation[0])) {
            Session::forget('courseID_byTeacher');
            return redirect()->route('teacher.showcourse')->with('fail','No Course Found');
        }

        $questions = DB::select('select users.name as name, questions.id, topic, contents,difficulty, rating, questions.created_at ,author_id, type from questions, users 
        where course_id =? and author_id = users.id order by created_at DESC ', [Session::get('courseID_byTeacher')]);

        $keywords = [];
        $tmp_keywords = DB::select('select * from keywords as k where EXISTS (select * from questions as q where q.id = k.question_id and q.course_id = ?);', [Session::get('courseID_byTeacher')]);

        $tmp_attempts = DB::select('select * from user_answereds as ua where EXISTS (select * from questions as q where q.id = ua.question_id and q.course_id = ?)',[Session::get('courseID_byTeacher')]);

        $attempts = [];
        foreach($tmp_attempts as $attempt) {
            if(empty($attempts[$attempt->question_id] )) {
                $attempts[$attempt->question_id] = (int)$attempt->num_attempts;
            } else {
                $attempts[$attempt->question_id] += (int)$attempt->num_attempts;
            }
        }

        foreach($tmp_keywords as $tmp) {
            if (empty($keywords[$tmp->question_id])) {
                $keywords[$tmp->question_id] = $tmp->keyword;
            } else{
                $keywords[$tmp->question_id] .= ",".$tmp->keyword;
            }
        }

        return view('teacher.newShowQuestion', ['questions' => $questions,'user_id'=>Auth::id(), 'keywords'=>$keywords, 'attempts'=>$attempts]);
    }

    public function announceform()
    {
        $courses = $this->db->getCourseByTeacherId();

        return view('teacher.newMailForm', ['courses'=>$courses]);

    }

    public function sendannounce(Request $request)
    {
        $this->validate($request, array(
            'topic' => 'required|max:255',
            'contents' => 'required|min:1',
            'courses' => 'required',
        ));

        //send email for each course
        foreach($request->courses as $course) {
            print_r($course);
            $students = $this->db->getStudentsByCourseId($course);

            foreach($students as $student) {
                $user = User::where('id', '=', $student->id)->first();
//                if(!in_array($student->email,$sentStudents)) {
//                    Mail::to($student->email)->send(new Announce($request->topic." : $course", $request->contents));

//                    array_push($sentStudents, $student->email);
//                }
                $content['content'] =$request->contents;
                $content['topic'] = $request->topic;
                $content['course_id'] = $course;
                $user->notify(new notifyStudent($content, 1));
//                print_r($user);
            }

            //save to DB
            $announce = new Announcement();
            $announce->topic = $request->topic;
            $announce->contents = $request->contents;
            $announce->author_id = Auth::id();
            $announce->course_id = $course;

            $announce->save();
        }

        return redirect()->route('teacher.announceform')->with('success','Successfully sent to students');

    }

    public function showannounce()
    {
        $courses = $this->db->getCourseByTeacherId();
        $mails = $this->db->getMailsByTeacherId();
        return view('teacher.showannounce', ['courses'=>$courses, 'mails'=>$mails]);
    }

    public function cancelannounce($id)
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        $sysannounce = DB::table('announcements')->where('id',$id)->get();

        if(empty($sysannounce)) {
            return redirect()->back()->with('fail', 'Cannout found the record from the DB');
        }
        DB::table('announcements')->where('id',$id)->delete();

        return redirect()->back()->with('success', 'Deleted Successfully');
    }

    public function editannounce($id)
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        $announcement = DB::table('announcements')->where('id',$id)->first();

        if(empty($announcement)) {
            return redirect()->back()->with('fail', 'Cannout found the record from the DB');
        }

        return view('teacher.editannounce', ['announcement'=>$announcement]);
    }

    public function successEditAnnounce($id)
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        $announcement = DB::table('announcements')->where('id',$id)->first();

        if(empty($announcement)) {
            return redirect()->back()->with('fail', 'Cannout found the record from the DB');
        }

        return view('teacher.editannounce', ['announcement'=>$announcement]);
    }

    public function showstudents()
    {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->route('teacher.home')->with('fail', "No Course Found Or The Course Session Out");
        }

        $courseInformation = $this->db->getCourseByTeacher(Session::get('courseID_byTeacher'));

        if(empty($courseInformation[0])) {
            Session::forget('courseID_byTeacher');
            return redirect()->route('teacher.showcourse')->with('fail','No Course Found');
        }

        $mining = new MiningController();
        $students_data = $mining->update_student_csv_data(Session::get('courseID_byTeacher'));

        $onlineStudents = $this->db->getActiveStudentsByCourseId(Session::get('courseID_byTeacher'));

        foreach ($students_data as $id => $data) {
            if(!empty($onlineStudents[$id])) {
                if($onlineStudents[$id] == 1) {
                    $students_data[$id][] = 1;
                    continue;
                }
            }
            $students_data[$id][] = 0;
        }


        return view('teacher.showstudents', ['students'=>$students_data]);
    }

    public function addStudents() {
        return view('teacher.addStudents');
    }

    public function insertstudents(Request $request) {
        if (!Session::get('courseID_byTeacher')) {
            return redirect()->back()->with('fail', "No Course Found Or The Course Session Out");
        }
        if (!empty($request->file('csv_file'))) {

            $successFlag = -1;

            $filePath = $request->file('csv_file')->getRealPath();

            $file = fopen($filePath,'r');

            $db = new DatabaseCapture();

            $course_id = Session::get('courseID_byTeacher');
            $enroll_code = $this->db->getEnrollCodeByCourseId($course_id)[0]->enroll_code;

            while($line=fgetcsv($file)) {

                if(empty($line[0]) || empty($line))
                {
                    continue;
                }

                $student_id = $line[0];

                if(filter_var($line[0], FILTER_VALIDATE_EMAIL)) {
                    return redirect()->back()->with('fail','Please insert only student id instead of email address');
                }

                //check if the student registered
                $email = $student_id.'@connect.polyu.hk';
                if (empty($db->getStudentByEmail($email))) {
                    $nickname = $student_id;
                    $name = $student_id;
                    $real_password = str_random(6);
                    $password = Hash::make($real_password);

                    DB::table('users')->insert([
                        'name' => $name,
                        'email' => $email,
                        'nickname' =>$nickname,
                        'password' => $password
                    ]);
                    Mail::to($email)->send(new \App\Mail\NewStudent_PreRegistration(' Your Account Is Ready! ','Your Password is ', $email, $real_password,$enroll_code ));

                    $successFlag = 1;
                }


            }
            if ($successFlag ==-1 ) {
                return redirect()->back()->withInput()->with('fail',"Failed to enroll students. Make sure you typed correct format");
            }
        }

        return redirect()->back()->with('success', 'Successfully Added Students');
    }

    public function removeElementsByTagName($tagName, $document) {
        $nodeList = $document->getElementsByTagName($tagName);
        for ($nodeIdx = $nodeList->length; --$nodeIdx >= 0; ) {
            $node = $nodeList->item($nodeIdx);
            $node->parentNode->removeChild($node);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\QuizAttempts;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Comment;
use \Datetime;
use \DateInterval;
use Mail;
use App\Mail\Announce;
use Session;
use App\User;
use App\Notifications\notifyStudent;
use Carbon\Carbon;
use App\UserAnswerSpent;

class TeacherAjaxController extends Controller
{
    private $json;
    private $db;
    public function __construct()
    {
        $this->json = new StorageController();
        $this->db = new DatabaseCapture();
    }

    public function insertTimeSpent(Request $request) {
        $log = array('question_id'=>$request->questionID, 'course_id'=>$request->course_id, 'timeSpent'=>floor($request->timeSpent/1000), 'user_id'=>$request->user_id, 'time'=>date("Y-m-d H:i:s"));

        $this->json->addLog($log, 'answer_spent');

        //store to DB
//        return $request->questionID;
//        $user_spent = new UserAnswerSpent();
//
//        $user_spent->user_id = $request->user_id;
//        $user_spent->question_id = $request->questionID;
//        $user_spent->time = 1;
//        $user_spent->save();

        return 'ok';

    }

    public function getOffsetTimeline($offset) {
        if($offset == 0) {
            Session::put('timeline_offset', 0);

            return 0;

        } else {

            if(Session::has('timeline_type') && Session::has('timeline_offset')) {
                if ($offset == 1000) {
                    $offset = Session::get('timeline_offset');
                } else {
                    $offset = Session::get('timeline_offset')+$offset;
                    Session::put('timeline_offset', $offset);
                }
            }

            return $offset;
        }
    }
    private function getOneWeek($offset = 0, $today = null)
    {
        if (is_null($today)) {
            $today = (new DateTime());
        }

        if ($offset < 0 ) {
            $offset_argument = 'P'.(abs($offset)*7).'D';
            $today->sub(new DateInterval($offset_argument));
        } elseif ($offset > 0) {
            $offset_argument = '+'.$offset.' week';
            $today->modify($offset_argument);
        }

        $week[0]=$today->format('Y-m-d');
        for($i=6;$i>0;$i--) {
            $week[7-$i] = ($today->modify('-1 day'))->format('Y-m-d');
        }

        return array_reverse($week);
    }

    public function getTimelineOverall(Request $request) {
        Session::put('timeline_type', 0);

        $offset = $this->getOffsetTimeline($request->offset);

        $week = $this->getOneWeek($offset);

        //initialize values
        $data_answer = [];
        $data_chats = [];
        $data_create = [];
        foreach ($week as $date) {
            $data_create[$date] = 0;
        }
        foreach ($week as $date) {
            $data_answer[$date] = 0;
        }
        foreach ($week as $date) {
            $data_chats[$date] = 0;
        }

        //get values from DB
        $answered = $this->db->getStudents_NumAnswered_ByCourseId_ByStudentID($request->courseID, $request->studentID);


        $questions = DB::table('questions')->where('course_id',$request->courseID)->get();
        $answers = DB::select('select u.question_id, sum(u.num_attempts) as num, u.created_at from user_answereds u, questions q where u.question_id = q.id and q.course_id = ? group by question_id, u.num_attempts, u.created_at;',[$request->courseID] );
        $chats_questions = DB::select('select count(c.id) as num, c.created_at from comments c, questions q where q.id = c.question_id and q.course_id = ? group by c.id, c.created_at;', [$request->courseID]);


        //Actions on Questions
        foreach($questions as $question) {

            $d = new DateTime($question->created_at);

            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data_create[$date] += 1;
                }
            }
        }

        //Actions on Answers
        foreach($answers as $answer) {
            $d = new DateTime($answer->created_at);

            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data_answer[$date] += $answer->num;
                }
            }
        }

        //Actions on Answered by others
        foreach($chats_questions as $chat) {
            $d = new DateTime($chat->created_at);

            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data_chats[$date] += $chat->num;
                }
            }
        }

        $response = array(
            'week' =>$week,
            'data_answer' =>$data_answer,
            'data_chats' =>$data_chats,
            'data_create' =>$data_create,
            'error' => $answered,
        );

        return \Response::json($response);
    }

    public function getTimelineAnswers(Request $request) {
        Session::put('timeline_type', 1);
        $offset = $this->getOffsetTimeline($request->offset);

        $week = $this->getOneWeek($offset);

        $data = [];

        foreach ($week as $date) {
            $data[$date] = 0;
        }

        $answers = $this->db->getStudents_NumAnswers_ByCourseId_ByStudentID($request->courseID, $request->studentID);

        //Actions on Questions
        foreach($answers as $answer) {
            $d = new DateTime($answer->created_at);

            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data[$date] += $answer->num;
                }
            }
        }

        $response = array(
            'week' =>$week,
            'data' =>$data
        );
        return \Response::json($response);
    }

    public function getTimelinePeersAnswered(Request $request) {
        Session::put('timeline_type', 3);
        $offset = $this->getOffsetTimeline($request->offset);

        $week = $this->getOneWeek($offset);

        $data = [];

        foreach ($week as $date) {
            $data[$date] = 0;
        }

        $answered = $this->db->getStudents_NumAnswered_ByCourseId_ByStudentID($request->courseID, $request->studentID);

        foreach($answered as $answer) {
            $d = new DateTime($answer->created_at);

            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data[$date] += $answer->num;
                }
            }
        }

        $response = array(
            'week' =>$week,
            'data' =>$data
        );
        return \Response::json($response);
    }

    public function getTimelineQuestions(Request $request) {
        $offset = $this->getOffsetTimeline($request->offset);

        $week = $this->getOneWeek($offset);
        Session::put('timeline_type', 2);

        $data = [];

        foreach ($week as $date) {
            $data[$date] = 0;
        }

        $questions = $this->db->getStudents_NumQuestions_ByCourseId_ByStudentID($request->courseID, $request->studentID);

        //Actions on Questions
        foreach($questions as $question) {

            $d = new DateTime($question->created_at);

            foreach($week as $date) {
                if (($d->format('Y-m-d')) == $date ) {
                    $data[$date] += 1;
                }
            }
        }

        $response = array(
            'week' =>$week,
            'data' =>$data
        );
        return \Response::json($response);
    }

    public function setDefaultCourseByTeacher(Request $request) {
        DB::table('courses')
            ->where([['instructor_id',$request->instructor_id]])
            ->update(['default_session' => 0]);

        DB::table('courses')
            ->where([['course_code', $request->course_code],['instructor_id',$request->instructor_id]])
            ->update(['default_session' => 1]);

        redirect()->route('teacher.showcourse')->with('success','Successfully Set Default Course');
        return 'ok';
    }

    public function user_setting (Request $request) {
        DB::table('users')
            ->where('id', Auth::id())
            ->update(['notify_answered' => $request->notify_rated], ['notify_viewed' => $request->notify_viewed]
                , ['notify_rated' => $request->notify_rated], ['announce_email' => $request->email_announce]
                ,['email_report' => $request->email_report]);

        return 'ok';
    }

}

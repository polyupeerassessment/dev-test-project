<?php
namespace App\Http\Controllers;

use Mail;
use App\Mail\Questions;
use Illuminate\Support\Facades\DB;
use App\User;

class PeerU
{
    private $student;
    private $leaderboard;

    public function __construct()
    {
        $this->student = new Student();
        $this->leaderboard = new LeaderboardController();
    }

    public function emailQuestions($course_id)
    {
        $students = $this->student->getStudents($course_id);
        if(empty($students) || empty($students[0])){
            return redirect()->back()->with('fail','No students found');
        }

        $question_ids = $this->leaderboard->getQuestionIDsByNumAnswers($course_id, 10);

        if(empty($question_ids) || empty($question_ids[0])){
            return redirect()->back()->with('fail','No question Ids found');
        }

        $sql_query = 'SELECT * from questions as q where q.course_id = ? and q.type = 0 ';
        $sql_argument = [$course_id];
        $if_first_time_then_no_or = true;

        foreach($question_ids as $question_id) {

            if($if_first_time_then_no_or) {
                $sql_query .= ' and ( id = ? ';
                $if_first_time_then_no_or = false;
            } else {
                $sql_query .= ' or id = ? ';
            }

            $sql_argument[] = $question_id->question_id;
        }

        if($if_first_time_then_no_or) {
            $questions = array();
        } else {
            $sql_query .= ' ) order by rating DESC,created_at DESC ';
            $questions = DB::select($sql_query,$sql_argument);
        }

        if(empty($questions) || empty($questions[0])){
            return redirect()->back()->with('fail','No question found');
        }

        foreach($students as $student) {
            $student_to_send = DB::table('users')->where('id',$student->id)->first();

        }

        return redirect()->back()->with('success','Sent Successfully');

    }



}

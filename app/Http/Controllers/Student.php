<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use \Exception;

class Student
{
    public function __construct(){}

    public function getNickname($id)
    {
        $user = DB::table('users')->where('id',$id)->first;

        if(empty($user) || empty($user->nickname)) {
            return null;
        }

        return $user->nickname;
    }

    public function getStudentById($id)
    {
        if (empty($id) )
        {
            return null;
        }

        $users = DB::select('select id, name, nickname, email from users where id =?', [$id]);

        if(empty($users[0])) {
            return null;
        }

        return $users[0];
    }

    public function getStudents ($course_id)
    {
        $students = DB::select('select id, email,name,nickname from users as u where exists (select * from enrollments as e where e.student_id = u.id and e.course_id = ? )', [$course_id]);
        return $students;
    }

    public function updateInfo ($id, $name, $nickname)
    {
        try {
            DB::table('users')
                ->where('id', $id)
                ->update(['name' => $name, 'nickname'=>$nickname]);

            return 1;

        } catch(Exception $e) {
            Log::warning('[Info.updateInfo] Failed to update student info due to: '. $e->getMessage());
            return -1;
        }
    }

    public function changePassword ($id, $old, $new)
    {
        $current = DB::table('users')->where('id', $id)->first();

        if (empty ($current) ) {
            Log::warning('[Info.NewPass] ['.$id.'] Failed to update new password becuase it failed to retrieve current password to mathc');
            return -1;
        }

        if (!Hash::check($old, $current->password) ) {
            Log::debugging('[Info.NewPass] ['.$id.'] Failed to update new password becuase it failed to match old password that user typed and current password');
            return -2;
        }

        try {
            DB::table('users')
                ->where('id', $id)
                ->update(['password' =>Hash::make($new)]);
            Log::info('[Info.NewPass] ['.$id.'] Succssfully Changed password ');
            return 1;

        } catch(Exception $e) {
            Log::warning('[Info.NewPass] ['.$id.'] Failed to change student password due to: '. $e->getMessage());
            return -3;
        }
    }

    public function getScore($id, $question_id)
    {
        $sql_result = DB::select('select result from user_answereds where user_id = ? and question_id = ? ;',[$id, $question_id]);

        if(empty($sql_result)) {
            return -1;
        }

        return $sql_result[0]->result;
    }

}
import pandas as pd
import numpy as np
from scipy.spatial.distance import cosine

def test(s1,s2):
    s1_c = s1 - s1.mean()
    s2_c = s2 - s2.mean()
    # print(s2_c)
    return np.sum(s1_c * s2_c) / np.sqrt(np.sum(s1_c ** 2) * np.sum(s2_c ** 2))

ratings = pd.read_csv('../../storage/app/csv/rating.csv')
M=ratings.pivot_table(index=['user_id'],columns=['question_id'],values='rating')

# M.reset_index(inplace=True)
# M = M.drop('user_id', axis=1)

# tmpM = pd.DataFrame(index=M.columns,columns=M.columns)
M_corr = M.corr(method='pearson', min_periods=100)
myRatings = M.loc[15].dropna()

# M.head()
# print(myRatings)


simCandidates = pd.Series()
for i in range(0, len(myRatings.index)):
    # print(myRatings.index[i])
    # Retrieve similar movies to this one that I rated
    sims = M_corr[myRatings.index[i]].dropna()
    # print(myRatings.index[i])

    # # Now scale its similarity by how well I rated this movie
    sims = sims.map(lambda x: x * myRatings[i])
    # # Add the score to the list of similarity candidates
    simCandidates = simCandidates.append(sims)

# Glance at our results so far:
print ("sorting...")
simCandidates.sort_values(inplace=True, ascending=False)
simCandidates = simCandidates.groupby(simCandidates.index).sum()
# filteredSims = simCandidates.drop(myRatings.index)
# print (filteredSims.head(10))
# print (simCandidates.head(10))







# for i in range(0, len(tmpMovieRatings.columns)):
#     for j in range(0, len(tmpMovieRatings.columns)):
#         tmpMovieRatings.ix[i, j] = 1 - cosine(M.ix[:, i], M.ix[:, j])


# for i in range(0,len(tmpM.columns)) :
    # Loop through the columns for each column
    # for j in range(0,len(tmpM.columns)) :
      # Fill in placeholder with cosine similarities
      # tmpM.ix[i,j] = 1-cosine(M.ix[:,i],M.ix[:,j])



# print(M[406])
# print(M[197])

# print(test(M[406],M[197]))
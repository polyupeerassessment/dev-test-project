<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\User;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $tmp_teacher = Auth::guard('teacher')->user();

        switch ($guard) {
            case 'teacher':
                if(Auth::guard($guard)->check()) {
                    return redirect()->route('teacher.home');
                } elseif (Auth::check()) {
                    return redirect()->route('student.home');
                }
                break;
            default:
                if (Auth::guard($guard)->check()) {
                    User::where('id', Auth::id())
                        ->update(['status' => 1]);
                    return redirect()->intended('/student/home');
                } elseif (!is_null($tmp_teacher) || !empty($tmp_teacher)) {
                  if(Auth::guard('teacher')->check()) {
                      return redirect()->route('teacher.home');
                  }
                }
                break;
        }

        return $next($request);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizUserQuestion extends Model
{
    protected $table = 'quiz_user_questions';
}

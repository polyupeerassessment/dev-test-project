<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizTimer extends Model
{
    protected $table = 'quiz_timers';
}

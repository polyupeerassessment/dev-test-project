<?php

namespace App\Console;

use App\Http\Controllers\DatabaseCapture;
use App\Http\Controllers\LeaderboardController;
use App\Http\Controllers\MiningController;
use App\Teacher;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Artisan;
use App\Notifications\notifyStudent;
use App\User;
use Mail;
use App\Mail\Report;
use App\Mail\StudentSummary;
use \Exception;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //Daily Notification
        $schedule->call(function () {
            Log::info('[Kernel] [Daily 8pm] [Notification] [Num Views]  Start Sending');
            $courses = DB::select('select * from courses;');
            $db = new DatabaseCapture();
            foreach($courses as $course) {

                $students = $db->getStudentsByCourseId($course->course_code);
                Log::info('[Kernel] [Daily Notification] [Num Views] [ '. $course->course_code.'] Start Sending on this course');

                foreach($students as $student) {

                    $num_views = DB::select('select Q.id, count(P.id) as num from questions as Q
                    join page_views as P on P.question_id = Q.id and DATE(P.created_at) = CURDATE() and Q.course_id = ? and Q.author_id = ? group by Q.id;', [$course->course_code, $student->id]);

                    //check if necessary to send
                    if(empty($num_views)) {
                        continue;
                    }

                    //check if count is zero
                    if($num_views[0]->num <= 0) {
                        continue;
                    }

                    $user = User::where('notify_viewed', '=', 1)->where('id', '=', $student->id)->first();

                    if(!empty($user)) {
                        $content['content'] =($num_views[0]->num)." number of users saw your questions! Come back and see more details!";
                        $content['topic'] = "Number of reviews for today";
                        $content['course_id'] = $course->course_code;

                        $user->notify(new notifyStudent($content, 0));
                        Log::info('[Kernel] [Daily Notification] [Num Views] [ '. $course->course_code.'] [ '.$user->id.' ] Successfully Sent to the student ');
                    }
                }
            }
        })->dailyAt('20:00');

        //Weeekly Notification
        $schedule->call(function() {
            Log::info('[Kernel] [Weekly Friday 8pm] [Report Email] Start Sending');

            $courses = DB::select('select * from courses;');
            $db = new DatabaseCapture();
            $leaderboard = new LeaderboardController();


            foreach($courses as $course) {
                Log::info('[Kernel] [Weekly Report Email] [ '. $course->course_code.'] Start Sending on this course');


                $num_questions_rank_tmp = $leaderboard->students_created_ranking(3,$course->course_code, 8);

                $top_sore_rank_tmp = $leaderboard->score_ranking(3,$course->course_code, 8);

                $rating_rank_tmp = $leaderboard->students_avg_rating(3,$course->course_code, 8);

                $answer_rank_tmp = $leaderboard->students_answers_ranking(3, $course->course_code, 8);

                $students = $db->getStudentsByCourseId($course->course_code);

                foreach($students as $student) {
                    $student_to_send = User::where('id', '=', $student->id)->where('email_report', '=', 1)->first();
                    Mail::to($student_to_send->email)->send(new Report($course->course_code.' Weekly Report','This is the weekly report for your reference.', $num_questions_rank_tmp,$top_sore_rank_tmp, $rating_rank_tmp ,$answer_rank_tmp));
                }
            }
        })->weekly()->fridays()->at('20:00');

        $schedule->call(function() {
            Log::info('[Kernel] [Daily 10pm] [Reset Student Record] Start Updating');

            $students = DB::table('users')->where('tried_first_answer_flag','=', 1)->get();
            $dt = Carbon::now();
            foreach($students as $student) {
                $last_activity = Carbon::parse($student->last_activity_date);
                if($dt->diffInDays($last_activity) >= 5){
                    DB::table('users')->where('id','=',$student->id)->update(['tried_first_answer_flag'=>1]);
                    Log::info('[Kernel] [Reset Student Record] [tried_first_answer_flag] [ '.$student->id.' ] Successfully Updated the record');
                }
            }
        })->dailyAt('22:00');

        $schedule->call(function(){
            Log::info('[Kernel] [Backup CSV 9pm] Start Backup');

            $path = "/var/www/html/dev-test-project/storage/backup/csv/";
            $tables = ['announcements','comments', 'course_schedules', 'courses', 'discussions', 'enrollments','keywords','notifications',
                'page_views','question_choices','questions','quiz','quiz_answers','quiz_attempts', 'quiz_questions','quiz_timers','teachers',
                'thread_messages','user_answereds','userattempts','users'];
            $today = date("Y-m-d");


            if (!is_dir($path.$today)) {
                $old = umask(0);
                mkdir($path.$today, 0777, true);
                umask($old);
            }

            foreach ($tables as $table) {
                DB::select("SELECT * INTO OUTFILE '". $path.$today."/".$table.".csv' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n' FROM ".$table);
            }

        })->dailyAt('21:00');

        $schedule->call(function(){
            Log::info('[Kernel] [Daily] [Student Summary] Start Summary');

            $db = new MiningController();
            $courses = DB::select('select * from courses;');

            foreach ($courses as $course) {
                Log::info('[Kernel] [Daily 8am] [Student Summary] ['.$course->course_code.'] Start Summary on this course');

                $db->update_student_csv_data($course->course_code);
                $filename = storage_path() . '/app/csv/'.$course->course_code.'/student_data.csv';

                $developer = User::where('id', '=', 1)->first();
                Mail::to([$developer->email, '14072882d@connect.polyu.hk'])->send(new StudentSummary($course->course_code.' User Daily Report','Attached CSV file for summary of user performance. ', $filename));

                #$lecturer = Teacher::where('id', '=', $course->instructor_id)->first();
                #Mail::to($lecturer->email)->send(new StudentSummary($course->course_code.' User Daily Report','Attached CSV file for summary of user performance. ', $filename));
            }
        })->dailyAt('8:00');

        $schedule->call(function(){
            Log::info('[Kernel] [Daily 3 am] [Update Popularity] Start Summary');

            $mining = new MiningController();
            $courses = DB::select('select * from courses;');

            foreach ($courses as $course) {
                Log::info('[Mining] Update Questions Coefficients and Intercepts for Popularity Order in '.$course->course_code);
                $mining->updateQuestionsPopularityRate($course->course_code);
            }
        })->dailyAt('3:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}

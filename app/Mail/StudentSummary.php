<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StudentSummary extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $topic;
    private $contents;
    private $filename;

    public function __construct($newTopic,$contents, $filename)
    {
        $this->topic = $newTopic;
        $this->contents = $contents;
        $this->filename = $filename;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->topic)->markdown('emails.user.user_summary',['contents'=>$this->contents])->attach($this->filename);
    }
}

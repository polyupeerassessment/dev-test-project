<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Report extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $topic;
    private $contents;
    private $num_questions;
    private $top_scorers;
    private $rating;
    private $num_answers;

    public function __construct($newTopic, $newContents, $num_questions, $top_scorers, $rating, $num_answers)
    {
        $this->topic = $newTopic;
        $this->contents = $newContents;
        $this->num_questions = $num_questions;
        $this->top_scorers = $top_scorers;
        $this->rating = $rating;
        $this->num_answers = $num_answers;

    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->topic)->markdown('emails.user.report',['contents'=>$this->contents, 'num_questions'=>$this->num_questions, 'top_scorers'=>$this->top_scorers, 'rating'=>$this->rating
        ,'num_answers'=>$this->num_answers]);
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewStudent_PreRegistration extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $topic;
    private $contents;
    private $pass;
    private $email;
    private $enroll_code;

    public function __construct($topic,$contents ,$email, $pass, $enroll_code)
    {
        $this->topic = $topic;
        $this->contents = $contents;
        $this->pass = $pass;
        $this->email = $email;
        $this->enroll_code = $enroll_code;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->topic)->markdown('emails.user.pre_register',['contents'=>$this->contents
        ,'pass'=>$this->pass, 'email'=>$this->email, 'enroll_code'=>$this->enroll_code]);
    }
}

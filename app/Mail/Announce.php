<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Announce extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $topic;
    private $contents;

    public function __construct($newTopic, $newContents)
    {
        $this->topic = $newTopic;
        $this->contents = $newContents;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->topic == 'You Have Received Rating!') {
            return $this->subject($this->topic)->markdown('emails.user.rating_notify',['topic'=>$this->topic, 'contents'=>$this->contents]);
        }
        return $this->subject($this->topic)->markdown('emails.user.announce',['topic'=>$this->topic, 'contents'=>$this->contents]);
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Questions extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $topic;
    private $contents;
    private $questions;


    public function __construct($topic, $contents, $questions)
    {
        $this->topic = $topic;
        $this->contents = $contents;
        $this->questions = $questions;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->topic)->markdown('emails.user.questions',['contents'=>$this->contents, 'questions'=>$this->questions]);
    }
}

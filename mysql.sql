-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 05, 2017 at 07:49 AM
-- Server version: 5.6.35
-- PHP Version: 7.0.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--xa
-- Database: `peer_assessment`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `author_id`, `question_id`, `created_at`, `updated_at`) VALUES
(1, 'asdsa', 7, 31, NULL, NULL),
(2, 'addasfads', 7, 31, NULL, NULL),
(3, 'adfdafdfva', 7, 31, NULL, NULL),
(4, 'sadsa', 7, 31, NULL, NULL),
(5, 'dfads', 7, 49, NULL, NULL),
(6, 'dafads', 7, 49, NULL, NULL),
(7, 'sadsa', 7, 49, NULL, NULL),
(8, 'sdasdscdavdvc', 7, 49, NULL, NULL),
(9, 'asda', 7, 49, NULL, NULL),
(10, 'asdasd', 7, 49, NULL, NULL),
(11, 'avfadv', 7, 49, NULL, NULL),
(12, 'sadsad', 2, 49, NULL, NULL),
(13, 'dsfasd', 2, 49, NULL, NULL),
(14, 'sadsa', 2, 49, NULL, NULL),
(15, 'cvdsvfd', 2, 49, NULL, NULL),
(16, 'sdcasd', 2, 50, NULL, NULL),
(17, 'sadasd', 2, 50, NULL, NULL),
(18, 'Dscds', 2, 49, NULL, NULL),
(19, 'dcdasc', 2, 49, NULL, NULL),
(20, 'dascads', 2, 50, NULL, NULL),
(21, 'mNkLnklnkl', 7, 49, NULL, NULL),
(22, 'asddsfvdasf', 2, 49, NULL, NULL),
(23, 'sDSac', 2, 50, NULL, NULL),
(24, 'ascsaczdsvdf', 2, 50, NULL, NULL),
(25, 'What!?', 2, 1042, NULL, NULL),
(26, 'aaaa', 2, 194, NULL, NULL),
(27, 'sadfdas', 2, 194, NULL, NULL),
(28, 'dsfasd', 2, 194, NULL, NULL),
(29, 'sdfasdfas', 2, 194, NULL, NULL),
(30, 'I dont know this', 8, 194, NULL, NULL),
(31, 'asbda\r\nsadsada\r\nsadsad', 8, 194, NULL, NULL),
(32, 'dvdsv\r\nascsdcd\r\nsvcdsv dsz\r\nv zfdv\r\nfzdv\r\nz\r\nv\r\nzs\r\nv\r\nsd\r\nvszd', 8, 194, NULL, NULL),
(33, 'assa', 8, 1148, NULL, NULL),
(34, 'sdvSD', 8, 1148, NULL, NULL),
(35, 'VDCdzvzf', 8, 1148, NULL, NULL),
(36, 'SDCdsCVdsc', 8, 1148, NULL, NULL),
(37, 'vfzvc', 8, 1148, NULL, NULL),
(38, 'what', 2, 1148, NULL, NULL),
(39, 'sadasfc', 2, 1148, NULL, NULL),
(40, 'sadsfsad', 2, 1148, NULL, NULL),
(41, 'asdsac', 2, 1148, NULL, NULL),
(42, 'asdsa', 2, 1148, NULL, NULL),
(43, 'sadas', 2, 1148, NULL, NULL),
(44, 'asdsa', 2, 1148, NULL, NULL),
(45, 'asdsa', 2, 1148, NULL, NULL),
(46, 'sadas', 2, 1148, NULL, NULL),
(47, 'sadsa', 2, 1148, NULL, NULL),
(48, 'fvzf', 2, 1148, NULL, NULL),
(49, 'asdscsad', 2, 1148, NULL, NULL),
(50, 'sadasd', 2, 1148, NULL, NULL),
(51, 'asdasdas', 2, 1148, NULL, NULL),
(52, 'cdsacv', 2, 1148, NULL, NULL),
(53, 'asdcdsc', 2, 1148, NULL, NULL),
(54, 'cv zdfsv', 2, 1148, NULL, NULL),
(55, 'kln', 2, 1148, NULL, NULL),
(56, 'asdsad', 2, 1148, NULL, NULL),
(57, 'asdfas\nsdfsaf', 2, 1148, NULL, NULL),
(58, 'asdsa', 2, 1148, NULL, NULL),
(59, 'asdscadsc', 2, 1148, NULL, NULL),
(60, 'dsfasd', 2, 7725, NULL, NULL),
(61, 'asdfdasf', 2, 7725, NULL, NULL),
(62, 'asdsf', 2, 7725, NULL, NULL),
(63, 'adsfdasf', 2, 7725, NULL, NULL),
(64, 'fadasf', 2, 7725, NULL, NULL),
(65, 'dasfvdasf', 2, 7725, NULL, NULL),
(66, 'dsfdsvfdv', 2, 7725, NULL, NULL),
(67, 'dfsd', 2, 7725, NULL, NULL),
(68, 'sdfdszv', 2, 7725, NULL, NULL),
(69, 'dsfdsfv', 2, 7725, NULL, NULL),
(70, 'asdsf', 2, 7725, NULL, NULL),
(71, 'adsfdasv', 2, 7725, NULL, NULL),
(72, 'asdfdsavdfvfd', 2, 7725, NULL, NULL),
(73, 'adfadsv', 2, 7725, NULL, NULL),
(74, 'sdfadsvcads', 2, 5709, NULL, NULL),
(75, 'dfcadsv', 2, 5709, NULL, NULL),
(76, 'fvavafd', 2, 5709, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `discussions`
--

CREATE TABLE `discussions` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discussions`
--

INSERT INTO `discussions` (`id`, `text`, `author_id`, `created_at`, `updated_at`) VALUES
(37, 'Testing', 7, '2017-11-03 02:11:49', '2017-11-03 02:11:49'),
(38, 'Dcdsfas', 7, '2017-11-03 02:13:05', '2017-11-03 02:13:05'),
(39, 'adfdafad', 7, '2017-11-03 02:13:25', '2017-11-03 02:13:25'),
(40, 'adfdaf', 7, '2017-11-03 02:13:35', '2017-11-03 02:13:35'),
(41, 'arvfrefv', 7, '2017-11-03 02:17:01', '2017-11-03 02:17:01'),
(42, 'adscvadscvafdcf', 6, '2017-11-03 02:17:19', '2017-11-03 02:17:19'),
(43, 'wfevev', 7, '2017-11-03 02:18:37', '2017-11-03 02:18:37'),
(44, 'ecfecvdfac', 6, '2017-11-03 02:18:43', '2017-11-03 02:18:43'),
(45, 'SFDs\r\nsdffds', 7, '2017-11-03 02:21:05', '2017-11-03 02:21:05'),
(46, 'acdscds', 7, '2017-11-03 06:39:33', '2017-11-03 06:39:33'),
(47, 'dsfsdvf', 2, '2017-11-03 09:30:21', '2017-11-03 09:30:21'),
(48, 'daadfsvsd', 2, '2017-11-03 09:30:27', '2017-11-03 09:30:27'),
(49, 'asdsa', 8, '2017-11-03 21:42:35', '2017-11-03 21:42:35'),
(50, 'sadsacds', 8, '2017-11-03 21:42:40', '2017-11-03 21:42:40'),
(51, 'sdsacds', 8, '2017-11-03 21:42:43', '2017-11-03 21:42:43'),
(52, 'dacdasd', 8, '2017-11-03 21:42:46', '2017-11-03 21:42:46'),
(53, 'scsacds', 8, '2017-11-03 21:58:40', '2017-11-03 21:58:40'),
(54, 'fvcdfv', 2, '2017-11-04 06:01:35', '2017-11-04 06:01:35'),
(55, 'asdscsdv', 2, '2017-11-04 06:58:28', '2017-11-04 06:58:28');

-- --------------------------------------------------------

--
-- Table structure for table `keywords`
--

CREATE TABLE `keywords` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `keywords`
--

INSERT INTO `keywords` (`id`, `question_id`, `author_id`, `keyword`, `created_at`, `updated_at`) VALUES
(3, 151050, 7, 'IT', NULL, NULL),
(4, 151050, 7, 'a', NULL, NULL),
(5, 151050, 7, 'sacxas', NULL, NULL),
(6, 151050, 7, 'acdsca', NULL, NULL),
(7, 151050, 7, 'dscasdcmasiodn', NULL, NULL),
(8, 151050, 7, 'dsjncasin', NULL, NULL),
(9, 151050, 7, 'dhscnain', NULL, NULL),
(10, 151050, 7, 'ncdnvciua', NULL, NULL),
(11, 151031, 7, 'IT', NULL, NULL),
(12, 151031, 7, 'a', NULL, NULL),
(13, 1053, 7, 'IT', NULL, NULL),
(14, 31, 7, 'IT', NULL, NULL),
(15, 228, 2, 'IT', NULL, NULL),
(16, 228, 2, 'dsavad', NULL, NULL),
(17, 228, 2, 'dacadq', NULL, NULL),
(18, 228, 2, 'adcaew', NULL, NULL),
(19, 228, 2, 'dcerc', NULL, NULL),
(20, 228, 2, 'php', NULL, NULL),
(21, 451, 2, 'IT', NULL, NULL),
(22, 451, 2, 'a', NULL, NULL),
(23, 451, 2, 'asas', NULL, NULL),
(24, 451, 2, 'dsvcz', NULL, NULL),
(25, 512, 2, 'IT', NULL, NULL),
(26, 512, 2, 'szdfsz', NULL, NULL),
(27, 512, 2, 'sadcvx', NULL, NULL),
(28, 557, 2, 'IT', NULL, NULL),
(29, 611, 2, 'IT', NULL, NULL),
(30, 880, 2, 'IT', NULL, NULL),
(31, 986, 2, 'IT', NULL, NULL),
(32, 296, 2, 'IT', NULL, NULL),
(33, 382, 2, 'IT', NULL, NULL),
(34, 1042, 2, 'IT', NULL, NULL),
(35, 194, 2, 'IT', NULL, NULL),
(36, 316, 2, 'IT', NULL, NULL),
(37, 609, 2, 'IT', NULL, NULL),
(38, 1148, 8, 'IT', NULL, NULL),
(39, 2977, 8, 'IT', NULL, NULL),
(40, 7725, 8, 'IT', NULL, NULL),
(41, 2252, 8, 'IT', NULL, NULL),
(42, 9635, 2, 'IT', NULL, NULL),
(43, 928, 2, 'IT', NULL, NULL),
(44, 928, 2, 'dsacvadsva dvadav', NULL, NULL),
(45, 928, 2, 'avadsv', NULL, NULL),
(46, 9156, 2, 'IT', NULL, NULL),
(47, 8054, 2, 'IT', NULL, NULL),
(48, 7258, 2, 'IT', NULL, NULL),
(49, 7679, 2, 'IT', NULL, NULL),
(50, 2301, 2, 'IT', NULL, NULL),
(51, 7069, 2, 'IT', NULL, NULL),
(52, 5804, 2, 'IT', NULL, NULL),
(53, 3765, 2, 'IT', NULL, NULL),
(54, 7292, 2, 'IT', NULL, NULL),
(55, 6529, 2, 'IT', NULL, NULL),
(56, 4333, 2, 'IT', NULL, NULL),
(57, 3294, 2, 'IT', NULL, NULL),
(58, 9905, 2, 'IT', NULL, NULL),
(59, 10049, 2, 'IT', NULL, NULL),
(60, 7768, 2, 'IT', NULL, NULL),
(61, 5634, 2, 'IT', NULL, NULL),
(62, 3530, 2, 'IT', NULL, NULL),
(63, 7103, 2, 'IT', NULL, NULL),
(64, 458, 2, 'IT', NULL, NULL),
(65, 4162, 2, 'IT', NULL, NULL),
(66, 5123, 2, 'IT', NULL, NULL),
(67, 7865, 2, 'IT', NULL, NULL),
(68, 9993, 2, 'IT', NULL, NULL),
(69, 9429, 2, 'IT', NULL, NULL),
(70, 9565, 2, 'IT', NULL, NULL),
(71, 5949, 2, 'IT', NULL, NULL),
(72, 6367, 2, 'IT', NULL, NULL),
(73, 6367, 2, 'asdfd adsf', NULL, NULL),
(74, 6367, 2, 'adfads', NULL, NULL),
(75, 8391, 2, 'IT', NULL, NULL),
(76, 8391, 2, 'asdfd adsf', NULL, NULL),
(77, 8391, 2, 'adfads', NULL, NULL),
(78, 7088, 2, 'IT', NULL, NULL),
(79, 7088, 2, 'asdfd adsf', NULL, NULL),
(80, 7088, 2, 'adfads', NULL, NULL),
(81, 9002, 6, 'IT', NULL, NULL),
(82, 9002, 6, 'zdscs ds.', NULL, NULL),
(83, 9002, 6, 'dzsvdsz', NULL, NULL),
(84, 9002, 6, 'dzscfzdsc', NULL, NULL),
(85, 1745, 7, 'IT', NULL, NULL),
(86, 5709, 7, 'IT', NULL, NULL),
(87, 9704, 2, 'IT', NULL, NULL),
(88, 2576, 6, 'IT', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2017_10_27_112939_create_teacher_tables', 1),
(7, '2017_10_28_023920_create_questions_table', 1),
(8, '2017_10_30_054022_create_user_answereds_table', 2),
(9, '2017_11_01_124951_create_userattemps_table', 3),
(10, '2017_11_02_131842_create_keywords_table', 4),
(11, '2017_11_02_161303_create_comments_table', 5),
(12, '2017_11_02_164944_create_discussions_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contents` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `difficulty` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `choices` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(100) DEFAULT NULL,
  `author_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_attempts` int(11) NOT NULL DEFAULT '0',
  `tips` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `topic`, `contents`, `difficulty`, `rating`, `remember_token`, `created_at`, `updated_at`, `choices`, `answer`, `type`, `author_id`, `num_attempts`, `tips`) VALUES
(458, 'fdasf', 'dafdsvf', '0', '0', NULL, '2017-11-04 19:19:23', '2017-11-04 19:19:23', 'asdsfc,sdafd', '0,0', 1, '2', 2, ''),
(928, 'asdvadfv adf', 'v avadscadsvdfvadv', '0', '0', NULL, '2017-11-04 07:04:19', '2017-11-04 07:04:19', NULL, NULL, 2, '2', 2, ''),
(1148, 'asdas', 'sdcdszvzdsvADSc', '0', '0', NULL, '2017-11-03 21:55:57', '2017-11-03 21:55:57', 'CdCD,aSCSAC,asCa,ascaS,', 'answer1', 0, '8', 2, 'aSCsaCsacdsVCdsVdcADC'),
(1745, 'dfawd', 'dasfsadf', '0', '0', NULL, '2017-11-04 21:51:33', '2017-11-04 21:51:33', NULL, NULL, 2, '7', 2, ''),
(2252, 'avcdsv', 'dsCzvsd', '0', '0', NULL, '2017-11-03 22:04:48', '2017-11-03 22:04:48', NULL, NULL, 2, '8', 2, ''),
(2301, 'advdafv', 'adcadsvcdv', '0', '0', NULL, '2017-11-04 07:12:15', '2017-11-04 07:12:15', NULL, NULL, 2, '2', 2, ''),
(2576, 'dsfdsa', 'adsvdsavfas', '0', '0', NULL, '2017-11-04 22:05:50', '2017-11-04 22:05:50', 'dsafdsaf,dsafads', '0,1', 1, '6', 2, ''),
(2977, 'adfad', 'dfasdfvads', '0', '0', NULL, '2017-11-03 22:02:58', '2017-11-03 22:02:58', NULL, NULL, 2, '8', 2, ''),
(3294, 'dsfdsv', 'dfvfdvsd', '0', '0', NULL, '2017-11-04 18:44:25', '2017-11-04 18:44:25', NULL, NULL, 2, '2', 2, ''),
(3530, 'fdasf', 'dafdsvf', '0', '0', NULL, '2017-11-04 19:18:47', '2017-11-04 19:18:47', 'asdsfc,sdafd', '0,0', 1, '2', 2, ''),
(3765, 'dasfdasf', 'davfdsv', '0', '0', NULL, '2017-11-04 07:14:18', '2017-11-04 07:14:18', 'aDFDAF,adsvfad', '0,0', 1, '2', 2, ''),
(4162, 'fdasf', 'dafdsvf', '0', '0', NULL, '2017-11-04 19:19:48', '2017-11-04 19:19:48', 'asdsfc,sdafd', '0,0', 1, '2', 2, ''),
(4333, 'fdzvdfv', 'sdfvdzbvf', '0', '0', NULL, '2017-11-04 18:43:58', '2017-11-04 18:43:58', NULL, NULL, 2, '2', 2, ''),
(5123, 'fdasf', 'dafdsvf', '0', '0', NULL, '2017-11-04 19:19:55', '2017-11-04 19:19:55', 'asdsfc,sdafd', '0,0', 1, '2', 2, ''),
(5634, 'fdasf', 'dafdsvf', '0', '0', NULL, '2017-11-04 19:18:22', '2017-11-04 19:18:22', 'asdsfc,sdafd', '0,0', 1, '2', 2, ''),
(5709, 'dafdasf', 'davfadva', '0', '0', NULL, '2017-11-04 21:52:01', '2017-11-04 21:52:01', 'asdfadsf,asdfadsf,davafdvad,vdascsadfasd,', 'answer4', 0, '7', 2, ''),
(5804, 'advdas', 'dsavdavfd', '0', '0', NULL, '2017-11-04 07:13:32', '2017-11-04 07:13:32', NULL, NULL, 2, '2', 2, ''),
(5949, 'fdasf', 'dafdsvf', '0', '0', NULL, '2017-11-04 19:22:53', '2017-11-04 19:22:53', 'asdsfc,sdafd', '0,0', 1, '2', 2, ''),
(6367, 'dafdsa', '<br>', '0', '0', NULL, '2017-11-04 19:27:07', '2017-11-04 19:27:07', NULL, NULL, 2, '2', 2, ''),
(6529, 'aaaaaaaaaaaa', 'dssda', '0', '0', NULL, '2017-11-04 07:20:09', '2017-11-04 07:20:09', 'adsfads,dfdasf,asdfadfdsafsda,adfadsf,', 'answer4', 0, '2', 2, ''),
(7069, 'sadfa', 'dafvadfdaf', '0', '0', NULL, '2017-11-04 07:13:06', '2017-11-04 07:13:06', NULL, NULL, 2, '2', 2, ''),
(7088, 'dafdsa', '<br>', '0', '0', NULL, '2017-11-04 19:27:35', '2017-11-04 19:27:35', NULL, NULL, 2, '2', 2, ''),
(7103, 'fdasf', 'dafdsvf', '0', '0', NULL, '2017-11-04 19:19:15', '2017-11-04 19:19:15', 'asdsfc,sdafd', '0,0', 1, '2', 2, ''),
(7258, 'avadvc', 'advafvaf', '0', '0', NULL, '2017-11-04 07:08:05', '2017-11-04 07:08:05', NULL, NULL, 2, '2', 2, ''),
(7292, 'advads', 'dsavcadsv', '0', '0', NULL, '2017-11-04 07:14:43', '2017-11-04 07:14:43', NULL, NULL, 2, '2', 2, ''),
(7679, 'asdcad', 'a', '0', '0', NULL, '2017-11-04 07:09:07', '2017-11-04 07:09:07', NULL, NULL, 2, '2', 2, ''),
(7725, 'dsvsv', 'asdcdsvdfs', '0', '0', NULL, '2017-11-03 22:03:59', '2017-11-03 22:03:59', NULL, NULL, 2, '8', 2, ''),
(7768, 'fdasf', 'dafdsvf', '0', '0', NULL, '2017-11-04 19:16:04', '2017-11-04 19:16:04', 'asdsfc,sdafd', '0,0', 1, '2', 2, ''),
(7865, 'fdasf', 'dafdsvf', '0', '0', NULL, '2017-11-04 19:21:16', '2017-11-04 19:21:16', 'asdsfc,sdafd', '0,0', 1, '2', 2, ''),
(8054, 'asdvfdavfad', 'vadvadscafv', '0', '0', NULL, '2017-11-04 07:07:43', '2017-11-04 07:07:43', NULL, NULL, 2, '2', 2, ''),
(8391, 'dafdsa', '<br>', '0', '0', NULL, '2017-11-04 19:27:32', '2017-11-04 19:27:32', NULL, NULL, 2, '2', 2, ''),
(9002, 'adfds', 'sdvdsvzdsc', '0', '0', NULL, '2017-11-04 19:32:29', '2017-11-04 19:32:29', 'dasdasd,scszd', '0,0', 1, '6', 2, ''),
(9156, 'dsvadfv', 'advdfvsfdv', '0', '0', NULL, '2017-11-04 07:06:31', '2017-11-04 07:06:31', NULL, NULL, 2, '2', 2, ''),
(9429, 'fdasf', 'dafdsvf', '0', '0', NULL, '2017-11-04 19:22:33', '2017-11-04 19:22:33', 'asdsfc,sdafd', '0,0', 1, '2', 2, ''),
(9565, 'fdasf', 'dafdsvf', '0', '0', NULL, '2017-11-04 19:22:46', '2017-11-04 19:22:46', 'asdsfc,sdafd', '0,0', 1, '2', 2, ''),
(9635, 'adsvcadsv adfva', 'asdcdv&nbsp;', '0', '0', NULL, '2017-11-04 07:03:57', '2017-11-04 07:03:57', 'dasvcadfv,asdvcadsv', '0,0', 1, '2', 2, ''),
(9704, 'dasdfsdaf', 'dsdsacda', '0', '0', NULL, '2017-11-04 21:55:06', '2017-11-04 21:55:06', 'a,a', '0,1', 1, '2', 2, ''),
(9905, 'sadscdsv', 'fvfzsdvsdCV', '0', '0', NULL, '2017-11-04 18:45:47', '2017-11-04 18:45:47', NULL, NULL, 2, '2', 2, ''),
(9993, 'fdasf', 'dafdsvf', '0', '0', NULL, '2017-11-04 19:21:58', '2017-11-04 19:21:58', 'asdsfc,sdafd', '0,0', 1, '2', 2, ''),
(10049, 'sadscdsv', 'fvfzsdvsdCV', '0', '0', NULL, '2017-11-04 18:46:09', '2017-11-04 18:46:09', NULL, NULL, 2, '2', 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `name`, `nickname`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'test teacher', 'test', 'teacher@a.com', '$2y$10$rNi1nXrCQHfRu4kDhrE0tumPOCoTtoJFZPbN.b1npo89N62ytJ6tW', NULL, '2017-10-27 09:35:40', '2017-10-27 09:35:40');

-- --------------------------------------------------------

--
-- Table structure for table `userattempts`
--

CREATE TABLE `userattempts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `login_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `userattempts`
--

INSERT INTO `userattempts` (`id`, `user_id`, `login_at`, `created_at`, `updated_at`) VALUES
(7, 2, '2017-11-01 13:03:06', '2017-11-01 05:03:06', '2017-11-01 05:03:06'),
(8, 2, '2017-11-01 13:03:19', '2017-11-01 05:03:19', '2017-11-01 05:03:19'),
(9, 6, '2017-11-01 13:08:14', '2017-11-01 05:08:14', '2017-11-01 05:08:14'),
(10, 7, '2017-11-02 05:06:22', '2017-11-01 21:06:22', '2017-11-01 21:06:22'),
(11, 7, '2017-11-02 05:09:06', '2017-11-01 21:09:06', '2017-11-01 21:09:06'),
(12, 7, '2017-11-02 05:09:26', '2017-11-01 21:09:26', '2017-11-01 21:09:26'),
(13, 7, '2017-11-02 06:21:21', '2017-11-01 22:21:21', '2017-11-01 22:21:21'),
(14, 6, '2017-11-02 06:21:32', '2017-11-01 22:21:32', '2017-11-01 22:21:32'),
(15, 3, '2017-11-02 06:21:57', '2017-11-01 22:21:57', '2017-11-01 22:21:57'),
(16, 2, '2017-11-02 06:22:48', '2017-11-01 22:22:48', '2017-11-01 22:22:48'),
(17, 2, '2017-11-02 06:42:13', '2017-11-01 22:42:13', '2017-11-01 22:42:13'),
(18, 7, '2017-11-02 06:46:07', '2017-11-01 22:46:07', '2017-11-01 22:46:07'),
(19, 2, '2017-11-02 06:46:57', '2017-11-01 22:46:57', '2017-11-01 22:46:57'),
(20, 7, '2017-11-02 06:48:26', '2017-11-01 22:48:26', '2017-11-01 22:48:26'),
(21, 6, '2017-11-02 06:49:53', '2017-11-01 22:49:53', '2017-11-01 22:49:53'),
(22, 4, '2017-11-02 06:50:14', '2017-11-01 22:50:14', '2017-11-01 22:50:14'),
(23, 2, '2017-11-02 07:53:01', '2017-11-01 23:53:01', '2017-11-01 23:53:01'),
(24, 2, '2017-11-02 10:32:00', '2017-11-02 02:32:00', '2017-11-02 02:32:00'),
(25, 3, '2017-11-02 10:33:46', '2017-11-02 02:33:46', '2017-11-02 02:33:46'),
(26, 7, '2017-11-02 10:34:03', '2017-11-02 02:34:03', '2017-11-02 02:34:03'),
(27, 2, '2017-11-02 13:57:44', '2017-11-02 05:57:44', '2017-11-02 05:57:44'),
(28, 7, '2017-11-02 13:58:03', '2017-11-02 05:58:03', '2017-11-02 05:58:03'),
(29, 2, '2017-11-02 17:23:01', '2017-11-02 09:23:01', '2017-11-02 09:23:01'),
(30, 2, '2017-11-03 02:21:12', '2017-11-02 18:21:12', '2017-11-02 18:21:12'),
(31, 2, '2017-11-03 02:37:07', '2017-11-02 18:37:07', '2017-11-02 18:37:07'),
(32, 7, '2017-11-03 02:37:26', '2017-11-02 18:37:26', '2017-11-02 18:37:26'),
(33, 7, '2017-11-03 02:54:16', '2017-11-02 18:54:16', '2017-11-02 18:54:16'),
(34, 2, '2017-11-03 03:22:29', '2017-11-02 19:22:29', '2017-11-02 19:22:29'),
(35, 2, '2017-11-03 06:39:17', '2017-11-02 22:39:17', '2017-11-02 22:39:17'),
(36, 2, '2017-11-03 07:06:05', '2017-11-02 23:06:05', '2017-11-02 23:06:05'),
(37, 6, '2017-11-03 07:11:16', '2017-11-02 23:11:16', '2017-11-02 23:11:16'),
(38, 7, '2017-11-03 07:14:13', '2017-11-02 23:14:13', '2017-11-02 23:14:13'),
(39, 2, '2017-11-03 07:14:32', '2017-11-02 23:14:32', '2017-11-02 23:14:32'),
(40, 6, '2017-11-03 07:32:56', '2017-11-02 23:32:56', '2017-11-02 23:32:56'),
(41, 6, '2017-11-03 10:14:03', '2017-11-03 02:14:03', '2017-11-03 02:14:03'),
(42, 2, '2017-11-03 15:33:50', '2017-11-03 07:33:50', '2017-11-03 07:33:50'),
(43, 2, '2017-11-04 03:01:34', '2017-11-03 19:01:34', '2017-11-03 19:01:34'),
(44, 2, '2017-11-04 10:27:25', '2017-11-04 02:27:25', '2017-11-04 02:27:25'),
(45, 6, '2017-11-04 15:20:33', '2017-11-04 07:20:33', '2017-11-04 07:20:33'),
(46, 2, '2017-11-05 02:32:02', '2017-11-04 18:32:02', '2017-11-04 18:32:02'),
(47, 2, '2017-11-05 03:02:19', '2017-11-04 19:02:19', '2017-11-04 19:02:19'),
(48, 2, '2017-11-05 03:03:07', '2017-11-04 19:03:07', '2017-11-04 19:03:07'),
(49, 2, '2017-11-05 03:04:28', '2017-11-04 19:04:28', '2017-11-04 19:04:28'),
(50, 2, '2017-11-05 03:08:39', '2017-11-04 19:08:39', '2017-11-04 19:08:39'),
(51, 2, '2017-11-05 03:10:26', '2017-11-04 19:10:26', '2017-11-04 19:10:26'),
(52, 2, '2017-11-05 03:10:59', '2017-11-04 19:10:59', '2017-11-04 19:10:59'),
(53, 2, '2017-11-05 03:11:42', '2017-11-04 19:11:42', '2017-11-04 19:11:42'),
(54, 2, '2017-11-05 03:12:06', '2017-11-04 19:12:06', '2017-11-04 19:12:06'),
(55, 2, '2017-11-05 03:12:27', '2017-11-04 19:12:27', '2017-11-04 19:12:27'),
(56, 2, '2017-11-05 03:12:58', '2017-11-04 19:12:58', '2017-11-04 19:12:58'),
(57, 2, '2017-11-05 03:15:24', '2017-11-04 19:15:24', '2017-11-04 19:15:24'),
(58, 2, '2017-11-05 03:23:27', '2017-11-04 19:23:27', '2017-11-04 19:23:27'),
(59, 6, '2017-11-05 03:32:07', '2017-11-04 19:32:07', '2017-11-04 19:32:07'),
(60, 2, '2017-11-05 05:48:56', '2017-11-04 21:48:56', '2017-11-04 21:48:56'),
(61, 6, '2017-11-05 05:50:31', '2017-11-04 21:50:31', '2017-11-04 21:50:31'),
(62, 6, '2017-11-05 05:50:47', '2017-11-04 21:50:47', '2017-11-04 21:50:47'),
(63, 7, '2017-11-05 05:51:02', '2017-11-04 21:51:02', '2017-11-04 21:51:02'),
(64, 2, '2017-11-05 05:52:40', '2017-11-04 21:52:40', '2017-11-04 21:52:40'),
(65, 6, '2017-11-05 05:55:36', '2017-11-04 21:55:36', '2017-11-04 21:55:36'),
(66, 2, '2017-11-05 06:06:39', '2017-11-04 22:06:39', '2017-11-04 22:06:39');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `nickname`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Masamichi Tanaka', 'Masa', 'a@a.com', '$2y$10$nEh.7DISh0eQ.ep9aK3Ny.71L45SGxFa4ZM5A5kLq1zvfWHJHXp6G', 'sqpzLNB2jJrRZjQ6HgZjNyHWHU1GvpnIOLZg8wHzcP8PyQfW3W76E7OEQS31', '2017-10-27 19:47:55', '2017-10-27 19:47:55'),
(3, 'Test', 'Test', 't@t.com', '$2y$10$dlU88zHaU.gbJU1dR7HIUun4K6plyz2Ig1MEBrfMOzCQz2r43Z3JG', 'L7Q5LLR9m0t5bGjVWSiIIxl3gA0IqNKC0W32SmmUWkRShAL4514kMAxqB7XL', '2017-10-29 22:16:05', '2017-10-29 22:16:05'),
(4, 'aaa', 'sdas', 'aa@a.com', '$2y$10$IMlV2oRAm3/.ZCdU3S72GOikLUz0YpzY83jfpWKJd3dpIMOFXd.bq', 'jANvGymWFOpG8ZMEj26FGFWveIkrLga7Bjj03cyjRk3WYpVl9Hp6mHtbPFsj', '2017-10-29 22:18:26', '2017-10-29 22:18:26'),
(6, 'leo', 'leo', 'l@l.com', '$2y$10$kRGpcn/q/UUN2F2S7ibIF.nvmvXAm/ARXRLGjrY6C6QMGsJ2rrhzW', '3nFFIt2rRHYpVrgdEyzNs03VHmCDtihGrGl9c8B5gkKxAz6mBHlrcMhgzxt6', '2017-10-29 23:13:27', '2017-10-29 23:13:27'),
(7, 'Test Updated', 'TESTT', 'aaa@a.com', '$2y$10$VVFHBpRIDJ4dNu3AEgggY.zCtn4tDX30NDmLBC6H3HGiPWAA93v9a', 'cYWwvdRwvlTT7Rjb3cIq6FcSm78VP9W4oT6COTN5x4rE5QjhvoLjt0QfFxF2', '2017-10-30 23:25:17', '2017-10-30 23:25:17'),
(8, 'AAA', 'ASSA', 'adasdas@a.com', '$2y$10$V/8Qzops.CziZXE2009Vk.mq37EzaP5QlLhDEeZY5hrEFzIQLy55K', 'nWaVoc20oGLL1IzCbwSHU1bMa14qRkrA9AaAH2VEpNpEaC0ZV5sMXrpAXFjq', '2017-11-03 21:37:47', '2017-11-03 21:37:47');

-- --------------------------------------------------------

--
-- Table structure for table `user_answereds`
--

CREATE TABLE `user_answereds` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `result` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `num_attempts` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_answereds`
--

INSERT INTO `user_answereds` (`id`, `user_id`, `question_id`, `result`, `created_at`, `updated_at`, `num_attempts`) VALUES
(114, 8, 1148, 1, '2017-11-03 21:56:08', '2017-11-03 21:56:08', 1),
(115, 2, 1148, -1, '2017-11-04 02:28:32', '2017-11-04 02:28:32', 3),
(116, 6, 6529, 1, '2017-11-04 07:20:37', '2017-11-04 07:20:37', 2),
(117, 2, 7725, -1, '2017-11-04 07:21:51', '2017-11-04 07:21:51', 1),
(118, 6, 9993, 1, '2017-11-04 19:35:44', '2017-11-04 19:35:44', 1),
(119, 6, 458, 1, '2017-11-04 19:38:12', '2017-11-04 19:38:12', 1),
(120, 2, 5709, 1, '2017-11-04 21:52:48', '2017-11-04 21:52:48', 4),
(121, 6, 9704, -1, '2017-11-04 21:56:41', '2017-11-04 21:56:41', 2),
(122, 2, 2576, -1, '2017-11-04 22:06:57', '2017-11-04 22:06:57', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discussions`
--
ALTER TABLE `discussions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keywords`
--
ALTER TABLE `keywords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `teacher_email_unique` (`email`);

--
-- Indexes for table `userattempts`
--
ALTER TABLE `userattempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_answereds`
--
ALTER TABLE `user_answereds`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `discussions`
--
ALTER TABLE `discussions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `keywords`
--
ALTER TABLE `keywords`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10050;
--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `userattempts`
--
ALTER TABLE `userattempts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_answereds`
--
ALTER TABLE `user_answereds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
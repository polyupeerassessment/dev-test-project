<?php
//student->home
Breadcrumbs::register('student_home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('student.home'));
});

//student->dashboard
Breadcrumbs::register('student_dashboard', function($breadcrumbs)
{
    $breadcrumbs->parent('student_home');
    $breadcrumbs->push('Dashboard', route('student.dashboard',['courseID'=>Session::get('courseID')]));
});

//student->dashboard->answer
Breadcrumbs::register('question_show', function($breadcrumbs)
{
    $breadcrumbs->parent('student_dashboard');
    $breadcrumbs->push('Show Questions', route('question.showquestion'));
});

//student->dashboard->create
Breadcrumbs::register('question_create', function($breadcrumbs)
{
    $breadcrumbs->parent('student_dashboard');
    $breadcrumbs->push('Create Questions', route('question.create'));
});

//student->dashboard->exercise
Breadcrumbs::register('question_exercise', function($breadcrumbs)
{
    $breadcrumbs->parent('student_dashboard');
    $breadcrumbs->push('Exercise', route('question.exercise'));
});

//student->dashboard->quiz
Breadcrumbs::register('question_quiz', function($breadcrumbs)
{
    $breadcrumbs->parent('student_dashboard');
    $breadcrumbs->push('Quiz', route('question.quiz'));
});

//student->dashboard->quiz->question_quiz_detail
Breadcrumbs::register('question_quiz_detail', function($breadcrumbs)
{
    $breadcrumbs->parent('question_quiz');
    $breadcrumbs->push('Take Quiz', route('question.quizdetail'));
});

//student->dashboard->assignment
Breadcrumbs::register('assignment', function($breadcrumbs)
{
    $breadcrumbs->parent('student_dashboard');
    $breadcrumbs->push('Assignment', route('question.assignment.show'));
});


//student->dashboard->my
Breadcrumbs::register('question_my', function($breadcrumbs)
{
    $breadcrumbs->parent('student_dashboard');
    $breadcrumbs->push('My Questions', route('question.my'));
});

//student->dashboard->my->edit
Breadcrumbs::register('question_edit', function($breadcrumbs, $question)
{
    $breadcrumbs->parent('student_dashboard');
    $breadcrumbs->push('My Questions', route('question.my'));
    $breadcrumbs->push('Edit My Question', route('question.edit',['id' => $question->id]));
});

//student->dashboard->answered
Breadcrumbs::register('question_answered', function($breadcrumbs)
{
    $breadcrumbs->parent('student_dashboard');
    $breadcrumbs->push('My Answers', route('question.answered'));
});

//student->dashboard->questiondetail
Breadcrumbs::register('question_detail', function($breadcrumbs, $question)
{
    $breadcrumbs->parent('student_dashboard');
    $breadcrumbs->push('Show Questions', route('question.showquestion'));
    $breadcrumbs->push('Detail Question', route('question.detail', ['id' => $question->id]));
});

//student->discussion
Breadcrumbs::register('discussion', function($breadcrumbs)
{
    $breadcrumbs->parent('student_dashboard');
    $breadcrumbs->push('Forum', route('discussion.index'));
});


////////// Teacher /////////

//teacher->home
Breadcrumbs::register('teacher_home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('teacher.home'));
});

//teacher->dashboard
Breadcrumbs::register('teacher_dashboard', function($breadcrumbs)
{
    $breadcrumbs->parent('teacher_home');
    $breadcrumbs->push('Dashboard', route('teacher.dashboard',['courseID'=>Session::get('courseID_byTeacher')]));
});

//teacher->dashboard->create
Breadcrumbs::register('teacher_question_create', function($breadcrumbs)
{
    $breadcrumbs->parent('teacher_dashboard');
    $breadcrumbs->push('Create Questions', route('teacher.createquestion'));
});

//teacher->dashboard->questiondetail
Breadcrumbs::register('teacher_question_detail', function($breadcrumbs, $question)
{
    $breadcrumbs->parent('teacher_dashboard');
    $breadcrumbs->push('Show Questions', route('teacher.showquestion'));
    $breadcrumbs->push('Detail Question', route('teacher.detailquestion', ['id' => $question->id]));
});

//teacher->dashboard->assignment
Breadcrumbs::register('teacher_assignment', function($breadcrumbs)
{
    $breadcrumbs->parent('teacher_dashboard');
    $breadcrumbs->push('Assignment', route('teacher.assignment.show'));
});

//teacher->dashboard->assignment->showquestion->questiondetail
Breadcrumbs::register('teacher_assignment_question_detail', function($breadcrumbs, $question)
{
    $breadcrumbs->parent('teacher_assignment');
    $breadcrumbs->push('Detail Question', route('teacher.assignment.question.detail',['id' => $question->id]));
});
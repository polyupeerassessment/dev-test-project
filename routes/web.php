<?php

use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PagesController@index')->name('home');
Route::get('/about', 'PagesController@about')->name('about');
Route::get('/home', 'StudentController@index')->name('home2');
Route::get('/access', 'PagesController@accessCode')->name('access_code');

Auth::routes();

Route::prefix('teacher')->group(function(){
    Route::get('/login', 'Auth\TeacherLoginController@showlogin')->name('teacher-login');
    Route::post('/login', 'Auth\TeacherLoginController@login')->name('teacher.login.submit');
    Route::get('/', 'TeacherController@index')->name('teacher.home');
    Route::get('/upload', 'PagesController@uploadFile')->name('upload');
    Route::post('/upload', 'PagesController@successUpload')->name('upload');
    Route::get('/dashboard/{course_code}', 'TeacherController@course_dashboard')->name('teacher.dashboard');
    Route::get('/createquestion', 'TeacherController@createquestion')->name('teacher.createquestion');
    Route::post('/insertquestion', 'TeacherController@insertquestion')->name('teacher.insertquestion');
    Route::post('/insertstudents', 'TeacherController@insertstudents')->name('teacher.insertstudents');
    Route::get('/showcourse', 'TeacherController@index')->name('teacher.showcourse');
    Route::get('/removecourse/{id}', 'TeacherController@removecourse')->name('teacher.removecourse');
    Route::post('/createcourse', 'TeacherController@insertCourse')->name('teacher.insertcourse');
    Route::get('/announce', 'TeacherController@announce')->name('teacher.announce');
    Route::get('/mycourse', 'TeacherController@myCourse')->name('teacher.mycourse');
    Route::get('/showquestion', 'TeacherController@showquestion')->name('teacher.showquestion');
    Route::get('/log', 'TeacherController@showLog')->name('teacher.log');
    Route::get('/announceform', 'TeacherController@announceform')->name('teacher.announceform');
    Route::get('/showannounce', 'TeacherController@showannounce')->name('teacher.showannounce');
    Route::post('/sendannounce', 'TeacherController@sendannounce')->name('teacher.sendannounce');
    Route::post('/updatecourse', 'TeacherController@updatecourse')->name('teacher.updatecourse');
    Route::post('/enrollstudent_newcourse', 'TeacherController@enrollstudent_newcourse')->name('teacher.enrollstudent_newcourse');
    Route::get('/removequestion/{id}', 'TeacherController@removequestion')->name('teacher.removequestion');
    Route::get('/mystudents', 'TeacherController@showstudents')->name('teacher.mystudents');
    Route::get('/createquiz', 'TeacherController@createquiz')->name('teacher.createquiz');
    Route::post('/createquiz', 'TeacherController@insertquiz')->name('teacher.quiz.insert');
    Route::get('/addstudents', 'TeacherController@addStudents')->name('teacher.addstudents');
    Route::get('/quizquestions', 'TeacherController@quizquestions')->name('teacher.quizquestions');
    Route::get('/detailquestion/{id}', 'TeacherController@detailquestion')->name('teacher.detailquestion');
    Route::get('/leaderboard', 'TeacherController@leaderboard')->name('teacher.leaderboard');
    Route::get('/quiz/list', 'TeacherController@quizlist')->name('teacher.quiz.list');
    Route::get('/quiz/result/{id}', 'TeacherController@quizresult')->name('teacher.quiz.result');
    Route::get('/cancelannounce/{id}', 'TeacherController@cancelannounce')->name('teacher.cancelannounce');
    Route::get('/editannounce/{id}', 'TeacherController@editannounce')->name('teacher.editannounce');
    Route::get('/votingquestions/', 'TeacherController@votingquestions')->name('teacher.voting.questions');
    Route::get('/votingresult/', 'TeacherController@votingresult')->name('teacher.voting.result');
    Route::get('/votingcongrate/', 'TeacherController@votingcongrate')->name('teacher.voting.congrat');
    Route::post('/successeditannounce/', 'TeacherController@successEditAnnounce')->name('teacher.success.edit.announcement');
    Route::get('/statistics', 'TeacherController@statistics')->name('teacher.statistics');
    Route::get('/assignment/question/list/{id}', 'TeacherController@assignmentquestionlist')->name('teacher.assignment.question.list');
    Route::get('/assignment/question/create/{id}', 'TeacherController@assignmentquestioncreate')->name('teacher.assignment.question.create');
    Route::post('/assignment/question/create', 'TeacherController@assignmentquestioninsert')->name('teacher.assignment.question.insert');
    Route::get('/assignment/question/result/{id}', 'TeacherController@assignmentquestionresult')->name('teacher.assignment.question.result');
    Route::get('/assignment/question/detail/{id}', 'TeacherController@assignmentquestiondetail')->name('teacher.assignment.question.detail');
    Route::get('/assignment/question/detail/delete/{id}', 'TeacherController@assignmentquestiondelete')->name('teacher.assignment.question.delete');
    Route::get('/assignment/show', 'TeacherController@showassignment')->name('teacher.assignment.show');
    Route::get('/assignment/create', 'TeacherController@assignmentcreate')->name('teacher.assignment.create');
    Route::post('/assignment/create', 'TeacherController@insertassignment')->name('teacher.assignment.insert');
    Route::get('/stuCourseStat/{std_id}','TeacherController@stuCourseStat')->name('teacher.stuCourseStat');

});


Route::prefix('student')->group(function(){
    Route::get('/home', 'StudentController@index')->name('student.home');
    Route::get('/main/{courseID}', 'StudentController@course_dashboard')->name('student.dashboard');
    Route::get('/ranking', 'StudentController@ranking')->name('student.ranking');
    Route::get('/info', 'StudentController@personalinformation')->name('student.info');
    Route::post('/info', 'StudentController@updateinfo')->name('student.update');
    Route::get('/enroll', 'StudentController@enrollCourse')->name('student.enroll');
    Route::post('/enroll', 'StudentController@enrollCourseSucess')->name('student.successenroll');
    Route::post('/setting', 'StudentController@setting')->name('student.setting');
    Route::post('/newpass', 'StudentController@newpass')->name('student.newpass');
    Route::get('/exit', 'StudentController@exitCourse')->name('student.exitCourse');
    Route::get('/remove', 'StudentController@removeCourse')->name('student.removeCourse');
    Route::get('/announcement', 'StudentController@showAnnouncement')->name('student.announcement');
    Route::get('/feedback', 'StudentController@showfeedback')->name('student.feedback');
    Route::get('/usermanual', 'StudentController@usermanual')->name('student.usermanual');
    Route::get('/newpassword',function(){
        Log::info('[Personal.Info.NewPass] ['.Auth::id().'] Enter New password change page.');
        return view('student.newpassword');
    })->name('student.newpassword');
});

Route::prefix('admin')->group(function(){
    Route::get('/createsysannounce', 'AdminController@createSystemAnnounce')->name('admin.create.sysannounce');
    Route::post('/sendsysannounce', 'AdminController@sendSystemAnnounce')->name('admin.send.sysannounce');
});

Route::prefix('discussion')->group(function(){
    Route::get('/', 'DiscussionController@discussion')->name('discussion.index');

    Route::get('/createpage', 'DiscussionController@createPage')->name('discussion.createpage');
    Route::post('/createpage', 'DiscussionController@insertThread')->name('discussion.insertthread');

    Route::get('/show/{id}', 'DiscussionController@showDetail')->name('discussion.show');
    Route::post('/insertmessage', 'DiscussionController@insertMessage')->name('discussion.insertmessage');

    Route::post('/threaddelete', 'DiscussionController@threadDelete')->name('discussion.threaddelete');
});


Route::prefix('peeru')->group(function(){
    Route::get('/emailquestions/{course_id}', 'PeerU@emailQuestions')->name('peeru.emailquestions');

});

Route::prefix('question')->group(function(){
    Route::get('/showquestion', 'QuestionController@showquestion')->name('question.showquestion');
    Route::get('/search_showquestion/{search}', 'QuestionController@searchquestion')->name('question.showquestion.get');
    Route::get('/search_showquestion/', 'QuestionController@searchquestion')->name('question.showquestion.get.without_search');
    Route::post('/showquestion', 'QuestionController@showquestion')->name('question.searchquestion');
    Route::get('/showquestion/popularity/{order}', 'QuestionController@showquestion_popularity')->name('question.showquestion.popularity');
    Route::get('/showquestion/date/{order}', 'QuestionController@showquestion_date')->name('question.showquestion.date');
    Route::get('/showquestion/rating/{order}', 'QuestionController@showquestion_rating')->name('question.showquestion.rating');
    Route::get('/create', 'QuestionController@createquestion')->name('question.create');
    Route::get('/clear', 'QuestionController@clearSearchquestion')->name('question.clear');
    Route::post('/clear', 'QuestionController@clearSearchquestion')->name('question.clear');
    Route::post('/create', 'QuestionController@insertquestion')->name('question.insert');
    Route::post('/update', 'QuestionController@updatequestion')->name('question.update');
    Route::get('/myquestion', 'QuestionController@showmyquestion')->name('question.my');
    Route::get('/myquestion/{sessionID}', 'QuestionController@showmyquestion')->name('question.my_withSession');
    Route::get('/detailquestion/{id}', 'QuestionController@detailquestion')->name('question.detail');
    Route::get('/detailquestion/{id}/{course_id}', 'QuestionController@detailquestion_email')->name('question.detail.email');
    Route::post('/detailquestion/success', 'QuestionController@successanswer')->name('question.success');
    Route::post('/detailquestion', 'AjaxController@insertcomment')->name('question.insertcomment');
    Route::post('/removemycomment_question', 'AjaxController@removemycomment_question')->name('question.removemycomment_question');
    Route::post('/removemycomment/{id}', 'QuestionController@removemycomment')->name('question.removemycomment');
    Route::get('/answered', 'QuestionController@showanswered')->name('question.answered');
    Route::get('/quiz', 'QuestionController@showquiz')->name('question.quiz');
    Route::get('/quizdetail', 'QuestionController@quizdetail')->name('question.quizdetail');
    Route::post('/quizdetail/success', 'QuestionController@successQuizAnswer')->name('question.success.quiz');
    Route::get('/exercise', 'QuestionController@showexercise')->name('question.exercise');

    Route::get('/assignment', 'QuestionController@showAssignment')->name('question.assignment.show');
    Route::get('/assignment/{id}', 'QuestionController@assignment_detail')->name('question.assignment.detail');
    Route::post('/assignment/success', 'QuestionController@successAssignmentAnswer')->name('question.assignment.success');
    
    Route::get('/editquestion/{id}', 'QuestionController@editquestion')->name('question.edit');
    Route::get('/showquestion_for_voting', 'QuestionController@showquestion_for_voting')->name('question.voting.questions');
    Route::get('/quizdetail/my/{id}', 'QuestionController@quizdetail_with_id')->name('question.quiz.my');
    Route::post('/exercise/success', 'QuestionController@successExerciseAnswer')->name('question.success.exercise');
    Route::post('/detailquestion/vote', 'QuestionController@insertvoting')->name('question.vote');
});

Route::prefix('statistic')->group(function(){
    Route::post('/getTimelineOverall', 'TeacherAjaxController@getTimelineOverall')->name('statistic.getTimelineOverall');
});

//Route::get('/mining/printkmeans', 'MiningController@printKMeansGroup')->name('mining.printkmeans');
//Route::get('/mining/test', 'MiningController@updateCsv')->name('mining.test');
//Route::get('/mining/readcsv_irt', 'MiningController@readcsv_irt')->name('mining.readcsv_irt');
//Route::get('/mining/readcsv_question', 'MiningController@updateQuestionLevel')->name('mining.readcsv_question');
Route::get('/mining/student_data', 'MiningController@update_student_csv_data')->name('mining.update_student_csv_data');
//Route::get('/mining/backup', 'MiningController@backup2Csv')->name('minsing.backup2Csv');
Route::get('/mining/regression', 'MiningController@updateQuestionsPopularityRate')->name('mining.regression');

Route::get('/ajax',function(){
    return view('message');
});


Route::post('/getmsg','AjaxController@index')->name('getmsg');
Route::post('/rating','AjaxController@insertRating')->name('insertrating');
Route::post('/timeSpent','AjaxController@insertTimeSpent')->name('inserttime');
Route::post('/getOverallStudent','AjaxController@getOverallStudent')->name('getOverallStudent');
Route::post('/setDefaultCourseByTeacher','AjaxController@setDefaultCourseByTeacher')->name('setDefaultCourseByTeacher');
Route::post('/getTimelineOverall','AjaxController@getTimelineOverall')->name('getTimelineOverall');
Route::post('/getTimelineAnswers','AjaxController@getTimelineAnswers')->name('getTimelineAnswers');
Route::post('/getTimelineQuestions','AjaxController@getTimelineQuestions')->name('getTimelineQuestions');
Route::post('/getTimelinePeersAnswered','AjaxController@getTimelinePeersAnswered')->name('getTimelinePeersAnswered');
Route::post('/getUserPerformance_Num_Questions','AjaxController@getUserPerformance_Num_Questions')->name('getUserPerformance_Num_Questions');
Route::post('/getQuizClockTimer','AjaxController@getQuizClockTimer')->name('getQuizClockTimer');
Route::post('/famouse_keywords','AjaxController@famouse_keywords')->name('famouse_keywords');
Route::post('/validateQuestionContents','AjaxController@validateQuestionContents')->name('validateQuestionContents');
Route::post('/getCommentsReceived','AjaxController@getCommentsReceived')->name('getCommentsReceived');
Route::post('/keywords_frequency','AjaxController@keywords_frequency')->name('keywords_frequency');


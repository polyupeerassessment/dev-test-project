-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 02, 2017 at 11:30 AM
-- Server version: 5.6.35
-- PHP Version: 7.0.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `peer_assessment`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2017_10_27_112939_create_teacher_tables', 1),
(7, '2017_10_28_023920_create_questions_table', 1),
(8, '2017_10_30_054022_create_user_answereds_table', 2),
(9, '2017_11_01_124951_create_userattemps_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contents` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `difficulty` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `choices` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(100) DEFAULT NULL,
  `author_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `topic`, `contents`, `difficulty`, `rating`, `remember_token`, `created_at`, `updated_at`, `choices`, `answer`, `type`, `author_id`) VALUES
(48, 'Short Question Test', 'aaaaaa<div>Testing</div>', '0', '0', NULL, '2017-10-30 21:27:20', '2017-10-30 21:27:20', NULL, NULL, 2, '2'),
(49, 'Multiple Question', 'sdfadfafa<div>Testing</div>', '0', '0', NULL, '2017-10-30 21:27:56', '2017-10-30 21:27:56', 'nonono,hahahaha,this is answer,this is answer too,', 'answer3,answer4', 0, '2'),
(50, 'True False Question', 'asdsajdbcdsbavs<div>Testing</div>', '0', '0', NULL, '2017-10-30 21:30:09', '2017-10-30 21:30:09', 'This is False,This is True', '0,1', 1, '2'),
(51, 'Test MC', 'dacadfc', '0', '0', NULL, '2017-10-30 23:25:45', '2017-10-30 23:25:45', 'awefaew,dfawef,adsfadf,adfaewf,', 'answer2', 0, '7'),
(52, 'aaa@a.com created Question', 'asdsadv<div>dscdscvds</div><div>YAAAA</div>', '0', '0', NULL, '2017-11-01 22:46:38', '2017-11-01 22:46:38', 'asdas,dfvdacdThis is falsw', '1,0', 1, '7');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `name`, `nickname`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'test teacher', 'test', 'teacher@a.com', '$2y$10$rNi1nXrCQHfRu4kDhrE0tumPOCoTtoJFZPbN.b1npo89N62ytJ6tW', NULL, '2017-10-27 09:35:40', '2017-10-27 09:35:40');

-- --------------------------------------------------------

--
-- Table structure for table `userattempts`
--

CREATE TABLE `userattempts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `login_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `userattempts`
--

INSERT INTO `userattempts` (`id`, `user_id`, `login_at`, `created_at`, `updated_at`) VALUES
(7, 2, '2017-11-01 13:03:06', '2017-11-01 05:03:06', '2017-11-01 05:03:06'),
(8, 2, '2017-11-01 13:03:19', '2017-11-01 05:03:19', '2017-11-01 05:03:19'),
(9, 6, '2017-11-01 13:08:14', '2017-11-01 05:08:14', '2017-11-01 05:08:14'),
(10, 7, '2017-11-02 05:06:22', '2017-11-01 21:06:22', '2017-11-01 21:06:22'),
(11, 7, '2017-11-02 05:09:06', '2017-11-01 21:09:06', '2017-11-01 21:09:06'),
(12, 7, '2017-11-02 05:09:26', '2017-11-01 21:09:26', '2017-11-01 21:09:26'),
(13, 7, '2017-11-02 06:21:21', '2017-11-01 22:21:21', '2017-11-01 22:21:21'),
(14, 6, '2017-11-02 06:21:32', '2017-11-01 22:21:32', '2017-11-01 22:21:32'),
(15, 3, '2017-11-02 06:21:57', '2017-11-01 22:21:57', '2017-11-01 22:21:57'),
(16, 2, '2017-11-02 06:22:48', '2017-11-01 22:22:48', '2017-11-01 22:22:48'),
(17, 2, '2017-11-02 06:42:13', '2017-11-01 22:42:13', '2017-11-01 22:42:13'),
(18, 7, '2017-11-02 06:46:07', '2017-11-01 22:46:07', '2017-11-01 22:46:07'),
(19, 2, '2017-11-02 06:46:57', '2017-11-01 22:46:57', '2017-11-01 22:46:57'),
(20, 7, '2017-11-02 06:48:26', '2017-11-01 22:48:26', '2017-11-01 22:48:26'),
(21, 6, '2017-11-02 06:49:53', '2017-11-01 22:49:53', '2017-11-01 22:49:53'),
(22, 4, '2017-11-02 06:50:14', '2017-11-01 22:50:14', '2017-11-01 22:50:14'),
(23, 2, '2017-11-02 07:53:01', '2017-11-01 23:53:01', '2017-11-01 23:53:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `nickname`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Masamichi Tanaka', 'Masa', 'a@a.com', '$2y$10$nEh.7DISh0eQ.ep9aK3Ny.71L45SGxFa4ZM5A5kLq1zvfWHJHXp6G', '6keJ5c4ZWQDwnTHnZAxSWMNloyLnX0OZST8e0mmPepdmE1fzGKx0U8rIgzJe', '2017-10-27 19:47:55', '2017-10-27 19:47:55'),
(3, 'Test', 'Test', 't@t.com', '$2y$10$dlU88zHaU.gbJU1dR7HIUun4K6plyz2Ig1MEBrfMOzCQz2r43Z3JG', 'BbHjMeUv12RtawWclq0ckxmiwhF4T7bWSnf853HYackFFtbvGq8PWJRzF5JX', '2017-10-29 22:16:05', '2017-10-29 22:16:05'),
(4, 'aaa', 'sdas', 'aa@a.com', '$2y$10$IMlV2oRAm3/.ZCdU3S72GOikLUz0YpzY83jfpWKJd3dpIMOFXd.bq', 'jANvGymWFOpG8ZMEj26FGFWveIkrLga7Bjj03cyjRk3WYpVl9Hp6mHtbPFsj', '2017-10-29 22:18:26', '2017-10-29 22:18:26'),
(6, 'leo', 'leo', 'l@l.com', '$2y$10$kRGpcn/q/UUN2F2S7ibIF.nvmvXAm/ARXRLGjrY6C6QMGsJ2rrhzW', 'EwgurTssvzCcYFaqI7eOp56uYfS5GF2aqqFtUedhwcszxbj2v8jskDuyGmUc', '2017-10-29 23:13:27', '2017-10-29 23:13:27'),
(7, 'test', 'test', 'aaa@a.com', '$2y$10$VVFHBpRIDJ4dNu3AEgggY.zCtn4tDX30NDmLBC6H3HGiPWAA93v9a', 'FZcqOr0wxmIA4OQqeD1v2bXHelfrk6nuZszzSN0494KRP55ydGQpieJBF1bU', '2017-10-30 23:25:17', '2017-10-30 23:25:17');

-- --------------------------------------------------------

--
-- Table structure for table `user_answereds`
--

CREATE TABLE `user_answereds` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `result` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_answereds`
--

INSERT INTO `user_answereds` (`id`, `user_id`, `question_id`, `result`, `created_at`, `updated_at`) VALUES
(30, 2, 49, 1, '2017-10-30 21:28:01', '2017-10-30 21:28:01'),
(31, 2, 49, 1, '2017-10-30 21:28:41', '2017-10-30 21:28:41'),
(32, 2, 49, 1, '2017-10-30 21:28:59', '2017-10-30 21:28:59'),
(33, 2, 49, 1, '2017-10-30 21:29:08', '2017-10-30 21:29:08'),
(34, 2, 49, 1, '2017-10-30 21:29:29', '2017-10-30 21:29:29'),
(35, 2, 49, 1, '2017-10-30 21:29:33', '2017-10-30 21:29:33'),
(36, 2, 49, 1, '2017-10-30 21:29:36', '2017-10-30 21:29:36'),
(37, 2, 50, 1, '2017-10-30 21:30:19', '2017-10-30 21:30:19'),
(38, 2, 50, 1, '2017-10-30 21:30:26', '2017-10-30 21:30:26'),
(39, 2, 50, 1, '2017-10-30 21:30:32', '2017-10-30 21:30:32'),
(40, 2, 50, 1, '2017-10-30 21:30:55', '2017-10-30 21:30:55'),
(41, 2, 50, 1, '2017-10-30 21:33:25', '2017-10-30 21:33:25'),
(42, 2, 50, 1, '2017-10-30 21:34:14', '2017-10-30 21:34:14'),
(43, 2, 50, 1, '2017-10-30 21:34:40', '2017-10-30 21:34:40'),
(44, 2, 50, 1, '2017-10-30 21:34:54', '2017-10-30 21:34:54'),
(45, 2, 50, 0, '2017-10-30 21:35:07', '2017-10-30 21:35:07'),
(46, 2, 50, 1, '2017-10-30 21:35:12', '2017-10-30 21:35:12'),
(47, 2, 50, 0, '2017-10-30 21:35:16', '2017-10-30 21:35:16'),
(48, 2, 50, 1, '2017-10-30 21:35:19', '2017-10-30 21:35:19'),
(49, 2, 49, 0, '2017-10-30 21:38:57', '2017-10-30 21:38:57'),
(50, 2, 49, 1, '2017-10-30 21:39:01', '2017-10-30 21:39:01'),
(51, 2, 50, 0, '2017-10-30 21:39:08', '2017-10-30 21:39:08'),
(52, 2, 50, 1, '2017-10-30 21:39:14', '2017-10-30 21:39:14'),
(53, 2, 50, 0, '2017-10-30 21:39:19', '2017-10-30 21:39:19'),
(54, 2, 50, 0, '2017-10-30 21:39:23', '2017-10-30 21:39:23'),
(55, 2, 50, 0, '2017-10-30 21:41:34', '2017-10-30 21:41:34'),
(56, 2, 50, 0, '2017-10-30 21:41:56', '2017-10-30 21:41:56'),
(57, 2, 49, 0, '2017-10-30 21:52:43', '2017-10-30 21:52:43'),
(58, 2, 50, 1, '2017-10-30 21:53:29', '2017-10-30 21:53:29'),
(59, 6, 49, 0, '2017-10-30 22:28:13', '2017-10-30 22:28:13'),
(60, 6, 50, 1, '2017-10-30 22:28:49', '2017-10-30 22:28:49'),
(61, 7, 49, 1, '2017-10-30 23:25:29', '2017-10-30 23:25:29'),
(62, 7, 51, 1, '2017-10-30 23:25:50', '2017-10-30 23:25:50'),
(63, 7, 50, 1, '2017-10-30 23:25:58', '2017-10-30 23:25:58'),
(64, 2, 49, 0, '2017-10-31 19:01:57', '2017-10-31 19:01:57'),
(65, 7, 52, 1, '2017-11-01 22:46:48', '2017-11-01 22:46:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `teacher_email_unique` (`email`);

--
-- Indexes for table `userattempts`
--
ALTER TABLE `userattempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_answereds`
--
ALTER TABLE `user_answereds`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `userattempts`
--
ALTER TABLE `userattempts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user_answereds`
--
ALTER TABLE `user_answereds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CourseSchedules extends Migration
{

    public function up()
    {
        Schema::create('course_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id');
            $table->string('week');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('course_schedules');
    }

}

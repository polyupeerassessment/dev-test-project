<?php

use Illuminate\Database\Seeder;
use App\Mail\NewStudent_PreRegistration;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filename = storage_path().'/app/csv/newstudent.csv';
        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while($line=fgetcsv($handle)) {

                if(empty($line[0]) || empty($line))
                {
                    continue;
                }
                $student_id = $line[0];

                $email = $student_id.'@connect.polyu.hk';
                $nickname = $student_id;
                $name = $student_id;
                $real_password = str_random(6);
                $password = Hash::make($real_password);

                //find if already exists
                $tmp = DB::table('users')->where('email',$email)->first();
                if (!empty($tmp)) {
                    continue;
                }

                DB::table('users')->insert([
                    'name' => $name,
                    'email' => $email,
                    'nickname' =>$nickname,
                    'password' => $password
                ]);

                Mail::to($email)->send(new NewStudent_PreRegistration(' Your Account Is Ready! ','Your Password is ', $email, $real_password));

            }

        }



    }
}
